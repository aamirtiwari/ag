﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AG_API.Interface;
using AG_API.Constants;
using System.Runtime.InteropServices;
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] appIDchar = { 'n', 'e', 'w', '1' };
            string APPID = new string(appIDchar);
            char[] handlechar = new char[1024];
            string Handle = new string(handlechar);
            char[] datachar = new char[1024];
            string data = new string(datachar);
      

            int status = 0;

            data = AGAPI.AG_Read_vb(APPID, ref status);

            if (status == Constants.AG_SUCCESS)
            {
                MessageBox.Show("AG_Read() Succesful: " + data);
            }
            else
            {
                MessageBox.Show("AG_Read() Failed");
            }

            AGAPI.AG_IsAuthentic_vb(APPID, ref status);

            if (status == Constants.AG_SUCCESS)
            {
                MessageBox.Show("AG_IsAuthentic() Succesful: ");
            }
            else
            {
                MessageBox.Show("AG_IsAuthentic() Failed");
            }
       
              data = AGAPI.AG_Encrypt_vb(APPID, ref status);

            if (status == Constants.AG_SUCCESS)
            {
                MessageBox.Show("AG_Encrypt() Succesful: "+data);
            }
            else
            {
                MessageBox.Show("AG_Encrypt() Failed");
            }
       
              data = AGAPI.AG_Decrypt_vb(data, ref status);

            if (status == Constants.AG_SUCCESS)
            {
                MessageBox.Show("AG_Decrypt() Succesful: "+data);
            }
            else
            {
                MessageBox.Show("AG_Decrypt() Failed");
            }

             AGAPI.AG_QuickProtect_net_vb(data, ref status);

            if (status == Constants.AG_SUCCESS)
            {
                MessageBox.Show("AG_QuickProtect_net() Succesful: ");
            }
            else
            {
                MessageBox.Show("AG_QuickProtect_net() Failed");
            }
        }
    }
}
