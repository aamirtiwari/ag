using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;
using AG_STATUS_CODE = System.UInt32;
namespace AG_API.Constants
{
    public class Constants
    {
        //STATUS CODES

        public const int AG_SUCCESS				=	0;
        public const int AG_FILE_IO_ERROR		=	1;
        public const int AG_NO_LICENSE			=	2;
        public const int AG_DATE_TAMPERED		=	3;
        public const int AG_EXPIRED				=	4;
        public const int AG_INVALID_LICENSE		=	5;
        public const int AG_EMPTY_MEMORY		=	6;
        public const int AG_INVALID_MEMORY		=	7;
        public const int AG_TERMINAL_SESSION	=	8;
        public const int AG_NO_KEY				=	9;

    }
}
namespace AG_API.Interface
{
   public class AGAPI
    {
       
       private  const string LibraryName ="AG_vb.dll";
      
        [DllImport(LibraryName, EntryPoint = "AG_Read_vb", CharSet = CharSet.Ansi)]
        public static extern string AG_Read_vb( string AppId,
            ref int status);

        [DllImport(LibraryName, EntryPoint = "AG_IsAuthentic_vb", CharSet = CharSet.Ansi)]
        public static extern string AG_IsAuthentic_vb(string Handle,
            ref int status);

        [DllImport(LibraryName, EntryPoint = "AG_Encrypt_vb", CharSet = CharSet.Ansi)]
        public static extern string AG_Encrypt_vb(string inputval,
            ref int status);

        [DllImport(LibraryName, EntryPoint = "AG_Decrypt_vb", CharSet = CharSet.Ansi)]
        public static extern string AG_Decrypt_vb(string inputval,
            ref int status);

        [DllImport(LibraryName, EntryPoint = "AG_QuickProtect_net_vb", CharSet = CharSet.Ansi)]
        public static extern void AG_QuickProtect_net_vb(string AppId,
            ref int status);
    }


}
