<%@ Page Language="C#" Debug="true" Description="AuthToken - Example" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Security" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Reflection" %>

<script language = "C#" runat="Server">
  
    string ServerDigest = "";  

    bool validate (string RanData, string ClientDigest)  
    {
        Type tp = Type.GetTypeFromProgID("AuthTokenserver.AuthToken.1");
        Object obj;
        obj = Activator.CreateInstance(tp);
        Object[] args = new object[] { RanData};
        Object Result;

       Result=(String)tp.InvokeMember("IsAuthenticserver", BindingFlags.InvokeMethod, null, obj, args);
       ServerDigest =   Result.ToString();     
       if (!Object.Equals(ServerDigest, ClientDigest))
            return false;
        return true;
           
     
    }
    
</script>

<% 
    string RanData = Session["Message"].ToString();
    string ClientDigest = Request.Form["Digest"];  
    string LicenseValue= Request.Form["License_ID"];
    int ErrCode = 0;   
   

        if (!validate(RanData, ClientDigest))
        {
             ErrCode = 2;
        }  
   
%>
  
<% 
    if (ErrCode != 0)
    {
        Msg1.Text += "<p>Error Code : " + ErrCode + "</p>";
        Msg1.Text += "<p>Random data : " + RanData + "</p>";
        Msg1.Text += "<p>Client digest : " + ClientDigest + "</p>";
        Msg1.Text += "<p>Server digest : " + ServerDigest + "</p>";
    }
%>
<%
    if (ErrCode == 0)
    {
        Msg2.Text += "<h1 align='center'><font color='blue'>Congratulations! You passed the authentication! </font></h1> "+"License Expiration Date: " + LicenseValue ;
    }
    if (ErrCode == 1)
    {
        Msg2.Text += "<h2 align='center'><font color='red'>Error : Unable to find this client.</font></h2> ";
    }
    if (ErrCode == 2)
    {
        Msg2.Text += "<h2 align='center'><font color='red'>Error : Identification unmatched.</font></h2> ";
    }
%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<style type="text/css">
<!--
@import "test.css";
-->
</style>
</head>
<body>
    <asp:Label ID="Msg1" runat="server" /><br>
    <table width="600" border="0" align="center">
        <tr>
            <td>
                <p>&nbsp;
                    </p>
                <p>&nbsp;
                    </p>
                <p>&nbsp;
                    </p>
                <p>&nbsp;
                    </p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                <asp:Label ID="Msg2" ForeColor="red" Font-Bold="true" runat="server" /><br>
                <p>&nbsp;
                    </p>
            </td>
        </tr>
    </table>
    <font color='#000000'>
        <p>&nbsp;
            </p>
        <p>&nbsp;
            </p>
        <p align="center" style="color: black; font-family: Verdana; font-size: 9pt; font-style: normal;">
            </p>
        <p align="center" style="color: black; font-family: Verdana; font-size: 9pt; font-style: normal;">
           </p>
    </font>
</body>
</html>
