Attribute VB_Name = "Module1"
DefLng A-Z

Global Const AG_SUCCESS = 0
Global Const AG_FILE_IO_ERROR = 1
Global Const AG_NO_LICENSE = 2
Global Const AG_DATE_TAMPERED = 3
Global Const AG_EXPIRED = 4
Global Const AG_INVALID_LICENSE = 5
Global Const AG_EMPTY_MEMORY = 6
Global Const AG_INVALID_MEMORY = 7
Global Const AG_TERMINAL_SESSION = 8
Global Const AG_NO_KEY = 9

Global AG_STATUS_CODE As Integer

 Declare Function AG_Read_vb Lib "AG_vb.dll" _
                                           (ByVal AppId As String, ByRef status As Long) As String

 Declare Function AG_IsAuthentic_vb Lib "AG_vb.dll" _
                                           (ByVal AppId As String, ByRef status As Long) As String

 Declare Function AG_Encrypt_vb Lib "AG_vb.dll" _
                                           (ByVal InputVal As String, ByRef status As Long) As String

 Declare Function AG_Decrypt_vb Lib "AG_vb.dll" _
                                           (ByVal InputVal As String, ByRef status As Long) As String

 Declare Function AG_QuickProtect_net_vb Lib "AG_vb.dll" _
                                           (ByVal AppId As String, ByRef status As Long) As Long
