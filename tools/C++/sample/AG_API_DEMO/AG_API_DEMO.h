
// AG_API_DEMO.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CAG_API_DEMOApp:
// See AG_API_DEMO.cpp for the implementation of this class
//

class CAG_API_DEMOApp : public CWinApp
{
public:
	CAG_API_DEMOApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAG_API_DEMOApp theApp;