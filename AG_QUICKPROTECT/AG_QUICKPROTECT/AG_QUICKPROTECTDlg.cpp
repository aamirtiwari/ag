
// AG_QUICKPROTECTDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_QUICKPROTECT.h"
#include "AG_QUICKPROTECTDlg.h"
#include "afxdialogex.h"
#include <stdio.h>
#include "resource.h"
#include <windows.h>
#include <string>
#include "filedlg.h"
#include <fstream>
#include "Stringobf.h"
#include "Encrypt_decrypt.h"
#include "AG_Client.h"
#include <fstream>
#include "resource.h"
#define INJECT_ERR_NOERROR      0
#define INJECT_ERR_FILENOTFOUND 1
#define INJECT_ERR_NOTAPEFILE   2
#define INJECT_ERR_PEHEADERFULL 3

#define INJECT_FLAG_DOIMPORTS   1
#define INJECT_FLAG_JUMPTOOEP   2
#define INJECT_FLAG_HANDLERELOC 4
#define INJECT_FLAG_STRIPRELOCS 8
#define INJECT_FLAG_COMPRESSDLL 16
#define INJECT_FLAG_BACKUPTLS   32

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
int signcheck=1,EXEMAKERcheck=0;
std::string appID="Demo";
std::string extension="";
std::string datafiles="";
std::string filenamepass="";
std::string filedataPE="";
std::string intermediate="";
std::string filename="";
std::string filename1="";
std::string folder="";
std::string error1="AG_SUCCESS";
std::string error2="AG_FILE_IO_ERROR";
std::string error3="AG_NO_LICENSE";
std::string error4="AG_DATE_TAMPERED";
std::string error5="AG_EXPIRED";
std::string error6="AG_INVALID_LICENSE";
std::string error7="AG_EMPTY_MEMORY";
std::string error8="AG_INVALID_MEMORY";
std::string error9="AG_TERMINAL_SESSION";
std::string error10="AG_UNKNOWN";
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

HBRUSH CAG_QUICKPROTECTDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
// CAG_QUICKPROTECTDlg dialog

CAG_QUICKPROTECTDlg::CAG_QUICKPROTECTDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAG_QUICKPROTECTDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}
void CAG_QUICKPROTECTDlg::ProcessEvents()
{
    MSG stmsg;
    while(::PeekMessageA(&stmsg,NULL,0,0,PM_REMOVE))
    {
        ::TranslateMessage(&stmsg);
        ::DispatchMessageA(&stmsg);
    }
}
void CAG_QUICKPROTECTDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT2, m_edit2);
    DDX_Control(pDX, IDC_EDIT3, m_edit3);
    DDX_Control(pDX, IDC_CHECK1, m_check1);
    DDX_Control(pDX, IDC_CHECK2, m_check2);
    DDX_Control(pDX, IDC_EDIT4, m_edit4);
    DDX_Control(pDX, IDC_EDIT5, m_edit5);
    DDX_Control(pDX, IDC_CHECK3, m_check3);
    DDX_Control(pDX, IDC_CHECK4, m_check4);
    DDX_Control(pDX, IDC_EDIT6, m_edit6);
    DDX_Control(pDX, IDC_PROGRESS1, m_progress);
    DDX_Control(pDX, IDC_CHECK5, m_check5);
    DDX_Control(pDX, IDC_EDIT7, m_edit7);
    DDX_Control(pDX, IDC_CHECK6, m_check6);
    DDX_Control(pDX, IDC_BUTTON1, m_button1);
}

BEGIN_MESSAGE_MAP(CAG_QUICKPROTECTDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON1, &CAG_QUICKPROTECTDlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_BUTTON3, &CAG_QUICKPROTECTDlg::OnBnClickedButton3)
    ON_BN_CLICKED(IDC_BUTTON2, &CAG_QUICKPROTECTDlg::OnBnClickedButton2)
    ON_BN_CLICKED(IDC_CHECK2, &CAG_QUICKPROTECTDlg::OnBnClickedCheck2)
    ON_BN_CLICKED(IDC_CHECK1, &CAG_QUICKPROTECTDlg::OnBnClickedCheck1)
    ON_BN_CLICKED(IDC_CHECK3, &CAG_QUICKPROTECTDlg::OnBnClickedCheck3)
    ON_BN_CLICKED(IDC_CHECK4, &CAG_QUICKPROTECTDlg::OnBnClickedCheck4)
    ON_BN_CLICKED(IDC_CHECK5, &CAG_QUICKPROTECTDlg::OnBnClickedCheck5)
    ON_COMMAND(ID_DATAFILES_OPENAG, &CAG_QUICKPROTECTDlg::OnDatafilesOpenag)
    ON_BN_CLICKED(IDC_CHECK6, &CAG_QUICKPROTECTDlg::OnBnClickedCheck6)
    ON_COMMAND(ID_PROJECT_SAVEAS32773, &CAG_QUICKPROTECTDlg::OnProjectSaveas32773)
    ON_COMMAND(ID_PROJECT_SAVEAS, &CAG_QUICKPROTECTDlg::OnProjectSaveas)
    ON_WM_CTLCOLOR()
    ON_COMMAND(ID_TOOLS_ENABLESIGNTOOL, &CAG_QUICKPROTECTDlg::OnToolsEnablesigntool)
    ON_COMMAND(ID_TOOLS_MERGEDLLSWITHEXE, &CAG_QUICKPROTECTDlg::OnToolsMergedllswithexe)
    ON_COMMAND(ID_TOOLS_HELP, &CAG_QUICKPROTECTDlg::OnToolsHelp)
END_MESSAGE_MAP()


// CAG_QUICKPROTECTDlg message handlers
TCHAR pwd[MAX_PATH];

void CAG_QUICKPROTECTDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

int check64(const char * lpFileName)
{
    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);

    if (pImageNtHeaders->OptionalHeader.Magic== 0x020B)
    {
        free(lpBuffer);
        CloseHandle(hOriginalFile);
        return 0;
    }
    free(lpBuffer);
    CloseHandle(hOriginalFile);
    return 1;
}

int netsection(const char * lpFileName, std::string folder)
{
    // Read the original file
    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;

    pImageNtHeaders->OptionalHeader.AddressOfEntryPoint= 0x0;
    pImageNtHeaders->OptionalHeader.DataDirectory[14].VirtualAddress = 0x0;


    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    std::string netfile = folder;
    netfile.append(filenamepass.c_str());
    HANDLE hUpdatedFile = CreateFile(netfile.append(".AuthGuru_net").c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);
    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        CloseHandle(hOriginalFile);
        return false;
    }

    // Write the original headers
    DWORD dwNumberOfBytesWritten;
    WriteFile(hUpdatedFile, lpBuffer, originalSizeOfHeaders, &dwNumberOfBytesWritten, NULL);

    // Read the original sections
    LPBYTE sectionsData = (LPBYTE)malloc(originalSizeOfImage - originalSizeOfHeaders);
    SetFilePointer(hOriginalFile,  pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    ReadFile(hOriginalFile, sectionsData, originalSizeOfImage - originalSizeOfHeaders, &dwNumberOfBytesRead, NULL);

    // Write the original sections
    SetFilePointer(hUpdatedFile, pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    WriteFile(hUpdatedFile, sectionsData, originalSizeOfImage - originalSizeOfHeaders, &dwNumberOfBytesWritten, NULL);

    free(sectionsData);

    CloseHandle(hUpdatedFile);

    free(lpBuffer);
    CloseHandle(hOriginalFile);
    return 0;
}

int allcodehidepe32(const char * lpFileName1, const char * lpFileName)
{

    HANDLE hOriginalFile = CreateFile(lpFileName1, GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;



    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    CloseHandle(hOriginalFile);

    std::string updatename= folder;
    updatename.append(filedataPE.c_str());
    updatename.append(".AuthGuru.exe");
    int size = dwFileSize/sizeof(unsigned char);
    size_t textLength = ((size / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

    unsigned char* lpBuffer1 = (LPBYTE)malloc(textLength);



    lpBuffer1=aes_encrypt_code(lpBuffer, STRING_TWO_DEFINE_NAME, dwFileSize);;




    HANDLE hUpdatedFile = CreateFile(updatename.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);
    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        free(lpBuffer1);
        return false;
    }

    // Write the original headers
    DWORD dwNumberOfBytesWritten;

    WriteFile(hUpdatedFile, lpBuffer1, (dwFileSize +16*sizeof(unsigned char)), &dwNumberOfBytesWritten, NULL);


    CloseHandle(hUpdatedFile);

    free(lpBuffer);
    free(lpBuffer1);
    return 0;

}


int allcodehide(const char * lpFileName)
{
    HANDLE hOriginalFile = CreateFile(lpFileName,GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;



    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    CloseHandle(hOriginalFile);


    HANDLE hUpdatedFile = CreateFile(lpFileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);

    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        return 1;
    }

    DWORD dwNumberOfBytesWritten;

    WriteFile(hUpdatedFile, lpBuffer, originalSizeOfHeaders, &dwNumberOfBytesWritten, NULL);
    // Read the original sections
    LPBYTE sectionsData = (LPBYTE)malloc(originalSizeOfImage - originalSizeOfHeaders);
    SetFilePointer(hOriginalFile,  pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    ReadFile(hOriginalFile, sectionsData, originalSizeOfImage - originalSizeOfHeaders, &dwNumberOfBytesRead, NULL);

    DWORD inadd= pImageNtHeaders->OptionalHeader.DataDirectory[12].VirtualAddress;
    DWORD insize= pImageNtHeaders->OptionalHeader.DataDirectory[12].Size;

    DWORD inadd1= pImageNtHeaders->OptionalHeader.DataDirectory[6].VirtualAddress;
    DWORD insize1= pImageNtHeaders->OptionalHeader.DataDirectory[6].Size;

    DWORD inadd2= pImageNtHeaders->OptionalHeader.DataDirectory[10].VirtualAddress;
    DWORD insize2= pImageNtHeaders->OptionalHeader.DataDirectory[10].Size;

    int size = (pImageSectionHeader[0].SizeOfRawData)/sizeof(unsigned char);
    size_t textLength = ((size / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

    unsigned char* lpBuffer1 = (LPBYTE)malloc(textLength);


    unsigned char* lpBuffer2 = (LPBYTE)malloc(pImageSectionHeader[0].SizeOfRawData);

    lpBuffer1=aes_encrypt_code((LPBYTE)(lpBuffer + originalSizeOfHeaders), STRING_TWO_DEFINE_NAME, pImageSectionHeader[0].SizeOfRawData);






    unsigned char data[16];
    for(int i=0; i<16; i++)
    {
        data[i]=lpBuffer1[i];
    }

    for(int i=0; i<textLength-16; i++)
    {
        lpBuffer2[i]=lpBuffer1[i+16];
    }

    // Write the original sections
    SetFilePointer(hUpdatedFile, pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    WriteFile(hUpdatedFile, lpBuffer2, textLength-16, &dwNumberOfBytesWritten, NULL);

    SetFilePointer(hUpdatedFile, pImageNtHeaders->OptionalHeader.SizeOfHeaders+textLength-16, NULL, FILE_BEGIN);
    WriteFile(hUpdatedFile, (lpBuffer+originalSizeOfHeaders+textLength-16), originalSizeOfImage - originalSizeOfHeaders - textLength+16, &dwNumberOfBytesWritten, NULL);


    SetFilePointer(hUpdatedFile,pImageNtHeaders->OptionalHeader.SizeOfHeaders +pImageNtHeaders->OptionalHeader.ImageBase+ pImageNtHeaders->OptionalHeader.DataDirectory[12].VirtualAddress, NULL, FILE_BEGIN);
    WriteFile(hUpdatedFile, (lpBuffer+ pImageNtHeaders->OptionalHeader.SizeOfHeaders+inadd+ pImageNtHeaders->OptionalHeader.ImageBase), insize, &dwNumberOfBytesWritten, NULL);

    CloseHandle(hUpdatedFile);
    free(sectionsData);
    free(lpBuffer);
    free(lpBuffer1);
    free(lpBuffer2);
    return 0;

}

int allcodehidelatest(const char * lpFileName)
{
    HANDLE hOriginalFile = CreateFile(lpFileName,GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;



    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    CloseHandle(hOriginalFile);


    HANDLE hUpdatedFile = CreateFile(lpFileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);

    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        return 1;
    }

    DWORD dwNumberOfBytesWritten;

    WriteFile(hUpdatedFile, lpBuffer, dwFileSize, &dwNumberOfBytesWritten, NULL);
    // Read the original sections
    LPBYTE sectionsData = (LPBYTE)malloc(originalSizeOfImage - originalSizeOfHeaders);
    SetFilePointer(hOriginalFile,  pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    ReadFile(hOriginalFile, sectionsData, originalSizeOfImage - originalSizeOfHeaders, &dwNumberOfBytesRead, NULL);

    DWORD inadd= pImageNtHeaders->OptionalHeader.DataDirectory[12].VirtualAddress;
    DWORD insize= pImageNtHeaders->OptionalHeader.DataDirectory[12].Size;

    DWORD inadd1= pImageNtHeaders->OptionalHeader.DataDirectory[6].VirtualAddress;
    DWORD insize1= pImageNtHeaders->OptionalHeader.DataDirectory[6].Size;

    DWORD inadd2= pImageNtHeaders->OptionalHeader.DataDirectory[10].VirtualAddress;
    DWORD insize2= pImageNtHeaders->OptionalHeader.DataDirectory[10].Size;

    int size = (pImageSectionHeader[0].SizeOfRawData)/sizeof(unsigned char);
    size_t textLength = ((size / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

    unsigned char* lpBuffer1 = (LPBYTE)malloc(textLength);


    unsigned char* lpBuffer2 = (LPBYTE)malloc(pImageSectionHeader[0].SizeOfRawData);

    lpBuffer1=aes_encrypt_code((LPBYTE)(lpBuffer + originalSizeOfHeaders), STRING_TWO_DEFINE_NAME, pImageSectionHeader[0].SizeOfRawData);






    unsigned char data[16];
    for(int i=0; i<16; i++)
    {
        data[i]=lpBuffer1[i];
    }

    for(int i=0; i<textLength-16; i++)
    {
        lpBuffer2[i]=lpBuffer1[i+16];
    }

    // Write the original sections
    SetFilePointer(hUpdatedFile, pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    WriteFile(hUpdatedFile, lpBuffer2, textLength-16, &dwNumberOfBytesWritten, NULL);

    CloseHandle(hUpdatedFile);
    free(sectionsData);
    free(lpBuffer);
    free(lpBuffer1);
    free(lpBuffer2);
    return 0;

}
int allcodehidenetnew(const char * lpFileName)
{

    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ , FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;



    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    pImageNtHeaders->OptionalHeader.DataDirectory[1].VirtualAddress=0x00000000;
    pImageNtHeaders->OptionalHeader.DataDirectory[5].VirtualAddress=0x00000000;
    pImageNtHeaders->OptionalHeader.DataDirectory[6].VirtualAddress=0x00000000;
    pImageNtHeaders->OptionalHeader.DataDirectory[10].VirtualAddress=0x00000000;
    CloseHandle(hOriginalFile);



    std::string temp=folder;
    temp.append(filenamepass.c_str());
    temp.append(".temp");

    HANDLE hUpdatedFile = CreateFile(temp.c_str(),GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);
    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        return false;
    }

    // Write the original headers
    DWORD dwNumberOfBytesWritten;

    WriteFile(hUpdatedFile, lpBuffer, dwFileSize, &dwNumberOfBytesWritten, NULL);


    CloseHandle(hUpdatedFile);

    free(lpBuffer);

    return 0;

}

int allcodehidenet(const char * lpFileName)
{

    HANDLE hOriginalFile = CreateFile(lpFileName,GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;



    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    CloseHandle(hOriginalFile);


    int size = dwFileSize/sizeof(unsigned char);
    size_t textLength = ((size / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

    unsigned char* lpBuffer1 = (LPBYTE)malloc(textLength);



    lpBuffer1=aes_encrypt_code(lpBuffer, STRING_TWO_DEFINE_NAME, dwFileSize);;




    HANDLE hUpdatedFile = CreateFile(lpFileName,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);
    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        free(lpBuffer1);
        return false;
    }

    // Write the original headers
    DWORD dwNumberOfBytesWritten;

    WriteFile(hUpdatedFile, lpBuffer1, (dwFileSize +16*sizeof(unsigned char)), &dwNumberOfBytesWritten, NULL);


    CloseHandle(hUpdatedFile);

    free(lpBuffer);
    free(lpBuffer1);
    return 0;

}

int checkifnet(const char * lpFileName)
{
    // Read the original file
    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;
    if (pImageNtHeaders->OptionalHeader.DataDirectory[14].VirtualAddress!=0x00000000)
    {
        free(lpBuffer);
        CloseHandle(hOriginalFile);
        return 0;
    }
    else
    {
        free(lpBuffer);
        CloseHandle(hOriginalFile);
        return 1;
    }
}

void sleep(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

bool manifestupdateold(const char * lpFileName, const char * pwd,const char * pwd1)
{

    std::string path = pwd;
    path.append("\\manifest.txt");

    std::string arg = "\"";
    arg.append(pwd);
    arg.append("\\");
    arg.append("mt.exe\" -inputresource:\"");
    arg.append( lpFileName);
    arg.append("\" -out:\"");
    arg.append(path.c_str());
    arg.append("\"");
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    if(signcheck==1)
    {
        CreateProcess( NULL,   // No module name (use command line)
                       LPSTR( arg.c_str()),        // Command line
                       NULL,           // Process handle not inheritable
                       NULL,           // Thread handle not inheritable
                       FALSE,          // Set handle inheritance to FALSE
                       0,              // No creation flags
                       NULL,           // Use parent's environment block
                       NULL,           // Use parent's starting directory
                       &si,            // Pointer to STARTUPINFO structure
                       &pi );
        WaitForSingleObject(pi.hProcess, INFINITE);
    }
    return 0;
}

bool manifestupdate(const char * lpFileName, const char * pwd,const char * pwd1)
{
    std::string lockcode="";
    // Read the original file
    std::string path = pwd;
    path.append("\\manifest.txt");
    std::string path1 = pwd1;
    path1.append("\\manifest1.txt");

    std::string arg1 = "\"";
    arg1.append(pwd);
    arg1.append("\\");
    arg1.append("mt.exe\" -manifest");
    arg1.append(" \"");
    arg1.append(path1.c_str());
    arg1.append("\"");
    arg1.append(" -outputresource:\"");
    path;
    arg1.append(lpFileName);
    arg1.append ("\"");
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    std::ifstream ifile;
    ifile.open(path.c_str(),std::ios::in|std::ios::binary|std::ios::ate);

    if(ifile==NULL)
    {
        path = pwd;
        path.append("\\manifest_orig.txt");
        ifile.open(path.c_str(),std::ios::in|std::ios::binary|std::ios::ate);
    }

    char *memblock;
    if (!ifile.is_open())
    {
        path = pwd;
        path.append("\\manifest_orig.txt");
        ifile.open(path.c_str(),std::ios::in|std::ios::binary|std::ios::ate);
    }
    int size=0;
    size = (int) ifile.tellg();


    memblock = new char[size];
    ifile.seekg (0, std::ios::beg);

    int size1=size;
    //file.read (&memblock[debug], size);
    //debug=strlen(&memblock[debug])+1;
    //lockcode.append(&memblock[0]);


    size_t debug=0;

    lockcode.resize(size);
    for (int read=0; read<size; read++)
    {
        ifile.read(&memblock[read],1);

        lockcode[read]=memblock[read];
    }
    lockcode = lockcode.substr(0,size);


    ifile.close();
    if(path.find("manifest_orig.txt")==std::string::npos)
        DeleteFile (path.c_str());
    if (lockcode == "")
    {
        delete[] memblock;
        return 1;
    }

    std::string lockcode1="";
    std::string dec= "";
    size_t found;

    found = lockcode.find("<trustInfo xmlns");
    if (found!=std::string::npos)
    {
        lockcode1=lockcode.substr(0,found);
        found = lockcode.find("</trustInfo>");
        dec = lockcode.substr(found+12);
        lockcode1.append(dec);
        lockcode=lockcode1;
    }
    found = lockcode.find("</assembly>");
    lockcode1=lockcode.substr(0,found);
    lockcode1.append("<trustInfo xmlns=\"urn:schemas-microsoft-com:asm.v3\"><security><requestedPrivileges><requestedExecutionLevel level=\"requireAdministrator\" uiAccess=\"false\"></requestedExecutionLevel></requestedPrivileges></security></trustInfo></assembly>");

    std::ofstream ifile1;


    ifile1.open(path1.c_str(),std::ios::out|std::ios::binary);
//	ifile.seekp(ios::ate);

    if (ifile1.is_open())
    {
        ifile1.seekp (0, std::ios::beg);
        ifile1.write (&lockcode1[0],strlen(lockcode1.c_str()));

        size_t debug=0;
        size_t debug1=0;
        debug=strlen(lockcode1.c_str())+1;
        debug1 =lockcode1.length()+1;
        while (debug<debug1)
        {
//	cout<<debug;

            ifile1.write("\0",1);
            ifile1.write(&lockcode1[debug],strlen(&lockcode1[debug]) );
            debug=strlen(&lockcode1[debug])+debug+1;
        }

        ZeroMemory( &si, sizeof(si) );
        si.cb = sizeof(si);
        ZeroMemory( &pi, sizeof(pi) );

        ifile1.close();
        if(signcheck==1)
        {
            CreateProcess( NULL,   // No module name (use command line)
                           LPSTR( arg1.c_str()),        // Command line
                           NULL,           // Process handle not inheritable
                           NULL,           // Thread handle not inheritable
                           FALSE,          // Set handle inheritance to FALSE
                           0,              // No creation flags
                           NULL,           // Use parent's environment block
                           NULL,           // Use parent's starting directory
                           &si,            // Pointer to STARTUPINFO structure
                           &pi );
            WaitForSingleObject(pi.hProcess, INFINITE);
        }

        DeleteFile (path1.c_str());

        return 0;
    }
    else
    {

        return 1;
    }

    return 0;
}

int signexe(const char * lpFileName)
{
    std::string certpath=pwd;
    certpath.append("\\");
    certpath.append("certificate.pfx");
    std::string arg = "\"";
    arg.append(pwd);
    arg.append("\\");
    arg.append("signtool.exe\" sign /f ");
    arg.append("\"");
    arg.append(certpath.c_str());
    arg.append("\"");
    arg.append(" /p 123 ");
    arg.append("\"");

    arg.append(lpFileName);
    arg.append("\"");
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    if(signcheck==1)
    {
        CreateProcess( NULL,   // No module name (use command line)
                       LPSTR( arg.c_str()),        // Command line
                       NULL,           // Process handle not inheritable
                       NULL,           // Thread handle not inheritable
                       FALSE,          // Set handle inheritance to FALSE
                       0,              // No creation flags
                       NULL,           // Use parent's environment block
                       NULL,           // Use parent's starting directory
                       &si,            // Pointer to STARTUPINFO structure
                       &pi );
        WaitForSingleObject(pi.hProcess, INFINITE);
    }
    return 0;
}

bool checkprotection(const char * lpFileName)
{
    // Read the original file
    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        DeleteFile (intermediate.c_str());
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;


    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;
    for(int i =0; i<originalNumberOfSections; i++)
    {
        std::string sectionname = (char *)pImageSectionHeader[i].Name;
        if(sectionname=="P")
        {
            free(lpBuffer);
            CloseHandle(hOriginalFile);
            return 1;
        }
    }
    free(lpBuffer);
    CloseHandle(hOriginalFile);
    return 0;
}


bool changeSection(const char * lpFileName, int cflag)
{
    // Read the original file
    HANDLE hOriginalFile = CreateFile(intermediate.c_str(), GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        DeleteFile (intermediate.c_str());
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;


    PIMAGE_SECTION_HEADER pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeaders);


    // Read the original fields of headers
    DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;
    WORD originalNumberOfSections = pImageNtHeaders->FileHeader.NumberOfSections;

    pImageSectionHeader[originalNumberOfSections-1].Characteristics=0xE0000020;
    pImageSectionHeader[originalNumberOfSections-1].Characteristics=0xE0000020;
    pImageSectionHeader[0].Characteristics=0xE0000020;
    if (cflag==0)
    {
        pImageNtHeaders->OptionalHeader.DataDirectory[14].VirtualAddress=0x0;
//pImageNtHeaders->OptionalHeader.SizeOfImage=pImageNtHeaders->OptionalHeader.SizeOfImage+0x1600;
    }



    // Update the headers


    HANDLE hUpdatedFile = CreateFile(lpFileName,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);
    if (hUpdatedFile == INVALID_HANDLE_VALUE)
    {
        free(lpBuffer);
        CloseHandle(hOriginalFile);
        DeleteFile (intermediate.c_str());
        return 1;
    }

    // Write the original headers
    DWORD dwNumberOfBytesWritten;
    WriteFile(hUpdatedFile, lpBuffer, originalSizeOfHeaders, &dwNumberOfBytesWritten, NULL);

    // Read the original sections
    LPBYTE sectionsData = (LPBYTE)malloc(originalSizeOfImage - originalSizeOfHeaders);
    SetFilePointer(hOriginalFile,  pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    ReadFile(hOriginalFile, sectionsData, originalSizeOfImage - originalSizeOfHeaders, &dwNumberOfBytesRead, NULL);

    // Write the original sections
    SetFilePointer(hUpdatedFile, pImageNtHeaders->OptionalHeader.SizeOfHeaders, NULL, FILE_BEGIN);
    WriteFile(hUpdatedFile, sectionsData, originalSizeOfImage - originalSizeOfHeaders, &dwNumberOfBytesWritten, NULL);

    free(sectionsData);

    CloseHandle(hUpdatedFile);

    free(lpBuffer);
    CloseHandle(hOriginalFile);
    DeleteFile (intermediate.c_str());
    return 0;
}

DWORD epoint(const char * lpFileName)
{
    // Read the original file
    HANDLE hOriginalFile = CreateFile(lpFileName,GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;
    pImageNtHeaders->OptionalHeader.DataDirectory[14];
    DWORD retvalue= pImageNtHeaders->OptionalHeader.AddressOfEntryPoint;
    free(lpBuffer);

    CloseHandle(hOriginalFile);
    return retvalue;
}
DWORD netd(const char * lpFileName)
{
    // Read the original file
    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ , FILE_SHARE_READ , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);


    PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader->e_lfanew);
    if (pImageNtHeaders->Signature != IMAGE_NT_SIGNATURE)
        return 1;
    DWORD retvalue=pImageNtHeaders->OptionalHeader.DataDirectory[14].VirtualAddress;
    free(lpBuffer);
    CloseHandle(hOriginalFile);
    return retvalue;
}

BOOL CAG_QUICKPROTECTDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));
    // Add "About..." menu item to system menu.
    m_edit3.SetWindowTextA("60");
    m_edit3.EnableWindow(false);
    m_edit6.EnableWindow(false);
    m_edit7.EnableWindow(false);
    m_check5.SetCheck(true);
    m_check6.SetCheck(false);
    m_check3.SetCheck(true);
    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);
    GetCurrentDirectory(MAX_PATH,pwd);
    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    if ( 	__argc >1 )
    {
        string projectname= __argv[1];
        string projectdata="";
        char *memblock;

        std::ifstream file (projectname, std::ios::in|std::ios::binary|std::ios::ate);

        if (file.is_open())
        {
            int size;
            size = (int) file.tellg();



            memblock = new char[size];
            file.seekg (0, std::ios::beg);

            int size1=size;
            //file.read (&memblock[debug], size);
            //debug=strlen(&memblock[debug])+1;
            //lockcode.append(&memblock[0]);


            size_t debug=0;

            projectdata.resize(size);
            for (int read=0; read<size; read++)
            {
                file.read (&memblock[read],1);

                projectdata[read]=memblock[read];
            }
            projectdata = projectdata.substr(0,size);

            file.close();
        }
        else
        {
            MessageBox("PROJECT NOT LOADED","ERROR",MB_ICONERROR);
            return TRUE;
        }

        if(projectdata=="")
        {
            MessageBox("PROJECT NOT LOADED","ERROR",MB_ICONERROR);
            return TRUE;
        }

        delete[] memblock;
        std::string temp="";
        size_t found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit4.SetWindowTextA(temp.c_str());
            if(temp!="")
            {
                filename =temp.c_str();;

                size_t found=0;
                found = filename.find_last_of("\\");
                if (found!=std::string::npos)
                {
                    filename1=filename.substr(found+1);
                    filenamepass=filename.substr(found+1);
                    if(filenamepass.find(".exe")!=std::string::npos)
                        extension="exe";
                }

            }
            found = projectdata.find("--");
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit5.SetWindowTextA(temp.c_str());
            if(temp!="")
            {
                filename1 = temp;

                size_t found=0;
                found = filename1.find_last_of("\\");
                if (found!=std::string::npos)
                {
                    filedataPE = filename1.substr(found+1);
                    folder = filename1.substr(0,found+1);

                }
            }
        }
        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit1.SetWindowTextA(temp.c_str());
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit2.SetWindowTextA(temp.c_str());
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                m_check1.SetCheck(true);
            }
            else
            {
                m_check1.SetCheck(false);
            }
            projectdata = projectdata.substr(found+2);
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                m_check2.SetCheck(true);
                m_edit3.EnableWindow(true);
            }
            else
            {
                m_check2.SetCheck(false);
                m_edit3.EnableWindow(false);
            }
            projectdata = projectdata.substr(found+2);
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit3.SetWindowTextA(temp.c_str());
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                m_check3.SetCheck(true);
            }
            else
            {
                m_check3.SetCheck(false);
            }
            projectdata = projectdata.substr(found+2);
        }


        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                m_check4.SetCheck(true);
                m_edit6.EnableWindow(true);
            }
            else
            {
                m_check4.SetCheck(false);
                m_edit6.EnableWindow(false);
            }
            projectdata = projectdata.substr(found+2);
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit6.SetWindowTextA(temp.c_str());
        }


        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                m_check6.SetCheck(true);
                m_edit7.EnableWindow(true);
            }
            else
            {
                m_check6.SetCheck(false);
                m_edit7.EnableWindow(false);
            }
            projectdata = projectdata.substr(found+2);
        }
        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit7.SetWindowTextA(temp.c_str());
        }

        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                m_check5.SetCheck(true);
            }
            else
            {
                m_check5.SetCheck(false);
            }
            projectdata = projectdata.substr(found+2);
        }
        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                MENUITEMINFO mItemInfo;

                CMenu *    m_menu = GetMenu();

                m_menu->CheckMenuItem(ID_TOOLS_ENABLESIGNTOOL, MF_CHECKED | MF_BYCOMMAND);
                signcheck=1;
            }
            else
            {
                MENUITEMINFO mItemInfo;

                CMenu *    m_menu = GetMenu();

                m_menu->CheckMenuItem(ID_TOOLS_ENABLESIGNTOOL, MF_UNCHECKED | MF_BYCOMMAND);
                signcheck=0;
            }
        }
        found = projectdata.find("--");

        if (found!=std::string::npos)
        {
            temp = projectdata.substr(0,found);
            if(temp=="1")
            {
                MENUITEMINFO mItemInfo;

                CMenu *    m_menu = GetMenu();

                m_menu->CheckMenuItem(ID_TOOLS_MERGEDLLSWITHEXE, MF_CHECKED | MF_BYCOMMAND);
                EXEMAKERcheck=1;
            }
            else
            {
                MENUITEMINFO mItemInfo;

                CMenu *    m_menu = GetMenu();

                m_menu->CheckMenuItem(ID_TOOLS_MERGEDLLSWITHEXE, MF_UNCHECKED | MF_BYCOMMAND);
                EXEMAKERcheck=0;
            }
        }
        std::string tempd= __argv[0];
        tempd= tempd.substr(0,tempd.find_last_of("\\"));
        SetCurrentDirectory(tempd.c_str());
        GetCurrentDirectory(MAX_PATH,pwd);
    }
    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_QUICKPROTECTDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_QUICKPROTECTDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_QUICKPROTECTDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}


typedef DWORD (__stdcall *INJECTFILEPROC)
(LPCSTR lpInputFile, LPCSTR lpOutputFile,
 LPCSTR lpDllFile, LPVOID lpExtraData,
 DWORD dwExtraDataSize, DWORD dwFlags);


void CAG_QUICKPROTECTDlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here
    m_button1.EnableWindow(false);
    HMODULE hPeinjectDll;
    INJECTFILEPROC InjectFile;
    DWORD result;

    if (filename == "") {
        MessageBox("Please select the application to protect","ERROR",MB_ICONERROR);
        m_button1.EnableWindow(true);
        return;
    }
    if (filename1 == "")
    {
        filename1.append("protected.exe");
    }
    if(EXEMAKERcheck==1)
    {
        std::string arg = "\"";
        arg.append(pwd);
        arg.append("\\");
        arg.append("AG_EXEMAKER.exe\"");
        arg.append(" ");
        arg.append("\"");
        arg.append( filename.c_str());
        arg.append("\"");
        STARTUPINFO si;
        PROCESS_INFORMATION pi;

        ZeroMemory( &si, sizeof(si) );
        si.cb = sizeof(si);
        ZeroMemory( &pi, sizeof(pi) );
        CreateProcess( NULL,   // No module name (use command line)
                       LPSTR( arg.c_str()),        // Command line
                       NULL,           // Process handle not inheritable
                       NULL,           // Thread handle not inheritable
                       FALSE,          // Set handle inheritance to FALSE
                       0,              // No creation flags
                       NULL,           // Use parent's environment block
                       NULL,           // Use parent's starting directory
                       &si,            // Pointer to STARTUPINFO structure
                       &pi );
        WaitForSingleObject(pi.hProcess, INFINITE);
        arg=filename.substr(filename.find_last_of("\\")+1);
        filename=pwd;
        filename.append("\\");
        filename.append(arg.c_str());
    }
    std::string Pdll=pwd;
    Pdll.append("\\AuthGuru.dll");
    hPeinjectDll = LoadLibrary(Pdll.c_str());

    if (!hPeinjectDll) {
        MessageBox("Failed to load AuthGuru.dll","ERROR",MB_ICONERROR);
        m_button1.EnableWindow(true);
        return;
    }

    InjectFile =
        (INJECTFILEPROC)GetProcAddress(hPeinjectDll,
                                       "InjectFile");

    if (!InjectFile) {
        MessageBox("InjectFile() not found","ERROR",MB_ICONERROR);
        FreeLibrary(hPeinjectDll);
        m_button1.EnableWindow(true);
        return;
    }
    std::string pwdxml=pwd;
    HANDLE hndl= CreateFileA((pwdxml+"\\error.xml").c_str(),GENERIC_READ, FILE_SHARE_READ,NULL, OPEN_EXISTING, NULL, NULL);
    int nbytes= GetFileSize(hndl,0);
    DWORD nbytesread;
    char * bufferread = new char[nbytes];
    ReadFile(hndl, bufferread,nbytes, &nbytesread,0);
    std::string error = bufferread;
    CloseHandle(hndl);
    delete[] bufferread;
    if(error.find("<1>")!=std::string::npos)
    {
        error1 = error.substr(error.find("<1>")+3);
        error1 = error1.substr(0,error1.find("</1>"));
    }
    if(error.find("<2>")!=std::string::npos)
    {
        error2 = error.substr(error.find("<2>")+3);
        error2 = error2.substr(0,error2.find("</2>"));
    }
    if(error.find("<3>")!=std::string::npos)
    {
        error3 = error.substr(error.find("<3>")+3);
        error3 = error3.substr(0,error3.find("</3>"));
    }
    if(error.find("<4>")!=std::string::npos)
    {
        error4 = error.substr(error.find("<4>")+3);
        error4 = error4.substr(0,error4.find("</4>"));
    }
    if(error.find("<5>")!=std::string::npos)
    {
        error5 = error.substr(error.find("<5>")+3);
        error5 = error5.substr(0,error5.find("</5>"));
    }
    if(error.find("<6>")!=std::string::npos)
    {
        error6 = error.substr(error.find("<6>")+3);
        error6 = error6.substr(0,error6.find("</6>"));
    }
    if(error.find("<7>")!=std::string::npos)
    {
        error7 = error.substr(error.find("<7>")+3);
        error7 = error7.substr(0,error7.find("</7>"));
    }
    if(error.find("<8>")!=std::string::npos)
    {
        error8 = error.substr(error.find("<8>")+3);
        error8 = error8.substr(0,error8.find("</8>"));
    }
    if(error.find("<9>")!=std::string::npos)
    {
        error9 = error.substr(error.find("<9>")+3);
        error9 = error9.substr(0,error9.find("</9>"));
    }
    if(error.find("<10>")!=std::string::npos)
    {
        error10 = error.substr(error.find("<10>")+4);
        error10 = error10.substr(0,error10.find("</10>"));
    }
    error1 = error1.append("---");
    error1 = error1.append(error2);
    error1 = error1.append("---");
    error1 = error1.append(error3);
    error1 = error1.append("---");
    error1 = error1.append(error4);
    error1 = error1.append("---");
    error1 = error1.append(error5);
    error1 = error1.append("---");
    error1 = error1.append(error6);
    error1 = error1.append("---");
    error1 = error1.append(error7);
    error1 = error1.append("---");
    error1 = error1.append(error8);
    error1 = error1.append("---");
    error1 = error1.append(error9);
    error1 = error1.append("---");
    error1 = error1.append(error10);
    error1 = error1.append("---");
    std::string blwrk=pwd;
    blwrk.append("\\Bastion.dll");
    char buff[1024]="";
    char returnval[1024]="";
    int cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string appid= buff;
    if (appid=="")
    {
        MessageBox("Please enter application ID","ERROR",MB_ICONERROR);
        m_button1.EnableWindow(true);
        return;
    }
    appid.append("%%%");
    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string version= buff;
    if (m_check1.GetCheck())
    {
        appid.append("1");
        appid.append("%%%");
    }
    else
    {
        appid.append("0");
        appid.append("%%%");
    }
    if (m_check2.GetCheck())
    {
        cTxtLen = m_edit3.GetWindowTextLength();
        m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
        appid.append(buff);
        appid.append("%%%");
    }
    else
    {
        appid.append("0");
        appid.append("%%%");
    }
    if( checkprotection(filename.c_str())==1)
    {
        if(EXEMAKERcheck==1)
        {
            MessageBox("Failed to merge dlls, uncheck merge dlls with exe","ERROR",MB_ICONERROR);
            cTxtLen = m_edit4.GetWindowTextLength();
            m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
            filename=buff;
            m_button1.EnableWindow(true);
            return;
        }

        MessageBox("Application already protected","ERROR",MB_ICONERROR);
        m_button1.EnableWindow(true);
        return;
    }
    int flag64= check64(filename.c_str());
    if (flag64==0)
    {

        Pdll=pwd;
        Pdll.append("\\AuthGuru64.dll");

        m_progress.SetRange(0,100);
        m_progress.SetPos(0);
        ProcessEvents();
        int cflag = 1;



        if (cflag==0)
        {
            appid.append("0");
            appid.append("%%%");
            DWORD ept = epoint(filename.c_str());
            sprintf_s(returnval,10,"%d",ept);
            appid.append(returnval);
            appid.append("%%%");
            DWORD nd = netd(filename.c_str());
            sprintf_s(returnval,10,"%d",nd);
            appid.append(returnval);

        }
        else
        {
            appid.append("1");
            appid.append("%%%");
            appid.append("0");
            appid.append("%%%");
            appid.append("0");
        }
        std::string out = "";

        out.resize(3*appid.size());
        int checkvalue=0;
        while (checkvalue==0)
        {
            out="";
            AG_Encrypt (&appid, &out);
            out.append("%%%");
            out=out.c_str();
            if(out.find("%%%")!=std::string::npos)
                checkvalue=1;
        }

        appid=out;

        appid.append(filenamepass.c_str());
        appid.append("%%%");
        appid.append(filedataPE.c_str());
        if(m_check3.GetCheck())
        {
            appid.append("%%%");
            appid.append("1");
        }
        else
        {
            appid.append("%%%");
            appid.append("0");
        }
        appid.append("%%%");
        if(m_check4.GetCheck())
        {
            cTxtLen = m_edit6.GetWindowTextLength();
            m_edit6.GetWindowTextA(&buff[0],cTxtLen+1);
            datafiles= buff;
            if(datafiles=="")
            {
                MessageBox("Please specify names of protected Files","ERROR",MB_ICONERROR);
                m_button1.EnableWindow(true);
                return;
            }
            else
            {
                appid.append("1");
                appid.append("%%%");
                appid.append(datafiles.c_str());
                appid.append("%%%");
            }
        }
        else
        {
            appid.append("0");
            appid.append("%%%");
            appid.append("BastionNill");
            appid.append("%%%");
        }
        if (m_check5.GetCheck())
        {
            appid.append("1");
        }
        else
        {
            appid.append("0");
        }
        appid.append("%%%");
        if(m_check6.GetCheck())
        {
            cTxtLen = m_edit7.GetWindowTextLength();
            m_edit7.GetWindowTextA(&buff[0],cTxtLen+1);

            appid.append("1");
            appid.append("%%%");
            appid.append(buff);
            appid.append("%%%");
        }
        else
        {
            appid.append("0");
            appid.append("%%%");
            appid.append("");
            appid.append("%%%");
        }
        appid.append(version.c_str());
        appid.append("%%%");
        appid.append(error1.c_str());

        intermediate=folder;
        intermediate.append("temp.exe");
        blwrk=pwd;
        blwrk.append("\\Bastion64.dll");


        std::string arg = "\"";
        arg.append(pwd);
        arg.append("\\");
        arg.append("AG_QuickProtect_cmd.exe\"");
        arg.append(" ");
        arg.append("\"");
        arg.append( filename.c_str());
        arg.append("\"");
        arg.append(" ");
        arg.append("\"");
        arg.append(intermediate.c_str());
        arg.append("\"");
        arg.append(" ");
        arg.append("\"");
        arg.append(blwrk.c_str());
        arg.append("\"");
        arg.append(" ");
        arg.append("\"");
        arg.append(appid.c_str());
        arg.append("\"");
        arg.append(" ");
        arg.append("\"");
        arg.append(Pdll.c_str());
        arg.append("\"");
        STARTUPINFO si;
        PROCESS_INFORMATION pi;

        ZeroMemory( &si, sizeof(si) );
        si.cb = sizeof(si);
        ZeroMemory( &pi, sizeof(pi) );

        CreateProcess( NULL,   // No module name (use command line)
                       LPSTR( arg.c_str()),        // Command line
                       NULL,           // Process handle not inheritable
                       NULL,           // Thread handle not inheritable
                       FALSE,          // Set handle inheritance to FALSE
                       0,              // No creation flags
                       NULL,           // Use parent's environment block
                       NULL,           // Use parent's starting directory
                       &si,            // Pointer to STARTUPINFO structure
                       &pi );


        WaitForSingleObject(pi.hProcess, INFINITE);
        DWORD exitCode=0;
        GetExitCodeProcess(pi.hProcess, &exitCode);
        if (exitCode)
        {
            m_button1.EnableWindow(true);
            return;
        }
        m_progress.SetPos(10);
        ProcessEvents();

        m_progress.SetPos(20);
        ProcessEvents();
        result = changeSection(filename1.c_str(),cflag);
        std::string manifest=pwd;
        std::string manifest1=pwd;
        m_progress.SetPos(30);
        ProcessEvents();

        m_progress.SetPos(90);
        ProcessEvents();
        m_progress.SetPos(90);
        int status=	manifestupdateold(filename1.c_str(),manifest.c_str(),manifest1.c_str());
        ProcessEvents();
        m_progress.SetPos(92);
        status=	manifestupdate(filename1.c_str(),manifest.c_str(),manifest1.c_str());
        ProcessEvents();
        m_progress.SetPos(94);
        ProcessEvents();

        status = signexe(filename1.c_str());
        ProcessEvents();
        m_progress.SetPos(95);
        FreeLibrary(hPeinjectDll);
        std::string AG_CLIENT=pwd;

        AG_CLIENT.append("\\AG_CLIENT_64.dll");

        std::string AG_CLIENTnew=folder;

        AG_CLIENTnew.append("AG_CLIENT_64.dll");

        CopyFile(AG_CLIENT.c_str(),AG_CLIENTnew.c_str(), FALSE);
        ProcessEvents();
        m_progress.SetPos(100);

        MessageBox("File successfuly protected","DONE",MB_ICONINFORMATION);
        m_button1.EnableWindow(true);
        if(EXEMAKERcheck==1)
            DeleteFileA(filename.c_str());
        return;
    }
    Pdll=pwd;
    Pdll.append("\\AuthGuru.dll");
    hPeinjectDll = LoadLibrary(Pdll.c_str());

    if (!hPeinjectDll) {
        MessageBox("Failed to load AuthGuru.dll","ERROR",MB_ICONERROR);
        m_button1.EnableWindow(true);
        return;

    }
    m_progress.SetRange(0,100);
    m_progress.SetPos(0);
    ProcessEvents();
    int cflag = checkifnet(filename.c_str());



    if (cflag==0)
    {
        appid.append("0");
        appid.append("%%%");
        DWORD ept = epoint(filename.c_str());
        sprintf_s(returnval,10,"%d",ept);
        appid.append(returnval);
        appid.append("%%%");
        DWORD nd = netd(filename.c_str());
        sprintf_s(returnval,10,"%d",nd);
        appid.append(returnval);

    }
    else
    {
        appid.append("1");
        appid.append("%%%");
        appid.append("0");
        appid.append("%%%");
        appid.append("0");
    }
    std::string out = "";

    out.resize(3*appid.size());
    int checkvalue=0;
    while (checkvalue==0)
    {
        out="";
        AG_Encrypt (&appid, &out);
        out.append("%%%");
        out=out.c_str();
        if(out.find("%%%")!=std::string::npos)
            checkvalue=1;
    }

    appid=out;
    appid.append(filenamepass.c_str());
    appid.append("%%%");
    appid.append(filedataPE.c_str());
    if(m_check3.GetCheck())
    {
        appid.append("%%%");
        appid.append("1");
    }
    else
    {
        appid.append("%%%");
        appid.append("0");
    }
    appid.append("%%%");
    if(m_check4.GetCheck())
    {
        cTxtLen = m_edit6.GetWindowTextLength();
        m_edit6.GetWindowTextA(&buff[0],cTxtLen+1);
        datafiles= buff;
        if(datafiles=="")
        {
            MessageBox("Please specify names of protected Files","ERROR",MB_ICONERROR);
            m_button1.EnableWindow(true);
            return;
        }
        else
        {
            appid.append("1");
            appid.append("%%%");
            appid.append(datafiles.c_str());
            appid.append("%%%");
        }
    }
    else
    {
        appid.append("0");
        appid.append("%%%");
        appid.append("BastionNill");
        appid.append("%%%");
    }
    if (m_check5.GetCheck())
    {
        appid.append("1");
    }
    else
    {
        appid.append("0");
    }
    appid.append("%%%");
    if(m_check6.GetCheck())
    {
        cTxtLen = m_edit7.GetWindowTextLength();
        m_edit7.GetWindowTextA(&buff[0],cTxtLen+1);

        appid.append("1");
        appid.append("%%%");
        appid.append(buff);
        appid.append("%%%");
    }
    else
    {
        appid.append("0");
        appid.append("%%%");
        appid.append("");
        appid.append("%%%");
    }
    appid.append(version.c_str());
    appid.append("%%%");
    appid.append(error1.c_str());
    intermediate=folder;
    intermediate.append("temp.exe");

    if (cflag==0 && m_check3.GetCheck())

    {
        std::string csharptemp=folder;
        csharptemp.append("bastion.exe");
        CopyFile(filename.c_str(),csharptemp.c_str(), FALSE);
        std::string manifest=pwd;
        std::string manifest1=pwd;
        int status=	manifestupdateold(csharptemp.c_str(),manifest.c_str(),manifest1.c_str());
        ProcessEvents();
        status=	manifestupdate(csharptemp.c_str(),manifest.c_str(),manifest1.c_str());
        ProcessEvents();

        allcodehidelatest(csharptemp.c_str());
        ProcessEvents();
        result = InjectFile(csharptemp.c_str(), intermediate.c_str(),
                            blwrk.c_str(), &appid[0], appid.length(),
                            INJECT_FLAG_DOIMPORTS
                            | INJECT_FLAG_JUMPTOOEP
                            | INJECT_FLAG_HANDLERELOC
                            | INJECT_FLAG_COMPRESSDLL
                            | INJECT_FLAG_BACKUPTLS);
        m_progress.SetPos(10);
        ProcessEvents();
        DeleteFile (csharptemp.c_str());

    }
    else
    {
        result = InjectFile(filename.c_str(), intermediate.c_str(),
                            blwrk.c_str(), &appid[0], appid.length(),
                            INJECT_FLAG_DOIMPORTS
                            | INJECT_FLAG_JUMPTOOEP
                            | INJECT_FLAG_HANDLERELOC
                            | INJECT_FLAG_COMPRESSDLL
                            | INJECT_FLAG_BACKUPTLS);
        m_progress.SetPos(10);
        ProcessEvents();
    }
    if (result) {

        // one of the INJECT_ERR_ constants
        MessageBox("An error occured","ERROR",MB_ICONERROR);
        exit(0);
        m_button1.EnableWindow(true);
        return;
    }
    m_progress.SetPos(20);
    ProcessEvents();
    result = changeSection(filename1.c_str(),cflag);
    std::string manifest=pwd;
    std::string manifest1=pwd;
    m_progress.SetPos(30);
    ProcessEvents();
    if (cflag==0)
    {
        m_progress.SetPos(30);
        netsection(filename.c_str(),folder);
        m_progress.SetPos(40);
        ProcessEvents();
        std::string netfile = folder;
        netfile.append(filenamepass.c_str());
        netfile.append(".AuthGuru_net");
        allcodehidenet(netfile.c_str());
        m_progress.SetPos(50);
        ProcessEvents();
        m_progress.SetPos(60);
        ProcessEvents();
        std::string authguru=pwd;
        authguru.append("\\AuthGuru_net.dll");
        std::string newfolder=folder;
        newfolder.append("AuthGuru_net.dll");
        CopyFile(authguru.c_str(),newfolder.c_str(), FALSE);

        std::string authguru1=pwd;
        authguru1.append("\\Bastion_net.dll");
        std::string newfolder1=folder;
        newfolder1.append("Bastion_net.dll");
        CopyFile(authguru1.c_str(),newfolder1.c_str(), FALSE);

        std::string authguru2=pwd;
        authguru2.append("\\msvcr100.dll");
        std::string newfolder2=folder;
        newfolder2.append("msvcr100.dll");
        CopyFile(authguru2.c_str(),newfolder2.c_str(), FALSE);

        std::string authguru3=pwd;
        authguru3.append("\\msvcr100_clr0400.dll");
        std::string newfolder3=folder;
        newfolder3.append("msvcr100_clr0400.dll");
        CopyFile(authguru3.c_str(),newfolder3.c_str(), FALSE);
        m_progress.SetPos(80);
        ProcessEvents();
    }

    else
    {

        if (extension=="exe" && m_check3.GetCheck())
        {
            m_progress.SetPos(30);
            std::string encfile=folder;
            encfile.append(filedataPE.c_str());
            allcodehidepe32(filename1.c_str(),encfile.c_str());
            /*	 allcodehide(filename1.c_str());

            	  allcodehidenetnew(filename.c_str());
            	   std::string temp = filename;
            	   temp.append(".temp");*/
            std::string blwrk=pwd;
            blwrk.append("\\Bastion_v2.dll");
            ProcessEvents();
            m_progress.SetPos(40);

            allcodehidenetnew(filename.c_str());
            std::string temp = folder;
            temp.append(filenamepass.c_str());
            temp.append(".temp");
            m_progress.SetPos(50);
            ProcessEvents();

            result = InjectFile(temp.c_str(), 	intermediate.c_str(),
                                blwrk.c_str(), &appid[0], appid.length(),
                                INJECT_FLAG_DOIMPORTS
                                | INJECT_FLAG_JUMPTOOEP
                                | INJECT_FLAG_HANDLERELOC
                                | INJECT_FLAG_COMPRESSDLL
                                | INJECT_FLAG_BACKUPTLS);
            ProcessEvents();
            m_progress.SetPos(60);
            if (result) {

                // one of the INJECT_ERR_ constants
                MessageBox("An error occured","ERROR",MB_ICONERROR);
                exit(0);
                m_button1.EnableWindow(true);
                return;
            }
            m_progress.SetPos(70);
            result = changeSection(filename1.c_str(),cflag);
            ProcessEvents();
            m_progress.SetPos(80);
            allcodehide(filename1.c_str());
            ProcessEvents();
            m_progress.SetPos(80);
            DeleteFile (temp.c_str());


        }

    }
    m_progress.SetPos(90);

    ProcessEvents();
    m_progress.SetPos(90);
    int status=	manifestupdateold(filename1.c_str(),manifest.c_str(),manifest1.c_str());
    ProcessEvents();
    m_progress.SetPos(92);
    status=	manifestupdate(filename1.c_str(),manifest.c_str(),manifest1.c_str());
    ProcessEvents();
    m_progress.SetPos(94);

    ProcessEvents();

    status = signexe(filename1.c_str());
    ProcessEvents();
    m_progress.SetPos(95);
    FreeLibrary(hPeinjectDll);
    std::string AG_CLIENT=pwd;

    AG_CLIENT.append("\\AG_CLIENT.dll");

    std::string AG_CLIENTnew=folder;

    AG_CLIENTnew.append("AG_CLIENT.dll");

    CopyFile(AG_CLIENT.c_str(),AG_CLIENTnew.c_str(), FALSE);
    ProcessEvents();
    m_progress.SetPos(100);

    MessageBox("File successfuly protected","DONE",MB_ICONINFORMATION);
    m_button1.EnableWindow(true);
    if(EXEMAKERcheck==1)
        DeleteFileA(filename.c_str());
    return;
}


void CAG_QUICKPROTECTDlg::OnBnClickedButton3()
{
    // TODO: Add your control notification handler code here
    if(extension=="")
    {
        MessageBox("Please select the application to protect first","ERROR",MB_ICONERROR);
        return;
    }
    filedlg fdlg(false,extension.c_str(),filename1.c_str(),OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.exe/*.dll",this);

    fdlg.DoModal();
    filename1 = fdlg.GetPathName();
    filedataPE = fdlg.GetFileName();
    m_edit5.SetWindowTextA(filename1.c_str());
    size_t found=0;
    found = filename1.find_last_of("\\");
    if (found!=std::string::npos)
    {

        folder = filename1.substr(0,found+1);

    }
//	filename1.append("\\");
//	filename1.append(fdlg.GetFileName());

}



void CAG_QUICKPROTECTDlg::OnBnClickedButton2()
{
    filedlg fdlg(true,"exe","",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.exe||*.dll||",NULL);

    fdlg.DoModal();
    filename = fdlg.GetPathName();
    m_edit4.SetWindowTextA(filename.c_str());
    extension = fdlg.GetFileExt();
    filename1=fdlg.GetFileName();
    filenamepass= fdlg.GetFileName();
    size_t found=0;
    found = filename.find_last_of("\\");
    if (found!=std::string::npos)
    {

        filename = filename.substr(0,found+1);

    }
    filename.append("\\");
    filename.append(fdlg.GetFileName());

    m_edit5.SetWindowTextA("");
//	filename.append("\\");
//	filename.append(fdlg.GetFileName());
    // TODO: Add your control notification handler code here
}


void CAG_QUICKPROTECTDlg::OnBnClickedCheck2()
{
    // TODO: Add your control notification handler code here
    if (m_check2.GetCheck())
    {

        m_edit3.EnableWindow(true);
    }
    else
    {
        m_edit3.EnableWindow(false);
    }
}


void CAG_QUICKPROTECTDlg::OnBnClickedCheck1()
{
    // TODO: Add your control notification handler code here
    if (m_check1.GetCheck())
    {
        MessageBox("It is not recommended to allow this \nas it could compromise security","WARNING",MB_ICONWARNING);
    }

}


void CAG_QUICKPROTECTDlg::OnBnClickedCheck3()
{
    if (!m_check3.GetCheck())
    {
        MessageBox("It is not recommended to uncheck this \nas it could compromise security","WARNING",MB_ICONWARNING);
    }
    // TODO: Add your control notification handler code here
}


void CAG_QUICKPROTECTDlg::OnBnClickedCheck4()
{
    // TODO: Add your control notification handler code here
    // TODO: Add your control notification handler code here
    if(m_check4.GetCheck())
    {
        m_edit6.EnableWindow(true);
    }
    else
    {
        m_edit6.EnableWindow(false);
        m_edit7.EnableWindow(false);
        m_check6.SetCheck(false);
    }
}


void CAG_QUICKPROTECTDlg::OnBnClickedCheck5()
{
    // TODO: Add your control notification handler code here

}


void CAG_QUICKPROTECTDlg::OnDatafilesOpenag()
{
    // TODO: Add your command handler code here
    char buffkey[1024];
    int cTxtLen = m_edit7.GetWindowTextLength();
    m_edit7.GetWindowTextA(&buffkey[0],cTxtLen+1);
    std::string arg = "\"";
    arg.append(pwd);
    arg.append("\\");
    arg.append("AG_Encrypt.exe\"");
    arg.append(" ");
    arg.append("\"");
    arg.append( buffkey);
    cTxtLen = m_edit6.GetWindowTextLength();
    m_edit6.GetWindowTextA(&buffkey[0],cTxtLen+1);
    arg.append("%%%");
    arg.append( buffkey);
    arg.append("\"");
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    CreateProcess( NULL,   // No module name (use command line)
                   LPSTR( arg.c_str()),        // Command line
                   NULL,           // Process handle not inheritable
                   NULL,           // Thread handle not inheritable
                   FALSE,          // Set handle inheritance to FALSE
                   0,              // No creation flags
                   NULL,           // Use parent's environment block
                   NULL,           // Use parent's starting directory
                   &si,            // Pointer to STARTUPINFO structure
                   &pi );
    WaitForSingleObject(pi.hProcess, INFINITE);
}


void CAG_QUICKPROTECTDlg::OnBnClickedCheck6()
{
    // TODO: Add your control notification handler code here
    if(m_check4.GetCheck())
    {
        if(m_check6.GetCheck())
        {
            m_edit7.EnableWindow(true);
        }
        else
        {
            m_edit7.EnableWindow(false);
        }
    }
    else
    {
        MessageBox("PLEASE CHECK DECRYPT DATA FILES FIRST","ERROR",MB_ICONERROR);
        m_check6.SetCheck(false);
        return;
    }
}


void CAG_QUICKPROTECTDlg::OnProjectSaveas32773()
{
    // TODO: Add your command handler code here
    filedlg fdlg(false,"qpproj","project",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.qpproj",NULL);

    fdlg.DoModal();
    string project= fdlg.GetFileName();


    if(project=="")
    {

        return;
    }

    project = fdlg.GetPathName();
    string projectdata=filename;
    projectdata.append("--");
    projectdata.append(filename1);
    projectdata.append("--");

    std::ofstream ifile;
    ifile.open(project,std::ios::out|std::ios::binary );
    ifile.seekp (0, std::ios::beg);

    char buff[1024]="";
    int cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string appid= buff;
    projectdata.append(appid);
    projectdata.append("--");

    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    projectdata.append(buff);
    projectdata.append("--");

    if (m_check1.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }
    if (m_check2.GetCheck())
    {

        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }

    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    projectdata.append(buff);
    projectdata.append("--");

    if(m_check3.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }


    if(m_check4.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }
    cTxtLen = m_edit6.GetWindowTextLength();
    m_edit6.GetWindowTextA(&buff[0],cTxtLen+1);
    projectdata.append(buff);
    projectdata.append("--");


    if(m_check6.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }
    cTxtLen = m_edit7.GetWindowTextLength();
    m_edit7.GetWindowTextA(&buff[0],cTxtLen+1);
    projectdata.append(buff);
    projectdata.append("--");
    if (m_check5.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }
    if	(signcheck==1)
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }

    if(EXEMAKERcheck==1)
    {
        projectdata.append("1");
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }
    MessageBox("Project successfuly saved","DONE",MB_ICONINFORMATION);
    ifile.write (&projectdata[0],strlen(projectdata.c_str()));
    ifile.close();
}


void CAG_QUICKPROTECTDlg::OnProjectSaveas()
{
    // TODO: Add your command handler code here
    // TODO: Add your command handler code here
    filedlg fdlg(true,"qpproj","project.qpproj",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.qpproj",NULL);

    fdlg.DoModal();
    string projectname= fdlg.GetFileName();
    if(projectname=="")
    {
        MessageBox("PROJECT NOT LOADED","WARNING",MB_ICONWARNING);
        return;
    }

    projectname= fdlg.GetPathName();
    string projectdata="";
    char *memblock;

    std::ifstream file (projectname, std::ios::in|std::ios::binary|std::ios::ate);

    if (file.is_open())
    {
        int size;
        size = (int) file.tellg();



        memblock = new char[size];
        file.seekg (0, std::ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        projectdata.resize(size);
        for (int read=0; read<size; read++)
        {
            file.read (&memblock[read],1);

            projectdata[read]=memblock[read];
        }
        projectdata = projectdata.substr(0,size);

        file.close();
    }
    else
    {
        MessageBox("PROJECT NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    if(projectdata=="")
    {
        MessageBox("PROJECT NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    delete[] memblock;
    std::string temp="";
    size_t found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit4.SetWindowTextA(temp.c_str());
        if(temp!="")
        {
            filename =temp.c_str();;

            size_t found=0;
            found = filename.find_last_of("\\");
            if (found!=std::string::npos)
            {
                filename1=filename.substr(found+1);
                filenamepass=filename.substr(found+1);
                if(filenamepass.find(".exe")!=std::string::npos)
                    extension="exe";
            }

        }
        found = projectdata.find("--");
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit5.SetWindowTextA(temp.c_str());
        if(temp!="")
        {
            filename1 = temp;

            size_t found=0;
            found = filename1.find_last_of("\\");
            if (found!=std::string::npos)
            {
                filedataPE = filename1.substr(found+1);
                folder = filename1.substr(0,found+1);

            }
        }
    }
    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit1.SetWindowTextA(temp.c_str());
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit2.SetWindowTextA(temp.c_str());
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            m_check1.SetCheck(true);
        }
        else
        {
            m_check1.SetCheck(false);
        }
        projectdata = projectdata.substr(found+2);
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            m_check2.SetCheck(true);
            m_edit3.EnableWindow(true);
        }
        else
        {
            m_check2.SetCheck(false);
            m_edit3.EnableWindow(false);
        }
        projectdata = projectdata.substr(found+2);
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit3.SetWindowTextA(temp.c_str());
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            m_check3.SetCheck(true);
        }
        else
        {
            m_check3.SetCheck(false);
        }
        projectdata = projectdata.substr(found+2);
    }


    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            m_check4.SetCheck(true);
            m_edit6.EnableWindow(true);
        }
        else
        {
            m_check4.SetCheck(false);
            m_edit6.EnableWindow(false);
        }
        projectdata = projectdata.substr(found+2);
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit6.SetWindowTextA(temp.c_str());
    }


    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            m_check6.SetCheck(true);
            m_edit7.EnableWindow(true);
        }
        else
        {
            m_check6.SetCheck(false);
            m_edit7.EnableWindow(false);
        }
        projectdata = projectdata.substr(found+2);
    }
    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit7.SetWindowTextA(temp.c_str());
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            m_check5.SetCheck(true);
        }
        else
        {
            m_check5.SetCheck(false);
        }
        projectdata = projectdata.substr(found+2);
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            MENUITEMINFO mItemInfo;

            CMenu *    m_menu = GetMenu();

            m_menu->CheckMenuItem(ID_TOOLS_ENABLESIGNTOOL, MF_CHECKED | MF_BYCOMMAND);
            signcheck=1;
        }
        else
        {
            MENUITEMINFO mItemInfo;

            CMenu *    m_menu = GetMenu();

            m_menu->CheckMenuItem(ID_TOOLS_ENABLESIGNTOOL, MF_UNCHECKED | MF_BYCOMMAND);
            signcheck=0;
        }
    }

    found = projectdata.find("--");

    if (found!=std::string::npos)
    {
        temp = projectdata.substr(0,found);
        if(temp=="1")
        {
            MENUITEMINFO mItemInfo;

            CMenu *    m_menu = GetMenu();

            m_menu->CheckMenuItem(ID_TOOLS_MERGEDLLSWITHEXE, MF_CHECKED | MF_BYCOMMAND);
            EXEMAKERcheck=1;
        }
        else
        {
            MENUITEMINFO mItemInfo;

            CMenu *    m_menu = GetMenu();

            m_menu->CheckMenuItem(ID_TOOLS_MERGEDLLSWITHEXE, MF_UNCHECKED | MF_BYCOMMAND);
            EXEMAKERcheck=0;
        }
    }

    return;
}


void CAG_QUICKPROTECTDlg::OnToolsEnablesigntool()
{

    MENUITEMINFO mItemInfo;

    CMenu *    m_menu = GetMenu();
    if(signcheck==1)
    {
        m_menu->CheckMenuItem(ID_TOOLS_ENABLESIGNTOOL, MF_UNCHECKED | MF_BYCOMMAND);
        signcheck=0;
    }
    else
    {
        m_menu->CheckMenuItem(ID_TOOLS_ENABLESIGNTOOL, MF_CHECKED | MF_BYCOMMAND);
        signcheck=1;
    }



}


void CAG_QUICKPROTECTDlg::OnToolsMergedllswithexe()
{
    // TODO: Add your command handler code here
    MENUITEMINFO mItemInfo;

    CMenu *    m_menu = GetMenu();
    if(EXEMAKERcheck==1)
    {
        m_menu->CheckMenuItem(ID_TOOLS_MERGEDLLSWITHEXE, MF_UNCHECKED | MF_BYCOMMAND);
        EXEMAKERcheck=0;
    }
    else
    {
        m_menu->CheckMenuItem(ID_TOOLS_MERGEDLLSWITHEXE, MF_CHECKED | MF_BYCOMMAND);
        EXEMAKERcheck=1;
    }
}


void CAG_QUICKPROTECTDlg::OnToolsHelp()
{
    // TODO: Add your command handler code here
    std::string arg ="cmd.exe /C ";
    arg.append("\"");
    arg.append(pwd);
    arg.append("Documents\\README.pdf");
    arg.append("\"");
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    CreateProcess( NULL,   // No module name (use command line)
                   LPSTR( arg.c_str()),        // Command line
                   NULL,           // Process handle not inheritable
                   NULL,           // Thread handle not inheritable
                   FALSE,          // Set handle inheritance to FALSE
                   NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,              // No creation flags
                   NULL,           // Use parent's environment block
                   NULL,           // Use parent's starting directory
                   &si,            // Pointer to STARTUPINFO structure
                   &pi );
}
