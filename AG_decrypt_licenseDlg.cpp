
// AG_decrypt_licenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_Client.h"
#include "AG_decrypt_license.h"
#include "AG_decrypt_licenseDlg.h"
#include "afxdialogex.h"
#include "resource.h"
#include "DIVIDEINFO.h"
#include "Encrypt_decrypt.h"
#include <direct.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "Stringobf.h"
#include "HARDDISKID.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()



// CAG_decrypt_licenseDlg dialog




CAG_decrypt_licenseDlg::CAG_decrypt_licenseDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAG_decrypt_licenseDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

void CAG_decrypt_licenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Control(pDX, IDC_MFCPROPERTYGRID1, m_mfcproperty);
	//  DDX_Control(pDX, IDC_TREE3, m_TREE);
	//  DDX_Control(pDX, IDC_TREE3, m_tree);
	DDX_Control(pDX, IDC_TREE1, m_tree);
}

BEGIN_MESSAGE_MAP(CAG_decrypt_licenseDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_ACTIONS_REFRESH, &CAG_decrypt_licenseDlg::OnActionsRefresh)
END_MESSAGE_MAP()

using namespace std;
// CAG_decrypt_licenseDlg message handlers

void CAG_decrypt_licenseDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}
BOOL CAG_decrypt_licenseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	SetBackGroundColor(RGB(192,192,192));
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

		BOOL bDebugged = FALSE;
	CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
	if( bDebugged )
	{
	MessageBox("Being debugged!","ERROR",MB_ICONERROR);
	exit(0);
	return true;
	}
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	HTREEITEM	hItem1[4];
	// TODO: Add extra initialization here
	std::string Handle="";
	Handle.resize(1024);
	std::string AppId="testing";
	std::string servername="";
	servername.resize(100);
	 int returnval =  AG_IsAuthentic (&Handle,&AppId, 0, 1, &servername);
	 if(returnval==0 && servername!="")
		 AG_Release(&Handle, &servername);
		std::string lockcode = "";
		std::string file="";
//	filedlg fdlg(true,"upd","update",OFN_HIDEREADONLY |OF_PROMPT ,"*.upd",NULL);
	
//	fdlg.DoModal();
	//file =  "update.upd";//fdlg.GetFileName();

		_mkdir("C:/Users");
		_mkdir("C:/Users/Public");
		_mkdir("C:/Users/Public/Application Data");
		_mkdir("C:/Users/Public/Application Data/Bastion");
		SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
	//file.append();

	

	std::ifstream ifile;
	ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",ios::in|ios::binary|ios::ate);
	
	if(ifile==NULL)
		{
		
			return 1;
	}

			  char *memblock;

  if (ifile.is_open())
  {
   int size;
size = (int) ifile.tellg(); 


 memblock = new char[size];
    ifile.seekg (0, ios::beg);

	int size1=size;
	//file.read (&memblock[debug], size);
	//debug=strlen(&memblock[debug])+1;
	//lockcode.append(&memblock[0]);
	

	size_t debug=0;

	lockcode.resize(size);
	for (int read=0;read<size;read++)
	{
		ifile.read(&memblock[read],1);
	
		lockcode[read]=memblock[read];
	}
	lockcode = lockcode.substr(0,size);
	
   
	 ifile.close();
  }
  else
  {

		 
			  MessageBox("FILE NOT LOADED","ERROR",MB_ICONERROR);
			  return 1;
		 
  }
	
  if(lockcode=="")
		  {
			  MessageBox("FILE NOT LOADED","ERROR",MB_ICONERROR);
			  return 1;
		  }
  	m_image.Create(16, 16, ILC_MASK, 0, 1);
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON7));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON8));
	m_tree.SetImageList(&m_image,TVSIL_NORMAL);
//	m_tree.SetBkColor(RGB(170,170,0));
		  std::string dec="";
		  std::string lockcode1="";
	//	  MessageBox("INSIDE FUNC","",0);
		   size_t found;
		  size_t found1;
		  size_t found2;
		   int USBlock=0;
		   int networkflag=0;
		   std::string network ="";
		 std::string USBID = "";
		  	hItem1[0]=m_tree.InsertItem("STANDALONE",0,0);
			HTREEITEM Htemp=hItem1[0];
			hItem1[0]=m_tree.InsertItem("NETWORK",0,0);
			std::string futuretemp ="";
		  while (lockcode!="")
		  {

			  		 
	
			  found = lockcode.find("%%%");
		  	if (found!=std::string::npos)
	{
			   lockcode1=lockcode.substr(0,found);

			   found1 = lockcode1.find("---v2---");
	if (found1!=std::string::npos)
	{
		lockcode1=lockcode1.substr(0,found1);
		USBlock=1;
	}
	else
	{
		USBlock=0;
	}

	if (USBlock==0)
	{

			    std::string runtime = getHardDriveComputerID ();
		  dec = aes_decrypt(lockcode1, STRING_THREE_DEFINE_NAME + runtime);
		   found1 = dec.find("---v3---");
			if (found1!=std::string::npos)
			{
				
				USBID = dec.substr(found1+8);
		dec=dec.substr(0,found1);
		found2 = USBID.find("---vn---");
			if (found2!=std::string::npos)
			{
				
				network = USBID.substr(found2);
				networkflag=1;
		USBID=USBID.substr(0,found2);
		USBlock=2;
			}
			else
			{
				networkflag=0;
			}
		USBlock=2;
			}
			  /////network//////
		   found2 = dec.find("---vn---");
			if (found2!=std::string::npos)
			{
				
				network = dec.substr(found2);
				networkflag=1;
		dec=dec.substr(0,found2);
			}
			else
			{
				networkflag=0;
			}
	}
	else
	{
dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
found2 = dec.find("---vn---");
			if (found2!=std::string::npos)
			{
				
				network = dec.substr(found2);
				networkflag=1;
		dec=dec.substr(0,found2);
			}
			else
			{
				networkflag=0;
			}
	}
		  WORD upw=0;
		 WORD upw1=0;
		 std::string divlic[10]={""};
		 divlic[0]=dec;
	std::string lic= LICENSE(&divlic[0]);
	dec=lic;
	std::string check =divlic[0];
	dec.append(check);
	if(networkflag==1)
	{
		//MessageBox(network.c_str(),"NETWORK",0);
		
		hItem1[1]=m_tree.InsertItem((divlic[2]+" (APPLICATION ID)").c_str(),1,1,hItem1[0]);
		  	  hItem1[2]=m_tree.InsertItem((divlic[3]+" (VERSION)").c_str(),2,2,hItem1[1]);
		  hItem1[2]=m_tree.InsertItem((divlic[1]+" (VENDOR ID)").c_str(),3,3,hItem1[1]);
	long date= strtol(divlic[4].c_str(),NULL,10);
	
	long month= date%100;
	date=date/100;
	char buff[1024]="";
		sprintf_s(buff,"%d/%d (START DATE)", date,month);
		  hItem1[2]=m_tree.InsertItem(buff,4,4,hItem1[1]);

		  hItem1[2]=m_tree.InsertItem((divlic[5]+" (START YEAR)").c_str(),4,4,hItem1[1]);

		  date= strtol(divlic[6].c_str(),NULL,10);
		  month= date%100;
		  date=date/100;
		  if (date==0 || month==0)
		  {
		  hItem1[2]=m_tree.InsertItem("NO EXPIRATION",4,4,hItem1[1]);
	//	  hItem1[2]=m_tree.InsertItem(divlic[7].c_str(),1,1,hItem1[1]);
		  }

		  else
		  {
			  char buff1[1024]="";
		sprintf_s(buff1,"%d/%d (END DATE)", date,month);

			  hItem1[2]=m_tree.InsertItem(buff1,4,4,hItem1[1]);
	 hItem1[2]=m_tree.InsertItem((divlic[7]+" (END YEAR)").c_str(),4,4,hItem1[1]);
		  }
		  date= strtol(divlic[9].c_str(),NULL,10);
		  if (date ==3)
		  {
			  if (USBlock==2)
			  {
				  hItem1[2]=m_tree.InsertItem("USB + HARD DISK + MAC ID",5,5,hItem1[1]);
			  }
			  else
			  {
		  hItem1[2]=m_tree.InsertItem("HARD DISK + MAC ID",5,5,hItem1[1]);
				}
		  }
		  else if (date ==1)
		  {

			  hItem1[2]=m_tree.InsertItem("BASTION USB KEY",5,5,hItem1[1]);
		  }
		  network= network.substr(8);
		  network= network.substr(0, network.find("--"));
		   hItem1[2]=m_tree.InsertItem((network +" (USERS)").c_str(),3,3,hItem1[1]);
		  m_tree.Expand(hItem1[0],TVE_EXPAND);

		  ///////////////////////////////////////////////////////////////////////////////
		  std::string recvalue= divlic[1];
		   std::string futuretemp1= divlic[1];
		   std::string futuretemp2 = futuretemp;
			   futuretemp1.append("--");
			   futuretemp1.append(divlic[2]);
			futuretemp1.append("--");
			futuretemp1.append(divlic[3]);
			futuretemp1.append("%%");
			int tempnet=0;
			while (futuretemp2!="")
			{
			if(futuretemp2.find(futuretemp1)!=std::string::npos)
			{
				futuretemp2= futuretemp2.substr(futuretemp2.find(futuretemp1)+futuretemp1.length());
				tempnet = tempnet + strtol (futuretemp2.substr(0,futuretemp2.find("%%")).c_str(),NULL,10);
			}
			else
			{
				futuretemp2="";
			}
			}
		   futuretemp.append(divlic[1]);
		    futuretemp.append("--");
			futuretemp.append(divlic[2]);
			futuretemp.append("--");
			futuretemp.append(divlic[3]);
			futuretemp.append("%%");
			futuretemp.append(network);
			   futuretemp.append("%%");
			  
			
		  recvalue.append("--");
		  recvalue.append(divlic[2]);
		  recvalue.append("--");
		  recvalue.append(divlic[3]);
		   recvalue.append("---vnbroadcast---");
		  			int currentnet =0;
					int maxnet =strtol(network.c_str(),NULL,10);
			std::string tempdata="";
			std::ifstream ifile;
	ifile.open("C:/Users/Public/Application Data/Bastion/AG_Server.temp",ios::in|ios::binary|ios::ate);

			  char *memblock;

  if (ifile.is_open())
  {
   int size;
size = (int) ifile.tellg(); 


 memblock = new char[size];
    ifile.seekg (0, ios::beg);

	int size1=size;
	//file.read (&memblock[debug], size);
	//debug=strlen(&memblock[debug])+1;
	//lockcode.append(&memblock[0]);
	

	size_t debug=0;

	tempdata.resize(size);
	for (int read=0;read<size;read++)
	{
		ifile.read(&memblock[read],1);
	
		tempdata[read]=memblock[read];
	}
	tempdata = tempdata.substr(0,size);
	
	delete[] memblock;
	 ifile.close();
  }

  while (tempdata!="")
  {
  if(tempdata.find(recvalue.substr(0, recvalue.find("---vnbroadcast---")))!=std::string::npos)
  {
	  tempdata = tempdata.substr(tempdata.find("---vnbroadcast---")+17);
		currentnet= currentnet +1;
  }
  else
		tempdata="";
	
  } 

  if(maxnet<currentnet)
  {
	  currentnet= currentnet-tempnet;
	   if(maxnet<currentnet)
		  {
	  sprintf_s(buff,"%d (IN USE)", maxnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else if (currentnet>0)
			  {
	  sprintf_s(buff,"%d (IN USE)", currentnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else
  {
	  sprintf_s(buff,"%d (IN USE)", 0);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
  }
  }
  else
  {
	 	  currentnet= currentnet-tempnet;
	   if(maxnet<currentnet)
		  {
	  sprintf_s(buff,"%d (IN USE)", maxnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else if (currentnet>0)
			  {
	  sprintf_s(buff,"%d (IN USE)", currentnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else
  {
	  sprintf_s(buff,"%d (IN USE)", 0);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
  }
  }
		  //////////////////////////////////////////////////////////////////////////////
	}
	else
	{

		  hItem1[1]=m_tree.InsertItem((divlic[2]+" (APPLICATION ID)").c_str(),1,1,Htemp);
		  	  hItem1[2]=m_tree.InsertItem((divlic[3]+" (VERSION)").c_str(),2,2,hItem1[1]);
		  hItem1[2]=m_tree.InsertItem((divlic[1]+" (VENDOR ID)").c_str(),3,3,hItem1[1]);
	long date= strtol(divlic[4].c_str(),NULL,10);
	
	long month= date%100;
	date=date/100;
	char buff[1024]="";
		sprintf_s(buff,"%d/%d (START DATE)", date,month);
		  hItem1[2]=m_tree.InsertItem(buff,4,4,hItem1[1]);

		  hItem1[2]=m_tree.InsertItem((divlic[5]+" (START YEAR)").c_str(),4,4,hItem1[1]);

		  date= strtol(divlic[6].c_str(),NULL,10);
		  month= date%100;
		  date=date/100;
		  if (date==0 || month==0)
		  {
		  hItem1[2]=m_tree.InsertItem("NO EXPIRATION",4,4,hItem1[1]);
	//	  hItem1[2]=m_tree.InsertItem(divlic[7].c_str(),1,1,hItem1[1]);
		  }

		  else
		  {
			  char buff1[1024]="";
		sprintf_s(buff1,"%d/%d (END DATE)", date,month);

			  hItem1[2]=m_tree.InsertItem(buff1,4,4,hItem1[1]);
	 hItem1[2]=m_tree.InsertItem((divlic[7]+" (END YEAR)").c_str(),4,4,hItem1[1]);
		  }
		  date= strtol(divlic[9].c_str(),NULL,10);
		  if (date ==3)
		  {
			  if (USBlock==2)
			  {
				  hItem1[2]=m_tree.InsertItem("USB + HARD DISK + MAC ID",5,5,hItem1[1]);
			  }
			  else
			  {
		  hItem1[2]=m_tree.InsertItem("HARD DISK + MAC ID",5,5,hItem1[1]);
				}
		  }
		  else if (date ==1)
		  {

			  hItem1[2]=m_tree.InsertItem("BASTION USB KEY",5,5,hItem1[1]);
		  }
		  
		  m_tree.Expand(hItem1[0],TVE_EXPAND);
		  }
				}

			lockcode = lockcode.substr(found+3);
			}
			m_tree.Expand(Htemp,TVE_EXPAND);
			
		   delete[] memblock;
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_decrypt_licenseDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_decrypt_licenseDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
void CAG_decrypt_licenseDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
	CDialogEx::SetBackgroundColor(color);
}




void CAG_decrypt_licenseDlg::OnActionsRefresh()
{
	m_tree.DeleteAllItems();
	// TODO: Add your command handler code here
	HTREEITEM	hItem1[4];
	// TODO: Add extra initialization here
	std::string Handle="";
	Handle.resize(1024);
	std::string AppId="testing";
	std::string servername="";
	servername.resize(100);
	 int returnval =  AG_IsAuthentic (&Handle,&AppId, 0, 1, &servername);
	 if(returnval==0 && servername!="")
		 AG_Release(&Handle, &servername);
		std::string lockcode = "";
		std::string file="";
//	filedlg fdlg(true,"upd","update",OFN_HIDEREADONLY |OF_PROMPT ,"*.upd",NULL);
	
//	fdlg.DoModal();
	//file =  "update.upd";//fdlg.GetFileName();

		_mkdir("C:/Users");
		_mkdir("C:/Users/Public");
		_mkdir("C:/Users/Public/Application Data");
		_mkdir("C:/Users/Public/Application Data/Bastion");
		SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
	//file.append();

	

	std::ifstream ifile;
	ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",ios::in|ios::binary|ios::ate);
	
	if(ifile==NULL)
		{
		
			return ;
	}

			  char *memblock;

  if (ifile.is_open())
  {
   int size;
size = (int) ifile.tellg(); 


 memblock = new char[size];
    ifile.seekg (0, ios::beg);

	int size1=size;
	//file.read (&memblock[debug], size);
	//debug=strlen(&memblock[debug])+1;
	//lockcode.append(&memblock[0]);
	

	size_t debug=0;

	lockcode.resize(size);
	for (int read=0;read<size;read++)
	{
		ifile.read(&memblock[read],1);
	
		lockcode[read]=memblock[read];
	}
	lockcode = lockcode.substr(0,size);
	
   
	 ifile.close();
  }
  else
  {

		 
			  MessageBox("FILE NOT LOADED","ERROR",MB_ICONERROR);
			  return ;
		 
  }
	
  if(lockcode=="")
		  {
			  MessageBox("FILE NOT LOADED","ERROR",MB_ICONERROR);
			  return ;
		  }
  	m_image.Create(16, 16, ILC_MASK, 0, 1);
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON7));
	m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON8));
	m_tree.SetImageList(&m_image,TVSIL_NORMAL);
//	m_tree.SetBkColor(RGB(170,170,0));
		  std::string dec="";
		  std::string lockcode1="";
	//	  MessageBox("INSIDE FUNC","",0);
		   size_t found;
		  size_t found1;
		  size_t found2;
		   int USBlock=0;
		   int networkflag=0;
		   std::string network ="";
		 std::string USBID = "";
		  	hItem1[0]=m_tree.InsertItem("STANDALONE",0,0);
			HTREEITEM Htemp=hItem1[0];
			hItem1[0]=m_tree.InsertItem("NETWORK",0,0);
			std::string futuretemp ="";
		  while (lockcode!="")
		  {

			  		 
	
			  found = lockcode.find("%%%");
		  	if (found!=std::string::npos)
	{
			   lockcode1=lockcode.substr(0,found);

			   found1 = lockcode1.find("---v2---");
	if (found1!=std::string::npos)
	{
		lockcode1=lockcode1.substr(0,found1);
		USBlock=1;
	}
	else
	{
		USBlock=0;
	}

	if (USBlock==0)
	{

			    std::string runtime = getHardDriveComputerID ();
		  dec = aes_decrypt(lockcode1, STRING_THREE_DEFINE_NAME + runtime);
		   found1 = dec.find("---v3---");
			if (found1!=std::string::npos)
			{
				
				USBID = dec.substr(found1+8);
		dec=dec.substr(0,found1);
		found2 = USBID.find("---vn---");
			if (found2!=std::string::npos)
			{
				
				network = USBID.substr(found2);
				networkflag=1;
		USBID=USBID.substr(0,found2);
		USBlock=2;
			}
			else
			{
				networkflag=0;
			}
		USBlock=2;
			}
			  /////network//////
		   found2 = dec.find("---vn---");
			if (found2!=std::string::npos)
			{
				
				network = dec.substr(found2);
				networkflag=1;
		dec=dec.substr(0,found2);
			}
			else
			{
				networkflag=0;
			}
	}
	else
	{
dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
found2 = dec.find("---vn---");
			if (found2!=std::string::npos)
			{
				
				network = dec.substr(found2);
				networkflag=1;
		dec=dec.substr(0,found2);
			}
			else
			{
				networkflag=0;
			}
	}
		  WORD upw=0;
		 WORD upw1=0;
		 std::string divlic[10]={""};
		 divlic[0]=dec;
	std::string lic= LICENSE(&divlic[0]);
	dec=lic;
	std::string check =divlic[0];
	dec.append(check);
	if(networkflag==1)
	{
		//MessageBox(network.c_str(),"NETWORK",0);
		
		hItem1[1]=m_tree.InsertItem((divlic[2]+" (APPLICATION ID)").c_str(),1,1,hItem1[0]);
		  	  hItem1[2]=m_tree.InsertItem((divlic[3]+" (VERSION)").c_str(),2,2,hItem1[1]);
		  hItem1[2]=m_tree.InsertItem((divlic[1]+" (VENDOR ID)").c_str(),3,3,hItem1[1]);
	long date= strtol(divlic[4].c_str(),NULL,10);
	
	long month= date%100;
	date=date/100;
	char buff[1024]="";
		sprintf_s(buff,"%d/%d (START DATE)", date,month);
		  hItem1[2]=m_tree.InsertItem(buff,4,4,hItem1[1]);

		  hItem1[2]=m_tree.InsertItem((divlic[5]+" (START YEAR)").c_str(),4,4,hItem1[1]);

		  date= strtol(divlic[6].c_str(),NULL,10);
		  month= date%100;
		  date=date/100;
		  if (date==0 || month==0)
		  {
		  hItem1[2]=m_tree.InsertItem("NO EXPIRATION",4,4,hItem1[1]);
	//	  hItem1[2]=m_tree.InsertItem(divlic[7].c_str(),1,1,hItem1[1]);
		  }

		  else
		  {
			  char buff1[1024]="";
		sprintf_s(buff1,"%d/%d (END DATE)", date,month);

			  hItem1[2]=m_tree.InsertItem(buff1,4,4,hItem1[1]);
	 hItem1[2]=m_tree.InsertItem((divlic[7]+" (END YEAR)").c_str(),4,4,hItem1[1]);
		  }
		  date= strtol(divlic[9].c_str(),NULL,10);
		  if (date ==3)
		  {
			  if (USBlock==2)
			  {
				  hItem1[2]=m_tree.InsertItem("USB + HARD DISK + MAC ID",5,5,hItem1[1]);
			  }
			  else
			  {
		  hItem1[2]=m_tree.InsertItem("HARD DISK + MAC ID",5,5,hItem1[1]);
				}
		  }
		  else if (date ==1)
		  {

			  hItem1[2]=m_tree.InsertItem("BASTION USB KEY",5,5,hItem1[1]);
		  }
		  network= network.substr(8);
		  network= network.substr(0, network.find("--"));
		   hItem1[2]=m_tree.InsertItem((network +" (USERS)").c_str(),3,3,hItem1[1]);
		  m_tree.Expand(hItem1[0],TVE_EXPAND);

		  ///////////////////////////////////////////////////////////////////////////////
		  std::string recvalue= divlic[1];
		   std::string futuretemp1= divlic[1];
		   std::string futuretemp2 = futuretemp;
			   futuretemp1.append("--");
			   futuretemp1.append(divlic[2]);
			futuretemp1.append("--");
			futuretemp1.append(divlic[3]);
			futuretemp1.append("%%");
			int tempnet=0;
			while (futuretemp2!="")
			{
			if(futuretemp2.find(futuretemp1)!=std::string::npos)
			{
				futuretemp2= futuretemp2.substr(futuretemp2.find(futuretemp1)+futuretemp1.length());
				tempnet = tempnet + strtol (futuretemp2.substr(0,futuretemp2.find("%%")).c_str(),NULL,10);
			}
			else
			{
				futuretemp2="";
			}
			}
		   futuretemp.append(divlic[1]);
		    futuretemp.append("--");
			futuretemp.append(divlic[2]);
			futuretemp.append("--");
			futuretemp.append(divlic[3]);
			futuretemp.append("%%");
			futuretemp.append(network);
			   futuretemp.append("%%");
			  
			
		  recvalue.append("--");
		  recvalue.append(divlic[2]);
		  recvalue.append("--");
		  recvalue.append(divlic[3]);
		   recvalue.append("---vnbroadcast---");
		  			int currentnet =0;
					int maxnet =strtol(network.c_str(),NULL,10);
			std::string tempdata="";
			std::ifstream ifile;
	ifile.open("C:/Users/Public/Application Data/Bastion/AG_Server.temp",ios::in|ios::binary|ios::ate);

			  char *memblock;

  if (ifile.is_open())
  {
   int size;
size = (int) ifile.tellg(); 


 memblock = new char[size];
    ifile.seekg (0, ios::beg);

	int size1=size;
	//file.read (&memblock[debug], size);
	//debug=strlen(&memblock[debug])+1;
	//lockcode.append(&memblock[0]);
	

	size_t debug=0;

	tempdata.resize(size);
	for (int read=0;read<size;read++)
	{
		ifile.read(&memblock[read],1);
	
		tempdata[read]=memblock[read];
	}
	tempdata = tempdata.substr(0,size);
	
	delete[] memblock;
	 ifile.close();
  }

  while (tempdata!="")
  {
  if(tempdata.find(recvalue.substr(0, recvalue.find("---vnbroadcast---")))!=std::string::npos)
  {
	  tempdata = tempdata.substr(tempdata.find("---vnbroadcast---")+17);
		currentnet= currentnet +1;
  }
  else
		tempdata="";
	
  } 

  if(maxnet<currentnet)
  {
	  currentnet= currentnet-tempnet;
	   if(maxnet<currentnet)
		  {
	  sprintf_s(buff,"%d (IN USE)", maxnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else if (currentnet>0)
			  {
	  sprintf_s(buff,"%d (IN USE)", currentnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else
  {
	  sprintf_s(buff,"%d (IN USE)", 0);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
  }
  }
  else
  {
	 	  currentnet= currentnet-tempnet;
	   if(maxnet<currentnet)
		  {
	  sprintf_s(buff,"%d (IN USE)", maxnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else if (currentnet>0)
			  {
	  sprintf_s(buff,"%d (IN USE)", currentnet);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
			  }
  else
  {
	  sprintf_s(buff,"%d (IN USE)", 0);
	    hItem1[2]=m_tree.InsertItem((buff),3,3,hItem1[1]);
  }
  }
		  //////////////////////////////////////////////////////////////////////////////
	}
	else
	{

		  hItem1[1]=m_tree.InsertItem((divlic[2]+" (APPLICATION ID)").c_str(),1,1,Htemp);
		  	  hItem1[2]=m_tree.InsertItem((divlic[3]+" (VERSION)").c_str(),2,2,hItem1[1]);
		  hItem1[2]=m_tree.InsertItem((divlic[1]+" (VENDOR ID)").c_str(),3,3,hItem1[1]);
	long date= strtol(divlic[4].c_str(),NULL,10);
	
	long month= date%100;
	date=date/100;
	char buff[1024]="";
		sprintf_s(buff,"%d/%d (START DATE)", date,month);
		  hItem1[2]=m_tree.InsertItem(buff,4,4,hItem1[1]);

		  hItem1[2]=m_tree.InsertItem((divlic[5]+" (START YEAR)").c_str(),4,4,hItem1[1]);

		  date= strtol(divlic[6].c_str(),NULL,10);
		  month= date%100;
		  date=date/100;
		  if (date==0 || month==0)
		  {
		  hItem1[2]=m_tree.InsertItem("NO EXPIRATION",4,4,hItem1[1]);
	//	  hItem1[2]=m_tree.InsertItem(divlic[7].c_str(),1,1,hItem1[1]);
		  }

		  else
		  {
			  char buff1[1024]="";
		sprintf_s(buff1,"%d/%d (END DATE)", date,month);

			  hItem1[2]=m_tree.InsertItem(buff1,4,4,hItem1[1]);
	 hItem1[2]=m_tree.InsertItem((divlic[7]+" (END YEAR)").c_str(),4,4,hItem1[1]);
		  }
		  date= strtol(divlic[9].c_str(),NULL,10);
		  if (date ==3)
		  {
			  if (USBlock==2)
			  {
				  hItem1[2]=m_tree.InsertItem("USB + HARD DISK + MAC ID",5,5,hItem1[1]);
			  }
			  else
			  {
		  hItem1[2]=m_tree.InsertItem("HARD DISK + MAC ID",5,5,hItem1[1]);
				}
		  }
		  else if (date ==1)
		  {

			  hItem1[2]=m_tree.InsertItem("BASTION USB KEY",5,5,hItem1[1]);
		  }
		  
		  m_tree.Expand(hItem1[0],TVE_EXPAND);
		  }
				}

			lockcode = lockcode.substr(found+3);
			}
			m_tree.Expand(Htemp,TVE_EXPAND);
			
		   delete[] memblock;
	return ;  // return TRUE  unless you set the focus to a control
}
