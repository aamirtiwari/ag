#pragma once


// filedlg

class filedlg : public CFileDialog
{
	DECLARE_DYNAMIC(filedlg)

public:
	filedlg(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);
	virtual ~filedlg();

protected:
	DECLARE_MESSAGE_MAP()
};


