
// AG_ENCRYPTDlg.h : header file
//

#pragma once


// CAG_ENCRYPTDlg dialog
class CAG_ENCRYPTDlg : public CDialogEx
{
// Construction
public:
	CAG_ENCRYPTDlg(CWnd* pParent = NULL);	// standard constructor
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg void CAG_ENCRYPTDlg::ProcessEvents();
	 int alldatahide(const char * lpFileName1, const char * lpFileName);

// Dialog Data
	enum { IDD = IDD_AG_ENCRYPT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton3();
	CEdit m_edit1;
	CEdit m_edit2;
	afx_msg void OnBnClickedButton4();
	CProgressCtrl m_progress;
	afx_msg void OnBnClickedButton5();
	CEdit m_edit3;
	CEdit m_edit4;
	CEdit m_edit5;
};
