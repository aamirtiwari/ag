
#include "openssl\aes.h" 
#include <iostream> 
#include <stdlib.h> 
#include <time.h>  
#include <string> 
#include <string.h> 
#include <fstream>

unsigned char* aes_decrypt_code(unsigned char* inold, std::string key, int size);
unsigned char* aes_encrypt_code(unsigned char* in, std::string key, int size);

unsigned char* aes_decrypt_code1(unsigned char* inold, std::string key, int size);
unsigned char* aes_encrypt_code1(unsigned char* in, std::string key, int size,unsigned char* out);
unsigned char* aes_encrypt_codelow(unsigned char* in, std::string key, int size,unsigned char* out);
unsigned char* aes_decrypt_code1ow(unsigned char* in, std::string key, int size,unsigned char* out);
std::string aes_encrypt(std::string in, std::string key);

std::string aes_decrypt(std::string in, std::string key);
