
// AG_ENCRYPTDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_ENCRYPT.h"
#include "AG_ENCRYPTDlg.h"
#include "afxdialogex.h"
#include "filedlg.h"
#include "Encrypt_decrypt.h"
#include "Stringobf.h"
#include <direct.h>
#include "resource.h"
#include <Windows.h>

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <string>
#include <conio.h>
#include "resource.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

std::string filename="";
std::string folder ="";
std::string filenameupdate="";

// CAboutDlg dialog used for App About
void sleep(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}
class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);

}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAG_ENCRYPTDlg dialog




CAG_ENCRYPTDlg::CAG_ENCRYPTDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAG_ENCRYPTDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAG_ENCRYPTDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_PROGRESS1, m_progress);
    DDX_Control(pDX, IDC_EDIT2, m_edit3);
    DDX_Control(pDX, IDC_EDIT3, m_edit4);
    DDX_Control(pDX, IDC_EDIT4, m_edit5);
}

BEGIN_MESSAGE_MAP(CAG_ENCRYPTDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON3, &CAG_ENCRYPTDlg::OnBnClickedButton3)
    ON_BN_CLICKED(IDC_BUTTON4, &CAG_ENCRYPTDlg::OnBnClickedButton4)
    ON_BN_CLICKED(IDC_BUTTON5, &CAG_ENCRYPTDlg::OnBnClickedButton5)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

void CAG_ENCRYPTDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

void CAG_ENCRYPTDlg::ProcessEvents()
{
    MSG stmsg;
    while(::PeekMessageA(&stmsg,NULL,0,0,PM_REMOVE))
    {
        ::TranslateMessage(&stmsg);
        ::DispatchMessageA(&stmsg);
    }
}

// CAG_ENCRYPTDlg message handlers
HBRUSH CAG_ENCRYPTDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL CAG_ENCRYPTDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));

    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
    if ( 	__argc >1 )
    {
        std::string projectname= __argv[1];
        m_edit4.SetWindowTextA(projectname.substr(0,projectname.find("%%%")).c_str());
        projectname = projectname.substr(projectname.find("%%%")+3);
        m_edit5.SetWindowTextA(projectname.c_str());
    }
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_ENCRYPTDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_ENCRYPTDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_ENCRYPTDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

int CAG_ENCRYPTDlg::alldatahide(const char * lpFileName1, const char * lpFileName)
{
    char buffkey[1024];
    int cTxtLen = m_edit4.GetWindowTextLength();
    m_edit4.GetWindowTextA(&buffkey[0],cTxtLen+1);
    std::string enckey1 =buffkey;
    std::string encnamefile=lpFileName1;
    if(encnamefile.find(".cfg")!=std::string::npos)
        return 1;
    cTxtLen = m_edit5.GetWindowTextLength();
    m_edit5.GetWindowTextA(&buffkey[0],cTxtLen+1);
    std::string datafiles6 =buffkey;
    HANDLE hOriginalFile = CreateFile(lpFileName1, GENERIC_READ, FILE_SHARE_READ| FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    std::string datafilestemp="";

    datafiles6.append(";");
    int cdata=0;
    while(datafiles6!="")
    {
        size_t  founddata = datafiles6.find(";");
        if(founddata!=std::string::npos )
        {
            datafilestemp= datafiles6.substr(0,founddata);
            if (encnamefile.find(".lock")==std::string::npos && encnamefile.find(datafilestemp.c_str())!=std::string::npos && datafilestemp !="" )
            {
                if(encnamefile.substr(encnamefile.find(datafilestemp.c_str()) + datafilestemp.length())=="")
                {
                    cdata=1;
                    datafiles6  ="";
                }
                else
                {
                    datafiles6 =datafiles6.substr(founddata+1);
                }
            }
            else
            {
                datafiles6 =datafiles6.substr(founddata+1);
            }
        }
        else
        {
            datafiles6="";
        }

    }
    if (cdata!=1 )
    {
        return 1;
    }
    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE || dwFileSize ==0)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }
    DWORD dwNumberOfBytesRead = 0;
    std::string updatename= lpFileName;

    HANDLE hUpdatedFile = CreateFile(updatename.c_str(), GENERIC_WRITE,  FILE_SHARE_READ| FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);

    LPBYTE lpBuffer = (LPBYTE)malloc(64000000);




    size_t textLength = dwFileSize;

    unsigned char* lpBuffer1 = (LPBYTE)malloc(64000000);
    std::string key=STRING_SIX_DEFINE_NAME;

    m_progress.SetRange(0,100);
    m_progress.SetPos(0);
    ProcessEvents();
    if(textLength<=64000000)
    {
        m_progress.SetPos((50*100)/textLength);
        ProcessEvents();





        for(int j=0; j<64000000; j++) {
            lpBuffer[j]=' ';
        }
        SetFilePointer(hOriginalFile,0,NULL,FILE_BEGIN);
        ReadFile(hOriginalFile, lpBuffer, textLength, &dwNumberOfBytesRead, NULL);



        lpBuffer1=aes_encrypt_codelow(lpBuffer,enckey1+ key, textLength,lpBuffer1);


        if (hUpdatedFile == INVALID_HANDLE_VALUE)
        {
            free(lpBuffer);
            free(lpBuffer1);

            CloseHandle(hOriginalFile);
            return false;
        }

        // Write the original headers
        DWORD dwNumberOfBytesWritten;
        SetFilePointer(hUpdatedFile,0,NULL,FILE_BEGIN);
        WriteFile(hUpdatedFile, lpBuffer1, (textLength), &dwNumberOfBytesWritten, NULL);
    }
    else
    {

        ProcessEvents();





        for(int j=0; j<64000000; j++) {
            lpBuffer[j]=' ';
        }
        SetFilePointer(hOriginalFile,0,NULL,FILE_BEGIN);
        ReadFile(hOriginalFile, lpBuffer,64000000, &dwNumberOfBytesRead, NULL);



        lpBuffer1=aes_encrypt_codelow(lpBuffer,enckey1+ key,64000000 ,lpBuffer1);


        if (hUpdatedFile == INVALID_HANDLE_VALUE)
        {
            free(lpBuffer);
            free(lpBuffer1);

            CloseHandle(hOriginalFile);
            return false;
        }

        // Write the original headers
        DWORD dwNumberOfBytesWritten;
        SetFilePointer(hUpdatedFile,0,NULL,FILE_BEGIN);
        WriteFile(hUpdatedFile, lpBuffer1, (64000000), &dwNumberOfBytesWritten, NULL);
        for(long int i=64000000; i<textLength; i=i+64000000)
        {
            size_t progressval=((i/1000)/(textLength/100000));
            m_progress.SetPos((int)progressval);
            ProcessEvents();


            SetFilePointer(hOriginalFile,i,NULL,FILE_BEGIN);
            if(textLength-i<=64000000)
            {
                ReadFile(hOriginalFile, lpBuffer, textLength-i, &dwNumberOfBytesRead, NULL);


                lpBuffer1=aes_encrypt_codelow(lpBuffer,enckey1+ key, textLength-i,lpBuffer1);


                if (hUpdatedFile == INVALID_HANDLE_VALUE)
                {
                    free(lpBuffer);
                    free(lpBuffer1);

                    CloseHandle(hOriginalFile);
                    return false;
                }

                // Write the original headers
                DWORD dwNumberOfBytesWritten;
                SetFilePointer(hUpdatedFile,i,NULL,FILE_BEGIN);
                WriteFile(hUpdatedFile, lpBuffer1, (textLength-i), &dwNumberOfBytesWritten, NULL);
            }
            else
            {
                ReadFile(hOriginalFile, lpBuffer, 64000000, &dwNumberOfBytesRead, NULL);


                lpBuffer1=aes_encrypt_codelow(lpBuffer,enckey1+ key, 64000000,lpBuffer1);


                if (hUpdatedFile == INVALID_HANDLE_VALUE)
                {
                    free(lpBuffer);
                    free(lpBuffer1);

                    CloseHandle(hOriginalFile);
                    return false;
                }

                // Write the original headers
                DWORD dwNumberOfBytesWritten;
                SetFilePointer(hUpdatedFile,i,NULL,FILE_BEGIN);
                WriteFile(hUpdatedFile, lpBuffer1, (64000000), &dwNumberOfBytesWritten, NULL);
            }
        }
    }
    CloseHandle(hOriginalFile);
    CloseHandle(hUpdatedFile);

    free(lpBuffer);
    free(lpBuffer1);
    m_progress.SetPos(100);
    ProcessEvents();
    return 0;

}

void CAG_ENCRYPTDlg::OnBnClickedButton3()
{

    BROWSEINFO bi = { 0 };
    TCHAR path[MAX_PATH];
    bi.lpszTitle = _T("Select a folder");
    LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );
    if ( pidl != 0 )
    {
        // get the name of the folder

        if ( SHGetPathFromIDList ( pidl, path ) )
        {
            _tprintf ( _T("Selected Folder: %s\n"), path );
        }

        // free memory used
        IMalloc * imalloc = 0;
        if ( SUCCEEDED( SHGetMalloc ( &imalloc )) )
        {
            imalloc->Free ( pidl );
            imalloc->Release ( );
        }
    }
    // TODO: Add your control notification handler code here
    filename="";
    filename.append(path);
    if(filename!="" && filename.find("\\")!=std::string::npos)
        filename.append(";");
    else
        filename="";
    m_edit1.SetWindowTextA(filename.c_str());
    /*	fdlg.DoModal();

    	if(filename=="")
    	{
    	filename = fdlg.GetPathName();
    	filenameupdate=fdlg.GetFileName();
    	}
    	else
    	{
    	filenametemp= fdlg.GetPathName();
    	filenameupdatetemp=fdlg.GetFileName();;
    	if(filenametemp!="")
    	{
    	filename.append(";");
    	filename.append(filenametemp);
    	filenameupdate.append(";");
    	filenameupdate.append(filenameupdatetemp);
    	}
    	}
    	folder = fdlg.GetPathName();
    	size_t found;
    	found = folder.find_last_of("\\");
    if (found!=std::string::npos)
    {

    folder = folder.substr(0,found+1);
    }
    	folder = folder.append("AG_protected");
    	_mkdir(folder.c_str());
    	m_edit1.SetWindowTextA(filename.c_str()); */

}

struct SearchFile
{
    typedef std::vector<std::string> FileNameArray;
    FileNameArray files;

    FileNameArray::iterator begin()
    {
        return files.begin();
    }

    FileNameArray::iterator end()
    {
        return files.end();
    }

    int count() const
    {
        return (int)files.size();
    }

    std::string operator[](int index)
    {
        return files[index];
    }

    void operator()(const std::string &path, const std::string &pattern)
    {
        WIN32_FIND_DATA wfd;
        HANDLE hf;
        std::string findwhat;
        std::vector<std::string> dir;

        findwhat = path + "\\*";  // directory

        hf = FindFirstFile(findwhat.c_str(), &wfd);
        while (hf != INVALID_HANDLE_VALUE)
        {
            if (wfd.cFileName[0] != '.' && (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                std::string found;

                found = path + "\\" + wfd.cFileName;
                dir.push_back(found);
            }

            if (!FindNextFile(hf, &wfd))
            {
                FindClose(hf);
                hf = INVALID_HANDLE_VALUE;
            }
        }

        findwhat = path + "\\" + pattern;  // files

        hf = FindFirstFile(findwhat.c_str(), &wfd);
        while (hf != INVALID_HANDLE_VALUE)
        {
            if (wfd.cFileName[0] != '.' && !(wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                std::string found;

                found = path + "\\" + wfd.cFileName;
                files.push_back(found);
            }

            if (!FindNextFile(hf, &wfd))
            {
                FindClose(hf);
                hf = INVALID_HANDLE_VALUE;
            }
        }

        // continue with directories
        for (std::vector<std::string>::iterator it = dir.begin(); it != dir.end(); ++it)
            this->operator()(*it, pattern);
    }
};

void CAG_ENCRYPTDlg::OnBnClickedButton4()
{
    // TODO: Add your control notification handler code here
    SearchFile sf;

    // get all .jpg files in current dir


    std::string filename1 = filename;
    std::string filename2 =filenameupdate;
    if(filename2=="")
    {
        MessageBox("PLEASE SELECT DESTINATION FOLDER", "ERROR",MB_ICONERROR);
        return;
    }
    if(filename1 == "")
    {
        MessageBox("NOTHING TO PROTECT", "ERROR",MB_ICONERROR);
        return;
    }


    while (filename1 != "")
    {
        size_t found;
        found = filename1.find(";");

        if(found!=std::string::npos )
        {
            std::string filenametemp= filename1.substr(0,found);

            filename1=filename1.substr(found+1);

            std::string updatefile ="";
            updatefile =filename2;
            updatefile.append("\\");
            _mkdir(updatefile.c_str());
            sf(filenametemp.c_str(), "*.*");

            for (int i = 0; i != sf.count(); ++i)
            {
                std::string tempupdate="";
                size_t found1 = sf[i].find(":\\");
                if (found1!=std::string::npos)
                {
                    tempupdate=sf[i].substr(found1+2);
                }
                printf("%s\n", sf[i].c_str());
                updatefile ="";
                updatefile =filename2;

                updatefile.append("\\");

                updatefile.append( tempupdate.c_str());
                std::string mkdir="mkdir -p \"";

                mkdir.append(updatefile.c_str());

                found1 = mkdir.find_last_of("\\");
                if (found1!=std::string::npos)
                {
                    mkdir=mkdir.substr(0,found1+1);
                }
                mkdir.append("\"");
                system(mkdir.c_str());

                alldatahide( sf[i].c_str(), updatefile.c_str());
            }

        }
        else
        {
            filename1="";
        }
    }
    MessageBox("ALL FILES PROTECTED","DONE",MB_ICONINFORMATION);

    exit(0);
    return;
}


void CAG_ENCRYPTDlg::OnBnClickedButton5()
{
    // TODO: Add your control notification handler code here
    BROWSEINFO bi = { 0 };
    TCHAR path[MAX_PATH];
    bi.lpszTitle = _T("Select a folder");
    LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );
    if ( pidl != 0 )
    {
        // get the name of the folder

        if ( SHGetPathFromIDList ( pidl, path ) )
        {
            _tprintf ( _T("Selected Folder: %s\n"), path );
        }

        // free memory used
        IMalloc * imalloc = 0;
        if ( SUCCEEDED( SHGetMalloc ( &imalloc )) )
        {
            imalloc->Free ( pidl );
            imalloc->Release ( );
        }
    }
    // TODO: Add your control notification handler code here
    filenameupdate= path;
    if(filenameupdate.find("\\")==std::string::npos)
        filenameupdate="";
    m_edit3.SetWindowTextA(filenameupdate.c_str());
}
