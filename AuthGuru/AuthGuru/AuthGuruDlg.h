
// AuthGuruDlg.h : header file
//

#pragma once


// CAuthGuruDlg dialog
class CAuthGuruDlg : public CDialogEx
{
// Construction
public:
	CAuthGuruDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_AUTHGURU_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	CEdit m_edit;
	CEdit m_edit1;
	CEdit m_edit3;
};
