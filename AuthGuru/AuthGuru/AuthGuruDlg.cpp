
// AuthGuruDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AuthGuru.h"
#include "AuthGuruDlg.h"
#include "afxdialogex.h"
#include "filedlg.h"
#include <fstream>
#include "DIVIDEINFO.h"
#include "Encrypt_decrypt.h"
#include "Stringobf.h"
#include "READWRITESECTOR.h"
#include "Sense_LC.h"
#include "SmartX1LiteApi.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAuthGuruDlg dialog




CAuthGuruDlg::CAuthGuruDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAuthGuruDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAuthGuruDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit);
    DDX_Control(pDX, IDC_EDIT2, m_edit1);
    DDX_Control(pDX, IDC_EDIT3, m_edit3);
}

BEGIN_MESSAGE_MAP(CAuthGuruDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON1, &CAuthGuruDlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_BUTTON2, &CAuthGuruDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// CAuthGuruDlg message handlers

void CAuthGuruDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

BOOL CAuthGuruDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    SetBackGroundColor(RGB(170,170,0));
    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox("Being debugged!","ERROR",0);
        exit(0);
        return true;
    }
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAuthGuruDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAuthGuruDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAuthGuruDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

using namespace std;
std::string lockcode = "";
std::string filename="";
char buff[1024]="";
char buff1[1024]="";
std::string aes = "";
int cTxtLen ;

void CAuthGuruDlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here

    cTxtLen = m_edit.GetWindowTextLength();
    m_edit.GetWindowTextA(&buff[0],cTxtLen+1);
    if(filename=="")
    {
        MessageBox("SYSINFO FILE NOT SELECTED","ERROR",0);
        return;
    }

    filedlg fdlg(false,"agl","AG_license",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.agl",NULL);

    fdlg.DoModal();
    string filename1= fdlg.GetFileName();


    if(filename1=="")
    {

        return;
    }

    filename1 = fdlg.GetPathName();

    aes.append("--");
    aes.append(buff);

    aes.append("--");
    WORD value1=0;
    int j = strtol(STRING_SIX_DEFINE_NAME,NULL,10)+60;
    ReadSectors(0,j,1,&value1);
    value1=value1+1;
    if (value1=='\0')
    {
        WriteSectors(0,j,1,0);
    }
    else
    {
        if (value1>5000)
        {
            WriteSectors(0,j,1,0);
        }
        else
        {
            WriteSectors(0,j,1,value1);
        }
    }

    char buffc[1024]="";
    sprintf_s(buffc,"%d", value1);
    aes.append(buffc);
    aes.append("--");
    sprintf_s(buffc,"%d", j);
    aes.append(buffc);

    aes.append("%");
    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff1[0],cTxtLen+1);
    aes.append(buff1);
    value1= strtol(buff1,NULL,10);
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff1[0],cTxtLen+1);
    int value2= strtol(buff1,NULL,10);
    int value3= strtol(STRING_SIX_DEFINE_NAME,NULL,10);
    if (buff1!="")
    {
        aes.append("---vn---");
        aes.append(buff1);
    }
    std::string mac[10]= {""};
    std::string runtime = DISKID(aes, &mac[0]);
    aes= aes_encrypt(aes, STRING_FOUR_DEFINE_NAME +runtime);




    std::string dec = "";
    std::string temp="";

    sprintf_s(buff,"%d",value1);
    temp = buff;
    temp=temp.c_str();
    temp=temp.append("---");

    sprintf_s(buff,"%d",value2);
    temp=temp.append(buff);
    temp=temp.c_str();
    temp=temp.append("---");

    sprintf_s(buff,"%d",value3);
    temp=temp.append(buff);
    temp=temp.c_str();
    temp=temp.append("---");

    int checkvalue=0;
    while (checkvalue==0)
    {
        dec= aes_encrypt(temp, STRING_SIX_DEFINE_NAME);
        dec.append("%%%");
        dec=dec.c_str();
        if(dec.find("%%%")!=std::string::npos)
        {
            dec= dec.substr(0, dec.find("%%%"));
            checkvalue=1;
        }
    }

    dec =dec.append("---");
    MessageBox(dec.c_str(),"",0);
    lc_handle_t handle;

    long nRet = 0;


    int res = SmartX1Find((char *) "54687837");
    if(!res) {

        res =SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
        if(!res) {
            // read back the data just writed into block 0
            unsigned char outdata[512];

            res = SmartX1WriteStorage(0, 256, (unsigned char *)dec.c_str());
            MessageBox("KEY UPDATED","DONE",0);
            LC_close(handle);
            EndDialog(0);
            return ;


            SmartX1Close();
        }
    }
    handle;
    res = LC_open(0, 0, &handle);
    if(!res) {

        res = LC_passwd(handle, 0, (unsigned char *) "54687837");
        if(!res) {
            // read back the data just writed into block 0
            unsigned char outdata[512];

            res = LC_write(handle, 0, (unsigned char *)dec.c_str());
            MessageBox("KEY UPDATED","DONE",0);
            LC_close(handle);
            EndDialog(0);
            return ;

        }
        LC_close(handle);
    }
    else
    {
        MessageBox("BASTION LICENSE KEY NOT FOUND","ERROR",MB_ICONERROR);
    }

    std::ofstream ifile;
    ifile.open(filename1,ios::out|ios::binary );
    ifile.seekp (0, ios::beg);
    ifile.write (&aes[0],strlen(aes.c_str()));

    size_t debug=0;
    size_t debug1=0;
    debug=strlen(aes.c_str())+1;
    debug1 =aes.length()+1;
    while (debug<debug1)
    {

        ifile.write("\0",1);
        ifile.write(&aes[debug],strlen(&aes[debug]) );
        debug=strlen(&aes[debug])+debug+1;
    }

    ifile.close();


    MessageBox("UPDATE FILE CREATED","DONE",0);
    EndDialog(0);

}


void CAuthGuruDlg::OnBnClickedButton2()
{
    // TODO: Add your control notification handler code here


    filedlg fdlg(true,"info","purchase.info",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);

    fdlg.DoModal();
    filename= fdlg.GetFileName();
    if(filename=="")
    {
        return;
    }

    filename= fdlg.GetPathName();
    WORD value=0;

    value=(WORD)strtol(buff,NULL,10);

    char *memblock;

    ifstream file (filename, ios::in|ios::binary|ios::ate);

    if (file.is_open())
    {
        int size;
        size = (int) file.tellg();



        memblock = new char[size];
        file.seekg (0, ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            file.read (&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);

        file.close();
    }
    else
    {
        MessageBox("SYSINFO FILE NOT LOADED","ERROR",0);
        return;
    }

    if(lockcode=="")
    {
        MessageBox("SYSINFO FILE NOT LOADED","ERROR",0);
        return;
    }

    aes= aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
    delete[] memblock;
    size_t found;
    WORD upw=0;
    found = aes.find_last_of("--");
    char * disk=new char[1024];
    if (found!=std::string::npos)
    {
        std::string disk = "";
        disk = aes.substr(found+1,aes.length()-1);
        upw = WORD(strtol(disk.c_str(),NULL,10));
        aes = aes.substr(0,found-1);
    }
    sprintf_s(disk,2*sizeof(int),"%d",upw+1);

    m_edit.SetWindowTextA(disk);
    delete[] disk;
}
