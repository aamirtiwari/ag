#pragma once
#include "maindialog.h"

// AuthTokenCtrl.h : Declaration of the CAuthTokenCtrl ActiveX Control class.


// CAuthTokenCtrl : See AuthTokenCtrl.cpp for implementation.

class CAuthTokenCtrl : public COleControl
{
	DECLARE_DYNCREATE(CAuthTokenCtrl)

// Constructor
public:
	CAuthTokenCtrl();

// Overrides
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual DWORD GetControlFlags();

// Implementation
protected:
	~CAuthTokenCtrl();

	DECLARE_OLECREATE_EX(CAuthTokenCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CAuthTokenCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CAuthTokenCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CAuthTokenCtrl)		// Type name and misc status

	// Subclassed control support
	BOOL IsSubclassedControl();
	LRESULT OnOcmCommand(WPARAM wParam, LPARAM lParam);

// Message maps
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	DECLARE_DISPATCH_MAP()

// Event maps
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
		dispidIsAuthentic = 12L,
		dispidyear = 11,
		dispidmonth = 10,
		dispiddate = 9,
		dispidAppId = 8,
		dispidEnd_Date = 7L,
		dispidServerHash = 5,
		dispidClientHash = 4,
		eventidIsAuthenticEvent = 1L,
		dispidIsAuthentic_client = 3L,
		dispidIsAuthentic_server = 6L,
		dispidmessage = 2,
		dispidInputData = 1
	};
public:
	CMainDialog m_MainDialog;
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	void OnInputDataChanged(void);
	CString m_InputData;
	void OnmessageChanged(void);
	CString m_message;
	void IsAuthentic_client(void);

	void FireIsAuthenticEvent(void)
	{
		FireEvent(eventidIsAuthenticEvent, EVENT_PARAM(VTS_NONE));
	}
	void IsAuthentic_server(void);
//	CString m_ClientHash;
	void OnClientHashChanged(void);
	CString m_ClientHash;
	void OnServerHashChanged(void);
	CString m_ServerHash;
//	void End_Date(void);
protected:
	void End_Date(void);
	void OnAppIdChanged(void);
	CString m_AppId;
	void OndateChanged(void);
	CString m_date;
	void OnmonthChanged(void);
	CString m_month;
	void OnyearChanged(void);
	CString m_year;
	void IsAuthentic(void);
};

