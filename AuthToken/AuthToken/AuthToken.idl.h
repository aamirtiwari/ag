

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed May 21 15:56:05 2014
 */
/* Compiler settings for AuthToken.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __MyActiveXidl_h__
#define __MyActiveXidl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DAuthToken_FWD_DEFINED__
#define ___DAuthToken_FWD_DEFINED__
typedef interface _DAuthToken _DAuthToken;
#endif 	/* ___DAuthToken_FWD_DEFINED__ */


#ifndef ___DAuthTokenEvents_FWD_DEFINED__
#define ___DAuthTokenEvents_FWD_DEFINED__
typedef interface _DAuthTokenEvents _DAuthTokenEvents;
#endif 	/* ___DAuthTokenEvents_FWD_DEFINED__ */


#ifndef __AuthToken_FWD_DEFINED__
#define __AuthToken_FWD_DEFINED__

#ifdef __cplusplus
typedef class AuthToken AuthToken;
#else
typedef struct AuthToken AuthToken;
#endif /* __cplusplus */

#endif 	/* __AuthToken_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __AuthTokenLib_LIBRARY_DEFINED__
#define __AuthTokenLib_LIBRARY_DEFINED__

/* library AuthTokenLib */
/* [control][helpstring][helpfile][version][uuid] */ 


EXTERN_C const IID LIBID_AuthTokenLib;

#ifndef ___DAuthToken_DISPINTERFACE_DEFINED__
#define ___DAuthToken_DISPINTERFACE_DEFINED__

/* dispinterface _DAuthToken */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DAuthToken;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("98AF8A06-7DFE-472D-A713-A0C6F22FDC3D")
    _DAuthToken : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DAuthTokenVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DAuthToken * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DAuthToken * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DAuthToken * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DAuthToken * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DAuthToken * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DAuthToken * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DAuthToken * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DAuthTokenVtbl;

    interface _DAuthToken
    {
        CONST_VTBL struct _DAuthTokenVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DAuthToken_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DAuthToken_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DAuthToken_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DAuthToken_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DAuthToken_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DAuthToken_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DAuthToken_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DAuthToken_DISPINTERFACE_DEFINED__ */


#ifndef ___DAuthTokenEvents_DISPINTERFACE_DEFINED__
#define ___DAuthTokenEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DAuthTokenEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DAuthTokenEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("78099343-A8FF-4867-A537-06F31034F22B")
    _DAuthTokenEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DAuthTokenEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DAuthTokenEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DAuthTokenEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DAuthTokenEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DAuthTokenEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DAuthTokenEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DAuthTokenEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DAuthTokenEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DAuthTokenEventsVtbl;

    interface _DAuthTokenEvents
    {
        CONST_VTBL struct _DAuthTokenEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DAuthTokenEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DAuthTokenEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DAuthTokenEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DAuthTokenEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DAuthTokenEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DAuthTokenEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DAuthTokenEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DAuthTokenEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_AuthToken;

#ifdef __cplusplus

class DECLSPEC_UUID("36299202-09EF-4ABF-ADB9-47C599DBE778")
AuthToken;
#endif
#endif /* __AuthTokenLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


