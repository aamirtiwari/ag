//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AuthToken.rc
//
#define IDS_AuthToken                   1
#define IDB_AuthToken                   1
#define IDS_AuthToken_PPG               2
#define IDS_AuthToken_PPG_CAPTION       200
#define IDD_PROPPAGE_AuthToken          200
#define IDR_GIF1                        201
#define IDR_PROGRESSBAR                 201
#define IDC_PROGRESSBAR                 201
#define IDD_DIALOG1                     202
#define IDD_MAINDIALOG                  202

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        203
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         202
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
