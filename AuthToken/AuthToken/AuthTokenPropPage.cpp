// AuthTokenPropPage.cpp : Implementation of the CAuthTokenPropPage property page class.

#include "stdafx.h"
#include "AuthToken.h"
#include "AuthTokenPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CAuthTokenPropPage, COlePropertyPage)



// Message map

BEGIN_MESSAGE_MAP(CAuthTokenPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAuthTokenPropPage, "AuthToken.AuthTokenPropPage.1",
                       0x746cccbd, 0x3589, 0x4370, 0x8d, 0x61, 0xa3, 0xef, 0x1c, 0x7c, 0x12, 0x24)



// CAuthTokenPropPage::CAuthTokenPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CAuthTokenPropPage

BOOL CAuthTokenPropPage::CAuthTokenPropPageFactory::UpdateRegistry(BOOL bRegister)
{
    if (bRegister)
        return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
                                               m_clsid, IDS_AuthToken_PPG);
    else
        return AfxOleUnregisterClass(m_clsid, NULL);
}



// CAuthTokenPropPage::CAuthTokenPropPage - Constructor

CAuthTokenPropPage::CAuthTokenPropPage() :
    COlePropertyPage(IDD, IDS_AuthToken_PPG_CAPTION)
{
}



// CAuthTokenPropPage::DoDataExchange - Moves data between page and properties

void CAuthTokenPropPage::DoDataExchange(CDataExchange* pDX)
{
    DDP_PostProcessing(pDX);
}



// CAuthTokenPropPage message handlers
