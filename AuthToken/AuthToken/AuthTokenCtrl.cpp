// AuthTokenCtrl.cpp : Implementation of the CAuthTokenCtrl ActiveX Control class.

#include "stdafx.h"
#include "AuthToken.h"
#include "AuthTokenCtrl.h"
#include "AuthTokenPropPage.h"
#include "Stringobf.h"
#include "AG_CLIENT.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CAuthTokenCtrl, COleControl)



// Message map

BEGIN_MESSAGE_MAP(CAuthTokenCtrl, COleControl)
    ON_MESSAGE(OCM_COMMAND, &CAuthTokenCtrl::OnOcmCommand)
    ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
    ON_WM_CREATE()
END_MESSAGE_MAP()



// Dispatch map

BEGIN_DISPATCH_MAP(CAuthTokenCtrl, COleControl)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "InputData", dispidInputData, m_InputData, OnInputDataChanged, VT_BSTR)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "message", dispidmessage, m_message, OnmessageChanged, VT_BSTR)
    DISP_FUNCTION_ID(CAuthTokenCtrl, "IsAuthentic_client", dispidIsAuthentic_client, IsAuthentic_client, VT_EMPTY, VTS_NONE)
    DISP_FUNCTION_ID(CAuthTokenCtrl, "IsAuthentic_server", dispidIsAuthentic_server, IsAuthentic_server, VT_EMPTY, VTS_NONE)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "ClientHash", dispidClientHash, m_ClientHash, OnClientHashChanged, VT_BSTR)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "ServerHash", dispidServerHash, m_ServerHash, OnServerHashChanged, VT_BSTR)
    DISP_FUNCTION_ID(CAuthTokenCtrl, "End_Date", dispidEnd_Date, End_Date, VT_EMPTY, VTS_NONE)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "AppId", dispidAppId, m_AppId, OnAppIdChanged, VT_BSTR)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "date", dispiddate, m_date, OndateChanged, VT_BSTR)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "month", dispidmonth, m_month, OnmonthChanged, VT_BSTR)
    DISP_PROPERTY_NOTIFY_ID(CAuthTokenCtrl, "year", dispidyear, m_year, OnyearChanged, VT_BSTR)
    DISP_FUNCTION_ID(CAuthTokenCtrl, "IsAuthentic", dispidIsAuthentic, IsAuthentic, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()



// Event map

BEGIN_EVENT_MAP(CAuthTokenCtrl, COleControl)
    EVENT_CUSTOM_ID("IsAuthenticEvent", eventidIsAuthenticEvent, FireIsAuthenticEvent, VTS_NONE)
END_EVENT_MAP()



// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CAuthTokenCtrl, 1)
    PROPPAGEID(CAuthTokenPropPage::guid)
END_PROPPAGEIDS(CAuthTokenCtrl)



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAuthTokenCtrl, "AuthToken.AuthTokenCtrl.1",
                       0x36299202, 0x9ef, 0x4abf, 0xad, 0xb9, 0x47, 0xc5, 0x99, 0xdb, 0xe7, 0x78)



// Type library ID and version

IMPLEMENT_OLETYPELIB(CAuthTokenCtrl, _tlid, _wVerMajor, _wVerMinor)



// Interface IDs

const IID BASED_CODE IID_DAuthToken =
{ 0x98AF8A06, 0x7DFE, 0x472D, { 0xA7, 0x13, 0xA0, 0xC6, 0xF2, 0x2F, 0xDC, 0x3D } };
const IID BASED_CODE IID_DAuthTokenEvents =
{ 0x78099343, 0xA8FF, 0x4867, { 0xA5, 0x37, 0x6, 0xF3, 0x10, 0x34, 0xF2, 0x2B } };



// Control type information

static const DWORD BASED_CODE _dwAuthTokenOleMisc =
    OLEMISC_ACTIVATEWHENVISIBLE |
    OLEMISC_SETCLIENTSITEFIRST |
    OLEMISC_INSIDEOUT |
    OLEMISC_CANTLINKINSIDE |
    OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAuthTokenCtrl, IDS_AuthToken, _dwAuthTokenOleMisc)



// CAuthTokenCtrl::CAuthTokenCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CAuthTokenCtrl

BOOL CAuthTokenCtrl::CAuthTokenCtrlFactory::UpdateRegistry(BOOL bRegister)
{
    // TODO: Verify that your control follows apartment-model threading rules.
    // Refer to MFC TechNote 64 for more information.
    // If your control does not conform to the apartment-model rules, then
    // you must modify the code below, changing the 6th parameter from
    // afxRegApartmentThreading to 0.

    if (bRegister)
        return AfxOleRegisterControlClass(
                   AfxGetInstanceHandle(),
                   m_clsid,
                   m_lpszProgID,
                   IDS_AuthToken,
                   IDB_AuthToken,
                   afxRegApartmentThreading,
                   _dwAuthTokenOleMisc,
                   _tlid,
                   _wVerMajor,
                   _wVerMinor);
    else
        return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CAuthTokenCtrl::CAuthTokenCtrl - Constructor

CAuthTokenCtrl::CAuthTokenCtrl()
{
    InitializeIIDs(&IID_DAuthToken, &IID_DAuthTokenEvents);
    // TODO: Initialize your control's instance data here.
    //  m_ClientHash = _T("");
}



// CAuthTokenCtrl::~CAuthTokenCtrl - Destructor

CAuthTokenCtrl::~CAuthTokenCtrl()
{
    // TODO: Cleanup your control's instance data here.
}



// CAuthTokenCtrl::OnDraw - Drawing function

void CAuthTokenCtrl::OnDraw(
    CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
    if (!pdc)
        return;

    DoSuperclassPaint(pdc, rcBounds);

    m_MainDialog.MoveWindow(rcBounds, TRUE);
    CBrush brBackGnd(TranslateColor(AmbientBackColor()));
    pdc->FillRect(rcBounds, &brBackGnd);
}



// CAuthTokenCtrl::DoPropExchange - Persistence support

void CAuthTokenCtrl::DoPropExchange(CPropExchange* pPX)
{
    ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
    COleControl::DoPropExchange(pPX);

    // TODO: Call PX_ functions for each persistent custom property.
}



// CAuthTokenCtrl::GetControlFlags -
// Flags to customize MFC's implementation of ActiveX controls.
//
DWORD CAuthTokenCtrl::GetControlFlags()
{
    DWORD dwFlags = COleControl::GetControlFlags();


    // The control will not be redrawn when making the transition
    // between the active and inactivate state.
    dwFlags |= noFlickerActivate;
    return dwFlags;
}



// CAuthTokenCtrl::OnResetState - Reset control to default state

void CAuthTokenCtrl::OnResetState()
{
    COleControl::OnResetState();  // Resets defaults found in DoPropExchange

    // TODO: Reset any other control state here.
}



// CAuthTokenCtrl::PreCreateWindow - Modify parameters for CreateWindowEx

BOOL CAuthTokenCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
    cs.lpszClass = _T("STATIC");
    return COleControl::PreCreateWindow(cs);
}



// CAuthTokenCtrl::IsSubclassedControl - This is a subclassed control

BOOL CAuthTokenCtrl::IsSubclassedControl()
{
    return TRUE;
}



// CAuthTokenCtrl::OnOcmCommand - Handle command messages

LRESULT CAuthTokenCtrl::OnOcmCommand(WPARAM wParam, LPARAM lParam)
{
#ifdef _WIN32
    WORD wNotifyCode = HIWORD(wParam);
#else
    WORD wNotifyCode = HIWORD(lParam);
#endif

    // TODO: Switch on wNotifyCode here.

    return 0;
}



// CAuthTokenCtrl message handlers

int CAuthTokenCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (COleControl::OnCreate(lpCreateStruct) == -1)
        return -1;

    // TODO:  Add your specialized creation code here
    m_MainDialog.Create(IDD_MAINDIALOG, this);

    return 0;
}

void CAuthTokenCtrl::OnInputDataChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}

void CAuthTokenCtrl::OnmessageChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}

void CAuthTokenCtrl::IsAuthentic_client(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

   int i=0;
  int  res = 0;
    if(res) {
        m_message = "AuthToken Password Error";
        printf("verify authorize user password failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("verify authorize user password success!\n");

    unsigned char digest_local[20]={'4'};
	unsigned char digest_device[20]={'4'};
    printf("get digest from device:\t");
     res = 0;
	 if(res) {
        m_message = "Hmac_Sha1 Client Error";

        printf("hmac_sha1 in device failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("success!\n");

     res = 0;
	 if(res) {
        printf("Hmac_Sha1 Server Error\n");
        m_message = "hmac_sha1 in device failed";
        FireIsAuthenticEvent();
        return ;
    }
    printf("success!\n");
    digest_local[20]='\0';
    digest_device[20]='\0';
    printf("compare the two digest:\t");
    for(i = 0; i < 20; i++) {
        if(digest_local[i] != digest_device[i]) {
            printf("not equal\n");
            m_message ="Digest Mismatch";
            FireIsAuthenticEvent();
            return ;
        }
    }
    printf("OK\n");

    printf("authoriz success!\n");
	 res = 0;
    if(res) {
        printf("close failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("\nclose success!\n");
    char buff[100];
    sprintf_s(buff,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",digest_device[0],digest_device[1],digest_device[2],digest_device[3],digest_device[4],digest_device[5],digest_device[6],digest_device[7],digest_device[8],digest_device[9],digest_device[10],digest_device[11],digest_device[12],digest_device[13],digest_device[14],digest_device[15],digest_device[16],digest_device[17],digest_device[18],digest_device[19]);

    m_ClientHash=( char *)buff;
    m_message = "Success";
    // Copy text from the input parameter to the output parameter

    // Fire an event to notify web page
    FireIsAuthenticEvent();
    return;
}


void CAuthTokenCtrl::IsAuthentic_server(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    // TODO: Add your dispatch handler code here
    int res, i;

    // opening LC device
     res = 0;
    if(res) {
        m_message = "AuthToken Not Found";
        printf("open failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("\nopen success!\n");
    // get hardware info
     res = 0;
    if(res) {
        m_message = "AuthToken Info Error";
        printf("get info failed\n");
        FireIsAuthenticEvent();
        return;
    }

    // authorize
    printf("\nverify authorize user password:\t");
     res = 0;
    if(res) {

        m_message = "AuthToken Password Error";
        printf("verify authorize user password failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("verify authorize user password success!\n");

    unsigned char key[20] = {0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, \
                             0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b
                            }; // this should be the hmac key in your LC device

    unsigned char digest_local[20];
    unsigned char digest_device[20];
    printf("get digest from device:\t");
     res = 0;
	 if(res) {
        m_message = "Hmac_Sha1 Client Error";

        printf("hmac_sha1 in device failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("success!\n");

    printf("get digest locally:\t");
     res = 0;
	 if(res) {
        printf("Hmac_Sha1 Server Error\n");
        m_message = "hmac_sha1 in device failed";
        FireIsAuthenticEvent();
        return ;
    }
    printf("success!\n");
    digest_local[20]='\0';
    digest_device[20]='\0';
    printf("compare the two digest:\t");
    for(i = 0; i < 20; i++) {
        if(digest_local[i] != digest_device[i]) {
            printf("not equal\n");
            m_message ="Digest Mismatch";
            FireIsAuthenticEvent();
            return ;
        }
    }
    printf("OK\n");

    printf("authoriz success!\n");

     res = 0;
    if(res) {
        printf("close failed\n");
        FireIsAuthenticEvent();
        return ;
    }
    printf("\nclose success!\n");
    char buff[100];
    sprintf_s(buff,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",digest_local[0],digest_local[1],digest_local[2],digest_local[3],digest_local[4],digest_local[5],digest_local[6],digest_local[7],digest_local[8],digest_local[9],digest_local[10],digest_local[11],digest_local[12],digest_local[13],digest_local[14],digest_local[15],digest_local[16],digest_local[17],digest_local[18],digest_local[19]);

    m_ServerHash=( char *)buff;
    m_message = "Success";
    // Copy text from the input parameter to the output parameter

    // Fire an event to notify web page
    FireIsAuthenticEvent();
    return;
}


void CAuthTokenCtrl::OnClientHashChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}


void CAuthTokenCtrl::OnServerHashChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}


//void CAuthTokenCtrl::End_Date(void)
//{
//}


void CAuthTokenCtrl::End_Date(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    std::string AppIdpar= (char *)m_AppId.GetBuffer(m_AppId.GetLength());
    int datep, monthp, yearp=0;
    if(AG_End_Date  (datep,monthp,yearp ,&AppIdpar, 0)==0)
    {
        m_date.Format("%d", datep);
        m_month.Format("%d", monthp);
        m_year.Format("%d", yearp);

        m_message="success";
    }
    else
    {
        m_message="AuthToken License Invalid";
    }
    // TODO: Add your dispatch handler code here
}


void CAuthTokenCtrl::OnAppIdChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}


void CAuthTokenCtrl::OndateChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}


void CAuthTokenCtrl::OnmonthChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}


void CAuthTokenCtrl::OnyearChanged(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    // TODO: Add your property handler code here

    SetModifiedFlag();
}


void CAuthTokenCtrl::IsAuthentic(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    std::string Handle="";
    Handle.resize(1024);
    std::string AppIdpar= (char *)m_AppId.GetBuffer(m_AppId.GetLength());
    if(AG_IsAuthentic (&Handle,&AppIdpar, 0)==0)
    {
		std::string ID="";
		AG_CheckUSB (&ID);
		m_ClientHash=ID.c_str();
        m_message="success";
    }
    else
    {
        m_message="AuthToken License Invalid";
    }
    // TODO: Add your dispatch handler code here
}
