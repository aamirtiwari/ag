#pragma once

// AuthTokenPropPage.h : Declaration of the CAuthTokenPropPage property page class.


// CAuthTokenPropPage : See AuthTokenPropPage.cpp for implementation.

class CAuthTokenPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAuthTokenPropPage)
	DECLARE_OLECREATE_EX(CAuthTokenPropPage)

// Constructor
public:
	CAuthTokenPropPage();

// Dialog Data
	enum { IDD = IDD_PROPPAGE_AuthToken };

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	DECLARE_MESSAGE_MAP()
};

