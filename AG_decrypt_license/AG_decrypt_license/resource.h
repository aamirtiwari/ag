//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AG_decrypt_license.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_AG_DECRYPT_LICENSE_DIALOG   102
#define IDI_ICON1                       131
#define IDI_ICON2                       133
#define IDI_ICON3                       134
#define IDI_ICON4                       135
#define IDI_ICON5                       136
#define IDI_ICON6                       138
#define IDI_ICON7                       139
#define IDI_ICON8                       140
#define IDR_MENU1                       141
#define IDB_BITMAP1                     142
#define IDB_BITMAP2                     143
#define IDB_BITMAP3                     144
#define IDC_TREE1                       1018
#define ID_ACTIONS_REFRESH              32771
#define ID_ACTIONS_DELETE               32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
