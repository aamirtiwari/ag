// AG_Release.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "Encrypt_decrypt.h"
#include <direct.h>
#include <fstream>
#include <stdio.h>
#include <string>
#include <Setupapi.h>
#include <devguid.h>
#include <Psapi.h>
#include <tchar.h>
#include <strsafe.h>
#include <time.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include "Stringobf.h"
#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT "27016"
int AG_Release(std::string Handle[1], std::string servername[1])
{

    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                         *ptr = NULL,
                          hints;

    std::string sendbuf =Handle[0];

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {

        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {

        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {

            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {

        return 1;
    }

    // Send an initial buffer
    std::string serverdata ="" ;
    std::string in ="";
    in=Handle[0];
    in.append("---deeletefile---");
    serverdata.resize(1024);
    if(in!="")
    {
        int checkvalue=0;
        while (checkvalue==0)
        {
            serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
            serverdata.append("%%%");
            serverdata=serverdata.c_str();
            if(serverdata.find("%%%")!=std::string::npos)
                checkvalue=1;
        }
    }
    else
        serverdata="";
    iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
    if (iResult == SOCKET_ERROR) {

        WSACleanup();
        return 1;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {

        closesocket(ConnectSocket);
        WSACleanup();

        return 1;
    }
    std::string receivedata="";
    for(int i =0; i<1024; i++)
    {
        recvbuf[i]='\0';
    }
    // Receive until the peer closes the connection
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    in.resize(recvbuflen);
    in=recvbuf;
    receivedata.resize(1024);

    if ( iResult > 0 )
    {

        for(int i = 1024; i >= 1; i--)
        {
            if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
            {
                recvbuf[i-1] = '\0';
                break;
            }
        }
        size_t foundc;
        size_t found;
        foundc=in.find("%%%");
        if( foundc!=std::string::npos)
        {
            in=in.substr(0,foundc);
            receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
        }
        if(receivedata.find("---deelete")!=std::string::npos)
        {

            return 0;
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();
    }

}



int _tmain(int argc, _TCHAR* argv[])
{
    std::string Handlenew = argv[1];
    std::string servernamenew = argv[2];
    AG_Release(&Handlenew, &servernamenew);
    return 0;
}

