///////////////////////////////////////////////////////////////////////

// GetMACAdapters.cpp : Defines the entry point for the console application.
//
// Author:	Khalid Shaikh [Shake@ShakeNet.com]
// Date:	April 5th, 2002
//
// This program fetches the MAC address of the localhost by fetching the
// information through GetAdapatersInfo.  It does not rely on the NETBIOS
// protocol and the ethernet adapter need not be connect to a network.
//
// Supported in Windows NT/2000/XP
// Supported in Windows 95/98/Me
//
// Supports multiple NIC cards on a PC.
#include "stdafxnew.h"
#include "MAC.h"

using std::string;

string macid="";


// Fetches the MAC address and prints it
std::string GetMACaddress(void)
{
    DWORD MACaddress = 0;
    IP_ADAPTER_INFO AdapterInfo[16]= {NULL};      // Allocate information
    // for up to 16 NICs
    DWORD dwBufLen = sizeof(AdapterInfo);  // Save memory size of buffer

    DWORD dwStatus = GetAdaptersInfo(      // Call GetAdapterInfo
                         AdapterInfo,                 // [out] buffer to receive data
                         &dwBufLen);                  // [in] size of receive data buffer
    assert(dwStatus == ERROR_SUCCESS);  // Verify return value is
    // valid, no buffer overflow

    PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo; // Contains pointer to
    // current adapter info



    char string [256];
    do {
        if (MACaddress == 0)
            MACaddress = pAdapterInfo->Address [5] + pAdapterInfo->Address [4] * 256 +
                         pAdapterInfo->Address [3] * 256 * 256 +
                         pAdapterInfo->Address [2] * 256 * 256 * 256;

        sprintf_s (string, "%02X-%02X-%02X-%02X-%02X-%02X", pAdapterInfo->Address[0], pAdapterInfo->Address[1],
                   pAdapterInfo->Address[2], pAdapterInfo->Address[3], pAdapterInfo->Address[4], pAdapterInfo->Address[5]);

        macid.append(string);
        macid = macid + "--";
//  strcat(macid,string);
        pAdapterInfo = pAdapterInfo->Next;    // Progress through linked list
    } while(pAdapterInfo);
// Terminate if last adapter

    return macid;
}
