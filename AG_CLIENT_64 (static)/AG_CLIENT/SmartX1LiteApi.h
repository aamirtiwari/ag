#if !defined __SmartX1LiteAPI_H__
#define __SmartX1LiteAPI_H__

#ifdef __cplusplus 
extern "C" 
{
#endif	

 	 long  SmartX1Find(char appName[32]);

	 long  SmartX1Open(char * password);
	
	 long  SmartX1GetUid(char * hardwareID);

	 long  SmartX1ReadStorage(long address, long length, unsigned char * pBuffer);

	 long  SmartX1WriteStorage(long address, long length, unsigned char * pBuffer);
	
	 long  SmartX1Close();
	

#ifdef __cplusplus 
}

#endif



#endif