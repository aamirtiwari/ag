#include <cstdlib>
#include <iostream>
#include <windows.h>

#define FILE_SHARE_VALID_FLAGS          0x00000007

#pragma pack(1)
typedef struct __PARTITION_RECORD_
{
BYTE           status;            /* 0x80=bootable, 0x00=non-bootable, else invalid*/
BYTE           begin_head;
BYTE           begin_sector:5;
WORD           begin_cylinder:10;

BYTE           partition_type;  
BYTE           end_head;
BYTE           end_sector:5;
WORD           end_cylinder:10;  

DWORD          begin_lba;
DWORD          lba_blocks;      

}PARTITION_RECORD;



int ReadSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD *value);
int WriteSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD value);