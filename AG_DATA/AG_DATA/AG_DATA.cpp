// AG_DATA.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "AG_DATA.h"
#using <mscorlib.dll>
#using <System.Data.dll>
#using <System.dll>
#include <tchar.h>

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Data::OleDb;
std::string returnval="";
AUTHGURU_NET_API int InsertSysInfo(std::string email[1],std::string nam1[1])
{
    std::string temp=nam1[0];
    String* nam = temp.c_str();
    temp=email[0];
    String* email1 = temp.c_str();

    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("insert into Cust_machine(SysInfo,Email) values('{0}','{1}')",nam,email1);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    cmd->ExecuteNonQuery();

    conn->Close();
    return 0;
}
AUTHGURU_NET_API int InsertLicInfo(std::string sysinfo[1],std::string appid[1],std::string version[1], std::string sdate[1], std::string edate[1], std::string network[1],std::string locking[1],std::string usbid[1],std::string readonly[1])
{
    std::string temp=sysinfo[0];
    String* nam = temp.c_str();
    temp=appid[0];
    String* email1 = temp.c_str();
    temp=version[0];
    String* phone1 = temp.c_str();
    temp=sdate[0];
    String* sdate1 = temp.c_str();
    temp=edate[0];
    String* edate1 = temp.c_str();
    temp=network[0];
    String* network1 = temp.c_str();
    temp=locking[0];
    String* locking1 = temp.c_str();
    temp=usbid[0];
    String* usbid1 = temp.c_str();
    temp=readonly[0];
    String* readonly1 = temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr1 = String::Format("insert into Machine_appID(SysInfo,AppID,Version,Start_Date,Expiration_Date,Network_Seats,Locking_Criteria,USBID,Read_Only) values('{0}','{1}'", nam,email1);
    String* sqlstr2 = String::Format("{0},'{1}'",sqlstr1,phone1);
    String* sqlstr3 = String::Format("{0},'{1}'",sqlstr2,sdate1);
    String* sqlstr4 = String::Format("{0},'{1}'",sqlstr3,edate1);
    String* sqlstr5 = String::Format("{0},'{1}'",sqlstr4,network1);
    String* sqlstr7 = String::Format("{0},'{1}'",sqlstr5,locking1);
    String* sqlstr8 = String::Format("{0},'{1}'",sqlstr7,usbid1);
    String* sqlstr = String::Format("{0},'{1}')",sqlstr8,readonly1);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    cmd->ExecuteNonQuery();

    conn->Close();
    return 0;
}

AUTHGURU_NET_API int DELETECUSTOMER(std::string nam1[1])
{
    std::string temp=nam1[0];
    String* nam = temp.c_str();

    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("DELETE FROM Customer WHERE EMAIL = '{0}'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    cmd->ExecuteNonQuery();

    conn->Close();
    return 0;
}
AUTHGURU_NET_API int InsertCustomer(std::string email[1],std::string nam1[1], std::string address[1], std::string phone[1])
{
    std::string temp=nam1[0];
    String* nam = temp.c_str();
    temp=email[0];
    String* email1 = temp.c_str();
    temp=address[0];
    String* address1 = temp.c_str();
    temp=phone[0];
    String* phone1 = temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr1 = String::Format("insert into Customer(Email,Name,Address,Phone) values('{0}','{1}','{2}'",email1, nam,address1);
    String* sqlstr = String::Format("{0},'{1}')",sqlstr1,phone1);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    cmd->ExecuteNonQuery();

    conn->Close();
    return 0;
}

AUTHGURU_NET_API int ShowAge(std::string nam1[1])
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\test.mdb");

    conn->Open();

    String* sqlstr = String::Format("select age from main where Name = '{0}'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();

    if(rdr->Read())
    {
        String* age = rdr->get_Item("Age")->ToString();
        IntPtr p = Marshal::StringToHGlobalAnsi(age);
        char *bufftrial = static_cast<char*>(p.ToPointer());
        MessageBox(NULL, bufftrial, "",0);
        Marshal::FreeHGlobal(p);
        //	sprintf_s(bufftrial,nam);

        //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
    }
    else
    {
        Console::WriteLine("No matches found for {0}!",nam);
        conn->Close();
        return 1;
    }

    conn->Close();
    return 0;
}
AUTHGURU_NET_API int SEARCHSYSINFO2(std::string nam1[1])
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Cust_machine where SysInfo = '{0}'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();

    if(rdr->Read())
    {
        String* age = rdr->get_Item("Email")->ToString();
        IntPtr p = Marshal::StringToHGlobalAnsi(age);
        char *bufftrial = static_cast<char*>(p.ToPointer());
//MessageBox(NULL, bufftrial, "",0);
        Marshal::FreeHGlobal(p);
        //	sprintf_s(bufftrial,nam);

        //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
    }
    else
    {
        conn->Close();
        return 1;
    }

    conn->Close();
    return 0;
}
AUTHGURU_NET_API int SEARCHMAIL2(std::string nam1[1])
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Customer where Email = '{0}'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();

    if(rdr->Read())
    {
        String* age = rdr->get_Item("Name")->ToString();
        IntPtr p = Marshal::StringToHGlobalAnsi(age);
        char *bufftrial = static_cast<char*>(p.ToPointer());
//MessageBox(NULL, bufftrial, "",0);
        Marshal::FreeHGlobal(p);
        //	sprintf_s(bufftrial,nam);

        //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
    }
    else
    {
        conn->Close();
        return 1;
    }

    conn->Close();
    return 0;
}
AUTHGURU_NET_API char * SEARCHID(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Cust_machine where Email = '{0}'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("ID")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}
AUTHGURU_NET_API char * SEARCHSYSINFO(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Cust_machine where ID = {0}",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("SysInfo")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}
AUTHGURU_NET_API char * SEARCHLICENSE(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Machine_appID where SysInfo = '{0}'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("AppID")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Version")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Start_Date")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Expiration_Date")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Network_Seats")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Locking_Criteria")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("USBID")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Read_Only")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}
AUTHGURU_NET_API char * SEARCHMAIL(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Customer where Email like '%{0}%'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("Email")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Name")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Address")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Phone")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}
AUTHGURU_NET_API char * SEARCHNAME(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Customer where NAME like '%{0}%'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("Email")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Name")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Address")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Phone")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        conn->Close();
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}
AUTHGURU_NET_API char * SEARCHADDRESS(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Customer where Address like '%{0}%'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("Email")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Name")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Address")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Phone")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        conn->Close();
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}
AUTHGURU_NET_API char * SEARCHPHONE(std::string nam1[1], char * returnstr)
{
    std::string temp=nam1[0];
    String* nam= temp.c_str();
    OleDbConnection* conn = new OleDbConnection(
        "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Program Files\\Bastion\\Authdata.mdb");

    conn->Open();

    String* sqlstr = String::Format("select * from Customer where Phone like '%{0}%'",nam);

    OleDbCommand* cmd = new OleDbCommand(sqlstr,conn);

    OleDbDataReader* rdr = cmd->ExecuteReader();
    returnval="";
    if(rdr->Read())
    {
        do
        {
            String* age = rdr->get_Item("Email")->ToString();
            IntPtr p = Marshal::StringToHGlobalAnsi(age);
            char *bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Name")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Address")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            age = rdr->get_Item("Phone")->ToString();
            p = Marshal::StringToHGlobalAnsi(age);
            bufftrial = static_cast<char*>(p.ToPointer());
            returnval.append(bufftrial);
            returnval.append("%%%");
            Marshal::FreeHGlobal(p);
            //	sprintf_s(bufftrial,nam);

            //	Console::WriteLine("{0} is {1} years old",nam,rdr->get_Item("Age"));
        } while(rdr->Read());
    }
    else
    {
        conn->Close();
        return "";
    }

    conn->Close();
    returnstr=(char *)returnval.c_str();
    return returnstr;
}