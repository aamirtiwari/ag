// AuthToken_Install.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

int _tWinMain (HINSTANCE hInstance,
               HINSTANCE hPrevInstance,
               LPSTR lpCmdLine,
               int nCmdShow)
{
    PROCESS_INFORMATION peProcessInformation;
    STARTUPINFO peStartUpInformation;
    CONTEXT pContext;
    memset(&peStartUpInformation,0,sizeof(STARTUPINFO));
    memset(&peProcessInformation,0,sizeof(PROCESS_INFORMATION));
    memset(&pContext,0,sizeof(CONTEXT));

    peStartUpInformation.cb = sizeof(peStartUpInformation);
    CreateProcess(NULL,"regsvr32.exe /s c:\\windows\\syswow64\\AuthToken.dll",NULL,NULL,0,0, NULL,NULL,&peStartUpInformation,&peProcessInformation);
    WaitForSingleObject(peProcessInformation.hProcess, INFINITE);
    CreateProcess(NULL,"regsvr32.exe /s c:\\windows\\system32\\AuthToken.dll",NULL,NULL,0,0, NULL,NULL,&peStartUpInformation,&peProcessInformation);
    WaitForSingleObject(peProcessInformation.hProcess, INFINITE);
    CreateProcess(NULL,"regsvr32.exe /s c:\\windows\\syswow64\\AuthTokenServer.dll",NULL,NULL,0,0, NULL,NULL,&peStartUpInformation,&peProcessInformation);
    WaitForSingleObject(peProcessInformation.hProcess, INFINITE);
    CreateProcess(NULL,"regsvr32.exe /s c:\\windows\\system32\\AuthTokenServer.dll",NULL,NULL,0,0, NULL,NULL,&peStartUpInformation,&peProcessInformation);
    WaitForSingleObject(peProcessInformation.hProcess, INFINITE);

    return 0;
}

