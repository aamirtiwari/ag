// SERVICENEW.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include <tchar.h>
#include <strsafe.h>
#include <fstream>

#include "Encrypt_decrypt.h"
#include "AG_LICENSE.h"

#include "DIVIDEINFO.h"
#include "READWRITESECTOR.h"

#include "Stringobf.h"
#include "Sense_LC.h"
#include "HARDDISKID.h"
#include <direct.h>
#include "AG_DATA.h"
using namespace std;
// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 3072
#define DEFAULT_PORT "27018"

std::string lockcode="";
std::string aes="";
std::string filename="";
std::string USBID="";
std::string memory= "";
std::string lic="";

SERVICE_STATUS_HANDLE g_ServiceStatusHandle;
HANDLE g_StopEvent;
DWORD g_CurrentState = 0;
bool g_SystemShutdown = false;
void ReportStatus(DWORD state) {
    g_CurrentState = state;
    SERVICE_STATUS serviceStatus = {        SERVICE_WIN32_OWN_PROCESS,        g_CurrentState,        state == SERVICE_START_PENDING ? 0 : SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN,        NO_ERROR,        0,        0,        0    };
    SetServiceStatus(g_ServiceStatusHandle, &serviceStatus);
}
void ReportErrorStatus(DWORD errorCode)
{   g_CurrentState = SERVICE_STOPPED;
    SERVICE_STATUS serviceStatus = {        SERVICE_WIN32_OWN_PROCESS,        g_CurrentState,        0,        ERROR_SERVICE_SPECIFIC_ERROR,        errorCode,        0,        0   };
    SetServiceStatus(g_ServiceStatusHandle, &serviceStatus);
}

void ReportProgressStatus(DWORD state, DWORD checkPoint, DWORD waitHint)
{   g_CurrentState = state;
    SERVICE_STATUS serviceStatus = {        SERVICE_WIN32_OWN_PROCESS,        g_CurrentState,        state == SERVICE_START_PENDING ? 0 : SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN,        NO_ERROR,        0,        checkPoint,        waitHint   };
    SetServiceStatus(g_ServiceStatusHandle, &serviceStatus);
}

// Handler for service control events.
DWORD WINAPI HandlerEx(DWORD control, DWORD eventType, void *eventData, void *context)
{   switch (control)    {    // Entrie system is shutting down.
    case SERVICE_CONTROL_SHUTDOWN:
        g_SystemShutdown = true;
    // continue...
    // Service is being stopped.
    case SERVICE_CONTROL_STOP:
        ReportStatus(SERVICE_STOP_PENDING);
        SetEvent(g_StopEvent);
        break;
    // Ignoring all other events, but we must always report service status.
    default:
        ReportStatus(g_CurrentState);
        break;
    }
    return NO_ERROR;
}


typedef struct __MBR_
{
    BYTE                 boot_code[440];
    DWORD                disk_signature; //4 bitai
    WORD                 unknown;
    PARTITION_RECORD     partition_table[4];
    WORD                 signature;          /*AA55*/
} MBR;
#pragma pack(16)
MBR mbr;

using namespace std;
const char *disk_default="\\\\.\\PhysicalDrive0";
WORD begin_cylinder,end_cylinder;

int ReadSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD *value, std::string key)

{
    int checkv= strtol(key.c_str(),NULL,10);
    if((startinglogicalsector == 58) || (startinglogicalsector==59) || (startinglogicalsector ==checkv))
    {
        *value=0;
        std::string lockcode="";
        lc_handle_t handle;
        int res=0,k=0;
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_read(handle, 0, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            LC_close(handle);
        }

        if(lockcode=="")
        {
            return 1;
        }
        std::string dec = aes_decrypt(lockcode, key);
        std::string temp="";
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value1= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value2= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        int value3= strtol(temp.c_str(),NULL,10);
        if(startinglogicalsector==58)
        {
            *value=value1;
            return value1;
        }
        if(startinglogicalsector==59)
        {
            *value=value2;
            return value2;
        }
        else
        {
            *value=value3;
            return value3;
        }
    }
    else
    {
        BOOL bDebugged = FALSE;
        CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
        if( bDebugged )
        {
            exit(0);
            return 1;
        }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
        struct
        {
            DWORD StartingSector ;
            WORD NumberOfSectors ;
            DWORD pBuffer;
        } ControlBlock;
#pragma pack ()

#pragma pack (1)
        typedef struct _DIOC_REGISTERS
        {
            DWORD reg_EBX;
            DWORD reg_EDX;
            DWORD reg_ECX;
            DWORD reg_EAX;
            DWORD reg_EDI;
            DWORD reg_ESI;
            DWORD reg_Flags;
        } DIOC_REGISTERS ;
#pragma pack ()

        char* buffer = (char*)malloc (512*numberofsectors);
        HANDLE hDevice ;
        DIOC_REGISTERS reg ;
        BOOL  fResult ;
        DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
        hDevice = CreateFile ( "\\\\.\\vwin32",
                               0,
                               0,
                               NULL,
                               0,
                               FILE_FLAG_DELETE_ON_CLOSE,
                               NULL );

        if ( hDevice == INVALID_HANDLE_VALUE )
        {
            // win NT/2K/XP code
            HANDLE hDevice;
            DWORD bytesread;

            const char * _devicename = disk_default;
            //_devicename[4] += drive;

            // Creating a handle to disk drive using CreateFile () function ..
            hDevice = CreateFile(_devicename,
                                 GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                 NULL, OPEN_EXISTING, 0, NULL);

            if (hDevice == INVALID_HANDLE_VALUE)
                return 1;

            // Setting the pointer to point to the start of the sector we want to read ..
            MBR mbr;
            MBR *_mbr=&mbr;
            memset(_mbr,0,sizeof(_mbr));
            SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
            if (!ReadFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL) )
                return 1;

            *value=_mbr->unknown;
        }
        else
        {
            // code for win 95/98
            ControlBlock.StartingSector = (DWORD)startinglogicalsector;
            ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
            ControlBlock.pBuffer = (DWORD)buffer ;

            //-----------------------------------------------------------
            // SI contains read/write mode flags
            // SI=0h for read and SI=1h for write
            // CX must be equal to ffffh for
            // int 21h's 7305h extention
            // DS:BX -> base addr of the
            // control block structure
            // DL must contain the drive number
            // (01h=A:, 02h=B: etc)
            //-----------------------------------------------------------

            reg.reg_ESI = 0x00;
            reg.reg_ECX = -1 ;
            reg.reg_EBX = (DWORD)(&ControlBlock);
            reg.reg_EDX = drive+1;
            reg.reg_EAX = 0x7305 ;

            //  6 == VWIN32_DIOC_DOS_DRIVEINFO
            fResult = DeviceIoControl ( hDevice,
                                        6,
                                        &(reg),
                                        sizeof (reg),
                                        &(reg),
                                        sizeof (reg),
                                        &cb,
                                        0);

            if (!fResult || (reg.reg_Flags & 0x0001)) return NULL;
        }
        free(buffer);
        CloseHandle(hDevice);

        return 0;
    }
}

int WriteSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD value, std::string key)
{
    int checkv= strtol(key.c_str(),NULL,10);
    if((startinglogicalsector == 58) || (startinglogicalsector==59) || (startinglogicalsector ==checkv))
    {
        std::string lockcode="";
        lc_handle_t handle;
        int res=0,k=0;
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_read(handle, 0, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            LC_close(handle);
        }

        if(lockcode=="")
        {
            return 1;
        }
        std::string dec = aes_decrypt(lockcode, key);
        std::string temp="";
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value1= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value2= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        int value3= strtol(temp.c_str(),NULL,10);

        if(startinglogicalsector==58)
            value1 = value;
        if(startinglogicalsector==59)
            value2=value;

        char buff[20];
        sprintf_s(buff,"%d",value1);
        temp = buff;
        temp=temp.c_str();
        temp=temp.append("---");

        sprintf_s(buff,"%d",value2);
        temp=temp.append(buff);
        temp=temp.c_str();
        temp=temp.append("---");

        sprintf_s(buff,"%d",value3);
        temp=temp.append(buff);
        temp=temp.c_str();
        temp=temp.append("---");

        int checkvalue=0;
        while (checkvalue==0)
        {
            dec= aes_encrypt(temp, key);
            dec.append("%%%");
            dec=dec.c_str();
            if(dec.find("%%%")!=std::string::npos)
            {
                dec= dec.substr(0, dec.find("%%%"));
                checkvalue=1;
            }
        }
        dec.append("---");
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_write(handle, 0, (unsigned char *)dec.c_str());
                LC_close(handle);
                return 0;

            }
            LC_close(handle);
        }
        else
        {
            return 1;
        }
    }
    else
    {
        BOOL bDebugged = FALSE;
        CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
        if( bDebugged )
        {
            exit(0);
            return 1;
        }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
        struct
        {
            DWORD StartingSector ;
            WORD NumberOfSectors ;
            DWORD pBuffer;
        } ControlBlock;
#pragma pack ()

#pragma pack (1)
        typedef struct _DIOC_REGISTERS
        {
            DWORD reg_EBX;
            DWORD reg_EDX;
            DWORD reg_ECX;
            DWORD reg_EAX;
            DWORD reg_EDI;
            DWORD reg_ESI;
            DWORD reg_Flags;
        } DIOC_REGISTERS ;
#pragma pack ()

        char* buffer = (char*)malloc (512*numberofsectors);
        HANDLE hDevice ;
        DIOC_REGISTERS reg ;
        BOOL  fResult ;
        DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
        hDevice = CreateFile ( "\\\\.\\vwin32",
                               0,
                               0,
                               NULL,
                               0,
                               FILE_FLAG_DELETE_ON_CLOSE,
                               NULL );

        if ( hDevice == INVALID_HANDLE_VALUE )
        {
            // win NT/2K/XP code
            HANDLE hDevice;
            DWORD bytesread;

            const char * _devicename = disk_default;
            //_devicename[4] += drive;

            // Creating a handle to disk drive using CreateFile () function ..
            hDevice = CreateFile(_devicename,
                                 GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                 NULL,OPEN_EXISTING, 0, NULL);

            if (hDevice == INVALID_HANDLE_VALUE)
                return 1;

            // Setting the pointer to point to the start of the sector we want to read ..
            SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
            MBR mbr;
            MBR *_mbr=&mbr;
            memset(_mbr,0,sizeof(_mbr));
            _mbr->unknown =value;
            int n =WriteFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL);
            if (!n )
                return 1;
        }
        else
        {
            // code for win 95/98
            ControlBlock.StartingSector = (DWORD)startinglogicalsector;
            ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
            ControlBlock.pBuffer = (DWORD)buffer ;

            //-----------------------------------------------------------
            // SI contains read/write mode flags
            // SI=0h for read and SI=1h for write
            // CX must be equal to ffffh for
            // int 21h's 7305h extention
            // DS:BX -> base addr of the
            // control block structure
            // DL must contain the drive number
            // (01h=A:, 02h=B: etc)
            //-----------------------------------------------------------

            reg.reg_ESI = 0x00;
            reg.reg_ECX = -1 ;
            reg.reg_EBX = (DWORD)(&ControlBlock);
            reg.reg_EDX = drive+1;
            reg.reg_EAX = 0x7305 ;

            //  6 == VWIN32_DIOC_DOS_DRIVEINFO
            fResult = DeviceIoControl ( hDevice,
                                        6,
                                        &(reg),
                                        sizeof (reg),
                                        &(reg),
                                        sizeof (reg),
                                        &cb,
                                        0);

            if (!fResult || (reg.reg_Flags & 0x0001)) return 1;
        }
        free(buffer);
        CloseHandle(hDevice);
        return 0;
    }
}
void AG_sleep(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

DWORD WINAPI threadfunction(LPVOID param)
{
    WSADATA wsaData;
    int iResult;
    int nclient =0;
    std::string m_edit3="";
    std::string m_edit1="";
    std::string m_edit2="";
    std::string m_edit4="";
    std::string m_edit5="";
    std::string m_edit6="";
    std::string checkconn="LOCAL MACHINE";
    std::string appid="";
    int m_date1=0;
    int m_date1year=0;
    int m_date2=0;
    int m_date2year=0;
    int USBlock=0;
    bool m_check=false;
    bool m_check2=false;
    bool m_check4=false;
    SOCKET ListenSocket[1024] = {INVALID_SOCKET};
    SOCKET ClientSocket[1024] = {INVALID_SOCKET};

    struct addrinfo *result = NULL;
    struct addrinfo hints;

    int iSendResult;
    char recvbuf[DEFAULT_BUFLEN]="";
    int recvbuflen = DEFAULT_BUFLEN;
    int tserver = 0;
    while (1)
    {

        iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
        if (iResult != 0) {
            printf("WSAStartup failed with error: %d\n", iResult);

        }

        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_flags = AI_PASSIVE;

        // Resolve the server address and port
        iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
        if ( iResult != 0 ) {
            printf("getaddrinfo failed with error: %d\n", iResult);
            WSACleanup();

        }

        // Create a SOCKET for connecting to server
        ListenSocket[nclient] = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        if (ListenSocket[nclient] == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            freeaddrinfo(result);
            WSACleanup();
        }

        // Setup the TCP listening socket
        iResult = bind( ListenSocket[nclient], result->ai_addr, (int)result->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            printf("bind failed with error: %d\n", WSAGetLastError());
            freeaddrinfo(result);
            closesocket(ListenSocket[nclient]);
            WSACleanup();
        }

        freeaddrinfo(result);

        iResult = listen(ListenSocket[nclient], SOMAXCONN);
        if (iResult == SOCKET_ERROR) {
            printf("listen failed with error: %d\n", WSAGetLastError());
            closesocket(ListenSocket[nclient]);
            WSACleanup();
        }

        // Accept a client socket
        ClientSocket[nclient] = accept(ListenSocket[nclient], NULL, NULL);
        if (ClientSocket[nclient] == INVALID_SOCKET) {
            printf("accept failed with error: %d\n", WSAGetLastError());
            closesocket(ListenSocket[nclient]);
            WSACleanup();
        }

        // No longer need server socket
        closesocket(ListenSocket[nclient]);
        for(int i =0; i<DEFAULT_BUFLEN; i++)
        {
            recvbuf[i]='\0';
        }
        iResult = recv(ClientSocket[nclient], recvbuf, recvbuflen, 0);
        if (iResult > 0) {

            for(int i = DEFAULT_BUFLEN; i >= 1; i--)
            {
                if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
                {
                    recvbuf[i-1] = '\0';
                    break;
                }
            }
            std::string recvalue = "";
            std::string in ="";
            in = recvbuf;
            std::string email ="";
            std::string name ="";
            std::string address ="";
            std::string phone ="";
            std::string sysinfodata ="";
            std::string versiondata ="0";
            std::string readonlydata= "";
            std::string network = "None";
            std::string sdate= "None";
            std::string edate= "None";
            std::string lockingdata="";
            std::string usbiddata="None";
            size_t foundc;
            foundc=in.find("---vk---");
            std::string keyIDrec="";
            if( foundc!=std::string::npos)
            {
                keyIDrec=in.substr(foundc);
                phone = keyIDrec.substr(keyIDrec.rfind("---vk---")+8);
                keyIDrec=keyIDrec.substr(0,keyIDrec.rfind("---vk---")+8);
                email = phone.substr(0,phone.find("%%%"));
                phone = phone.substr(phone.find("%%%")+3);
                name = phone.substr(0,phone.find("%%%"));
                phone = phone.substr(phone.find("%%%")+3);
                address = phone.substr(0,phone.find("%%%"));
                phone = phone.substr(phone.find("%%%")+3);
                phone = phone.substr(0,phone.find("%%%"));
                in=in.substr(0,foundc);
                recvalue= in;

            }

            string projectdata="";
            std::string projectdata1="";
            std::string projectdata2="";
            std::string keyID="";
            char *memblock;
            char * sendbuff;
            sendbuff=new char[DEFAULT_BUFLEN];
            sendbuff = "LICENSE CREATION FAILED";

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            aes = recvalue;
            sysinfodata = recvalue;
            size_t found2 = aes.find("---v2---");
            if (found2!=std::string::npos)
            {
                std::string loc="";
                loc = aes.substr(found2+8);
                USBID =  aes_decrypt(loc, STRING_TWO_DEFINE_NAME);

                aes = aes.substr(0,found2);
            }

            aes= aes_decrypt(aes, STRING_TWO_DEFINE_NAME);

            string projectname= "C:/Users/Public/Application Data/Bastion/AG_Online.agl";
            ifstream file (projectname, ios::in|ios::binary|ios::ate);

            if(projectname=="")
            {
                sendbuff= "FILE NOT LOADED";
                goto CONTINUESERVER;
            }




            if (file.is_open())
            {
                int size;
                size = (int) file.tellg();



                memblock = new char[size];
                file.seekg (0, ios::beg);

                int size1=size;
                //file.read (&memblock[debug], size);
                //debug=strlen(&memblock[debug])+1;
                //lockcode.append(&memblock[0]);


                size_t debug=0;

                projectdata.resize(size);
                for (int read=0; read<size; read++)
                {
                    file.read (&memblock[read],1);

                    projectdata[read]=memblock[read];
                }
                projectdata = projectdata.substr(0,size);

                file.close();
            }
            else
            {
                sendbuff= "FILE NOT LOADED";
                goto CONTINUESERVER;
            }

            if(projectdata=="")
            {
                sendbuff= "PROJECT NOT LOADED";
                goto CONTINUESERVER;
            }

//		while (projectdata!="")
//		{

            size_t found = projectdata.find(keyIDrec);
            if (found!=std::string::npos)
            {
                keyID=projectdata.substr(found);
                projectdata=projectdata.substr(0, found);

                found = keyID.find("---vk---");
                if (found!=std::string::npos)
                {
                    keyID=keyID.substr(found+8);
                    found = keyID.find("---vk---");
                    if (found!=std::string::npos)
                    {
                        projectdata1=keyID.substr(found+8);
                        keyID=keyID.substr(0, found);
                    }

                }


                found = projectdata.rfind("---vk---");
                if (found!=std::string::npos)
                {
                    projectdata2=projectdata.substr(0,found);
                    projectdata=projectdata.substr(found+8);

                }

                found = projectdata.find("---v2---");
                if (found!=std::string::npos)
                {
                    std::string loc="";
                    loc = projectdata.substr(found+8);
                    projectdata = projectdata.substr(0,found);
                    m_edit3=loc.c_str();
                    if(loc=="HARDWARE LOCK")
                    {
                        USBlock=1;
                        lockingdata="USB ID";
                        usbiddata=USBID;
                    }
                    else if(loc=="SL + HL")
                    {
                        USBlock=2;
                        lockingdata="SOFTWARE LICENSE + USB";
                        usbiddata=USBID;
                    }
                    else
                    {
                        USBlock=0;
                        lockingdata="SOFTWARE LICENSE";
                    }
                }
                found = projectdata.find("--");
                if (found!=std::string::npos)
                {
                    std::string temp="";
                    temp = projectdata.substr(0,found);
                    projectdata = projectdata.substr(found+2);
                    m_edit1=temp.c_str();
                    found = projectdata.find("--");
                    temp = projectdata.substr(0,found);
                    projectdata = projectdata.substr(found+2);
                    m_edit2=temp.c_str();

                    found = projectdata.find("--");
                    temp = projectdata.substr(0,found);
                    projectdata = projectdata.substr(found+2);
                    m_edit4=temp.c_str();

                    found = projectdata.find("--");
                    temp = projectdata.substr(0,found);
                    projectdata = projectdata.substr(found+2);

                    if (temp!="0")
                    {
                        if (temp=="1")
                        {
                            m_check=true;
                            m_check2=false;

                            found = projectdata.find("--");
                            temp = projectdata.substr(0,found);
                            projectdata = projectdata.substr(found+2);
                            SYSTEMTIME TimeDest;

                            m_date1=strtol(temp.c_str(),NULL,10);
                            found = projectdata.find("--");
                            temp = projectdata.substr(0,found);
                            projectdata = projectdata.substr(found+2);
                            m_date1year=strtol(temp.c_str(),NULL,10);
                            sdate="";
                            char buffdate[20];
                            sprintf_s(buffdate,"%d", m_date1/100);
                            sdate.append(buffdate);
                            sdate.append("/");
                            sprintf_s(buffdate,"%d", m_date1%100);
                            sdate.append(buffdate);
                            sdate.append("/");
                            sprintf_s(buffdate,"%d", m_date1year);
                            sdate.append(buffdate);

                            found = projectdata.find("--");
                            temp = projectdata.substr(0,found);
                            projectdata = projectdata.substr(found+2);

                            m_date2=strtol(temp.c_str(),NULL,10);

                            found = projectdata.find("--");
                            temp = projectdata.substr(0,found);
                            projectdata = projectdata.substr(found+2);
                            m_date2year=strtol(temp.c_str(),NULL,10);
                            sprintf_s(buffdate,"%d", m_date2/100);
                            edate="";
                            edate.append(buffdate);
                            edate.append("/");
                            sprintf_s(buffdate,"%d", m_date2%100);
                            edate.append(buffdate);
                            edate.append("/");
                            sprintf_s(buffdate,"%d", m_date2year);
                            edate.append(buffdate);
                        }
                        else
                        {
                            m_check2=true;
                            m_check= false;

                            found = projectdata.find("--");
                            temp = projectdata.substr(0,found);
                            m_edit5= (temp.c_str());
                        }
                    }
                    found = projectdata.find("--");
                    temp = projectdata.substr(0,found);
                    projectdata = projectdata.substr(found+2);

                    if (temp=="1")
                    {
                        m_check4= true;
                        found = projectdata.find("--");
                        m_edit6 = projectdata.substr(0,found);
                        projectdata = projectdata.substr(found+2);
                    }
                    else
                    {
                        m_check4= false;
                    }
                }
                delete[] memblock;
            }
            else
            {
                sendbuff = "License Key ID not found";

                projectdata="";
                string filename2= "C:\\Program Files\\Bastion\\";
                foundc=keyIDrec.find("---vk---");
                if( foundc!=std::string::npos)
                {
                    keyIDrec=keyIDrec.substr(foundc+8);
                    foundc=keyIDrec.find("---vk---");
                    if( foundc!=std::string::npos)
                    {
                        keyIDrec=keyIDrec.substr(0, foundc);
                    }
                }
                filename2.append(keyIDrec);
                filename2.append(".upd");

                ifstream file1 (filename2, ios::in|ios::binary|ios::ate);

                if (file1.is_open())
                {
                    int size;
                    size = (int) file1.tellg();



                    char *memblock1 = new char[size];
                    file1.seekg (0, ios::beg);

                    int size1=size;
                    //file.read (&memblock[debug], size);
                    //debug=strlen(&memblock[debug])+1;
                    //lockcode.append(&memblock[0]);


                    size_t debug=0;

                    projectdata.resize(size);
                    for (int read=0; read<size; read++)
                    {
                        file1.read (&memblock1[read],1);

                        projectdata[read]=memblock1[read];
                    }
                    projectdata = projectdata.substr(0,size);
                    projectdata.append("---vk---");
                    sendbuff =(char *) projectdata.c_str();
                    file1.close();
                    delete[] memblock1;
                }
                goto CONTINUESERVER;
            }
//	projectdata=projectdata1;
//	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            appid= m_edit1;
            if(appid=="")
            {
                sendbuff= ("PLEASE ENTER APPLICATION ID");
                goto CONTINUESERVER;
            }

            int v= atoi(m_edit2.c_str());
            if(v>0)
            {
                versiondata=m_edit2.c_str();
            }
            if (m_check2)
            {

                sendbuff= ("THIS LICENSE COULD BE \nINSTALLED ON ANY MACHINE ONCE");

                int tdays= atoi(m_edit5.c_str());
                if(tdays>30)
                {
                    sendbuff=("TRIAL DAYS CANNOT BE MORE THAN 30");
                    goto CONTINUESERVER;
                }

                std::string triallic="";
                triallic.append(STRING_FIVE_DEFINE_NAME);
                triallic.append("--");
                triallic.append(appid);
                char bufftrial[1024]="";
                char buff[1024]="";
                triallic.append("--");
                sprintf_s(bufftrial,"%d", v);
                triallic.append(bufftrial);
                triallic.append("--");
                triallic.append("10");
                triallic.append("--");
                triallic.append(m_edit5);
                triallic.append("--");
                triallic.append("10");
                triallic.append("--");

                std::string memory= m_edit4;
                readonlydata=m_edit4;
                triallic.append(memory);
                triallic.append("--");
                sprintf_s(buff,"%d", 3);
                triallic.append(buff);
                triallic.append("--");
                std::string checkconn="LOCAL MACHINE";

                if (checkconn.find("LOCAL MACHINE")!=std::string::npos)
                {
                    WORD value1=0;
                    int j = strtol(STRING_SIX_DEFINE_NAME,NULL,10)+60;
                    ReadSectors(0,j,1,&value1,STRING_SIX_DEFINE_NAME);
                    value1=value1+1;
                    if (value1=='\0')
                    {
                        WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                    }
                    else
                    {
                        if (value1>5000)
                        {
                            WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                        }
                        else
                        {
                            WriteSectors(0,j,1,value1,STRING_SIX_DEFINE_NAME);
                        }
                    }

                    char buffc[1024]="";
                    sprintf_s(buffc,"%d", value1);
                    triallic.append(buffc);
                    triallic.append("--");
                    sprintf_s(buffc,"%d", j);
                    triallic.append(buffc);
                    triallic.append("--");

                    size_t found = memory.find("--");
                    if (found!=std::string::npos)
                    {
                        triallic= "";
                    }

                    found = memory.find("%");
                    if (found!=std::string::npos)
                    {
                        triallic= "";
                    }
                    found = appid.find("--");
                    if (found!=std::string::npos)
                    {
                        triallic= "";
                    }

                    found = appid.find("%");
                    if (found!=std::string::npos)
                    {
                        triallic= "";
                    }

                    if(triallic=="" )
                    {
                        sendbuff= ("-- or % NOT ALLOWED");
                    }
                    else
                    {
                        _mkdir("C:\\Program Files\\Bastion");
                        string filename1= "C:\\Program Files\\Bastion\\";
                        filename1.append(keyID);
                        filename1.append(".upd");

                        if(filename1=="")
                        {
                            sendbuff=( "file I/O error");
                            goto CONTINUESERVER;
                        }

                        std::string mac[10]= {""};
                        std::string runtime = DISKID(aes, &mac[0]);

                        int checkvalue=0;
                        while (checkvalue==0)
                        {

                            aes= aes_encrypt(triallic, STRING_TWO_DEFINE_NAME);
                            aes.append("%%%");
                            aes=aes.c_str();
                            if(aes.find("%%%")!=std::string::npos)
                            {
                                aes= aes.substr(0, aes.find("%%%"));
                                checkvalue=1;
                            }
                        }



                        WORD licval1=0;

                        ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);
                        if (licval1>0 || (USBlock==1))
                        {
                            std::ofstream ifile;
                            ifile.open(filename1,ios::out|ios::binary );
                            ifile.seekp (0, ios::beg);
                            ifile.write (&aes[0],strlen(aes.c_str()));

                            size_t debug=0;
                            size_t debug1=0;
                            debug=strlen(aes.c_str())+1;
                            debug1 =aes.length()+1;
                            while (debug<debug1)
                            {
                                cout<<debug;

                                ifile.write("\0",1);
                                ifile.write(&aes[debug],strlen(&aes[debug]) );
                                debug=strlen(&aes[debug])+debug+1;
                            }

                            ////////////////////////////////////////////////////////////////////////

                            ifile.write("%%%",3);
                            //////////////////////////////////////////////////////////////////////////////////
                            ifile.close();

                            /*	WriteSectors(0,58,1,licval1-1);
                            	MessageBox("LICENSE UPDATE FILE CREATED","DONE",0);
                            	EndDialog(0);
                            	return;
                            	}*/

                            _mkdir("C:/Users");
                            _mkdir("C:/Users/Public");
                            _mkdir("C:/Users/Public/Application Data");
                            _mkdir("C:/Users/Public/Application Data/Bastion");
                            SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
                            std::ofstream ifilec;
                            ifilec.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::out|ios::binary);
//	ifile.seekp(ios::ate);
                            char bufflic[1024]="";
                            sprintf_s(bufflic,"%d", licval1-1);
                            lockcode=bufflic;
                            std::string runtime = getHardDriveComputerID ();
                            lockcode= aes_encrypt(lockcode, STRING_FOUR_DEFINE_NAME+runtime);

                            if (ifilec.is_open())
                            {
                                ifilec.seekp (0, ios::beg);
                                ifilec.write (&lockcode[0],strlen(lockcode.c_str()));

                                size_t debug=0;
                                size_t debug1=0;
                                debug=strlen(lockcode.c_str())+1;
                                debug1 =lockcode.length()+1;
                                while (debug<debug1)
                                {
//	cout<<debug;

                                    ifilec.write("\0",1);
                                    ifilec.write(&lockcode[debug],strlen(&lockcode[debug]) );
                                    debug=strlen(&lockcode[debug])+debug+1;
                                }

                                ifilec.close();
                            }
                            else
                            {

                                sendbuff= ("FILE I/O ERROR");

                                goto CONTINUESERVER;
                            }
                            if(USBlock!=1)
                                WriteSectors(0,58,1,licval1-1,STRING_SIX_DEFINE_NAME);
                            sendbuff=("LICENSE UPDATE FILE CREATED");
                            USBID="";
                            std:: string files = "C:/Users/Public/Application Data/Bastion/AG_Online.agl";

                            std::ofstream ifiles;
                            if(projectdata2!="")
                            {
                                projectdata2.append("---vk---");
                            }
                            projectdata2.append(projectdata1);

                            ifiles.open(files,ios::out|ios::binary );
                            ifiles.seekp (0, ios::beg);
                            ifiles.write (&projectdata2[0],strlen(projectdata2.c_str()));

                            debug=0;
                            debug1=0;
                            debug=strlen(projectdata2.c_str())+1;
                            debug1 =projectdata2.length()+1;
                            while (debug<debug1)
                            {
//	cout<<debug;

                                ifiles.write("\0",1);
                                ifiles.write(&projectdata2[debug],strlen(&projectdata2[debug]) );
                                debug=strlen(&projectdata2[debug])+debug+1;
                            }
                            aes.append("---vk---");
                            sendbuff = (char *)aes.c_str();

                            //////////////

                            int result =0;
                            result =SEARCHMAIL2(&email);
                            if( result!=0)
                            {
                                InsertCustomer( &email, &name,  &address, &phone);
                            }
                            result =0;
                            result =SEARCHSYSINFO2(&sysinfodata);
                            if( result!=0)
                            {
                                if(sysinfodata!="")
                                {
                                    InsertSysInfo( &email, &sysinfodata);
                                }
                            }

                            if(email!="")
                            {
                                InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                            }

                            goto CONTINUESERVER;
                        }
                        else
                        {
                            sendbuff= ("NO LICENSE - PLEASE PURCHASE LICENSES");

                            goto CONTINUESERVER;
                        }
                    }

                }


            }

            if(USBlock==0 || USBlock==2)
            {
                if(aes=="")
                {
                    sendbuff= ("SYSINFO FILE NOT LOADED");
                    goto CONTINUESERVER;
                }
            }

            memory= m_edit4;
            readonlydata=m_edit4;
            lic="";
            if (m_check)
            {
                SYSTEMTIME TimeDest;

                int startdate = m_date1;
                int startYear= m_date1year;
                int enddate = m_date2;
                int endYear= m_date2year;
                if (USBlock==0 || USBlock==2)
                {
                    lic=  getLicense (STRING_FIVE_DEFINE_NAME, appid,v, 3 ,aes,memory,startdate,startYear,enddate,endYear);
                    if (USBlock==2)
                    {
                        size_t found;

                        found = USBID.find("HID\\VID_1BC0&PID_8101");
                        if (found==std::string::npos)
                        {
                            sendbuff= ("PLEASE ENTER CORRECT USB ID");
                            goto CONTINUESERVER;
                        }
                    }
                }
                else if (USBlock==1)
                {
                    size_t found;

                    found = USBID.find("HID\\VID_1BC0&PID_8101");
                    if (found==std::string::npos)
                    {
                        sendbuff= ("PLEASE ENTER CORRECT USB ID");
                        goto CONTINUESERVER;
                    }
                    lic=  getLicense1 (STRING_FIVE_DEFINE_NAME, appid,v, 1 ,USBID,memory,startdate,startYear,enddate,endYear);
                }
            }
            else
            {
                if (USBlock==0 || USBlock==2)
                {
                    lic=  getLicense (STRING_FIVE_DEFINE_NAME, appid,v, 3 ,aes,memory);
                    if (USBlock==2)
                    {
                        size_t found;

                        found = USBID.find("HID\\VID_1BC0&PID_8101");
                        if (found==std::string::npos)
                        {
                            sendbuff = ("PLEASE ENTER CORRECT USB ID");
                            goto CONTINUESERVER;
                        }
                    }
                }

                else if (USBlock==1)
                {
                    size_t found;

                    found = USBID.find("HID\\VID_1BC0&PID_8101");
                    if (found==std::string::npos)
                    {
                        sendbuff= ("PLEASE ENTER CORRECT USB ID");
                        goto CONTINUESERVER;
                    }
                    lic=  getLicense1 (STRING_FIVE_DEFINE_NAME, appid,v, 1 ,USBID,memory);
                }
            }


            if (checkconn.find("LOCAL MACHINE")!=std::string::npos)
            {
                if(lic=="")
                {
                    sendbuff= ("-- or % NOT ALLOWED");
                    goto CONTINUESERVER ;
                }
                else
                {
                    lic.append("--");
                    WORD value1=0;
                    int j = strtol(STRING_SIX_DEFINE_NAME,NULL,10)+60;
                    ReadSectors(0,j,1,&value1,STRING_SIX_DEFINE_NAME);
                    value1=value1+1;
                    if (value1=='\0')
                    {
                        WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                    }
                    else
                    {
                        if (value1>5000)
                        {
                            WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                        }
                        else
                        {
                            WriteSectors(0,j,1,value1,STRING_SIX_DEFINE_NAME);
                        }
                    }

                    char buffc[1024]="";
                    sprintf_s(buffc,"%d", value1);
                    lic.append(buffc);
                    lic.append("--");
                    sprintf_s(buffc,"%d", j);
                    lic.append(buffc);
                    lic.append("%");
                    if (USBlock==2)
                    {

                        lic.append("---v3---");
                        lic.append(USBID.c_str());
                    }
                    _mkdir("C:\\Program Files\\Bastion");
                    string filename1= "C:\\Program Files\\Bastion\\";
                    filename1.append(keyID);
                    filename1.append(".upd");

                    if(filename1=="")
                    {
                        sendbuff = "file I/O error";
                        goto CONTINUESERVER;
                    }


                    std::string mac[10]= {""};
                    std::string runtime = DISKID(aes, &mac[0]);
                    if(m_check4)
                    {
                        lic.append("---vn---");
                        lic.append(m_edit6.c_str());
                        network = m_edit6.c_str();
                        lic.append("--");

                    }
                    if (USBlock==0 || USBlock==2)
                    {
                        int checkvalue=0;
                        while (checkvalue==0)
                        {

                            aes= aes_encrypt(lic, STRING_THREE_DEFINE_NAME+runtime);
                            aes.append("%%%");
                            aes=aes.c_str();
                            if(aes.find("%%%")!=std::string::npos)
                            {
                                aes= aes.substr(0, aes.find("%%%"));
                                checkvalue=1;
                            }
                        }

                    }
                    if (USBlock==1)
                    {
                        int checkvalue=0;
                        while (checkvalue==0)
                        {

                            aes= aes_encrypt(lic, STRING_TWO_DEFINE_NAME);
                            aes.append("%%%");
                            aes=aes.c_str();
                            if(aes.find("%%%")!=std::string::npos)
                            {
                                aes= aes.substr(0, aes.find("%%%"));
                                checkvalue=1;
                            }
                        }

                        aes.append("---v2---");
                    }

                    WORD licval1=0;

                    ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);

                    WORD licval1net=0;
                    WORD licvalnet = strtol(m_edit6.c_str(),NULL,10);
                    ReadSectors(0,59,1,&licval1net,STRING_SIX_DEFINE_NAME);
                    if(m_check4)
                    {
                        if(licval1net>=licvalnet)
                        {
                            if (licval1>0 || (USBlock==1))
                            {
                                WriteSectors(0,59,1,licval1net-licvalnet,STRING_SIX_DEFINE_NAME);
                            }
                        }
                        else
                        {
                            sendbuff=("NOT ENOUGH NETWORK LICENSES - PLEASE PURCHASE LICENSES");
                            goto CONTINUESERVER;
                        }
                    }
                    if((USBlock!=1) )
                        ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);
                    if (licval1>0 || (USBlock==1))
                    {

                        std::ofstream ifile;
                        ifile.open(filename1,ios::out|ios::binary );
                        ifile.seekp (0, ios::beg);
                        ifile.write (&aes[0],strlen(aes.c_str()));

                        size_t debug=0;
                        size_t debug1=0;
                        debug=strlen(aes.c_str())+1;
                        debug1 =aes.length()+1;
                        while (debug<debug1)
                        {
                            cout<<debug;

                            ifile.write("\0",1);
                            ifile.write(&aes[debug],strlen(&aes[debug]) );
                            debug=strlen(&aes[debug])+debug+1;
                        }

                        /////////////////////////////////


                        ////////////////////////////////////

                        ifile.close();

                        _mkdir("C:/Users");
                        _mkdir("C:/Users/Public");
                        _mkdir("C:/Users/Public/Application Data");
                        _mkdir("C:/Users/Public/Application Data/Bastion");
                        SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
                        std::ofstream ifilec;
                        ifilec.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::out|ios::binary);
//	ifile.seekp(ios::ate);
                        char bufflic[1024]="";
                        sprintf_s(bufflic,"%d", licval1-1);
                        lockcode=bufflic;
                        std::string runtime = getHardDriveComputerID ();
                        lockcode= aes_encrypt(lockcode, STRING_FOUR_DEFINE_NAME+runtime);


                        if(USBlock!=1)
                            int r = WriteSectors(0,58,1,licval1-1,STRING_SIX_DEFINE_NAME);

                        if (ifilec.is_open())
                        {
                            ifilec.seekp (0, ios::beg);
                            ifilec.write (&lockcode[0],strlen(lockcode.c_str()));

                            size_t debug=0;
                            size_t debug1=0;
                            debug=strlen(lockcode.c_str())+1;
                            debug1 =lockcode.length()+1;
                            while (debug<debug1)
                            {
//	cout<<debug;

                                ifilec.write("\0",1);
                                ifilec.write(&lockcode[debug],strlen(&lockcode[debug]) );
                                debug=strlen(&lockcode[debug])+debug+1;
                            }

                            ifilec.close();
                        }
                        else
                        {

                            sendbuff= ("FILE I/O ERROR");

                            goto CONTINUESERVER;
                        }
                        sendbuff = ("LICENSE UPDATE FILE CREATED");
                        USBID="";
                        std:: string files = "C:/Users/Public/Application Data/Bastion/AG_Online.agl";

                        std::ofstream ifiles;
                        projectdata2.append("---vk---");
                        projectdata2.append(projectdata1);

                        ifiles.open(files,ios::out|ios::binary );
                        ifiles.seekp (0, ios::beg);
                        ifiles.write (&projectdata2[0],strlen(projectdata2.c_str()));

                        debug=0;
                        debug1=0;
                        debug=strlen(projectdata2.c_str())+1;
                        debug1 =projectdata2.length()+1;
                        while (debug<debug1)
                        {
//	cout<<debug;

                            ifiles.write("\0",1);
                            ifiles.write(&projectdata2[debug],strlen(&projectdata2[debug]) );
                            debug=strlen(&projectdata2[debug])+debug+1;
                        }
                        aes.append("---vk---");
                        sendbuff = (char *)aes.c_str();

                        //////////////

                        int result =0;
                        result =SEARCHMAIL2(&email);
                        if( result!=0)
                        {
                            InsertCustomer( &email, &name,  &address, &phone);
                        }
                        result =0;
                        result =SEARCHSYSINFO2(&sysinfodata);
                        if( result!=0)
                        {
                            if(sysinfodata!="")
                            {
                                InsertSysInfo( &email, &sysinfodata);
                            }
                        }

                        if(email!="")
                        {
                            InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                        }

                        goto CONTINUESERVER;

                    }
                    else
                    {
                        sendbuff = ("NO LICENSE - PLEASE PURCHASE LICENSES");
                        goto CONTINUESERVER;
                    }

                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Echo the buffer back to the sender
CONTINUESERVER:
            std::string serverdata= sendbuff;
            aes="";
            iSendResult = send( ClientSocket[nclient], serverdata.c_str(), serverdata.length(), 0 );
            if (iSendResult == SOCKET_ERROR)
            {
                printf("send failed with error: %d\n", WSAGetLastError());
                closesocket(ClientSocket[nclient]);
                WSACleanup();

            }
            printf("Bytes sent: %d\n", iSendResult);

        }
        else if (iResult == 0)
            printf("Connection closing...\n");
        else  {
            closesocket(ClientSocket[nclient]);
            WSACleanup();

        }

    }


    return 0;
}
// Main function to be executed as entire service code.
void WINAPI ServiceMain(DWORD argc, LPTSTR *argv) {
    // Must be called at start.
    HANDLE g_clienth;
    g_ServiceStatusHandle = RegisterServiceCtrlHandlerEx(_T("AG_ONLINE"), &HandlerEx, NULL);
    // Startup code.
    ReportStatus(SERVICE_START_PENDING);
    g_StopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    g_clienth = CreateMutex(NULL,NULL,NULL);
    /* Here initialize service...    Load configuration, acquire resources etc. */
    ReportStatus(SERVICE_RUNNING);
    /* Main service code    Loop, do some work, block if nothing to do,    wait or poll for g_StopEvent... */



    HANDLE hndl;

    LPTHREAD_START_ROUTINE threadFunc= &threadfunction;

    LPVOID param=NULL;

    LPDWORD threadID= NULL;

    hndl = CreateThread(NULL,
                        0,
                        threadFunc,
                        param,
                        0,
                        threadID);

    while (WaitForSingleObject(g_StopEvent, 0) != WAIT_OBJECT_0)
    {   // This sample service does "BEEP!" every 3 seconds.
//WaitForSingleObject(g_clienth, INFINITE);

        Sleep(2000);

        // Receive until the peer shuts down the connection




//nclient++;
//ReleaseMutex(g_clienth);
    }
    ReportStatus(SERVICE_STOP_PENDING);
    /* Here finalize service...    Save all unsaved data etc., but do it quickly.    If g_SystemShutdown, you can skip freeing memory etc. */
    CloseHandle(g_StopEvent);
    ReportStatus(SERVICE_STOPPED);
}

// Standard console application entry point.
int main(int argc, char **argv) {
    SERVICE_TABLE_ENTRY serviceTable[] = {        { _T("AG_ONLINE"), &ServiceMain },        { NULL, NULL }    };

    if (StartServiceCtrlDispatcher(serviceTable))
        return 0;
    else if (GetLastError() == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT)
        return -1;
    // Program not started as a service.
    else
        return -2;
    // Other error.
}
