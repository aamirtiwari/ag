// AG_QuickProtect_cmd.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <string>

#define INJECT_ERR_NOERROR      0
#define INJECT_ERR_FILENOTFOUND 1
#define INJECT_ERR_NOTAPEFILE   2
#define INJECT_ERR_PEHEADERFULL 3

#define INJECT_FLAG_DOIMPORTS   1
#define INJECT_FLAG_JUMPTOOEP   2
#define INJECT_FLAG_HANDLERELOC 4
#define INJECT_FLAG_STRIPRELOCS 8
#define INJECT_FLAG_COMPRESSDLL 16
#define INJECT_FLAG_BACKUPTLS   32

typedef DWORD (__stdcall *INJECTFILEPROC)
(LPCSTR lpInputFile, LPCSTR lpOutputFile,
 LPCSTR lpDllFile, LPVOID lpExtraData,
 DWORD dwExtraDataSize, DWORD dwFlags);

int _tmain(int argc, _TCHAR* argv[])
{
    if(argc>3)
    {
        HMODULE hPeinjectDll;
        INJECTFILEPROC InjectFile;
        DWORD result;

        std::string filename=argv[1];
        std::string intermediate=argv[2];
        std::string blwrk=argv[3];
        std::string appid=argv[4];
        std::string Pdll=argv[5];
        hPeinjectDll = LoadLibrary(Pdll.c_str());

        if (!hPeinjectDll) {
            Sleep(1000);
            hPeinjectDll = LoadLibrary(Pdll.c_str());

            if (!hPeinjectDll) {
                Sleep(1000);
                hPeinjectDll = LoadLibrary(Pdll.c_str());

                if (!hPeinjectDll) {
                    MessageBox(NULL,"Failed to load PEinject64.dll","ERROR",MB_ICONERROR);
                    return 1;
                }
            }
        }

        InjectFile =
            (INJECTFILEPROC)GetProcAddress(hPeinjectDll,
                                           "InjectFile");

        if (!InjectFile) {
            MessageBox(NULL,"InjectFile() not found","ERROR",MB_ICONERROR);
            FreeLibrary(hPeinjectDll);
            return 1;
        }
        result = InjectFile(filename.c_str(), intermediate.c_str(),
                            blwrk.c_str(), &appid[0], appid.length(),
                            INJECT_FLAG_DOIMPORTS
                            | INJECT_FLAG_JUMPTOOEP
                            | INJECT_FLAG_HANDLERELOC
                            | INJECT_FLAG_BACKUPTLS);
        return result;
        if (result) {

            // one of the INJECT_ERR_ constants
            MessageBox(NULL,"An error occured","ERROR",MB_ICONERROR);
            exit(0);
            return result;
        }
    }
    else
    {
        MessageBox(NULL,"Incorrect Argument", "Error",MB_ICONERROR);
        return 1;
    }
}

