#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "71MrcCQSlT0iDxHBG/q+Vug5OAZIJ93hLXajymEpWsUnot4vk68KdwFRYzePN2bf"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("D+c6D+c6D+c6D+c6D+c6DL7=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("DjTXO+lRHrJkDrc8O+uyHQCXOjc8O+76DjJYxFCXDjl7")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("DjTXO+lRHrJ7")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("DjJYxFCXDjl7")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("GmuDudCqq81g/Vzc+wlLDG7=")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("DG7=")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif