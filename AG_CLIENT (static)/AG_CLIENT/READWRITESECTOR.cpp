
#include "stdafxnew.h"
#include "READWRITESECTOR.h"


typedef struct __MBR_
{
    BYTE                 boot_code[440];
    DWORD                disk_signature; //4 bitai
    WORD                 unknown;
    PARTITION_RECORD     partition_table[4];
    WORD                 signature;          /*AA55*/
} MBR;
#pragma pack(16)
MBR mbr;

using namespace std;
const char *disk_default="\\\\.\\PhysicalDrive0";
WORD begin_cylinder,end_cylinder;

int ReadSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD *value)

{
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox(NULL,"Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return 1;
    }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
    struct
    {
        DWORD StartingSector ;
        WORD NumberOfSectors ;
        DWORD pBuffer;
    } ControlBlock;
#pragma pack ()

#pragma pack (1)
    typedef struct _DIOC_REGISTERS
    {
        DWORD reg_EBX;
        DWORD reg_EDX;
        DWORD reg_ECX;
        DWORD reg_EAX;
        DWORD reg_EDI;
        DWORD reg_ESI;
        DWORD reg_Flags;
    } DIOC_REGISTERS ;
#pragma pack ()

    char* buffer = (char*)malloc (512*numberofsectors);
    HANDLE hDevice ;
    DIOC_REGISTERS reg ;
    BOOL  fResult ;
    DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
    hDevice = CreateFile ( "\\\\.\\vwin32",
                           0,
                           0,
                           NULL,
                           0,
                           FILE_FLAG_DELETE_ON_CLOSE,
                           NULL );

    if ( hDevice == INVALID_HANDLE_VALUE )
    {
        // win NT/2K/XP code
        HANDLE hDevice;
        DWORD bytesread;

        const char * _devicename = disk_default;
        //_devicename[4] += drive;

        // Creating a handle to disk drive using CreateFile () function ..
        hDevice = CreateFile(_devicename,
                             GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
                             NULL, OPEN_EXISTING, 0, NULL);

        if (hDevice == INVALID_HANDLE_VALUE)
            return NULL;

        // Setting the pointer to point to the start of the sector we want to read ..
        MBR mbr;
        MBR *_mbr=&mbr;
        memset(_mbr,0,sizeof(_mbr));
        SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
        if (!ReadFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL) )
            return NULL;

        *value=_mbr->unknown;
    }
    else
    {
        // code for win 95/98
        ControlBlock.StartingSector = (DWORD)startinglogicalsector;
        ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
        ControlBlock.pBuffer = (DWORD)buffer ;

        //-----------------------------------------------------------
        // SI contains read/write mode flags
        // SI=0h for read and SI=1h for write
        // CX must be equal to ffffh for
        // int 21h's 7305h extention
        // DS:BX -> base addr of the
        // control block structure
        // DL must contain the drive number
        // (01h=A:, 02h=B: etc)
        //-----------------------------------------------------------

        reg.reg_ESI = 0x00;
        reg.reg_ECX = -1 ;
        reg.reg_EBX = (DWORD)(&ControlBlock);
        reg.reg_EDX = drive+1;
        reg.reg_EAX = 0x7305 ;

        //  6 == VWIN32_DIOC_DOS_DRIVEINFO
        fResult = DeviceIoControl ( hDevice,
                                    6,
                                    &(reg),
                                    sizeof (reg),
                                    &(reg),
                                    sizeof (reg),
                                    &cb,
                                    0);

        if (!fResult || (reg.reg_Flags & 0x0001)) return NULL;
    }
    free(buffer);
    CloseHandle(hDevice);

    return 0;
}

int WriteSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD value)
{
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox(NULL,"Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return 1;
    }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
    struct
    {
        DWORD StartingSector ;
        WORD NumberOfSectors ;
        DWORD pBuffer;
    } ControlBlock;
#pragma pack ()

#pragma pack (1)
    typedef struct _DIOC_REGISTERS
    {
        DWORD reg_EBX;
        DWORD reg_EDX;
        DWORD reg_ECX;
        DWORD reg_EAX;
        DWORD reg_EDI;
        DWORD reg_ESI;
        DWORD reg_Flags;
    } DIOC_REGISTERS ;
#pragma pack ()

    char* buffer = (char*)malloc (512*numberofsectors);
    HANDLE hDevice ;
    DIOC_REGISTERS reg ;
    BOOL  fResult ;
    DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
    hDevice = CreateFile ( "\\\\.\\vwin32",
                           0,
                           0,
                           NULL,
                           0,
                           FILE_FLAG_DELETE_ON_CLOSE,
                           NULL );

    if ( hDevice == INVALID_HANDLE_VALUE )
    {
        // win NT/2K/XP code
        HANDLE hDevice;
        DWORD bytesread;

        const char * _devicename = disk_default;
        //_devicename[4] += drive;

        // Creating a handle to disk drive using CreateFile () function ..
        hDevice = CreateFile(_devicename,
                             GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                             NULL,OPEN_EXISTING, 0, NULL);

        if (hDevice == INVALID_HANDLE_VALUE)
            return NULL;

        // Setting the pointer to point to the start of the sector we want to read ..
        SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
        MBR mbr;
        MBR *_mbr=&mbr;
        memset(_mbr,0,sizeof(_mbr));
        _mbr->unknown =value;
        int n =WriteFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL);
        if (!n )
            return NULL;
    }
    else
    {
        // code for win 95/98
        ControlBlock.StartingSector = (DWORD)startinglogicalsector;
        ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
        ControlBlock.pBuffer = (DWORD)buffer ;

        //-----------------------------------------------------------
        // SI contains read/write mode flags
        // SI=0h for read and SI=1h for write
        // CX must be equal to ffffh for
        // int 21h's 7305h extention
        // DS:BX -> base addr of the
        // control block structure
        // DL must contain the drive number
        // (01h=A:, 02h=B: etc)
        //-----------------------------------------------------------

        reg.reg_ESI = 0x00;
        reg.reg_ECX = -1 ;
        reg.reg_EBX = (DWORD)(&ControlBlock);
        reg.reg_EDX = drive+1;
        reg.reg_EAX = 0x7305 ;

        //  6 == VWIN32_DIOC_DOS_DRIVEINFO
        fResult = DeviceIoControl ( hDevice,
                                    6,
                                    &(reg),
                                    sizeof (reg),
                                    &(reg),
                                    sizeof (reg),
                                    &cb,
                                    0);

        if (!fResult || (reg.reg_Flags & 0x0001)) return NULL;
    }
    free(buffer);
    CloseHandle(hDevice);
    return 0;
}