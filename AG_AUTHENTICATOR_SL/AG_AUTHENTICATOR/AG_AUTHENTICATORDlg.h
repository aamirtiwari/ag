
// AG_AUTHENTICATORDlg.h : header file
//

#pragma once


// CAG_AUTHENTICATORDlg dialog
class CAG_AUTHENTICATORDlg : public CDialogEx
{
// Construction
public:
	CAG_AUTHENTICATORDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_AG_AUTHENTICATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit1;
	CEdit m_edit2;
	CEdit m_edit3;
	void ProcessEvents();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton1();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	CEdit m_edit4;
	CDateTimeCtrl m_date1;
	CDateTimeCtrl m_date2;
//	int m_radio;
//	CButton m_radio;
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio1();
	CButton m_check;
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnEnChangeEdit2();
	CEdit m_edit5;
	CButton m_check2;
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnProjectOpen();
	afx_msg void OnProjectSaveas();
	afx_msg void OnProjectResetall();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnServerConnectto();
	afx_msg void OnServerCreateaccount();
	CEdit m_edit7;
	afx_msg void OnBnClickedButton4();
	CEdit m_edit8;
	afx_msg void OnBnClickedCheck4();
	CButton m_check4;
	CEdit m_edit9;
	CEdit m_edit10;
	afx_msg void OnCustomerCreatenew();
	afx_msg void OnCustomerLinktoexisting();
	afx_msg void OnBnClickedCheck5();
	CButton m_checkcust;
	afx_msg void OnUsbdongleFormat();
	afx_msg void OnCustomerHelp();
};
