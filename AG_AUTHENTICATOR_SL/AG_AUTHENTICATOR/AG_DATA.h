// AG_DATA.h : main header file for the AG_DATA DLL
//
#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <string>


#define AUTHGURU_NET_API __declspec(dllexport)


using std::string;

AUTHGURU_NET_API int InsertCustomer(std::string email[1],std::string nam1[1], std::string address[1], std::string phone[1]);
AUTHGURU_NET_API int InsertLicInfo(std::string sysinfo[1],std::string appid[1],std::string version[1], std::string sdate[1], std::string edate[1], std::string network[1],std::string locking[1],std::string usbid[1],std::string readonly[1]);
AUTHGURU_NET_API int InsertSysInfo(std::string email[1],std::string nam1[1]);
AUTHGURU_NET_API int ShowAge(std::string nam1[1]);
AUTHGURU_NET_API char * SEARCHMAIL(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API char * SEARCHPHONE(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API char * SEARCHNAME(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API char * SEARCHLICENSE(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API char * SEARCHADDRESS(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API int SEARCHMAIL2(std::string nam1[1]);
AUTHGURU_NET_API char * SEARCHSYSINFO(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API char * SEARCHID(std::string nam1[1], char * returnstr);
AUTHGURU_NET_API int SEARCHSYSINFO2(std::string nam1[1]);
AUTHGURU_NET_API int DELETECUSTOMER(std::string nam1[1]);