#if !defined __SmartX1LiteAPI_H__
#define __SmartX1LiteAPI_H__

#ifdef __cplusplus 
extern "C" 
{
#endif	

 	 long  SmartX1LiteFind(char appName[32]);

	 long  SmartX1LiteOpen(char * password);
	
	 long  SmartX1LiteGetUid(char * hardwareID);

	 long  SmartX1LiteReadStorage(long address, long length, unsigned char * pBuffer);

	 long  SmartX1LiteWriteStorage(long address, long length, unsigned char * pBuffer);
	
	 long  SmartX1LiteClose();
	

#ifdef __cplusplus 
}

#endif



#endif