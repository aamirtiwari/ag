//C++ HEAADER FOR AUTHGURU CLIENT APIs

#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using std::string;

typedef int AG_STATUS;
//STATUS CODES

#define AG_SUCCESS					0
#define AG_FILE_IO_ERROR			1
#define AG_NO_LICENSE				2
#define AG_DATE_TAMPERED			3
#define AG_EXPIRED					4
#define AG_INVALID_LICENSE			5
#define AG_EMPTY_MEMORY				6
#define AG_INVALID_MEMORY			7
#define AG_TERMINAL_SESSION			8
#define AG_NO_KEY					9
//MEMORY TYPE
#define AG_READ_ONLY				0
#define AG_READ_WRITE				1

//APIs
extern "C" int __cdecl AG_VerifyVendor(std::string *out);
extern "C" int __cdecl AG_IsAuthentic (std::string *Handle,std::string *AppId, int version);
extern "C" int __cdecl AG_Read (std::string *Handle,int memType, std::string *Data);
extern "C" int __cdecl AG_Write (std::string *Handle,std::string *Data);
extern "C" int __cdecl AG_Encrypt (std::string *in, std::string *out);
extern "C" int __cdecl AG_Decrypt (std::string *in,std::string *out);
extern "C" int __cdecl AG_QuickProtect (std::string *AppId, int version, int backgroundcheck=60);
extern "C" int __cdecl AG_AllowRemote (std::string *Handle,BOOL flag);
extern "C" int __cdecl AG_CheckUSB (std::string *ID);
extern "C" int __cdecl AG_code_32(char * lpFileName1);
extern "C" int __cdecl AG_APPDATA(std::string in[1]);
extern "C" std::string __cdecl AG_Error(int status1);