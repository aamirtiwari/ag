// Account.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include " Customer1.h"
#include "afxdialogex.h"


// Account dialog

IMPLEMENT_DYNAMIC( Customer1, CDialogEx)

 Customer1:: Customer1(CWnd* pParent /*=NULL*/)
	: CDialogEx( Customer1::IDD, pParent)
{

}

 Customer1::~ Customer1()
{
}

void  Customer1::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
	CDialogEx::SetBackgroundColor(color);
}
void  Customer1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE1, m_tree);
}

BOOL  Customer1::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	SetBackGroundColor(RGB(192,192,192));
		HTREEITEM	hItem1[4];
		hItem1[0]=m_tree.InsertItem("EMAIL : abc@xyz.com",0,0);
		hItem1[1]=m_tree.InsertItem("NAME : abc",1,1,hItem1[0]);
		  m_tree.Expand(hItem1[0],TVE_EXPAND);
return TRUE;
}
BEGIN_MESSAGE_MAP( Customer1, CDialogEx)
	ON_BN_CLICKED(IDOK, & Customer1::OnBnClickedOk)
END_MESSAGE_MAP()


// Account message handlers


void  Customer1::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}
