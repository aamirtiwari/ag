
// AG_VENDOR_LICENSESDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_VENDOR_LICENSES.h"
#include "AG_VENDOR_LICENSESDlg.h"
#include "afxdialogex.h"
#include "READWRITESECTOR.h"
#include <direct.h>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include "Encrypt_decrypt.h"
#include "filedlg.h"
#include "MAC.h"
#include "HARDDISKID.h"
#include "DATE_YEAR.h"
#include "DIVIDEINFO.h"
#include "Stringobf.h"
#include "resource.h"
#include "Sense_LC.h"
#include "Encrypt_decrypt.h"
#include "SmartX1LiteApi.h"
typedef struct __MBR_
{
    BYTE                 boot_code[440];
    DWORD                disk_signature; //4 bitai
    WORD                 unknown;
    PARTITION_RECORD     partition_table[4];
    WORD                 signature;          /*AA55*/
} MBR;
#pragma pack(16)
MBR mbr;

using namespace std;
const char *disk_default="\\\\.\\PhysicalDrive0";
WORD begin_cylinder,end_cylinder;

int ReadSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD *value, std::string key)

{
    int checkv= strtol(key.c_str(),NULL,10);
    if((startinglogicalsector == 58) || (startinglogicalsector==59) || (startinglogicalsector ==checkv))
    {
        std::string lockcode="";

        int res=0,k=0;
        res = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
        if(!res) {

            res =SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = SmartX1ReadStorage( 0, 256,outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }
                SmartX1Close();
            }
        }

        lc_handle_t handle;
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_read(handle, 0, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            LC_close(handle);
        }

        if(lockcode=="")
        {
            MessageBox(NULL,"BASTION LICENSE KEY NOT FOUND, PLEASE PURCHASE KEY","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }
        std::string dec = aes_decrypt(lockcode, key);
        std::string temp="";
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value1= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value2= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        int value3= strtol(temp.c_str(),NULL,10);
        if(startinglogicalsector==58)
        {
            *value=value1;
            return value1;
        }
        if(startinglogicalsector==59)
        {
            *value=value2;
            return value2;
        }
        else
        {
            *value=value3;
            return value3;
        }
    }
    else
    {
        BOOL bDebugged = FALSE;
        CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
        if( bDebugged )
        {
            MessageBox(NULL,"Being debugged!","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
        struct
        {
            DWORD StartingSector ;
            WORD NumberOfSectors ;
            DWORD pBuffer;
        } ControlBlock;
#pragma pack ()

#pragma pack (1)
        typedef struct _DIOC_REGISTERS
        {
            DWORD reg_EBX;
            DWORD reg_EDX;
            DWORD reg_ECX;
            DWORD reg_EAX;
            DWORD reg_EDI;
            DWORD reg_ESI;
            DWORD reg_Flags;
        } DIOC_REGISTERS ;
#pragma pack ()

        char* buffer = (char*)malloc (512*numberofsectors);
        HANDLE hDevice ;
        DIOC_REGISTERS reg ;
        BOOL  fResult ;
        DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
        hDevice = CreateFile ( "\\\\.\\vwin32",
                               0,
                               0,
                               NULL,
                               0,
                               FILE_FLAG_DELETE_ON_CLOSE,
                               NULL );

        if ( hDevice == INVALID_HANDLE_VALUE )
        {
            // win NT/2K/XP code
            HANDLE hDevice;
            DWORD bytesread;

            const char * _devicename = disk_default;
            //_devicename[4] += drive;

            // Creating a handle to disk drive using CreateFile () function ..
            hDevice = CreateFile(_devicename,
                                 GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                 NULL, OPEN_EXISTING, 0, NULL);

            if (hDevice == INVALID_HANDLE_VALUE)
                return 1;

            // Setting the pointer to point to the start of the sector we want to read ..
            MBR mbr;
            MBR *_mbr=&mbr;
            memset(_mbr,0,sizeof(_mbr));
            SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
            if (!ReadFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL) )
                return 1;

            *value=_mbr->unknown;
        }
        else
        {
            // code for win 95/98
            ControlBlock.StartingSector = (DWORD)startinglogicalsector;
            ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
            ControlBlock.pBuffer = (DWORD)buffer ;

            //-----------------------------------------------------------
            // SI contains read/write mode flags
            // SI=0h for read and SI=1h for write
            // CX must be equal to ffffh for
            // int 21h's 7305h extention
            // DS:BX -> base addr of the
            // control block structure
            // DL must contain the drive number
            // (01h=A:, 02h=B: etc)
            //-----------------------------------------------------------

            reg.reg_ESI = 0x00;
            reg.reg_ECX = -1 ;
            reg.reg_EBX = (DWORD)(&ControlBlock);
            reg.reg_EDX = drive+1;
            reg.reg_EAX = 0x7305 ;

            //  6 == VWIN32_DIOC_DOS_DRIVEINFO
            fResult = DeviceIoControl ( hDevice,
                                        6,
                                        &(reg),
                                        sizeof (reg),
                                        &(reg),
                                        sizeof (reg),
                                        &cb,
                                        0);

            if (!fResult || (reg.reg_Flags & 0x0001)) return NULL;
        }
        free(buffer);
        CloseHandle(hDevice);

        return 0;
    }
}

int WriteSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD value, std::string key)
{
    int checkv= strtol(key.c_str(),NULL,10);
    if((startinglogicalsector == 58) || (startinglogicalsector==59) || (startinglogicalsector ==checkv))
    {
        std::string lockcode="";
        lc_handle_t handle;
        int res=0,k=0;

        long nRet = 0;


        res = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
        if(!res) {

            res =SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = SmartX1ReadStorage(0, 256, (unsigned char *)outdata);
                std::string checkstring = (char *)outdata;
                if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                {
                    checkstring= checkstring.substr(0,checkstring.find("---"));
                    lockcode= checkstring.c_str();
                }


                SmartX1Close();
            }
        }
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_read(handle, 0, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            LC_close(handle);
        }

        if(lockcode=="")
        {
            MessageBox(NULL,"BASTION LICENSE KEY NOT FOUND, PLEASE PURCHASE KEY","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }
        std::string dec = aes_decrypt(lockcode, key);
        std::string temp="";
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value1= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value2= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        int value3= strtol(temp.c_str(),NULL,10);
        if(startinglogicalsector==58)
            value1 = value;
        if(startinglogicalsector==59)
            value2=value;

        char buff[20];
        sprintf_s(buff,"%d",value1);
        temp = buff;
        temp=temp.c_str();
        temp=temp.append("---");

        sprintf_s(buff,"%d",value2);
        temp=temp.append(buff);
        temp=temp.c_str();
        temp=temp.append("---");

        sprintf_s(buff,"%d",value3);
        temp=temp.append(buff);
        temp=temp.c_str();
        temp=temp.append("---");

        int checkvalue=0;
        while (checkvalue==0)
        {
            dec= aes_encrypt(temp, key);
            dec.append("%%%");
            dec=dec.c_str();
            if(dec.find("%%%")!=std::string::npos)
            {
                dec= dec.substr(0, dec.find("%%%"));
                checkvalue=1;
            }
        }
        dec.append("---");

        res = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
        if(!res) {

            res =SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = SmartX1WriteStorage( 0, 256,(unsigned char *)dec.c_str());
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }
                SmartX1Close();
            }
        }
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_write(handle, 0, (unsigned char *)dec.c_str());
                LC_close(handle);
                return 0;

            }
            LC_close(handle);
        }
        else
        {
            MessageBox(NULL,"BASTION LICENSE KEY NOT FOUND, PLEASE PURCHASE KEY","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }
    }
    else
    {
        BOOL bDebugged = FALSE;
        CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
        if( bDebugged )
        {
            MessageBox(NULL,"Being debugged!","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
        struct
        {
            DWORD StartingSector ;
            WORD NumberOfSectors ;
            DWORD pBuffer;
        } ControlBlock;
#pragma pack ()

#pragma pack (1)
        typedef struct _DIOC_REGISTERS
        {
            DWORD reg_EBX;
            DWORD reg_EDX;
            DWORD reg_ECX;
            DWORD reg_EAX;
            DWORD reg_EDI;
            DWORD reg_ESI;
            DWORD reg_Flags;
        } DIOC_REGISTERS ;
#pragma pack ()

        char* buffer = (char*)malloc (512*numberofsectors);
        HANDLE hDevice ;
        DIOC_REGISTERS reg ;
        BOOL  fResult ;
        DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
        hDevice = CreateFile ( "\\\\.\\vwin32",
                               0,
                               0,
                               NULL,
                               0,
                               FILE_FLAG_DELETE_ON_CLOSE,
                               NULL );

        if ( hDevice == INVALID_HANDLE_VALUE )
        {
            // win NT/2K/XP code
            HANDLE hDevice;
            DWORD bytesread;

            const char * _devicename = disk_default;
            //_devicename[4] += drive;

            // Creating a handle to disk drive using CreateFile () function ..
            hDevice = CreateFile(_devicename,
                                 GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                 NULL,OPEN_EXISTING, 0, NULL);

            if (hDevice == INVALID_HANDLE_VALUE)
                return 1;

            // Setting the pointer to point to the start of the sector we want to read ..
            SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
            MBR mbr;
            MBR *_mbr=&mbr;
            memset(_mbr,0,sizeof(_mbr));
            _mbr->unknown =value;
            int n =WriteFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL);
            if (!n )
                return 1;
        }
        else
        {
            // code for win 95/98
            ControlBlock.StartingSector = (DWORD)startinglogicalsector;
            ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
            ControlBlock.pBuffer = (DWORD)buffer ;

            //-----------------------------------------------------------
            // SI contains read/write mode flags
            // SI=0h for read and SI=1h for write
            // CX must be equal to ffffh for
            // int 21h's 7305h extention
            // DS:BX -> base addr of the
            // control block structure
            // DL must contain the drive number
            // (01h=A:, 02h=B: etc)
            //-----------------------------------------------------------

            reg.reg_ESI = 0x00;
            reg.reg_ECX = -1 ;
            reg.reg_EBX = (DWORD)(&ControlBlock);
            reg.reg_EDX = drive+1;
            reg.reg_EAX = 0x7305 ;

            //  6 == VWIN32_DIOC_DOS_DRIVEINFO
            fResult = DeviceIoControl ( hDevice,
                                        6,
                                        &(reg),
                                        sizeof (reg),
                                        &(reg),
                                        sizeof (reg),
                                        &cb,
                                        0);

            if (!fResult || (reg.reg_Flags & 0x0001)) return 1;
        }
        free(buffer);
        CloseHandle(hDevice);
        return 0;
    }
}
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAG_VENDOR_LICENSESDlg dialog




CAG_VENDOR_LICENSESDlg::CAG_VENDOR_LICENSESDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAG_VENDOR_LICENSESDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAG_VENDOR_LICENSESDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_BUTTON1, m_button1);
    DDX_Control(pDX, IDC_EDIT2, m_edit2);
}

BEGIN_MESSAGE_MAP(CAG_VENDOR_LICENSESDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON2, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton2)
    ON_BN_CLICKED(IDC_BUTTON3, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton3)
    ON_BN_CLICKED(IDC_BUTTON1, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_BUTTON4, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton4)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAG_VENDOR_LICENSESDlg message handlers
void CAG_VENDOR_LICENSESDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}
HBRUSH CAG_VENDOR_LICENSESDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL CAG_VENDOR_LICENSESDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);
    SetBackGroundColor(RGB(0,114,148));

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox("Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return true;
    }
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    if ( 	__argc >1 )
    {
        m_button1.EnableWindow(TRUE)	;
    }
    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_VENDOR_LICENSESDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_VENDOR_LICENSESDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_VENDOR_LICENSESDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

using namespace std;

void CAG_VENDOR_LICENSESDlg::OnBnClickedButton2()
{
    // TODO: Add your control notification handler code here
    WORD value=0;
    char buff[1024]="";

    ReadSectors(0,58,1,&value,STRING_SIX_DEFINE_NAME);
    if(value =='\0')
    {

        WriteSectors(0,58,1,0,STRING_SIX_DEFINE_NAME);
        sprintf_s(buff,"%d", value);
        m_edit1.SetWindowTextA(buff);
    }


    else
    {
        sprintf_s(buff,"%d", value);

        m_edit1.SetWindowTextA(buff);

    }

}


void CAG_VENDOR_LICENSESDlg::OnBnClickedButton3()
{
    // TODO: Add your control notification handler code here
    filedlg fdlg(false,"info","purchase",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);

    fdlg.DoModal();
    string file = "";
    file = fdlg.GetFileName();

    std::ofstream ifile;

    if(file=="")
    {

        return;
    }

    file = fdlg.GetPathName();

    ifile.open(file,ios::out|ios::binary );
    string HardDriveSerialNumbers1="";
    string macid1;
    HardDriveSerialNumbers1 = getHardDriveComputerID ();
    // TODO: Add extra initialization here




    macid1 = GetMACaddress ();

    HardDriveSerialNumbers1.append("--");
    HardDriveSerialNumbers1.append(macid1);

    WORD  value=0;

    WORD date=0;
    WORD year=0;
    getdate_year( &date,  &year);

    ////YEAR
    if (ifile.is_open())
    {

        int r = ReadSectors(0,56,1,&value,STRING_SIX_DEFINE_NAME);
        if((value =='\0')|| value==year)
        {

            WriteSectors(0,56,1,year,STRING_SIX_DEFINE_NAME);

            r = ReadSectors(0,55,1,&value,STRING_SIX_DEFINE_NAME);
            if( value =='\0'|| (value%100)<=(date%100) || (value%100)>12 || (value/100)>31)
            {
                if ((value%100)!=(date%100) || value =='\0' || (value%100)>12 || (value/100)>31 )
                {
                    WriteSectors(0,55,1,date,STRING_SIX_DEFINE_NAME);

                    ////DATE


                    char buff[1024]="";
                    sprintf_s(buff,"%d", date);
                    HardDriveSerialNumbers1.append(buff);
                    HardDriveSerialNumbers1.append("--");
                    sprintf_s(buff,"%d", year);
                    HardDriveSerialNumbers1.append(buff);

                    HardDriveSerialNumbers1.append("--");
                }
                else if((value%100)==(date%100) || (value%100)>12 || (value/100)>31 )
                {
                    if((value/100)<=(date/100)|| (value%100)>12 || (value/100)>31)
                    {
                        WriteSectors(0,55,1,date,STRING_SIX_DEFINE_NAME);

                        ////DATE


                        char buff[1024]="";
                        sprintf_s(buff,"%d", date);
                        HardDriveSerialNumbers1.append(buff);
                        HardDriveSerialNumbers1.append("--");
                        sprintf_s(buff,"%d", year);
                        HardDriveSerialNumbers1.append(buff);

                        HardDriveSerialNumbers1.append("--");
                    }
                    else
                    {
                        r = ReadSectors(0,55,1,&value,STRING_SIX_DEFINE_NAME);
                        MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
                        char buff[1024]="";
                        sprintf_s(buff,"%d", value);
                        HardDriveSerialNumbers1.append(buff);
                        HardDriveSerialNumbers1.append("--");

                        int r = ReadSectors(0,56,1,&value,STRING_SIX_DEFINE_NAME);
                        sprintf_s(buff,"%d", value);
                        HardDriveSerialNumbers1.append(buff);

                        HardDriveSerialNumbers1.append("--");
                    }
                }

            }
            else
            {
                r = ReadSectors(0,55,1,&value,STRING_SIX_DEFINE_NAME);
                MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
                char buff[1024]="";
                sprintf_s(buff,"%d", value);
                HardDriveSerialNumbers1.append(buff);
                HardDriveSerialNumbers1.append("--");

                int r = ReadSectors(0,56,1,&value,STRING_SIX_DEFINE_NAME);
                sprintf_s(buff,"%d", value);
                HardDriveSerialNumbers1.append(buff);

                HardDriveSerialNumbers1.append("--");
            }

        }

        else if(value<year)
        {
            WriteSectors(0,56,1,year,STRING_SIX_DEFINE_NAME);
            WriteSectors(0,55,1,date,STRING_SIX_DEFINE_NAME);

            ////DATE


            char buff[1024]="";
            sprintf_s(buff,"%d", date);
            HardDriveSerialNumbers1.append(buff);
            HardDriveSerialNumbers1.append("--");
            sprintf_s(buff,"%d", year);
            HardDriveSerialNumbers1.append(buff);

            HardDriveSerialNumbers1.append("--");
        }

        else
        {
            r = ReadSectors(0,55,1,&value,STRING_SIX_DEFINE_NAME);
            MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
            char buff[1024]="";
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);
            HardDriveSerialNumbers1.append("--");

            int r = ReadSectors(0,56,1,&value,STRING_SIX_DEFINE_NAME);
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);

            HardDriveSerialNumbers1.append("--");

        }

        ReadSectors(0,57,1,&value,STRING_SIX_DEFINE_NAME);
        if(value =='\0')
        {
            WriteSectors(0,57,1,0,STRING_SIX_DEFINE_NAME);
            char buff[1024]="";
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);
            //	HardDriveSerialNumbers1.append("--");
        }
        else
        {

            char buff[1024]="";
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);

            //	HardDriveSerialNumbers1.append("--");
        }
        string aes;
        aes= aes_encrypt(HardDriveSerialNumbers1, STRING_TWO_DEFINE_NAME);


        ifile.seekp (0, ios::beg);
        ifile.write (&aes[0],strlen(aes.c_str()));

        size_t debug=0;
        size_t debug1=0;
        debug=strlen(aes.c_str())+1;
        debug1 =aes.length()+1;
        while (debug<debug1)
        {
//	cout<<debug;

            ifile.write("\0",1);
            ifile.write(&aes[debug],strlen(&aes[debug]) );
            debug=strlen(&aes[debug])+debug+1;
        }

        ifile.close();
        MessageBox("PURCHASE FILE WRITTEN","DONE",MB_ICONINFORMATION);
        EndDialog(0);
    }

    else  {
        MessageBox("FILE IN USE OR NOT ENOUGH PERMISSION","ERROR",MB_ICONERROR);
        return;
    }
}


void CAG_VENDOR_LICENSESDlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here
    std::string lockcode = "";
    std::string file="";
//	filedlg fdlg(true,"upd","update",OFN_HIDEREADONLY |OF_PROMPT ,"*.upd",NULL);

//	fdlg.DoModal();
    //file =  "update.upd";//fdlg.GetFileName();
    file = __argv[1];

    //file.append();



    std::ifstream ifile;
    ifile.open(file,ios::in|ios::binary|ios::ate);

    if(ifile==NULL)
    {

        return;
    }

    char *memblock;

    if (ifile.is_open())
    {
        int size;
        size = (int) ifile.tellg();


        memblock = new char[size];
        ifile.seekg (0, ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            ifile.read(&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);


        ifile.close();
    }
    else
    {


        MessageBox("UPDATE FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;

    }

    if(lockcode=="")
    {
        MessageBox("UPDATE FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    std::string dec="";
    //	  MessageBox("INSIDE FUNC","",0);
    std::string runtime = getHardDriveComputerID ();
    dec = aes_decrypt(lockcode, STRING_FOUR_DEFINE_NAME +runtime);
    delete[] memblock;
    int net = 0;
    size_t found;
    found = dec.find("---vn---");
    if (found!=std::string::npos)
    {
        net=strtol(dec.substr(dec.find("---vn---")+8).c_str(),NULL,10);
        dec = dec .substr( 0, dec.find("---vn---"));
    }
    found = dec.find("%");
    lockcode=dec.substr(found+1);
    dec=dec.substr(0,found);

    if (found!=std::string::npos)
    {
        found = dec.find_last_of("--");
        std::string temp=dec.substr(found+1);
        dec=dec.substr(0,found-1);

        int upcell=strtol(temp.c_str(),NULL,10);
        WORD upvalue=0;
        ReadSectors(0,upcell,1,&upvalue,STRING_SIX_DEFINE_NAME);

        found = dec.find_last_of("--");
        temp=dec.substr(found+1);
        dec=dec.substr(0,found-1);
        int upvalueorig= strtol(temp.c_str(),NULL,10);

        WORD upw=0;
        WORD upw1=0;
        std::string check =dec;
        dec.append(check);

        UPDATECOUNTER(check, &upw);
        WORD value =0;
        string HardDriveSerialNumbers1="";

        HardDriveSerialNumbers1 = getHardDriveComputerID ();
        std::string mac[5]= {""};
        std::string disk =  DISKID(check,&mac[0]);
        dec=UPDATECOUNTER(dec, &upw1);
        std::string checkmac =GetMACaddress();
        int cmac=0;

        int found1 = checkmac.find("--");
        if (found1!=std::string::npos)
        {

            checkmac.append("--");

            int i=0;
            while (checkmac!="")
            {

                if (found1!=std::string::npos)
                {
                    if(i<5)
                    {
                        std::string disk="";
                        disk = checkmac.substr(0,found1);
                        checkmac = checkmac.substr(found1+2);
                        found1 = checkmac.find("--");

                        if(mac[i]!="")
                        {
                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                            {
                                cmac=1;
                            }
                        }
                        i++;
                    }
                    else
                    {
                        std::string disk="";
                        disk = checkmac.substr(0,found1);
                        checkmac = checkmac.substr(found1+2);
                        found1 = checkmac.find("--");
                    }
                }
                else
                {
                    std::string disk="";
                    disk = checkmac.substr(0,found1);
                    checkmac = checkmac.substr(found1+2);
                    found1 = checkmac.find("--");
                }
            }

        }

        if (HardDriveSerialNumbers1==disk || cmac==1)
        {
            ReadSectors(0,57,1,&value,STRING_SIX_DEFINE_NAME);
            if (upvalue='\0' || upvalue<=upvalueorig || value+1==upw)
            {
                upvalue=upvalueorig+1;
                int licval= strtol(lockcode.c_str(),NULL,10);
                WORD licval1=0;
                ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);
                licval=licval+licval1;
                if (licval<50000)
                {
                    _mkdir("C:/Users");
                    _mkdir("C:/Users/Public");
                    _mkdir("C:/Users/Public/Application Data");
                    _mkdir("C:/Users/Public/Application Data/Bastion");
                    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
                    std::ofstream ifile;
                    ifile.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::out|ios::binary);
//	ifile.seekp(ios::ate);
                    char bufflic[1024]="";
                    sprintf_s(bufflic,"%d", licval);
                    lockcode=bufflic;
                    std::string runtime = getHardDriveComputerID ();
                    lockcode= aes_encrypt(lockcode, STRING_FOUR_DEFINE_NAME+runtime);

                    if (ifile.is_open())
                    {
                        ifile.seekp (0, ios::beg);
                        ifile.write (&lockcode[0],strlen(lockcode.c_str()));

                        size_t debug=0;
                        size_t debug1=0;
                        debug=strlen(lockcode.c_str())+1;
                        debug1 =lockcode.length()+1;
                        while (debug<debug1)
                        {
//	cout<<debug;

                            ifile.write("\0",1);
                            ifile.write(&lockcode[debug],strlen(&lockcode[debug]) );
                            debug=strlen(&lockcode[debug])+debug+1;
                        }

                        ifile.close();
                    }
                    else
                    {

                        MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
                        EndDialog(0);
                        return;
                    }
                    if (upvalue==5000)
                    {
                        WriteSectors(0,upcell,1,0,STRING_SIX_DEFINE_NAME);
                    }
                    else
                    {
                        WriteSectors(0,upcell,1,upvalue,STRING_SIX_DEFINE_NAME);
                    }
                    if (upw==5000)
                    {
                        WriteSectors(0,57,1,0,STRING_SIX_DEFINE_NAME);
                    }
                    else
                    {
                        WriteSectors(0,57,1,upw,STRING_SIX_DEFINE_NAME);
                    }

                    int r = WriteSectors(0,58,1,licval,STRING_SIX_DEFINE_NAME);

                    if (net>0)
                    {
                        WORD licval1net=0;
                        ReadSectors(0,59,1,&licval1net,STRING_SIX_DEFINE_NAME);
                        net=net+licval1net;
                        WriteSectors(0,59,1,net,STRING_SIX_DEFINE_NAME);
                    }
                    MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                    EndDialog(0);
                    return;
                }
                else
                {
                    MessageBox("LICENSES CANNOT BE MORE THAN 50000","ERROR",MB_ICONERROR);
                    EndDialog(0);
                    return;
                }
                EndDialog(0);
                return;
            }

            else
            {
                MessageBox("FILE ALREADY UPDATED","ERROR",MB_ICONERROR);
                EndDialog(0);
                return;
            }

        }
        else
        {
            MessageBox("INCORRECT MACHINE","ERROR",MB_ICONERROR);
            EndDialog(0);
            return;
        }

        EndDialog(0);
        return;
    }
    else
    {
        MessageBox("INCORRECT MACHINE","ERROR",MB_ICONERROR);
        EndDialog(0);
    }

}


void CAG_VENDOR_LICENSESDlg::OnBnClickedButton4()
{
    // TODO: Add your control notification handler code here
    WORD value=0;
    char buff[100];

    ReadSectors(0,59,1,&value,STRING_SIX_DEFINE_NAME);
    if(value =='\0')
    {

        WriteSectors(0,59,1,0,STRING_SIX_DEFINE_NAME);
        sprintf_s(buff,"%d", value);
        m_edit2.SetWindowTextA(buff);
    }
    else
    {
        sprintf_s(buff,"%d", value);
        m_edit2.SetWindowTextA(buff);
    }
}
