
#include "openssl\aes.h" 
#include <iostream> 
#include <stdlib.h> 
#include <time.h>  
#include <string> 
#include <string.h> 
#include <fstream>

std::string aes_encrypt(std::string in, std::string key);

std::string aes_decrypt(std::string in, std::string key);
