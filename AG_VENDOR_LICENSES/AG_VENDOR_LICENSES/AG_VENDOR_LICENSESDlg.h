
// AG_VENDOR_LICENSESDlg.h : header file
//

#pragma once


// CAG_VENDOR_LICENSESDlg dialog
class CAG_VENDOR_LICENSESDlg : public CDialogEx
{
// Construction
public:
	CAG_VENDOR_LICENSESDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_AG_VENDOR_LICENSES_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButton2();
	CEdit m_edit1;
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton1();
	CButton m_button1;
	CEdit m_edit2;
	afx_msg void OnBnClickedButton4();
};
