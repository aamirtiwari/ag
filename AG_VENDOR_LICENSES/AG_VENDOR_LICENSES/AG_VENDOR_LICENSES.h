
// AG_VENDOR_LICENSES.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CAG_VENDOR_LICENSESApp:
// See AG_VENDOR_LICENSES.cpp for the implementation of this class
//

class CAG_VENDOR_LICENSESApp : public CWinApp
{
public:
	CAG_VENDOR_LICENSESApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAG_VENDOR_LICENSESApp theApp;