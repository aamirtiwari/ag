
// AG_KEYINFODlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_KEYINFO.h"
#include "AG_KEYINFODlg.h"
#include "afxdialogex.h"

#include <stdio.h>
#include <windows.h>
#include <Setupapi.h>
#include <devguid.h>
#include "conio.h"
#include "tchar.h"
#include <string>
#include "Stringobf.h"
#include <WinIOCtl.h>
#include "resource.h"
#include "SmartX1LiteApi.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

void CAG_KEYINFODlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}


CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAG_KEYINFODlg dialog




CAG_KEYINFODlg::CAG_KEYINFODlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAG_KEYINFODlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAG_KEYINFODlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_BUTTON1, m_button);
    DDX_Control(pDX, IDC_EDIT1, m_edit);
}

BEGIN_MESSAGE_MAP(CAG_KEYINFODlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDOK, &CAG_KEYINFODlg::OnBnClickedOk)
    ON_BN_CLICKED(IDC_BUTTON1, &CAG_KEYINFODlg::OnBnClickedButton1)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAG_KEYINFODlg message handlers
HBRUSH CAG_KEYINFODlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL CAG_KEYINFODlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);
    SetBackGroundColor(RGB(0,114,148));

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_KEYINFODlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_KEYINFODlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_KEYINFODlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}



void CAG_KEYINFODlg::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    CDialogEx::OnOK();
}

int memberIndex = 0;

/*void usbEjectDevice(unsigned vid, unsigned pid)
{
    TCHAR devicepath[8];
    _tcscpy(devicepath, _T("\\\\.\\?:"));

    TCHAR drivepath[4];
    _tcscpy(drivepath, _T("?:\\"));

// Iterate through every drive letter and check if it is our device.
    for(TCHAR driveletter = _T('A'); driveletter <= _T('Z'); driveletter++)
    {
    // We are only interested in CDROM drives.
        drivepath[0] = driveletter;
        if(DRIVE_CDROM != GetDriveType(drivepath))
            continue;

    // Get the "storage device number" for the current drive.
        long DeviceNumber = -1;
        devicepath[4]     = driveletter;
        HANDLE hVolume    = CreateFile(devicepath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                        NULL, OPEN_EXISTING, 0, NULL);
        if(INVALID_HANDLE_VALUE == hVolume)
            continue;

        STORAGE_DEVICE_NUMBER sdn;
        DWORD dwBytesReturned = 0;
        if(DeviceIoControl(hVolume, IOCTL_STORAGE_GET_DEVICE_NUMBER,
                            NULL, 0, &sdn, sizeof(sdn), &dwBytesReturned, NULL))
            DeviceNumber = sdn.DeviceNumber;
        CloseHandle(hVolume);
        if(DeviceNumber < 0)
            continue;

    // Use the data we have collected so far on our drive to find a device instance.
        DEVINST DevInst = GetDrivesDevInstByDeviceNumber(DeviceNumber);

    // If the device instance corresponds to the USB device we are looking for, eject it.
        if(DevInst)
        {
            if(matchDevInstToUsbDevice(DevInst, vid, pid))
                ejectDrive(driveletter);
        }
    }
}
*/
int l =0;
void CAG_KEYINFODlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here


    HDEVINFO deviceInfoSet;
    GUID *guidDev = (GUID*) &GUID_DEVCLASS_HIDCLASS;
    deviceInfoSet = SetupDiGetClassDevs(guidDev, NULL, NULL, DIGCF_PRESENT);
    TCHAR buffer [4000];
    DWORD buffersize =4000;
    std::string keyid="";
    BYTE                             buf[1024];

    PSP_DEVICE_INTERFACE_DETAIL_DATA pspdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)buf;
    pspdidd->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);
    SP_DEVICE_INTERFACE_DATA         spdid;
    SP_DEVINFO_DATA                  spdd;
    DWORD                            dwSize=1024;

    int i =0;
    int j = i+1;
    while (i!=j) {
        SP_DEVINFO_DATA deviceInfoData;
        ZeroMemory(&deviceInfoData, sizeof(SP_DEVINFO_DATA));
        deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
        if (SetupDiEnumDeviceInfo(deviceInfoSet, memberIndex, &deviceInfoData) == FALSE) {
            if (GetLastError() == ERROR_NO_MORE_ITEMS) {
       //         MessageBox("No more keys","ERROR",0);
                m_edit.SetWindowTextA("");
                l=0;
                memberIndex = 0;
                return;
            }
        }
        DWORD nSize=0 ;
        SetupDiGetDeviceInstanceId (deviceInfoSet, &deviceInfoData, buffer, sizeof(buffer), &nSize);
        buffer [nSize] ='\0';
        keyid = buffer;
        size_t found;

        spdid.cbSize = sizeof(spdid);


        SetupDiGetDeviceInterfaceDetail (deviceInfoSet,
                                         &spdid,
                                         NULL, //interfaceDetail,
                                         0, //interfaceDetailSize,
                                         &dwSize,
                                         0); //infoData))
        pspdidd = (PSP_INTERFACE_DEVICE_DETAIL_DATA)malloc(dwSize);
        pspdidd->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

        SetupDiGetDeviceInterfaceDetail(deviceInfoSet, &spdid, pspdidd,
                                        dwSize, &dwSize, &spdd);

        found = keyid.find("HID\\VID_0925&PID_A508");
        if (found!=std::string::npos)
        {
			long nRet = 0;
                char hwID[64] = {0};
                nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                if(0 == nRet)
                {
                    nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                    if( 0 == nRet)
                    {
                        nRet = SmartX1GetUid(hwID);
                        if(!nRet) {
                            keyid= "";
                            keyid.append(hwID);
							MessageBox(hwID,"ERROR",0);
                        }

                        SmartX1Close();
                        l=l+1;
                    }
                    i=i+1;
                }

            m_edit.SetWindowTextA(keyid.c_str());

            STORAGE_DEVICE_NUMBER sdn;
            sdn.DeviceNumber = -1;
            DWORD dwBytesReturned = 0;
            /*DeviceIoControl( deviceInfoSet,
                                      IOCTL_STORAGE_GET_DEVICE_NUMBER,
                                      NULL, 0, &sdn, sizeof( sdn ),
                                      &dwBytesReturned, NULL );
            						  */






            pspdidd->DevicePath[0];
            m_button.SetWindowTextA("FIND NEXT");
        }
        memberIndex++;
    }
    if (deviceInfoSet) {
        SetupDiDestroyDeviceInfoList(deviceInfoSet);
    }
    getch();
    return;
}
