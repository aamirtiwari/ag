#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "rn5Eg4pm9ibeRLxyBUd0XlufOQHjSIV18MkNsJWA27cTYG/ozDF6C+hKwvqaZtP3"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("R0gDR0gDR0gDR0gDR09DRrr=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("RNiMO09KxESzREgFO0lsxp4MONgFO0rDRNSwLh4MRN9r")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("RNiMO09KxESr")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("RNSwLh4MRN9r")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("BXv0XFnuUXvg0+98x0sr")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("x0sr")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("UgDRLEOwR6s+RNBr")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("LEOwR68KRNOr")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("QNnMx0lsQErDQNOhRuXhR6RhRNLMOuUsONLkx0OwLhgr")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif