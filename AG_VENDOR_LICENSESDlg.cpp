
// AG_VENDOR_LICENSESDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_VENDOR_LICENSES.h"
#include "AG_VENDOR_LICENSESDlg.h"
#include "afxdialogex.h"
#include "READWRITESECTOR.h"
#include <direct.h>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include "Encrypt_decrypt.h"
#include "filedlg.h"
#include "MAC.h"
#include "HARDDISKID.h"
#include "DATE_YEAR.h"
#include "DIVIDEINFO.h"
#include "Stringobf.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAG_VENDOR_LICENSESDlg dialog




CAG_VENDOR_LICENSESDlg::CAG_VENDOR_LICENSESDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAG_VENDOR_LICENSESDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAG_VENDOR_LICENSESDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit1);
	DDX_Control(pDX, IDC_BUTTON1, m_button1);
	DDX_Control(pDX, IDC_EDIT2, m_edit2);
}

BEGIN_MESSAGE_MAP(CAG_VENDOR_LICENSESDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON1, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &CAG_VENDOR_LICENSESDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CAG_VENDOR_LICENSESDlg message handlers
void CAG_VENDOR_LICENSESDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
	CDialogEx::SetBackgroundColor(color);
}

BOOL CAG_VENDOR_LICENSESDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	SetBackGroundColor(RGB(192,192,192));

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
		BOOL bDebugged = FALSE;
	CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
	if( bDebugged )
	{
	MessageBox("Being debugged!","ERROR",MB_ICONERROR);
	exit(0);
	return true;
	}
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if ( 	__argc >1 )
		{
			m_button1.EnableWindow(TRUE)	;
	}
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_VENDOR_LICENSESDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_VENDOR_LICENSESDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_VENDOR_LICENSESDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

using namespace std;

void CAG_VENDOR_LICENSESDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	WORD value=0;
	char buff[1024]="";
	
	ReadSectors(0,8,1,&value);
	if(value =='\0')
	{

		WriteSectors(0,8,1,0);
		sprintf_s(buff,"%d", value);
		m_edit1.SetWindowTextA(buff);
	}
	
		
	else
	{
		std::string lockcode = "";
		_mkdir("C:/Users");
		_mkdir("C:/Users/Public");
		_mkdir("C:/Users/Public/Application Data");
		_mkdir("C:/Users/Public/Application Data/Bastion");
		SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
	//file.append();

	

	std::ifstream ifile;
	ifile.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::in|ios::binary|ios::ate);
	
	if(ifile==NULL)
		{
			MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
	}

	char *memblock;

  if (ifile.is_open())
  {
   int size=0;
size = (int) ifile.tellg(); 


 memblock = new char[size];
    ifile.seekg (0, ios::beg);

	int size1=size;
	//file.read (&memblock[debug], size);
	//debug=strlen(&memblock[debug])+1;
	//lockcode.append(&memblock[0]);
	

	size_t debug=0;

	lockcode.resize(size);
	for (int read=0;read<size;read++)
	{
		ifile.read(&memblock[read],1);
	
		lockcode[read]=memblock[read];
	}
	lockcode = lockcode.substr(0,size);
	
   
	 ifile.close();
  }
  else
  {
			 MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
		 
  }
   if (lockcode=="")
		  {
			  delete[] memblock;
			  MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
		  }

		  std::string dec="";
		  std::string runtime = getHardDriveComputerID ();
		  dec = aes_decrypt(lockcode, STRING_FOUR_DEFINE_NAME +runtime);
		  sprintf_s(buff,"%d", value);
		  if (dec==buff)
		  {
			  m_edit1.SetWindowTextA(buff);
		  }
		  else
		  {
			  MessageBox("Tampering with licenses","ERROR",MB_ICONERROR);
		  }
	}

}


void CAG_VENDOR_LICENSESDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
		filedlg fdlg(false,"info","purchase",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);
	
		fdlg.DoModal();
	string file = "";
	file = fdlg.GetFileName();

	std::ofstream ifile;
	
	if(file=="")
		{
		
			return;
	}

	file = fdlg.GetPathName();

	ifile.open(file,ios::out|ios::binary );
	string HardDriveSerialNumbers1="";
	string macid1;
	HardDriveSerialNumbers1 = getHardDriveComputerID ();
	// TODO: Add extra initialization here

	

	
	macid1 = GetMACaddress ();
	
		HardDriveSerialNumbers1.append("--");
	HardDriveSerialNumbers1.append(macid1);

	WORD  value=0;

	WORD date=0;
	WORD year=0;
	getdate_year( &date,  &year);

	////YEAR
	 if (ifile.is_open())
  {

	int r = ReadSectors(0,6,1,&value);
	if((value =='\0')|| value==year)
	{

		WriteSectors(0,6,1,year);

		r = ReadSectors(0,5,1,&value);
	if(value =='\0'|| (value%100)<=(date%100))
	{
	if ((value%100)!=(date%100) || value =='\0')
	{
	WriteSectors(0,5,1,date);
	
	////DATE
	
		
		char buff[1024]="";
		sprintf_s(buff,"%d", date);
	HardDriveSerialNumbers1.append(buff);
		HardDriveSerialNumbers1.append("--");
		sprintf_s(buff,"%d", year);
		HardDriveSerialNumbers1.append(buff);

		HardDriveSerialNumbers1.append("--");
	}
	else if((value%100)==(date%100) )
	{
		if((value/100)<=(date/100))
		{
			WriteSectors(0,5,1,date);
	
	////DATE
	
		
		char buff[1024]="";
		sprintf_s(buff,"%d", date);
	HardDriveSerialNumbers1.append(buff);
		HardDriveSerialNumbers1.append("--");
		sprintf_s(buff,"%d", year);
		HardDriveSerialNumbers1.append(buff);

		HardDriveSerialNumbers1.append("--");
		}
		else
		{
			r = ReadSectors(0,5,1,&value);
	MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
	char buff[1024]="";
		sprintf_s(buff,"%d", value);
	HardDriveSerialNumbers1.append(buff);
		HardDriveSerialNumbers1.append("--");

		int r = ReadSectors(0,6,1,&value);
		sprintf_s(buff,"%d", value);
		HardDriveSerialNumbers1.append(buff);

		HardDriveSerialNumbers1.append("--");
		}
	}

	}
	else
	{
		r = ReadSectors(0,5,1,&value);
	MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
	char buff[1024]="";
		sprintf_s(buff,"%d", value);
	HardDriveSerialNumbers1.append(buff);
		HardDriveSerialNumbers1.append("--");

		int r = ReadSectors(0,6,1,&value);
		sprintf_s(buff,"%d", value);
		HardDriveSerialNumbers1.append(buff);

		HardDriveSerialNumbers1.append("--");
	}

	}

	else if(value<year)
	{
		WriteSectors(0,6,1,year);
		WriteSectors(0,5,1,date);
	
	////DATE
	
		
		char buff[1024]="";
		sprintf_s(buff,"%d", date);
	HardDriveSerialNumbers1.append(buff);
		HardDriveSerialNumbers1.append("--");
		sprintf_s(buff,"%d", year);
		HardDriveSerialNumbers1.append(buff);

		HardDriveSerialNumbers1.append("--");
	}

	else
	{
	r = ReadSectors(0,5,1,&value);
	MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
	char buff[1024]="";
		sprintf_s(buff,"%d", value);
	HardDriveSerialNumbers1.append(buff);
		HardDriveSerialNumbers1.append("--");

		int r = ReadSectors(0,6,1,&value);
		sprintf_s(buff,"%d", value);
		HardDriveSerialNumbers1.append(buff);

		HardDriveSerialNumbers1.append("--");

	}

 ReadSectors(0,7,1,&value);
	if(value =='\0')
	{
	WriteSectors(0,7,1,0);
	char buff[1024]="";
		sprintf_s(buff,"%d", value);
	HardDriveSerialNumbers1.append(buff);
	//	HardDriveSerialNumbers1.append("--");
	}
	else
	{
		
		char buff[1024]="";
		sprintf_s(buff,"%d", value);
		HardDriveSerialNumbers1.append(buff);

	//	HardDriveSerialNumbers1.append("--");
	}
	string aes;
	aes= aes_encrypt(HardDriveSerialNumbers1, STRING_TWO_DEFINE_NAME);

	
	 ifile.seekp (0, ios::beg);
    ifile.write (&aes[0],strlen(aes.c_str()));
	
	size_t debug=0;
	size_t debug1=0;
	debug=strlen(aes.c_str())+1;
	debug1 =aes.length()+1;
	while (debug<debug1)
	{
//	cout<<debug;

	ifile.write("\0",1);
		ifile.write(&aes[debug],strlen(&aes[debug]) );
		debug=strlen(&aes[debug])+debug+1;
	}

    ifile.close();
	MessageBox("PURCHASE FILE WRITTEN","DONE",MB_ICONINFORMATION);
	EndDialog(0);
	}

	else  {
			  MessageBox("FILE IN USE OR NOT ENOUGH PERMISSION","ERROR",MB_ICONERROR);
			  return;
		  }
}


void CAG_VENDOR_LICENSESDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	std::string lockcode = "";
		std::string file="";
//	filedlg fdlg(true,"upd","update",OFN_HIDEREADONLY |OF_PROMPT ,"*.upd",NULL);
	
//	fdlg.DoModal();
	//file =  "update.upd";//fdlg.GetFileName();
file = __argv[1];

	//file.append();

	

	std::ifstream ifile;
	ifile.open(file,ios::in|ios::binary|ios::ate);
	
	if(ifile==NULL)
		{
		
			return;
	}

			  char *memblock;

  if (ifile.is_open())
  {
   int size;
size = (int) ifile.tellg(); 


 memblock = new char[size];
    ifile.seekg (0, ios::beg);

	int size1=size;
	//file.read (&memblock[debug], size);
	//debug=strlen(&memblock[debug])+1;
	//lockcode.append(&memblock[0]);
	

	size_t debug=0;

	lockcode.resize(size);
	for (int read=0;read<size;read++)
	{
		ifile.read(&memblock[read],1);
	
		lockcode[read]=memblock[read];
	}
	lockcode = lockcode.substr(0,size);
	
   
	 ifile.close();
  }
  else
  {

		 
			  MessageBox("UPDATE FILE NOT LOADED","ERROR",MB_ICONERROR);
			  return;
		 
  }
	
  if(lockcode=="")
		  {
			  MessageBox("UPDATE FILE NOT LOADED","ERROR",MB_ICONERROR);
			  return;
		  }

		  std::string dec="";
	//	  MessageBox("INSIDE FUNC","",0);
		  std::string runtime = getHardDriveComputerID ();
		  dec = aes_decrypt(lockcode, STRING_FOUR_DEFINE_NAME +runtime);
		  delete[] memblock;
		  int net = 0;
		  size_t found;
		  found = dec.find("---vn---");
		    if (found!=std::string::npos)
{
	net=strtol(dec.substr(dec.find("---vn---")+8).c_str(),NULL,10);
	dec = dec .substr( 0, dec.find("---vn---"));
			}
		  found = dec.find("%");
		  lockcode=dec.substr(found+1);
		  dec=dec.substr(0,found);

		  if (found!=std::string::npos)
{
		found = dec.find_last_of("--");
		std::string temp=dec.substr(found+1);
		  dec=dec.substr(0,found-1);

		  int upcell=strtol(temp.c_str(),NULL,10);
	WORD upvalue=0;
		ReadSectors(0,upcell,1,&upvalue);

		found = dec.find_last_of("--");
		temp=dec.substr(found+1);
		  dec=dec.substr(0,found-1);
	int upvalueorig= strtol(temp.c_str(),NULL,10);

		 WORD upw=0;
		 WORD upw1=0;
	std::string check =dec;
	dec.append(check);
	
		UPDATECOUNTER(check, &upw);
	WORD value =0;
	string HardDriveSerialNumbers1="";

	HardDriveSerialNumbers1 = getHardDriveComputerID ();
	std::string mac[5]={""};
	std::string disk =  DISKID(check,&mac[0]);
	dec=UPDATECOUNTER(dec, &upw1);
		std::string checkmac =GetMACaddress();
	int cmac=0;

		int found1 = checkmac.find("--");
	if (found1!=std::string::npos)
	{

	checkmac.append("--");

	int i=0;
	while (checkmac!="")
	{

	if (found1!=std::string::npos)
	{
		if(i<5)
		{
		std::string disk="";
	disk = checkmac.substr(0,found1);
	checkmac = checkmac.substr(found1+2);
		found1 = checkmac.find("--");

	if(mac[i]!="")
	{
		if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
		{
			 cmac=1;
		}
	}
			i++;
		}
		else
		{
		std::string disk="";
	disk = checkmac.substr(0,found1);
	checkmac = checkmac.substr(found1+2);
		found1 = checkmac.find("--");	
		}
	}
	else
		{
			std::string disk="";
	disk = checkmac.substr(0,found1);
	checkmac = checkmac.substr(found1+2);
		found1 = checkmac.find("--");
		}
	}
		
	}
	
if (HardDriveSerialNumbers1==disk || cmac==1)
{
	ReadSectors(0,7,1,&value);
	if (upvalue='\0' || upvalue<=upvalueorig || value+1==upw)
	{
		upvalue=upvalueorig+1;
		int licval= strtol(lockcode.c_str(),NULL,10);
		WORD licval1=0;
		ReadSectors(0,8,1,&licval1);
		licval=licval+licval1;
		if (licval<50000)
		{
			_mkdir("C:/Users");
		_mkdir("C:/Users/Public");
		_mkdir("C:/Users/Public/Application Data");
		_mkdir("C:/Users/Public/Application Data/Bastion");
		SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
		std::ofstream ifile;
		ifile.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::out|ios::binary);
//	ifile.seekp(ios::ate);
		char bufflic[1024]="";
		sprintf_s(bufflic,"%d", licval);
		lockcode=bufflic;
		 std::string runtime = getHardDriveComputerID ();
		lockcode= aes_encrypt(lockcode, STRING_FOUR_DEFINE_NAME+runtime);
		
		 if (ifile.is_open())
			{
	 ifile.seekp (0, ios::beg);
    ifile.write (&lockcode[0],strlen(lockcode.c_str()));
	
	size_t debug=0;
	size_t debug1=0;
	debug=strlen(lockcode.c_str())+1;
	debug1 =lockcode.length()+1;
	while (debug<debug1)
				{
//	cout<<debug;

	ifile.write("\0",1);
		ifile.write(&lockcode[debug],strlen(&lockcode[debug]) );
		debug=strlen(&lockcode[debug])+debug+1;
				}

    ifile.close();
			}
		 else
			{

			MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
			EndDialog(0);
		return;
			}
		 if (upvalue==5000)
			{
		WriteSectors(0,upcell,1,0);
			}
	else
			{
	WriteSectors(0,upcell,1,upvalue);
			}
	if (upw==5000)
			{
		WriteSectors(0,7,1,0);
			}
	else
			{
	WriteSectors(0,7,1,upw);
			}
	
		int r = WriteSectors(0,8,1,licval);
		
			if (net>0)
			{
				WORD licval1net=0;
		ReadSectors(0,9,1,&licval1net);
		net=net+licval1net;
		 WriteSectors(0,9,1,net);
			}
		MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
		EndDialog(0);
		return;
		}
		else
		{
			MessageBox("LICENSES CANNOT BE MORE THAN 50000","ERROR",MB_ICONERROR);
			EndDialog(0);
		return;
		}
		EndDialog(0);
		return;
	}

	else
	{
		MessageBox("FILE ALREADY UPDATED","ERROR",MB_ICONERROR);
EndDialog(0);
	return;	
	}

}
	else
	{
		MessageBox("INCORRECT MACHINE","ERROR",MB_ICONERROR);
		EndDialog(0);
	return;
	}

	EndDialog(0);
	return;
		  }
		  else
		  {
			  MessageBox("INCORRECT MACHINE","ERROR",MB_ICONERROR);
		EndDialog(0);
			  }

}


void CAG_VENDOR_LICENSESDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
	WORD value=0;
	char buff[100];

	ReadSectors(0,9,1,&value);
	if(value =='\0')
	{

		WriteSectors(0,9,1,0);
		sprintf_s(buff,"%d", value);
		m_edit2.SetWindowTextA(buff);
	}
	else
	{
		sprintf_s(buff,"%d", value);
		m_edit2.SetWindowTextA(buff);
	}
}
