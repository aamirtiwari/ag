#include "stdafx.h"
#include "AG_LICENSE.h"
#include "DIVIDEINFO.h"
#include "DATE_YEAR.h"

std::string getLicense (std::string vid, std::string appid,int version, int lockto ,std::string sysinfo,std::string memory, int startdate, int startyear, int enddate,int endyear )
{

    std::string mac[5]= {""};
    std::string hdisk = DISKID(sysinfo, &mac[0]);
    if (hdisk =="")
    {
        MessageBox(NULL,"VM NOT SUPPORTED IN THIS VERSION","ERROR",MB_ICONERROR);
    }
    size_t found;

    if (appid=="")
    {
        return "";
    }
    else
    {
        found = memory.find("--");
        if (found!=std::string::npos)
        {
            return "";
        }

        found = memory.find("%");
        if (found!=std::string::npos)
        {
            return "";
        }
        found = appid.find("--");
        if (found!=std::string::npos)
        {
            return "";
        }

        found = appid.find("%");
        if (found!=std::string::npos)
        {
            return "";
        }

        vid.append("--");
        vid.append(appid);
        vid.append("--");
        char buff[1024]="";
        sprintf_s(buff,"%d", version);
        vid.append(buff);
        vid.append("--");

        if (startdate==0 || startyear==0)
        {
            WORD date=0;
            WORD year=0;
            getdate_year(&date, &year);
            sprintf_s(buff,"%d", date);
            vid.append(buff);
            vid.append("--");
            sprintf_s(buff,"%d", year);
            vid.append(buff);
            vid.append("--");
        }
        else
        {
            sprintf_s(buff,"%d", startdate);
            vid.append(buff);
            vid.append("--");
            sprintf_s(buff,"%d", startyear);
            vid.append(buff);
            vid.append("--");
        }
        sprintf_s(buff,"%d", enddate);
        vid.append(buff);
        vid.append("--");
        sprintf_s(buff,"%d", endyear);
        vid.append(buff);
        vid.append("--");
        vid.append(memory);
        vid.append("--");
        sprintf_s(buff,"%d", lockto);

        vid.append(buff);
        vid.append("--");
        WORD upw=0;
        sysinfo = UPDATECOUNTER(sysinfo,&upw);
        sysinfo.append("--");
        sprintf_s(buff,"%d", upw+1);

        sysinfo.append(buff);
        vid.append(sysinfo);

        return vid;
    }
}

std::string getLicense1 (std::string vid, std::string appid,int version, int lockto ,std::string sysinfo,std::string memory, int startdate, int startyear, int enddate,int endyear )
{

    std::string hdisk = sysinfo;
    if (hdisk =="")
    {

        return "";
    }
    size_t found;

    if (appid=="")
    {
        return "";
    }
    else
    {
        found = memory.find("--");
        if (found!=std::string::npos)
        {
            return "";
        }

        found = memory.find("%");
        if (found!=std::string::npos)
        {
            return "";
        }
        found = appid.find("--");
        if (found!=std::string::npos)
        {
            return "";
        }

        found = appid.find("%");
        if (found!=std::string::npos)
        {
            return "";
        }

        vid.append("--");
        vid.append(appid);
        vid.append("--");
        char buff[1024]="";
        sprintf_s(buff,"%d", version);
        vid.append(buff);
        vid.append("--");

        if (startdate==0 || startyear==0)
        {
            WORD date=0;
            WORD year=0;
            getdate_year(&date, &year);
            sprintf_s(buff,"%d", date);
            vid.append(buff);
            vid.append("--");
            sprintf_s(buff,"%d", year);
            vid.append(buff);
            vid.append("--");
        }
        else
        {
            sprintf_s(buff,"%d", startdate);
            vid.append(buff);
            vid.append("--");
            sprintf_s(buff,"%d", startyear);
            vid.append(buff);
            vid.append("--");
        }
        sprintf_s(buff,"%d", enddate);
        vid.append(buff);
        vid.append("--");
        sprintf_s(buff,"%d", endyear);
        vid.append(buff);
        vid.append("--");
        vid.append(memory);
        vid.append("--");
        sprintf_s(buff,"%d", lockto);

        vid.append(buff);
        vid.append("--");
        WORD upw=0;
        sysinfo.append("--00-00-00-00-00-00--00-00-00-00-00-00--00-00-00-00-00-00--00-00-00-00-00-00--00-00-00-00-00-00");
        sysinfo.append("--");
        sprintf_s(buff,"%d", upw+1);

        sysinfo.append(buff);
        vid.append(sysinfo);

        return vid;
    }
}