
// AG_AUTHENTICATOR.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CAG_AUTHENTICATORApp:
// See AG_AUTHENTICATOR.cpp for the implementation of this class
//

class CAG_AUTHENTICATORApp : public CWinApp
{
public:
	CAG_AUTHENTICATORApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAG_AUTHENTICATORApp theApp;