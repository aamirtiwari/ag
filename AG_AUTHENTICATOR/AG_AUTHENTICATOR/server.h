#pragma once


// server dialog

class server : public CDialogEx
{
	DECLARE_DYNAMIC(server)

public:
	server(CWnd* pParent = NULL);   // standard constructor
	virtual ~server();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
//	CEdit m_value1;
//	CEdit m_value2;
//	CEdit m_value3;

	CEdit m_edit1;
	CEdit m_edit2;
	CEdit m_edit3;
	CString m_value1;
	CString m_value2;
	CString m_value3;
	CString GetIDValue() const {return m_value1;}
	CString GetpassValue() const {return m_value2;}
	CString GetserverValue() const {return m_value3;}
	afx_msg void OnBnClickedOk();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
};
