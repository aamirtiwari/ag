#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "HqiY+0Px3FT1v8MSo5EtNjDIgd6kUfBJuCAV9OZ4ezLK7wRpybanmc2QXrWls/Gh"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("vt+bvt+bvt+bvt+bvtvmMHH=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("vVFCgt3QMYUyvY+agtj9MP0CgV+agtHbvVUX820CvV3H")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("vVFCgt3QMYUH")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("vVUX820CvV3H")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("tN0q5+OxEEHavnUH")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("vVvQHH==")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("8YgX8Y+nvY9H")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("8YgXvnuQvVgH")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("dVqCMtj9dYHbdVg2vDN2vnv2vV8CgD59gV8AMtgX82+H")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif