
// AKLM_sysinfo.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CAKLM_sysinfoApp:
// See AKLM_sysinfo.cpp for the implementation of this class
//

class CAKLM_sysinfoApp : public CWinApp
{
public:
	CAKLM_sysinfoApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAKLM_sysinfoApp theApp;