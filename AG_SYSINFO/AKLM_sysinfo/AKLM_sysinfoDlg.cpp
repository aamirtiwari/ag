
// AKLM_sysinfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AKLM_sysinfo.h"
#include "AKLM_sysinfoDlg.h"
#include "afxdialogex.h"
#include "filedlg.h"
#include "HARDDISKID.h"
#include "MAC.h"
#include "DATE_YEAR.h"
#include "READWRITESECTOR.h"
#include "Encrypt_decrypt.h"
#include "DIVIDEINFO.h"
#include <direct.h>
#include "Stringobf.h"
#include "resource.h"
#include <Setupapi.h>
#include <devguid.h>
#include "Sense_LC.h"
#include "COPYLOCK.h"
#include "SmartX1LiteApi.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

std::string file1="";
int filecheck=0;
// CAKLM_sysinfoDlg dialog

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


CAKLM_sysinfoDlg::CAKLM_sysinfoDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAKLM_sysinfoDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAKLM_sysinfoDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_BUTTON2, m_button);
}

BEGIN_MESSAGE_MAP(CAKLM_sysinfoDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON1, &CAKLM_sysinfoDlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_BUTTON2, &CAKLM_sysinfoDlg::OnBnClickedButton2)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()



// CAKLM_sysinfoDlg message handlers
HBRUSH CAKLM_sysinfoDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

int  AG_CheckUSB (std::string ID[1])
{
    int memberIndex = 0;
    int lastkey=0;
    int l=0;
    do
    {
        HDEVINFO deviceInfoSet;
        GUID *guidDev = (GUID*) &GUID_DEVCLASS_HIDCLASS;
        deviceInfoSet = SetupDiGetClassDevs(guidDev, NULL, NULL, DIGCF_PRESENT);
        TCHAR buffer [4000];
        DWORD buffersize =4000;
        std::string keyid="";
        int i =0;
        int j = i+1;
        while (i!=j) {
            SP_DEVINFO_DATA deviceInfoData;
            ZeroMemory(&deviceInfoData, sizeof(SP_DEVINFO_DATA));
            deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
            if (SetupDiEnumDeviceInfo(deviceInfoSet, memberIndex, &deviceInfoData) == FALSE) {
                if (GetLastError() == ERROR_NO_MORE_ITEMS) {
                    lastkey=1;
                    return 9;
                }
            }
            DWORD nSize=0 ;
            SetupDiGetDeviceInstanceId (deviceInfoSet, &deviceInfoData, buffer, sizeof(buffer), &nSize);
            buffer [nSize] ='\0';
            keyid = buffer;
            size_t found;
            found = keyid.find("HID\\VID_0925&PID_A508");
            if (found==std::string::npos)
            {
                found = keyid.find("HID\\VID_0925&PID_2000");
                if (found==std::string::npos)
                {
                    found = keyid.find("HID\\VID_1BC0&PID_8101");
                }
            }
            if (found!=std::string::npos)
            {

                char buff[128];
                int retVal =0;
                int a, b;

                InitDrivers();
                a= GetDongleCount();
                for(b=1; b<=a; b++)
                {
                    retVal = OpenDongle(b);
                    if( retVal == 1)
                    {

                        if (ReadDongleFlash(b,buff, 0, 128) == 1)
                        {
                            std::string IDUNIQUE= buff;
                            if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                            {
                                keyid= "HID\\VID_0925&PID_2000\\";
                                keyid.append(STRING_FIVE_DEFINE_NAME);
                                char bufftime[128];
                                if (ReadDongleFlash(b,bufftime, 64, 64) == 1)
                                {

                                }
                                std::string IDbufftime= (char *)bufftime;
                                keyid.append(IDbufftime);
                                i=i+1;
                                ID[0].append(keyid.c_str());
                                return 0;
                            }
                        }

                    }
                }

                long nRet = 0;
                char hwID[64] = {0};
                nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                if(0 == nRet)
                {
                    nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                    if( 0 == nRet)
                    {
                        nRet = SmartX1GetUid(hwID);
                        if(!nRet) {
                            keyid= "HID\\VID_0925&PID_A508\\";
                            keyid.append(hwID);

                        }

                        SmartX1Close();
                        l=l+1;
                    }
                    i=i+1;
                    ID[0]=keyid;
                    return 0;
                }


                lc_handle_t handle;
                int res,k=0;

                // opening LC device
                res = LC_open(1112100422, l, &handle);
                if(!res) {

                    // verify normal user password
                    res = LC_passwd(handle, 1, (unsigned char *) STRING_SEVEN_DEFINE_NAME);  //STRING_SEVEN_DEFINE_NAME is user password
                    if(!res) {
                        LC_hardware_info info;
                        res = LC_get_hardware_info(handle, &info);
                        if(!res) {
                            keyid= "HID\\VID_1BC0&PID_8101\\";
                            char buffkey[20];
                            sprintf_s(buffkey,"%d",info.developerNumber);
                            keyid.append(buffkey);
                            keyid.append("\\");
                            for(k = 0; k<8; k++)
                            {
                                sprintf_s(buffkey,"%i", info.serialNumber[k]);

                                keyid.append(buffkey);
                            }

                        }
                        // get hardware info

                    }
                    LC_close(handle);
                    l=l+1;
                }
                i=i+1;
                ID[0].append(keyid.c_str());
                return 0;
            }
            memberIndex++;
        }
        if (deviceInfoSet) {
            SetupDiDestroyDeviceInfoList(deviceInfoSet);
        }
    }
    while (lastkey==0);
    return 0;
}
BOOL CAKLM_sysinfoDlg::OnInitDialog()
{

    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox("Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return true;
    }


    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon


    HANDLE fileh = CreateFile("license.upd",GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);
    if(fileh==INVALID_HANDLE_VALUE)
    {
        m_button.EnableWindow(FALSE)	;
    }
    else
    {
        m_button.EnableWindow(TRUE)	;
        file1="license.upd";
        filecheck=1;
        CloseHandle(fileh);
    }

    if ( 	__argc >1 )
    {
        m_button.EnableWindow(TRUE)	;
        filecheck=0;
    }
    //info.pa
    // TODO: Add extra initialization here

    /*			char *buff;
    	char *command_line = GetCommandLine();
    	buff= strchr(command_line,' ');
    	buff++;
    	if(buff!=NULL)
    	{
    m_button.EnableWindow(FALSE)	;
    	}*/


    return TRUE;  // return TRUE  unless you set the focus to a control
}


void CAKLM_sysinfoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}
// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.


void CAKLM_sysinfoDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

void CAKLM_sysinfoDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAKLM_sysinfoDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}



//HRESULT CAKLM_sysinfoDlg::accDoDefaultAction(VARIANT varChild)
//{
//	// TODO: Add your specialized code here and/or call the base class
//
//	return CDialogEx::accDoDefaultAction(varChild);
//}


using namespace std;

void CAKLM_sysinfoDlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox("Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return;
    }
    filedlg fdlg(false,"info","sysinfo",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);

    fdlg.DoModal();
    string file = "";
    file = fdlg.GetFileName();

    std::ofstream ifile;

    if(file=="")
    {

        return;
    }

    file = fdlg.GetPathName();

    ifile.open(file,ios::out|ios::binary );
    string HardDriveSerialNumbers1="";
    string macid1;
    HardDriveSerialNumbers1 = getHardDriveComputerID ();
    if(HardDriveSerialNumbers1 =="")
        HardDriveSerialNumbers1 =getHardDriveComputerIDVM();
    // TODO: Add extra initialization here




    macid1 = GetMACaddress ();

    HardDriveSerialNumbers1.append("--");
    HardDriveSerialNumbers1.append(macid1);

    WORD  value=0;

    WORD date=0;
    WORD year=0;
    getdate_year( &date,  &year);

    ////YEAR
    if (ifile.is_open())
    {

        int r = ReadSectors(0,56,1,&value);
        if((value =='\0')|| value==year || value>9999)
        {

            WriteSectors(0,56,1,year);

            r = ReadSectors(0,55,1,&value);
            if(value =='\0'|| (value%100)<=(date%100) || (value%100)>12 || (value/100)>31)
            {
                if ((value%100)!=(date%100) || value =='\0' || (value%100)>12 || (value/100)>31)
                {
                    WriteSectors(0,55,1,date);

                    ////DATE


                    char buff[1024]="";
                    sprintf_s(buff,"%d", date);
                    HardDriveSerialNumbers1.append(buff);
                    HardDriveSerialNumbers1.append("--");
                    sprintf_s(buff,"%d", year);
                    HardDriveSerialNumbers1.append(buff);

                    HardDriveSerialNumbers1.append("--");
                }
                else if((value%100)==(date%100) || (value%100)>12 || (value/100)>31)
                {
                    if((value/100)<=(date/100)|| (value%100)>12 || (value/100)>31)
                    {
                        WriteSectors(0,55,1,date);

                        ////DATE


                        char buff[1024]="";
                        sprintf_s(buff,"%d", date);
                        HardDriveSerialNumbers1.append(buff);
                        HardDriveSerialNumbers1.append("--");
                        sprintf_s(buff,"%d", year);
                        HardDriveSerialNumbers1.append(buff);

                        HardDriveSerialNumbers1.append("--");
                    }
                    else
                    {
                        r = ReadSectors(0,55,1,&value);
                        MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
                        char buff[1024]="";
                        sprintf_s(buff,"%d", value);
                        HardDriveSerialNumbers1.append(buff);
                        HardDriveSerialNumbers1.append("--");

                        int r = ReadSectors(0,56,1,&value);
                        sprintf_s(buff,"%d", value);
                        HardDriveSerialNumbers1.append(buff);

                        HardDriveSerialNumbers1.append("--");
                    }
                }

            }
            else
            {
                r = ReadSectors(0,55,1,&value);
                MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
                char buff[1024]="";
                sprintf_s(buff,"%d", value);
                HardDriveSerialNumbers1.append(buff);
                HardDriveSerialNumbers1.append("--");

                int r = ReadSectors(0,56,1,&value);
                sprintf_s(buff,"%d", value);
                HardDriveSerialNumbers1.append(buff);

                HardDriveSerialNumbers1.append("--");
            }

        }

        else if(value<year)
        {
            WriteSectors(0,56,1,year);
            WriteSectors(0,55,1,date);

            ////DATE


            char buff[1024]="";
            sprintf_s(buff,"%d", date);
            HardDriveSerialNumbers1.append(buff);
            HardDriveSerialNumbers1.append("--");
            sprintf_s(buff,"%d", year);
            HardDriveSerialNumbers1.append(buff);

            HardDriveSerialNumbers1.append("--");
        }

        else
        {
            r = ReadSectors(0,55,1,&value);
            MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
            char buff[1024]="";
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);
            HardDriveSerialNumbers1.append("--");

            int r = ReadSectors(0,56,1,&value);
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);

            HardDriveSerialNumbers1.append("--");

        }

        ReadSectors(0,57,1,&value);
        if(value =='\0')
        {
            WriteSectors(0,57,1,0);
            char buff[1024]="";
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);
            //	HardDriveSerialNumbers1.append("--");
        }
        else
        {

            char buff[1024]="";
            sprintf_s(buff,"%d", value);
            HardDriveSerialNumbers1.append(buff);

            //	HardDriveSerialNumbers1.append("--");
        }
        string aes;
        int checkvalue=0;
        while (checkvalue==0)
        {
            aes= aes_encrypt(HardDriveSerialNumbers1, STRING_TWO_DEFINE_NAME);
            aes.append("%%%");
            aes=aes.c_str();
            if(aes.find("%%%")!=std::string::npos && aes.find("'")==std::string::npos)
            {
                aes= aes.substr(0, aes.find("%%%"));
                checkvalue=1;
            }
        }
        std::string ID="";
        std::string ID1="";
        int result =  AG_CheckUSB (&ID);
        ID1=ID;
        if(result==0)
        {
            aes.append("---v2---");
            checkvalue=0;
            while (checkvalue==0)
            {
                ID= aes_encrypt(ID1, STRING_TWO_DEFINE_NAME);
                ID.append("%%%");
                ID=ID.c_str();
                if(ID.find("%%%")!=std::string::npos && ID.find("'")==std::string::npos)
                {
                    ID= ID.substr(0, ID.find("%%%"));
                    checkvalue=1;
                }
            }
            aes.append(ID);
        }
        ifile.seekp (0, ios::beg);
        ifile.write (&aes[0],strlen(aes.c_str()));

        size_t debug=0;
        size_t debug1=0;
        debug=strlen(aes.c_str())+1;
        debug1 =aes.length()+1;
        while (debug<debug1)
        {
//	cout<<debug;

            ifile.write("\0",1);
            ifile.write(&aes[debug],strlen(&aes[debug]) );
            debug=strlen(&aes[debug])+debug+1;
        }

        ifile.close();
        MessageBox("SYSINFO FILE WRITTEN","DONE",MB_ICONINFORMATION);
        EndDialog(0);
    }

    else  {
        MessageBox("FILE IN USE OR NOT ENOUGH PERMISSION","ERROR",MB_ICONERROR);
        return;
    }
}


void CAKLM_sysinfoDlg::OnBnClickedButton2()
{


    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox("Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return;
    }
    // TODO: Add your control notification handler code here
    std::string lockcode = "";
    if(filecheck==0)
    {
        file1="";
//	filedlg fdlg(true,"upd","update",OFN_HIDEREADONLY |OF_PROMPT ,"*.upd",NULL);

//	fdlg.DoModal();
        //file =  "update.upd";//fdlg.GetFileName();
        file1 = __argv[1];
    }
    //file.append();



    std::ifstream ifile;
    ifile.open(file1,ios::in|ios::binary|ios::ate);

    if(ifile==NULL)
    {

        return;
    }

    char *memblock;

    if (ifile.is_open())
    {
        int size;
        size = (int) ifile.tellg();


        memblock = new char[size];
        ifile.seekg (0, ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            ifile.read(&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);


        ifile.close();
    }
    else
    {


        MessageBox("UPDATE FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;

    }

    if(lockcode=="")
    {
        MessageBox("UPDATE FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    std::string dec="";
    /////////////////////////////////////////////////////

    size_t foundtrial;
    std::string dectrial="";
    foundtrial = lockcode.find("%%%");
    if (foundtrial!=std::string::npos)
    {
        dectrial=lockcode.substr(0,foundtrial);
        dectrial=aes_decrypt(dectrial,STRING_TWO_DEFINE_NAME);
        delete[] memblock;

        std::string divlic[12]= {""};
        divlic[0]=dectrial;
        TRIALLICENSE(&divlic[0]);

        int upcell=58;//strtol(divlic[10].c_str(),NULL,10);
        WORD upvalue=0;
        ReadSectors(0,upcell,1,&upvalue);
        char itr[1024];

        int upvalueorig= strtol(divlic[9].c_str(),NULL,10);
        if(upvalue>200)
        {
            WriteSectors(0,upcell,1,0);
            ReadSectors(0,upcell,1,&upvalue);
        }
        if (upvalue='\0' || (upvalue<=upvalueorig ) || upvalue>200)
        {
            ReadSectors(0,upcell,1,&upvalue);
            if (upvalue==1 && upvalueorig>5000)
            {
                MessageBox("FILE ALREADY UPDATED","ERROR",MB_ICONERROR);
                EndDialog(0);
                return;
            }
            WORD datetrial=0;
            WORD yeartrial=0;
            getdate_year(&datetrial, &yeartrial);
            if( (upvalue<=upvalueorig))
            {
                WriteSectors(0,56,1,yeartrial);
                WriteSectors(0,55,1,datetrial);
            }
            upvalue=upvalueorig+1;
            ///LICENSE CREATE DEMO VENDOR--demo--0--211--2013--211--2013----3--ST9320325AS            6VD3KE89--00-00-00-00-00-00--1C-65-9D-5B-48-66--1C-65-9D-5B-48-66--1C-65-9D-5B-48-66--F0-4D-A2-8B-F0-A2--3010--2013--18%;
            std::string lic= divlic[1];
            lic.append("--");
            lic.append( divlic[2]);
            lic.append("--");
            lic.append( divlic[3]);
            lic.append("--");

            char buffc[1024]="";
            sprintf_s(buffc,"%d", datetrial);
            lic.append(buffc);
            lic.append("--");
            sprintf_s(buffc,"%d", yeartrial);
            lic.append(buffc);
            lic.append("--");
            WORD dateend=0;
            WORD yearend=0;
            adddays(&dateend,&yearend,strtol(divlic[5].c_str(),NULL,10));
            sprintf_s(buffc,"%d",dateend);
            lic.append(buffc);
            lic.append("--");
            sprintf_s(buffc,"%d", yearend);
            lic.append(buffc);
            lic.append("--");
            lic.append(divlic[7].c_str());
            lic.append("--");
            lic.append("3");
            lic.append("--");
            string HardDriveSerialNumberstrial="";

            HardDriveSerialNumberstrial = getHardDriveComputerID ();
            if (HardDriveSerialNumberstrial=="")
            {
                HardDriveSerialNumberstrial =getHardDriveComputerIDVM();
            }
            lic.append(HardDriveSerialNumberstrial);
            lic.append("--");
            std::string macIDs = GetMACaddress();
            lic.append(macIDs);
            lic.append("--");
            sprintf_s(buffc,"%d", datetrial);
            lic.append(buffc);
            lic.append("--");
            sprintf_s(buffc,"%d", yeartrial);
            lic.append(buffc);
            dec=lic;
            std::ofstream ifile;
            _mkdir("C:/Users");
            _mkdir("C:/Users/Public");
            _mkdir("C:/Users/Public/Application Data");
            _mkdir("C:/Users/Public/Application Data/Bastion");
            SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
            ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",ios::out|ios::binary | ios::app);
//	ifile.seekp(ios::ate);
            WORD  value=0;

            WORD date=0;
            WORD year=0;
            getdate_year( &date,  &year);
            if (ifile.is_open())
            {
                /////update counter update
                ////////
                int r = ReadSectors(0,56,1,&value);
                if((value =='\0')|| value==year)
                {

                    WriteSectors(0,56,1,year);

                    r = ReadSectors(0,55,1,&value);
                    if(value =='\0'|| (value%100)<=(date%100) || (value%100)>12 || (value/100)>31)
                    {
                        if ((value%100)!=(date%100) || value =='\0' || (value%100)>12 || (value/100)>31)
                        {
                            WriteSectors(0,55,1,date);

                        }
                        else if((value%100)==(date%100) || (value%100)>12 || (value/100)>31 )
                        {
                            if((value/100)<=(date/100)|| (value%100)>12 || (value/100)>31)
                            {
                                WriteSectors(0,55,1,date);

                            }
                            else
                            {
                                r = ReadSectors(0,55,1,&value);
                                MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
//	EndDialog(0);
//	return;
                            }
                        }

                    }
                    else
                    {
                        r = ReadSectors(0,55,1,&value);
                        MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
//	EndDialog(0);
//	return;
                    }

                }

                else if(value<year)
                {
                    WriteSectors(0,56,1,year);
                    WriteSectors(0,55,1,date);

                }

                else
                {
                    MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
//	EndDialog(0);
//	return;
                }
                DWORD j=rand();
                WORD value1=0;

                char buff[1024]="";
                sprintf_s(buff,"%d", j);
                dec.append("--");
                dec.append(buff);
                std::string runtime = getHardDriveComputerID ();
                if (runtime=="")
                {
                    runtime =getHardDriveComputerIDVM();
                }
                dec=aes_encrypt(dec, STRING_THREE_DEFINE_NAME + runtime);
                //dec.append("%%%");

                ifile.seekp (0, ios::end);
                ifile.write (&dec[0],strlen(dec.c_str()));

                size_t debug=0;
                size_t debug1=0;
                debug=strlen(dec.c_str())+1;
                debug1 =dec.length()+1;
                while (debug<debug1)
                {
//	cout<<debug;

                    ifile.write("\0",1);
                    ifile.write(&dec[debug],strlen(&dec[debug]) );
                    debug=strlen(&dec[debug])+debug+1;
                }
                ifile.write("%%%",3);
                ifile.close();
                ReadSectors(0,57,1,&value);
                value=value+1;
                if (upvalue<=200)
                {
                    WriteSectors(0,upcell,1,upvalue);
                }
                else
                {
                    WriteSectors(0,upcell,1,1);
                }


                SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
                MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                EndDialog(0);
                return;
            }

            else
            {
                MessageBox("NOT ENOUGH PERMISSION","ERROR",MB_ICONERROR);
                return;
            }
        }

        else
        {
            MessageBox("FILE ALREADY UPDATED","ERROR",MB_ICONERROR);
            EndDialog(0);
            return;
        }

    }


////////////////////////////////////////////////////////////////
    int USBlock=0;
    int networkflag=0;
    std::string USBID="";
    std::string network="";
    size_t found = lockcode.find("---v2---");
    if (found!=std::string::npos)
    {
        USBlock=1;
        lockcode = lockcode.substr(0,found);
        dec = aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
        found = dec.find("---vn---");
        if (found!=std::string::npos)
        {

            network = dec.substr(found);
            networkflag=1;
            dec=dec.substr(0,found);
        }
        else
        {
            networkflag=0;
        }
    }
    else
    {
        std::string runtime = getHardDriveComputerID ();
        if (runtime=="")
        {
            runtime =getHardDriveComputerIDVM();
        }
        dec = aes_decrypt(lockcode, STRING_THREE_DEFINE_NAME +runtime);

        found = dec.find("---v3---");
        if (found!=std::string::npos)
        {

            USBID = dec.substr(found);
            dec=dec.substr(0,found);
            found = USBID.find("---vn---");
            if (found!=std::string::npos)
            {

                network = USBID.substr(found);
                networkflag=1;
                USBID=USBID.substr(0,found);
                USBlock=2;
            }
            else
            {
                networkflag=0;
            }
            USBlock=2;
        }
        /////network//////
        found = dec.find("---vn---");
        if (found!=std::string::npos)
        {
            network = dec.substr(found);
            networkflag=1;
            dec=dec.substr(0,found);
        }
        else
        {
            networkflag=0;
        }
    }
    delete[] memblock;


    found = dec.find("%");
    dec=dec.substr(0,found);
    if (found!=std::string::npos)
    {
        found = dec.find_last_of("--");
        std::string temp=dec.substr(found+1);
        dec=dec.substr(0,found-1);

        int upcell=strtol(temp.c_str(),NULL,10);
        WORD upvalue=0;
        ReadSectors(0,upcell,1,&upvalue);

        found = dec.find_last_of("--");
        temp=dec.substr(found+1);
        dec=dec.substr(0,found-1);
        int upvalueorig= strtol(temp.c_str(),NULL,10);

        WORD upw=0;
        WORD upw1=0;
        std::string divlic[10]= {""};
        divlic[0]=dec;
        std::string lic= LICENSE(&divlic[0]);
        dec=lic;
        std::string check =divlic[0];
        dec.append(check);

        UPDATECOUNTER(check, &upw);
        WORD value =0;
        string HardDriveSerialNumbers1="";

        HardDriveSerialNumbers1 = getHardDriveComputerID ();
        if (HardDriveSerialNumbers1=="")
        {
            HardDriveSerialNumbers1 =getHardDriveComputerIDVM();
        }
        std::string mac[5]= {""};
        std::string disk =  DISKID(check,&mac[0]);
        dec=UPDATECOUNTER(dec, &upw1);
        std::string checkmac =GetMACaddress();
        int cmac=0;

        int found1 = checkmac.find("--");
        if (found1!=std::string::npos)
        {

            checkmac.append("--");

            int i=0;
            while (checkmac!="")
            {

                if (found1!=std::string::npos)
                {
                    if(i<5)
                    {
                        std::string disk="";
                        disk = checkmac.substr(0,found1);
                        checkmac = checkmac.substr(found1+2);
                        found1 = checkmac.find("--");

                        if(mac[i]!="")
                        {
                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                            {
                                cmac=1;
                            }
                        }
                        i++;
                    }
                    else
                    {
                        std::string disk="";
                        disk = checkmac.substr(0,found1);
                        checkmac = checkmac.substr(found1+2);
                        found1 = checkmac.find("--");
                    }
                }
                else
                {
                    std::string disk="";
                    disk = checkmac.substr(0,found1);
                    checkmac = checkmac.substr(found1+2);
                    found1 = checkmac.find("--");
                }
            }

        }

        //////////////////////////////////////////////////////////

        if (HardDriveSerialNumbers1==disk || cmac==1 || USBlock==1)
        {
            ReadSectors(0,57,1,&value);
            if (upvalue='\0' || upvalue<=upvalueorig || value+1==upw || (networkflag==0))
            {
                WORD date=0;
                WORD year=0;
                getdate_year( &date,  &year);
                if( (value+1==upw))
                {
                    WriteSectors(0,56,1,year);
                    WriteSectors(0,55,1,date);
                }
                upvalue=upvalueorig+1;
                std::ofstream ifile;
                _mkdir("C:/Users");
                _mkdir("C:/Users/Public");
                _mkdir("C:/Users/Public/Application Data");
                _mkdir("C:/Users/Public/Application Data/Bastion");
                SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
                ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",ios::out|ios::binary | ios::app);
//	ifile.seekp(ios::ate);
                WORD  value=0;
                if (ifile.is_open())
                {
                    /////update counter update
                    ////////
                    int r = ReadSectors(0,56,1,&value);
                    if((value =='\0')|| value==year)
                    {

                        WriteSectors(0,56,1,year);

                        r = ReadSectors(0,55,1,&value);
                        if(value =='\0'|| (value%100)<=(date%100) || (value%100)>12 || (value/100)>31)
                        {
                            if ((value%100)!=(date%100) || value =='\0' || (value%100)>12 || (value/100)>31)
                            {
                                WriteSectors(0,55,1,date);

                            }
                            else if((value%100)==(date%100) || (value%100)>12 || (value/100)>31 )
                            {
                                if((value/100)<=(date/100)|| (value%100)>12 || (value/100)>31)
                                {
                                    WriteSectors(0,55,1,date);

                                }
                                else
                                {
                                    r = ReadSectors(0,55,1,&value);
                                    MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
//	EndDialog(0);
//	return;
                                }
                            }

                        }
                        else
                        {
                            r = ReadSectors(0,55,1,&value);
                            MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
//	EndDialog(0);
//	return;
                        }

                    }

                    else if(value<year)
                    {
                        WriteSectors(0,56,1,year);
                        WriteSectors(0,55,1,date);

                    }

                    else
                    {
                        MessageBox("SYSTEM DATE TAMPERED","ERROR",MB_ICONERROR);
//	EndDialog(0);
//	return;
                    }
                    DWORD j=rand();
                    char buff[1024]="";
                    sprintf_s(buff,"%d", j);
                    dec.append("--");
                    dec.append(buff);
                    std::string runtime = getHardDriveComputerID ();
                    if (runtime=="")
                    {
                        runtime =getHardDriveComputerIDVM();
                    }
                    if (USBlock==0 || USBlock==2)
                    {
                        if(USBlock==2)
                        {
                            dec.append(USBID);
                        }
                        if(networkflag==1)
                        {
                            dec.append(network);
                        }
                        dec=aes_encrypt(dec, STRING_THREE_DEFINE_NAME + runtime);
                    }
                    else
                    {
                        if(networkflag==1)
                        {
                            dec.append(network);
                        }

                        int checkvalue=0;
                        std::string dectemp=dec;
                        while (checkvalue==0)
                        {
                            dec=aes_encrypt(dectemp, STRING_TWO_DEFINE_NAME);
                            dec.append("%%%");
                            dec=dec.c_str();
                            if(dec.find("%%%")!=std::string::npos)
                            {
                                dec= dec.substr(0, dec.find("%%%"));
                                checkvalue=1;
                            }
                        }
                    }

                    //dec.append("%%%");
                    if((USBlock==1) )
                    {

                        char buff[128]="";
                        char buff1[512]="";
                        int retVal ;
                        retVal = open_dongle();

                        if( retVal == 1)
                        {
                            if (read_dongle_flash(buff, 0, 64) == 1)
                            {

                                std::string IDUNIQUE= buff;
                                if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                                {
                                    if (read_dongle((char *)buff1, 0, 256) == 1)
                                    {
                                        std::string checkstring = (char *)buff1;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string decaes = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = decaes.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = decaes.substr(found2);
                                                decaes=decaes.substr(0,found2);
                                            }

                                            std::string divlicaes[10]= {""};
                                            divlicaes[0]=decaes;
                                            std::string licaes= LICENSE(&divlicaes[0]);
                                            decaes=licaes;
                                            std::string APPLICATIONID =divlicaes[2], VERSION = divlicaes[3];
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = dec;
                                        checkstring2 = checkstring2.append("---v2---");
                                        checkstring2 = checkstring2.append("%%%");
                                        if(checkstring=="")
                                        {
                                            WORD datenew= strtol(divlic[6].c_str(),NULL,10);
                                            WORD month= datenew%100;
                                            datenew=datenew/100;
                                            if (date!=0 || month!=0)
                                            {
                                                char bufft[4];
                                                if (read_dongle_flash(bufft, 126, 2) == 1)
                                                {
                                                    std::string TIME= (char *)bufft;
                                                    if(TIME!="1")
                                                    {
                                                        MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                        return;
                                                    }
                                                }
                                            }
                                            if(write_dongle((char *)checkstring2.c_str(), 0, 256) == 1)
                                            {
                                                MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                                                EndDialog(0);
                                                return;
                                            }
                                        }
                                        if(checkstring!="")
                                        {
                                            if (read_dongle((char *)buff1,256, 256) == 1)
                                            {
                                                std::string checkstring3 = (char *)buff1;
                                                if(checkstring3!=""&& checkstring3.find("%%%")!=std::string::npos)
                                                {
                                                    checkstring3=checkstring3.substr(0,checkstring3.find("%%%"));
                                                    std::string   lockcode1=checkstring3;
                                                    size_t found1 = lockcode1.find("---v2---");
                                                    if (found1!=std::string::npos)
                                                    {
                                                        lockcode1=lockcode1.substr(0,found1);
                                                        USBlock=1;
                                                    }
                                                    std::string decaes = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                    size_t found2 = decaes.find("---vn---");
                                                    if (found2!=std::string::npos)
                                                    {

                                                        network = decaes.substr(found2);
                                                        decaes=decaes.substr(0,found2);
                                                    }

                                                    std::string divlicaes[10]= {""};
                                                    divlicaes[0]=decaes;
                                                    std::string licaes= LICENSE(&divlicaes[0]);
                                                    decaes=licaes;
                                                    std::string APPLICATIONID =divlicaes[2], VERSION = divlicaes[3];
                                                    if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                    {
                                                        checkstring3="";
                                                    }

                                                }
                                                std::string checkstring2 = dec;
                                                checkstring2 = checkstring2.append("---v2---");
                                                checkstring2 = checkstring2.append("%%%");
                                                if(checkstring3=="")
                                                {
                                                    WORD datenew= strtol(divlic[6].c_str(),NULL,10);
                                                    WORD month= datenew%100;
                                                    datenew=datenew/100;
                                                    if (date!=0 || month!=0)
                                                    {
                                                        char bufft[4];
                                                        if (read_dongle_flash(bufft, 126, 2) == 1)
                                                        {
                                                            std::string TIME= (char *)bufft;
                                                            if(TIME!="1")
                                                            {
                                                                MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                                return;
                                                            }
                                                        }
                                                    }
                                                    read_dongle((char *)buff1, 0, 256);
                                                    if(write_dongle((char *)checkstring2.c_str(), 256, 256) == 1)
                                                    {
                                                        write_dongle((char *)buff1, 0, 256);
                                                        MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                                                        EndDialog(0);
                                                        return;
                                                    }
                                                }
                                                if(checkstring3!="")
                                                {
                                                    MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            return;
                        }

                        long nRet = 0;

                        nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                        if(0 == nRet)
                        {
                            nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                            if( 0 == nRet)
                            {

                                for(int l =0; l<4; l++)
                                {
                                    unsigned char outdata[512];
                                    nRet = SmartX1ReadStorage(l*256, 256, outdata);
                                    if(!nRet) {

                                        std::string checkstring = (char *)outdata;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string decaes = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = decaes.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = decaes.substr(found2);
                                                decaes=decaes.substr(0,found2);
                                            }

                                            std::string divlicaes[10]= {""};
                                            divlicaes[0]=decaes;
                                            std::string licaes= LICENSE(&divlicaes[0]);
                                            decaes=licaes;
                                            std::string APPLICATIONID =divlicaes[2], VERSION = divlicaes[3];
                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = dec;
                                        checkstring2 = checkstring2.append("---v2---");
                                        checkstring2 = checkstring2.append("%%%");
                                        if(checkstring=="")
                                        {
                                            nRet = SmartX1WriteStorage(l*256, 256, (unsigned char *)checkstring2.c_str());
                                            MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                                            nRet = SmartX1Close();
                                            EndDialog(0);
                                            return;
                                        }
                                        if(checkstring!="" && l==3 ) {
                                            MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                        }
                                    }
                                }

                                nRet = SmartX1Close();
                            }

                        }
                        lc_handle_t handle;
                        int res=0,k=0;
                        // opening LC device
                        res = LC_open(1112100422, 0, &handle);
                        if(!res) {

                            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
                            if(!res) {
                                // read back the data just writed into block 0
                                for(int l =0; l<4; l++)
                                {
                                    unsigned char outdata[512];
                                    res = LC_read(handle, l, outdata);
                                    if(!res) {

                                        std::string checkstring = (char *)outdata;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string decaes = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = decaes.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = decaes.substr(found2);
                                                decaes=decaes.substr(0,found2);
                                            }

                                            std::string divlicaes[10]= {""};
                                            divlicaes[0]=decaes;
                                            std::string licaes= LICENSE(&divlicaes[0]);
                                            decaes=licaes;
                                            std::string APPLICATIONID =divlicaes[2], VERSION = divlicaes[3];
                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = dec;
                                        checkstring2 = checkstring2.append("---v2---");
                                        checkstring2 = checkstring2.append("%%%");
                                        if(checkstring=="")
                                        {
                                            res = LC_write(handle, l, (unsigned char *)checkstring2.c_str());
                                            MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                                            LC_close(handle);
                                            EndDialog(0);
                                            return;
                                        }
                                        if(checkstring!="" && l==3 ) {
                                            MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                        }
                                    }
                                }
                            }
                            LC_close(handle);
                        }
                        else
                        {
                            MessageBox("DONGLE NOT FOUND","ERROR",MB_ICONERROR);
                        }

                    }
                    ifile.seekp (0, ios::end);
                    ifile.write (&dec[0],strlen(dec.c_str()));

                    size_t debug=0;
                    size_t debug1=0;
                    debug=strlen(dec.c_str())+1;
                    debug1 =dec.length()+1;
                    while (debug<debug1)
                    {
//	cout<<debug;

                        ifile.write("\0",1);
                        ifile.write(&dec[debug],strlen(&dec[debug]) );
                        debug=strlen(&dec[debug])+debug+1;
                    }
                    if (USBlock==1)
                    {
                        ifile.write("---v2---",8);
                    }

                    ifile.write("%%%",3);

                    ifile.close();
                    if (USBlock==0 || USBlock==2 || networkflag==1)
                    {
                        ReadSectors(0,57,1,&value);
                        value=value+1;
                        if (upvalue==5000)
                        {
                            WriteSectors(0,upcell,1,0);
                        }
                        else
                        {
                            WriteSectors(0,upcell,1,upvalue);
                        }
                        if (upw==5000)
                        {
                            WriteSectors(0,57,1,0);
                        }
                        else
                        {
                            WriteSectors(0,57,1,upw);
                        }

                    }
                    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_HIDDEN);
                    MessageBox("LICENSE UPDATED","DONE",MB_ICONINFORMATION);
                    EndDialog(0);
                    return;
                }

                else
                {
                    MessageBox("NOT ENOUGH PERMISSION","ERROR",MB_ICONERROR);
                    return;
                }
            }

            else
            {
                MessageBox("FILE ALREADY UPDATED","ERROR",MB_ICONERROR);
                EndDialog(0);
                return;
            }

        }

        else
        {
            MessageBox("INCORRECT MACHINE","ERROR",MB_ICONERROR);
            EndDialog(0);
            return;
        }
//////////////////////////////

        return;



    }

    else
    {
        WORD upw=0;
        UPDATECOUNTER(dec, &upw);
        WORD value =0;
        string HardDriveSerialNumbers1="";

        HardDriveSerialNumbers1 = getHardDriveComputerID ();
        if (HardDriveSerialNumbers1=="")
        {
            HardDriveSerialNumbers1 =getHardDriveComputerIDVM();
        }
        std::string mac[5]= {""};
        std::string disk =  DISKID(dec,&mac[0]);
        /////////////////////////////////////////////
        std::string checkmac =GetMACaddress();
        int cmac=0;

        int found1 = checkmac.find("--");
        if (found1!=std::string::npos)
        {

            checkmac.append("--");

            int i=0;
            while (checkmac!="")
            {

                if (found1!=std::string::npos)
                {
                    if(i<5)
                    {
                        std::string disk="";
                        disk = checkmac.substr(0,found1);
                        checkmac = checkmac.substr(found1+2);
                        found1 = checkmac.find("--");

                        if(mac[i]!="")
                        {
                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                            {
                                cmac=1;
                            }
                        }
                        i++;
                    }
                    else
                    {
                        std::string disk="";
                        disk = checkmac.substr(0,found1);
                        checkmac = checkmac.substr(found1+2);
                        found1 = checkmac.find("--");
                    }
                }
                else
                {
                    std::string disk="";
                    disk = checkmac.substr(0,found1);
                    checkmac = checkmac.substr(found1+2);
                    found1 = checkmac.find("--");
                }
            }

        }
        //////////////////////////////////////////////////
        if (HardDriveSerialNumbers1==disk || cmac==1)
        {
            ReadSectors(0,57,1,&value);
            if (value+1==upw)
            {


                WORD date=0;
                WORD year=0;
                getdate_year( &date,  &year);

//	value=value+1;
//	WriteSectors(0,57,1,value);

                WriteSectors(0,55,1,date);
                WriteSectors(0,56,1,year);
                value=value+1;
                if (value>5000)
                {
                    WriteSectors(0,57,1,0);
                }
                else
                {
                    WriteSectors(0,57,1,value);
                }
                MessageBox("UPDATED SUCCESSFULLY","ERROR",MB_ICONINFORMATION);

                EndDialog(0);
                return;
            }
            else
            {
                MessageBox("FILE ALREADY UPDATED","ERROR",MB_ICONERROR);
                EndDialog(0);
                return;
            }

        }
        else
        {
            MessageBox("INCORRECT MACHINE","ERROR",MB_ICONERROR);
            EndDialog(0);
            return;
        }
        EndDialog(0);
        return;
    }

}
