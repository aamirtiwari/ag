#include <Iphlpapi.h>
#include <Assert.h>
#include <string>
#pragma comment(lib, "iphlpapi.lib")

// Fetches the MAC address and prints it
std::string GetMACaddress(void);