
// AG_API_DEMODlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_API_DEMO.h"
#include "AG_API_DEMODlg.h"
#include "afxdialogex.h"
#include "AG_Client.h"
#include "resource.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

std::string Handle="";
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAG_API_DEMODlg dialog




CAG_API_DEMODlg::CAG_API_DEMODlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAG_API_DEMODlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

void CAG_API_DEMODlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT2, m_edit2);
    DDX_Control(pDX, IDC_EDIT3, m_edit3);
    DDX_Control(pDX, IDC_BUTTON1, m_button1);
    DDX_Control(pDX, IDC_BUTTON2, m_button2);
    DDX_Control(pDX, IDC_BUTTON3, m_button3);
    DDX_Control(pDX, IDC_BUTTON4, m_button4);
    DDX_Control(pDX, IDC_BUTTON5, m_button5);
    DDX_Control(pDX, IDC_BUTTON6, m_button6);
}

BEGIN_MESSAGE_MAP(CAG_API_DEMODlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON1, &CAG_API_DEMODlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_BUTTON2, &CAG_API_DEMODlg::OnBnClickedButton2)
    ON_BN_CLICKED(IDC_BUTTON3, &CAG_API_DEMODlg::OnBnClickedButton3)
    ON_BN_CLICKED(IDC_BUTTON4, &CAG_API_DEMODlg::OnBnClickedButton4)
    ON_BN_CLICKED(IDC_BUTTON5, &CAG_API_DEMODlg::OnBnClickedButton5)
    ON_BN_CLICKED(IDC_BUTTON6, &CAG_API_DEMODlg::OnBnClickedButton6)
    ON_BN_CLICKED(IDC_BUTTON7, &CAG_API_DEMODlg::OnBnClickedButton7)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAG_API_DEMODlg message handlers

void CAG_API_DEMODlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}
HBRUSH CAG_API_DEMODlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL CAG_API_DEMODlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // Add "About..." menu item to system menu.
    SetBackGroundColor(RGB(0,114,148));
    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);
    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_API_DEMODlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else  if ((nID & 0xFFF0) == SC_CLOSE)
    {
        exit(0);
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_API_DEMODlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_API_DEMODlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}



void CAG_API_DEMODlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here
    int cTxtLen ;
    char buff[1024]="";
    char buff1[1024]="";
    std::string appID="";
    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);

    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff1[0],cTxtLen+1);
    int v= atoi(buff1);
    appID=buff;
    Handle.resize(1024);
    std::string servername;
    AG_STATUS status= AG_IsAuthentic (&Handle,&appID, v,1,&servername);

    char returnval[10];

    //IF the license is available
    if (status == AG_SUCCESS)
    {
        MessageBox("Successful","Done",MB_ICONINFORMATION);
    }

    //If license is not available
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
    }


}


void CAG_API_DEMODlg::OnBnClickedButton2()
{
    // TODO: Add your control notification handler code here
    int cTxtLen ;
    char buff[1024]="";
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string wval="";
    wval=buff;
    AG_STATUS status = AG_Write(&Handle,&wval);
    char returnval[10];
    if (status == AG_SUCCESS)
    {
        MessageBox("WRITTEN","Done",MB_ICONINFORMATION);
    }
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
    }
}


void CAG_API_DEMODlg::OnBnClickedButton3()
{
    // TODO: Add your control notification handler code here
    std::string Data;
    AG_STATUS status =AG_Read (&Handle,0, &Data);
    char returnval[10];
    if (status == AG_SUCCESS)
    {
        MessageBox(Data.c_str(),"Done",MB_ICONINFORMATION);
    }
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
    }
}


void CAG_API_DEMODlg::OnBnClickedButton4()
{
    // TODO: Add your control notification handler code here
    std::string Data;
    AG_STATUS status =AG_Read (&Handle,1, &Data);
    char returnval[10];
    if (status == AG_SUCCESS)
    {
        MessageBox(Data.c_str(),"Done",MB_ICONINFORMATION);
    }
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
    }
}


void CAG_API_DEMODlg::OnBnClickedButton5()
{
    // TODO: Add your control notification handler code here

    AG_STATUS status = AG_AllowRemote (&Handle,TRUE);
    char returnval[10];
    if (status == AG_SUCCESS)
    {
        MessageBox("Successful","Done",MB_ICONINFORMATION);
    }
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
    }
}


void CAG_API_DEMODlg::OnBnClickedButton6()
{
    // TODO: Add your control notification handler code here
    AG_STATUS status = AG_AllowRemote (	&Handle,FALSE);
    char returnval[10];
    if (status == AG_SUCCESS)
    {
        MessageBox("Successful","Done",MB_ICONINFORMATION);
    }
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
    }
}


void CAG_API_DEMODlg::OnBnClickedButton7()
{
    // TODO: Add your control notification handler code here
    m_button2.EnableWindow(0);
    m_button1.EnableWindow(0);
    m_button3.EnableWindow(0);
    m_button4.EnableWindow(0);
    m_button5.EnableWindow(0);
    m_button6.EnableWindow(0);

    int cTxtLen ;
    char buff[1024]="";
    char buff1[1024]="";
    std::string appID="";
    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);

    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff1[0],cTxtLen+1);
    int v= atoi(buff1);

    appID=buff;
    AG_STATUS status= AG_QuickProtect_net (&appID, v);

    char returnval[10];

    //IF the license is available
    if (status == AG_SUCCESS)
    {
        MessageBox("Successful","Done",MB_ICONINFORMATION);
    }

    //If license is not available
    else
    {
        sprintf_s(returnval,10,"ERROR + %d",status);
        MessageBox(returnval,"Error",MB_ICONERROR);
        m_button2.EnableWindow(1);
        m_button1.EnableWindow(1);
        m_button3.EnableWindow(1);
        m_button4.EnableWindow(1);
        m_button5.EnableWindow(1);
        m_button6.EnableWindow(1);
    }
}
