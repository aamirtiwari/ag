// AuthServer_Install.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <string>
#include <windows.h>
#include <crtdbg.h>
#include <netfw.h>
#include <objbase.h>
#include <oleauto.h>
#include <stdio.h>

#pragma comment( lib, "ole32.lib" )
#pragma comment( lib, "oleaut32.lib" )


HRESULT WindowsFirewallInitialize(OUT INetFwProfile** fwProfile)
{
    HRESULT hr = S_OK;
    INetFwMgr* fwMgr = NULL;
    INetFwPolicy* fwPolicy = NULL;

    _ASSERT(fwProfile != NULL);

    *fwProfile = NULL;

    // Create an instance of the firewall settings manager.
    hr = CoCreateInstance(
             __uuidof(NetFwMgr),
             NULL,
             CLSCTX_INPROC_SERVER,
             __uuidof(INetFwMgr),
             (void**)&fwMgr
         );
    if (FAILED(hr))
    {
        printf("CoCreateInstance failed: 0x%08lx\n", hr);
        goto error;
    }

    // Retrieve the local firewall policy.
    hr = fwMgr->get_LocalPolicy(&fwPolicy);
    if (FAILED(hr))
    {
        printf("get_LocalPolicy failed: 0x%08lx\n", hr);
        goto error;
    }

    // Retrieve the firewall profile currently in effect.
    hr = fwPolicy->get_CurrentProfile(fwProfile);
    if (FAILED(hr))
    {
        printf("get_CurrentProfile failed: 0x%08lx\n", hr);
        goto error;
    }

error:

    // Release the local firewall policy.
    if (fwPolicy != NULL)
    {
        fwPolicy->Release();
    }

    // Release the firewall settings manager.
    if (fwMgr != NULL)
    {
        fwMgr->Release();
    }

    return hr;
}


void WindowsFirewallCleanup(IN INetFwProfile* fwProfile)
{
    // Release the firewall profile.
    if (fwProfile != NULL)
    {
        fwProfile->Release();
    }
}


HRESULT WindowsFirewallIsOn(IN INetFwProfile* fwProfile, OUT BOOL* fwOn)
{
    HRESULT hr = S_OK;
    VARIANT_BOOL fwEnabled;

    _ASSERT(fwProfile != NULL);
    _ASSERT(fwOn != NULL);

    *fwOn = FALSE;

    // Get the current state of the firewall.
    hr = fwProfile->get_FirewallEnabled(&fwEnabled);
    if (FAILED(hr))
    {
        printf("get_FirewallEnabled failed: 0x%08lx\n", hr);
        goto error;
    }

    // Check to see if the firewall is on.
    if (fwEnabled != VARIANT_FALSE)
    {
        *fwOn = TRUE;
        printf("The firewall is on.\n");
    }
    else
    {
        printf("The firewall is off.\n");
    }

error:

    return hr;
}




HRESULT WindowsFirewallAppIsEnabled(
    IN INetFwProfile* fwProfile,
    IN const wchar_t* fwProcessImageFileName,
    OUT BOOL* fwAppEnabled
)
{
    HRESULT hr = S_OK;
    BSTR fwBstrProcessImageFileName = NULL;
    VARIANT_BOOL fwEnabled;
    INetFwAuthorizedApplication* fwApp = NULL;
    INetFwAuthorizedApplications* fwApps = NULL;

    _ASSERT(fwProfile != NULL);
    _ASSERT(fwProcessImageFileName != NULL);
    _ASSERT(fwAppEnabled != NULL);

    *fwAppEnabled = FALSE;

    // Retrieve the authorized application collection.
    hr = fwProfile->get_AuthorizedApplications(&fwApps);
    if (FAILED(hr))
    {
        printf("get_AuthorizedApplications failed: 0x%08lx\n", hr);
        goto error;
    }

    // Allocate a BSTR for the process image file name.
    fwBstrProcessImageFileName = SysAllocString(fwProcessImageFileName);
    if (fwBstrProcessImageFileName == NULL)
    {
        hr = E_OUTOFMEMORY;
        printf("SysAllocString failed: 0x%08lx\n", hr);
        goto error;
    }

    // Attempt to retrieve the authorized application.
    hr = fwApps->Item(fwBstrProcessImageFileName, &fwApp);
    if (SUCCEEDED(hr))
    {
        // Find out if the authorized application is enabled.
        hr = fwApp->get_Enabled(&fwEnabled);
        if (FAILED(hr))
        {
            printf("get_Enabled failed: 0x%08lx\n", hr);
            goto error;
        }

        if (fwEnabled != VARIANT_FALSE)
        {
            // The authorized application is enabled.
            *fwAppEnabled = TRUE;

            printf(
                "Authorized application %lS is enabled in the firewall.\n",
                fwProcessImageFileName
            );
        }
        else
        {
            printf(
                "Authorized application %lS is disabled in the firewall.\n",
                fwProcessImageFileName
            );
        }
    }
    else
    {
        // The authorized application was not in the collection.
        hr = S_OK;

        printf(
            "Authorized application %lS is disabled in the firewall.\n",
            fwProcessImageFileName
        );
    }

error:

    // Free the BSTR.
    SysFreeString(fwBstrProcessImageFileName);

    // Release the authorized application instance.
    if (fwApp != NULL)
    {
        fwApp->Release();
    }

    // Release the authorized application collection.
    if (fwApps != NULL)
    {
        fwApps->Release();
    }

    return hr;
}


HRESULT WindowsFirewallAddApp(
    IN INetFwProfile* fwProfile,
    IN const wchar_t* fwProcessImageFileName,
    IN const wchar_t* fwName
)
{
    HRESULT hr = S_OK;
    BOOL fwAppEnabled;
    BSTR fwBstrName = NULL;
    BSTR fwBstrProcessImageFileName = NULL;
    INetFwAuthorizedApplication* fwApp = NULL;
    INetFwAuthorizedApplications* fwApps = NULL;

    _ASSERT(fwProfile != NULL);
    _ASSERT(fwProcessImageFileName != NULL);
    _ASSERT(fwName != NULL);

    // First check to see if the application is already authorized.
    hr = WindowsFirewallAppIsEnabled(
             fwProfile,
             fwProcessImageFileName,
             &fwAppEnabled
         );
    if (FAILED(hr))
    {
        printf("WindowsFirewallAppIsEnabled failed: 0x%08lx\n", hr);
        goto error;
    }

    // Only add the application if it isn't already authorized.
    if (!fwAppEnabled)
    {
        // Retrieve the authorized application collection.
        hr = fwProfile->get_AuthorizedApplications(&fwApps);
        if (FAILED(hr))
        {
            printf("get_AuthorizedApplications failed: 0x%08lx\n", hr);
            goto error;
        }

        // Create an instance of an authorized application.
        hr = CoCreateInstance(
                 __uuidof(NetFwAuthorizedApplication),
                 NULL,
                 CLSCTX_INPROC_SERVER,
                 __uuidof(INetFwAuthorizedApplication),
                 (void**)&fwApp
             );
        if (FAILED(hr))
        {
            printf("CoCreateInstance failed: 0x%08lx\n", hr);
            goto error;
        }

        // Allocate a BSTR for the process image file name.
        fwBstrProcessImageFileName = SysAllocString(fwProcessImageFileName);
        if (fwBstrProcessImageFileName == NULL)
        {
            hr = E_OUTOFMEMORY;
            printf("SysAllocString failed: 0x%08lx\n", hr);
            goto error;
        }

        // Set the process image file name.
        hr = fwApp->put_ProcessImageFileName(fwBstrProcessImageFileName);
        if (FAILED(hr))
        {
            printf("put_ProcessImageFileName failed: 0x%08lx\n", hr);
            goto error;
        }

        // Allocate a BSTR for the application friendly name.
        fwBstrName = SysAllocString(fwName);
        if (SysStringLen(fwBstrName) == 0)
        {
            hr = E_OUTOFMEMORY;
            printf("SysAllocString failed: 0x%08lx\n", hr);
            goto error;
        }

        // Set the application friendly name.
        hr = fwApp->put_Name(fwBstrName);
        if (FAILED(hr))
        {
            printf("put_Name failed: 0x%08lx\n", hr);
            goto error;
        }

        // Add the application to the collection.
        hr = fwApps->Add(fwApp);
        if (FAILED(hr))
        {
            printf("Add failed: 0x%08lx\n", hr);
            goto error;
        }

        printf(
            "Authorized application %lS is now enabled in the firewall.\n",
            fwProcessImageFileName
        );
    }

error:

    // Free the BSTRs.
    SysFreeString(fwBstrName);
    SysFreeString(fwBstrProcessImageFileName);

    // Release the authorized application instance.
    if (fwApp != NULL)
    {
        fwApp->Release();
    }

    // Release the authorized application collection.
    if (fwApps != NULL)
    {
        fwApps->Release();
    }

    return hr;
}


HRESULT WindowsFirewallPortIsEnabled(
    IN INetFwProfile* fwProfile,
    IN LONG portNumber,
    IN NET_FW_IP_PROTOCOL ipProtocol,
    OUT BOOL* fwPortEnabled
)
{
    HRESULT hr = S_OK;
    VARIANT_BOOL fwEnabled;
    INetFwOpenPort* fwOpenPort = NULL;
    INetFwOpenPorts* fwOpenPorts = NULL;

    _ASSERT(fwProfile != NULL);
    _ASSERT(fwPortEnabled != NULL);

    *fwPortEnabled = FALSE;

    // Retrieve the globally open ports collection.
    hr = fwProfile->get_GloballyOpenPorts(&fwOpenPorts);
    if (FAILED(hr))
    {
        printf("get_GloballyOpenPorts failed: 0x%08lx\n", hr);
        goto error;
    }

    // Attempt to retrieve the globally open port.
    hr = fwOpenPorts->Item(portNumber, ipProtocol, &fwOpenPort);
    if (SUCCEEDED(hr))
    {
        // Find out if the globally open port is enabled.
        hr = fwOpenPort->get_Enabled(&fwEnabled);
        if (FAILED(hr))
        {
            printf("get_Enabled failed: 0x%08lx\n", hr);
            goto error;
        }

        if (fwEnabled != VARIANT_FALSE)
        {
            // The globally open port is enabled.
            *fwPortEnabled = TRUE;

            printf("Port %ld is open in the firewall.\n", portNumber);
        }
        else
        {
            printf("Port %ld is not open in the firewall.\n", portNumber);
        }
    }
    else
    {
        // The globally open port was not in the collection.
        hr = S_OK;

        printf("Port %ld is not open in the firewall.\n", portNumber);
    }

error:

    // Release the globally open port.
    if (fwOpenPort != NULL)
    {
        fwOpenPort->Release();
    }

    // Release the globally open ports collection.
    if (fwOpenPorts != NULL)
    {
        fwOpenPorts->Release();
    }

    return hr;
}


HRESULT WindowsFirewallPortAdd(
    IN INetFwProfile* fwProfile,
    IN LONG portNumber,
    IN NET_FW_IP_PROTOCOL ipProtocol,
    IN const wchar_t* name
)
{
    HRESULT hr = S_OK;
    BOOL fwPortEnabled;
    BSTR fwBstrName = NULL;
    INetFwOpenPort* fwOpenPort = NULL;
    INetFwOpenPorts* fwOpenPorts = NULL;

    _ASSERT(fwProfile != NULL);
    _ASSERT(name != NULL);

    // First check to see if the port is already added.
    hr = WindowsFirewallPortIsEnabled(
             fwProfile,
             portNumber,
             ipProtocol,
             &fwPortEnabled
         );
    if (FAILED(hr))
    {
        printf("WindowsFirewallPortIsEnabled failed: 0x%08lx\n", hr);
        goto error;
    }

    // Only add the port if it isn't already added.
    if (!fwPortEnabled)
    {
        // Retrieve the collection of globally open ports.
        hr = fwProfile->get_GloballyOpenPorts(&fwOpenPorts);
        if (FAILED(hr))
        {
            printf("get_GloballyOpenPorts failed: 0x%08lx\n", hr);
            goto error;
        }

        // Create an instance of an open port.
        hr = CoCreateInstance(
                 __uuidof(NetFwOpenPort),
                 NULL,
                 CLSCTX_INPROC_SERVER,
                 __uuidof(INetFwOpenPort),
                 (void**)&fwOpenPort
             );
        if (FAILED(hr))
        {
            printf("CoCreateInstance failed: 0x%08lx\n", hr);
            goto error;
        }

        // Set the port number.
        hr = fwOpenPort->put_Port(portNumber);
        if (FAILED(hr))
        {
            printf("put_Port failed: 0x%08lx\n", hr);
            goto error;
        }

        // Set the IP protocol.
        hr = fwOpenPort->put_Protocol(ipProtocol);
        if (FAILED(hr))
        {
            printf("put_Protocol failed: 0x%08lx\n", hr);
            goto error;
        }

        // Allocate a BSTR for the friendly name of the port.
        fwBstrName = SysAllocString(name);
        if (SysStringLen(fwBstrName) == 0)
        {
            hr = E_OUTOFMEMORY;
            printf("SysAllocString failed: 0x%08lx\n", hr);
            goto error;
        }

        // Set the friendly name of the port.
        hr = fwOpenPort->put_Name(fwBstrName);
        if (FAILED(hr))
        {
            printf("put_Name failed: 0x%08lx\n", hr);
            goto error;
        }

        // Opens the port and adds it to the collection.
        hr = fwOpenPorts->Add(fwOpenPort);
        if (FAILED(hr))
        {
            printf("Add failed: 0x%08lx\n", hr);
            goto error;
        }

        printf("Port %ld is now open in the firewall.\n", portNumber);
    }

error:

    // Free the BSTR.
    SysFreeString(fwBstrName);

    // Release the open port instance.
    if (fwOpenPort != NULL)
    {
        fwOpenPort->Release();
    }

    // Release the globally open ports collection.
    if (fwOpenPorts != NULL)
    {
        fwOpenPorts->Release();
    }

    return hr;
}

inline std::wstring convert( const std::string& as )
{
    // deal with trivial case of empty string
    if( as.empty() )    return std::wstring();

    // determine required length of new string
    size_t reqLength = ::MultiByteToWideChar( CP_UTF8, 0, as.c_str(), (int)as.length(), 0, 0 );

    // construct new string of required length
    std::wstring ret( reqLength, L'\0' );

    // convert old string to new string
    ::MultiByteToWideChar( CP_UTF8, 0, as.c_str(), (int)as.length(), &ret[0], (int)ret.length() );

    // return new string ( compiler should optimize this away )
    return ret;
}
int main(int argc, char *argv[])
{
    HRESULT hr = S_OK;
    HRESULT comInit = E_FAIL;
    INetFwProfile* fwProfile = NULL;
    std::string systemstr= "";
    std::string temp= "";
    std::wstring newtemp=L"";
    std::wstring newtemp1=L"";
    std::string check="";

    if(argc>1)
    {
        check=argv[1];
        if(check.find("delete")!=std::string::npos)
        {
            system("sc stop AuthGuru_LM");
            system(" sc delete AuthGuru_LM");
        }
        else
        {
            // Initialize COM.
            comInit = CoInitializeEx(
                          0,
                          COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE
                      );

            // Ignore RPC_E_CHANGED_MODE; this just means that COM has already been
            // initialized with a different mode. Since we don't care what the mode is,
            // we'll just use the existing mode.
            if (comInit != RPC_E_CHANGED_MODE)
            {
                hr = comInit;
                if (FAILED(hr))
                {
                    printf("CoInitializeEx failed: 0x%08lx\n", hr);
                    goto error;
                }
            }

            // Retrieve the firewall profile currently in effect.
            hr = WindowsFirewallInitialize(&fwProfile);
            if (FAILED(hr))
            {
                printf("WindowsFirewallInitialize failed: 0x%08lx\n", hr);
                goto error;
            }

            // Add Windows Messenger to the authorized application collection.
            temp=argv[0];
            size_t found = temp.find_last_of("\\");
            if(found!=std::string::npos)
            {
                temp= temp.substr(0,found);
                temp.append("\\");
                systemstr.append(temp.c_str());
                systemstr.append("AuthGuru_LM.exe");
            }
            else
            {
                char buff[1024];
                GetCurrentDirectoryA(MAX_PATH,buff);
                systemstr.append(buff);
                systemstr.append("\\");
                systemstr.append("AuthGuru_LM.exe");
            }

            newtemp= L"AuthGuru_LM.exe";
            newtemp1= L"AuthGuru_LM_Install.exe";
            hr = WindowsFirewallAddApp(
                     fwProfile,
                     newtemp1.c_str(),
                     L"AuthGuru_LM_Install"
                 );
            if (FAILED(hr))
            {
                printf("WindowsFirewallAddApp failed: 0x%08lx\n", hr);
                goto error;
            }
            hr = WindowsFirewallAddApp(
                     fwProfile,
                     newtemp.c_str(),
                     L"AuthGuru_LM"
                 );
            if (FAILED(hr))
            {
                printf("WindowsFirewallAddApp failed: 0x%08lx\n", hr);
                goto error;
            }

            // Add TCP::80 to list of globally open ports.
            hr = WindowsFirewallPortAdd(fwProfile, 27016, NET_FW_IP_PROTOCOL_TCP, L"ALM1");
            hr = WindowsFirewallPortAdd(fwProfile, 27016, NET_FW_IP_PROTOCOL_UDP, L"ALM2");
            if (FAILED(hr))
            {
                printf("WindowsFirewallPortAdd failed: 0x%08lx\n", hr);
                goto error;
            }

            systemstr= "sc create AuthGuru_LM start= auto binPath= \"";
            temp=argv[0];
            found = temp.find_last_of("\\");
            if(found!=std::string::npos)
            {
                temp= temp.substr(0,found);
                temp.append("\\");
                systemstr.append(temp.c_str());
                systemstr.append("AuthGuru_LM.exe");
            }
            else
            {
                char buff[1024];
                GetCurrentDirectoryA(MAX_PATH,buff);
                systemstr.append(buff);
                systemstr.append("\\");
                systemstr.append("AuthGuru_LM.exe");
            }

            newtemp= L"AuthGuru_LM.exe";
            newtemp1= L"AuthGuru_LM_Install.exe";

            systemstr.append("\"");
            system(systemstr.c_str());
            system( "sc description AuthGuru_LM \"Manages Network Licenses\"");
            system("sc start AuthGuru_LM");
        }
    }
    else
    {
        comInit = CoInitializeEx(
                      0,
                      COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE
                  );

        // Ignore RPC_E_CHANGED_MODE; this just means that COM has already been
        // initialized with a different mode. Since we don't care what the mode is,
        // we'll just use the existing mode.
        if (comInit != RPC_E_CHANGED_MODE)
        {
            hr = comInit;
            if (FAILED(hr))
            {
                printf("CoInitializeEx failed: 0x%08lx\n", hr);
                goto error;
            }
        }

        // Retrieve the firewall profile currently in effect.
        hr = WindowsFirewallInitialize(&fwProfile);
        if (FAILED(hr))
        {
            printf("WindowsFirewallInitialize failed: 0x%08lx\n", hr);
            goto error;
        }




        // Add Windows Messenger to the authorized application collection.
        temp=argv[0];
        size_t found = temp.find_last_of("\\");
        if(found!=std::string::npos)
        {
            temp= temp.substr(0,found);
            temp.append("\\");
            systemstr.append(temp.c_str());
            systemstr.append("AuthGuru_LM.exe");
        }
        else
        {
            char buff[1024];
            GetCurrentDirectoryA(MAX_PATH,buff);
            systemstr.append(buff);
            systemstr.append("\\");
            systemstr.append("AuthGuru_LM.exe");
        }

        newtemp= L"AuthGuru_LM.exe";
        newtemp1= L"AuthGuru_LM_Install.exe";
        hr = WindowsFirewallAddApp(
                 fwProfile,
                 newtemp.c_str(),
                 L"AuthGuru_LM"
             );
        if (FAILED(hr))
        {
            printf("WindowsFirewallAddApp failed: 0x%08lx\n", hr);
            goto error;
        }

        // Add TCP::80 to list of globally open ports.
        hr = WindowsFirewallPortAdd(fwProfile, 27016, NET_FW_IP_PROTOCOL_TCP, L"ALM1");
        hr = WindowsFirewallPortAdd(fwProfile, 27016, NET_FW_IP_PROTOCOL_UDP, L"ALM2");
        if (FAILED(hr))
        {
            printf("WindowsFirewallPortAdd failed: 0x%08lx\n", hr);
            goto error;
        }

error:

        // Release the firewall profile.
        WindowsFirewallCleanup(fwProfile);

        // Uninitialize COM.
        if (SUCCEEDED(comInit))
        {
            CoUninitialize();
        }
        systemstr= "sc create AuthGuru_LM start= auto binPath= \"";
        temp=argv[0];
        found = temp.find_last_of("\\");
        if(found!=std::string::npos)
        {
            temp= temp.substr(0,found);
            temp.append("\\");
            systemstr.append(temp.c_str());
            systemstr.append("AuthGuru_LM.exe");
        }
        else
        {
            char buff[1024];
            GetCurrentDirectoryA(MAX_PATH,buff);
            systemstr.append(buff);
            systemstr.append("\\");
            systemstr.append("AuthGuru_LM.exe");
        }

        newtemp= L"AuthGuru_LM.exe";
        newtemp1= L"AuthGuru_LM_Install.exe";

        systemstr.append("\"");
        system(systemstr.c_str());
        system( "sc description AuthGuru_LM \"Manages Network Licenses\"");
        system("sc start AuthGuru_LM");
    }
    return 0;
}
