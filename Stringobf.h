#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "ytNEnpUP/3DCk8zRJhYSfqcLH71GIasZK4gOe9XwWxBArmiQvbMuToFj506Vd2l+"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("kSnbkSnbkSnbkSnbkSnbkKy=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("kO34HS/jzEIvkEnMHSqezUp4HOnMHSybkOI58Fp4kO/y")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("kO34HS/jzEIy")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("kOI58Fp4kO/y")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("J9qkqTpYYMtchf0nSo/KkJy=")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("kJy=")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif