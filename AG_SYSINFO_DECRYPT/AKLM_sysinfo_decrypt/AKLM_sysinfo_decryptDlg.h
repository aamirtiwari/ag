
// CLEARDATEDlg.h : header file
//

#pragma once


// CCLEARDATEDlg dialog
class CCLEARDATEDlg : public CDialogEx
{
// Construction
public:
	CCLEARDATEDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CLEARDATE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	CEdit m_edit;
	afx_msg void OnBnClickedButton2();
	CEdit m_year;
	CEdit m_date;
	CEdit m_mac;
	CEdit m_disk;
	CEdit m_USB;
};
