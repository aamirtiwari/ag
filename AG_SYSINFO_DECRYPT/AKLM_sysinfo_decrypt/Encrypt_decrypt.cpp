#include "stdafx.h"
#include "Encrypt_decrypt.h"

/*!  * Encrypts a string using AES with a 256 bit key
* Note: If the key is less than 32 bytes, it will be null padded.
*       If the key is greater than 32 bytes, it will be truncated
* \param in The string to encrypt  * \param key The key to encrypt with
* \return The encrypted data  */
bool seed = true;

std::string aes_encrypt(std::string in, std::string key)
{
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox(NULL,"Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return "";
    }
    // Seed the random number generator once
    if(seed) {
        srand( (unsigned int) time(NULL));
        seed = false;
    }

    // Generate a random ivec

    unsigned char ivec[16];

    // set ivec to random numbers

    /*	std::string ivecString((char*) ivec, 16);

    	// encrypt data

    	return ivecString + encryptedData; */

    for(int i=0; i<16; i++) {
        ivec[i] = (unsigned char) rand();
    }

    // Round up to AES_BLOCK_SIZE

    std::string ivecString((char*) ivec, 16);

    size_t textLength = ((in.length() / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

    // Always pad the key to 32 bits.. because we can

    if(key.length() < 32) {
        key.append(32 - key.length(), '\0');
    }      // Get some space ready for the output

    unsigned char *output = new unsigned char[textLength];

    // Generate a key

    AES_KEY *aesKey = new AES_KEY;

    AES_set_encrypt_key((unsigned char*)key.c_str(), 256, aesKey);

    // Encrypt the data

    AES_cbc_encrypt((unsigned char*)in.c_str(), output, in.length() + 1, aesKey, ivec, AES_ENCRYPT);

    // Make the data into a string

    std::string ret((char*) output, textLength);

    // Add the ivec to the front

    ret = ivecString + ret;

    // Clean up


    delete[] output;

    delete aesKey;


    return ret;
}

/*!  * Decrypts a string using AES with a 256 bit key

* Note: If the key is less than 32 bytes, it will be null padded.

*       If the key is greater than 32 bytes, it will be truncated

* \param in The string to decrypt

* \param key The key to decrypt with

* \return The decrypted data  */

std::string aes_decrypt(std::string in, std::string key)

{
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox(NULL,"Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return "";
    }
    // Get the ivec from the front

    unsigned char ivec[16];

    for(int i=0; i<16; i++)

    {
        ivec[i] = in[i];
    }

    in = in.substr(16);

    // Always pad the key to 32 bits.. because we can

    if(key.length() < 32) {
        key.append(32 - key.length(), '\0');

    }      // Create some space for output

    unsigned char *output = new unsigned char[in.length()];

    // Generate a key

    AES_KEY *aesKey = new AES_KEY;

    AES_set_decrypt_key((unsigned char*)key.c_str(), 256, aesKey);

    // key length is in bits, so 32 * 8 = 256

    // Decrypt the data

    AES_cbc_encrypt((unsigned char*)in.c_str(), output, in.length(), aesKey, ivec, AES_DECRYPT);

    // Make the output into a string

    std::string ret((char*) output);

    // Clean up

    delete[] output;

    delete aesKey;

    return ret;
}
