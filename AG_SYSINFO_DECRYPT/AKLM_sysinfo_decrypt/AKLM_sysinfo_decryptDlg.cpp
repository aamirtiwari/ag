
// CLEARDATEDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AKLM_sysinfo_decrypt.h"
#include "AKLM_sysinfo_decryptDlg.h"
#include "afxdialogex.h"
#include "Encrypt_decrypt.h"
#include "filedlg.h"
#include "DIVIDEINFO.h"
#include "Stringobf.h"
#include "resource.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CCLEARDATEDlg dialog




CCLEARDATEDlg::CCLEARDATEDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CCLEARDATEDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCLEARDATEDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit);
    DDX_Control(pDX, IDC_EDIT2, m_year);
    DDX_Control(pDX, IDC_EDIT3, m_date);
    DDX_Control(pDX, IDC_EDIT4, m_mac);
    DDX_Control(pDX, IDC_EDIT5, m_disk);
    DDX_Control(pDX, IDC_EDIT6, m_USB);
}

BEGIN_MESSAGE_MAP(CCLEARDATEDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON1, &CCLEARDATEDlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_BUTTON2, &CCLEARDATEDlg::OnBnClickedButton2)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CCLEARDATEDlg message handlers
void CCLEARDATEDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}
HBRUSH CCLEARDATEDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL CCLEARDATEDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
        MessageBox("Being debugged!","ERROR",MB_ICONERROR);
        exit(0);
        return true;
    }
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCLEARDATEDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCLEARDATEDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCLEARDATEDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}
std::string lockcode = "";
std::string filename="";
char buff[1024]="";
std::string aes = "";
int cTxtLen ;
using namespace std;
void CCLEARDATEDlg::OnBnClickedButton1()
{



    cTxtLen = m_edit.GetWindowTextLength();
    m_edit.GetWindowTextA(&buff[0],cTxtLen+1);
    if(filename=="")
    {
        MessageBox("SYSINFO FILE NOT SELECTED","ERROR",MB_ICONERROR);
        return;
    }

    filedlg fdlg(false,"upd","update",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.upd",NULL);

    fdlg.DoModal();
    string filename1= fdlg.GetFileName();


    if(filename1=="")
    {

        return;
    }

    filename1 = fdlg.GetPathName();

    aes.append("--");
    aes.append(buff);
    aes= aes_encrypt(aes, "22aa27870012a5d8aab12a012787aa22");



    std::ofstream ifile;
    ifile.open(filename1,ios::out|ios::binary );
    ifile.seekp (0, ios::beg);
    ifile.write (&aes[0],strlen(aes.c_str()));

    size_t debug=0;
    size_t debug1=0;
    debug=strlen(aes.c_str())+1;
    debug1 =aes.length()+1;
    while (debug<debug1)
    {
        cout<<debug;

        ifile.write("\0",1);
        ifile.write(&aes[debug],strlen(&aes[debug]) );
        debug=strlen(&aes[debug])+debug+1;
    }

    ifile.close();
    MessageBox("UPDATE FILE CREATED","DONE",MB_ICONERROR);
    EndDialog(0);
}


void CCLEARDATEDlg::OnBnClickedButton2()
{
    // TODO: Add your control notification handler code here

    filedlg fdlg(true,"info","sysinfo.info",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);

    fdlg.DoModal();
    filename= fdlg.GetFileName();
    if(filename=="")
    {
        return;
    }

    filename= fdlg.GetPathName();
    WORD value=0;

    value=(WORD)strtol(buff,NULL,10);

    char *memblock;

    ifstream file (filename, ios::in|ios::binary|ios::ate);

    if (file.is_open())
    {
        int size;
        size = (int) file.tellg();



        memblock = new char[size];
        file.seekg (0, ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            file.read (&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);

        file.close();
    }
    else
    {
        MessageBox("SYSINFO FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    if(lockcode=="")
    {
        MessageBox("SYSINFO FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }
    int USBlock=0;
    size_t found1 = lockcode.find("---v2---");
    std::string USBID="";
    if (found1!=std::string::npos)
    {
        USBID=lockcode.substr(found1+8);
        USBID= aes_decrypt(USBID, STRING_TWO_DEFINE_NAME);
        lockcode=lockcode.substr(0,found1);

        USBlock=1;
    }
    else
    {
        USBlock=0;
    }

    aes= aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
    delete[] memblock;

    char * disk=new char[1024];
    WORD upw=0;
    WORD upw1=0;

    string in = UPDATECOUNTER(aes, &upw);
    sprintf_s(disk,2*sizeof(int),"%d",upw+1);

    m_edit.SetWindowTextA(disk);

    DATEYEAR(in, &upw,&upw1);
    char * disk1=new char[1024];
    sprintf_s(disk1,2*sizeof(int),"%d%d",upw1/100,upw1%100);
    m_year.SetWindowTextA(disk1);

    char * disk2=new char[1024];
    sprintf_s(disk2,2*sizeof(int)+1,"%d/%d",upw/100,upw%100);
    m_date.SetWindowTextA(disk2);

    std::string mac[5]= {""};
    in = DISKID(aes, &mac[0]);
    std::string mac1 = mac[0];
    for (int i=1; i<5; i++)
    {
        if (mac[i]!="")
        {
            mac1.append("--");
            mac1.append(mac[i]);
        }
    }
    m_disk.SetWindowTextA(in.c_str());
    m_mac.SetWindowTextA(mac1.c_str());
    if(USBlock==1)
    {
        m_USB.SetWindowTextA(USBID.c_str());
    }
    delete[] disk;
    delete[] disk1;
    delete[] disk2;

}
