#pragma once


// Account dialog

class Account : public CDialogEx
{
	DECLARE_DYNAMIC(Account)

public:
	Account(CWnd* pParent = NULL);   // standard constructor
	virtual ~Account();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit1;
	CEdit m_edit2;
	CString m_value1;
	CString m_value2;
	CString GetIDValue() const {return m_value1;}
	CString GetpassValue() const {return m_value2;}
	afx_msg void OnBnClickedOk();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
};
