
// AG_AUTHENTICATORDlg.cpp : implementation file
//

#include "stdafx.h"
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <Setupapi.h>
#include <devguid.h>
#include "Sense_LC.h"
#include "COPYLOCK.h"
// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"
#include "AG_AUTHENTICATOR.h"
#include "AG_AUTHENTICATORDlg.h"
#include "afxdialogex.h"
#include "Encrypt_decrypt.h"
#include "AG_LICENSE.h"
#include "filedlg.h"
#include "DIVIDEINFO.h"
#include "READWRITESECTOR.h"
#include <direct.h>
#include "Stringobf.h"
#include "HARDDISKID.h"
#include "criteria.h"
#include "Account.h"
#include "server.h"
#include "resource.h"
#include "Sense_LC.h"
#include "SmartX1LiteApi.h"
#include "Encrypt_decrypt.h"

typedef struct __MBR_
{
    BYTE                 boot_code[440];
    DWORD                disk_signature; //4 bitai
    WORD                 unknown;
    PARTITION_RECORD     partition_table[4];
    WORD                 signature;          /*AA55*/
} MBR;
#pragma pack(16)
MBR mbr;

using namespace std;
const char *disk_default="\\\\.\\PhysicalDrive0";
WORD begin_cylinder,end_cylinder;

int ReadSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD *value, std::string key)
{
    int checkv= strtol(key.c_str(),NULL,10);
    if((startinglogicalsector == 58) || (startinglogicalsector==59) || (startinglogicalsector ==checkv))
    {
        std::string lockcode="";

        /*						   	char buff[128];
        int retVal ;
        int a, b;

          retVal = open_dongle();

          if( retVal == 1)
          {

        		if (read_dongle_flash(buff, 0, 64) == 1)
        		{
        			  std::string IDUNIQUE= buff;
        			  if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
        			  {
        				   char buf[128];
        				  if (read_dongle(buf, 0, 256) == 1)
        					{
        					 std::string checkstring = (char *)buf;
        					 if(checkstring!="" && checkstring.find("---")!=std::string::npos)
        						{
        						checkstring= checkstring.substr(0,checkstring.find("---"));
        						lockcode= checkstring.c_str();
        						}
        					}

        			  }
        		}

          }*/

        long nRet = 0;

        nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
        if(0 == nRet)
        {
            nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
            if( 0 == nRet)
            {

                unsigned char outdata[512];
                nRet = SmartX1ReadStorage(0, 256, outdata);
                if(!nRet) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            SmartX1Close();
        }

        lc_handle_t handle;
        int res=0,k=0;
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_read(handle, 0, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            LC_close(handle);
        }

        if(lockcode=="")
        {
            MessageBox(NULL,"BASTION LICENSE KEY NOT FOUND, PLEASE PURCHASE KEY","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }
        std::string dec = aes_decrypt(lockcode, key);
        std::string temp="";
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value1= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value2= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        int value3= strtol(temp.c_str(),NULL,10);
        if(startinglogicalsector==58)
        {
            *value=value1;
            return value1;
        }
        if(startinglogicalsector==59)
        {
            *value=value2;
            return value2;
        }
        else
        {
            *value=value3;
            return value3;
        }
    }
    else
    {
        BOOL bDebugged = FALSE;
        CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
        if( bDebugged )
        {
            exit(0);
            return 1;
        }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
        struct
        {
            DWORD StartingSector ;
            WORD NumberOfSectors ;
            DWORD pBuffer;
        } ControlBlock;
#pragma pack ()

#pragma pack (1)
        typedef struct _DIOC_REGISTERS
        {
            DWORD reg_EBX;
            DWORD reg_EDX;
            DWORD reg_ECX;
            DWORD reg_EAX;
            DWORD reg_EDI;
            DWORD reg_ESI;
            DWORD reg_Flags;
        } DIOC_REGISTERS ;
#pragma pack ()

        char* buffer = (char*)malloc (512*numberofsectors);
        HANDLE hDevice ;
        DIOC_REGISTERS reg ;
        BOOL  fResult ;
        DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
        hDevice = CreateFile ( "\\\\.\\vwin32",
                               0,
                               0,
                               NULL,
                               0,
                               FILE_FLAG_DELETE_ON_CLOSE,
                               NULL );

        if ( hDevice == INVALID_HANDLE_VALUE )
        {
            // win NT/2K/XP code
            HANDLE hDevice;
            DWORD bytesread;

            const char * _devicename = disk_default;
            //_devicename[4] += drive;

            // Creating a handle to disk drive using CreateFile () function ..
            hDevice = CreateFile(_devicename,
                                 GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                 NULL, OPEN_EXISTING, 0, NULL);

            if (hDevice == INVALID_HANDLE_VALUE)
                return 1;

            // Setting the pointer to point to the start of the sector we want to read ..
            MBR mbr;
            MBR *_mbr=&mbr;
            memset(_mbr,0,sizeof(_mbr));
            SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
            if (!ReadFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL) )
                return 1;

            *value=_mbr->unknown;
        }
        else
        {
            // code for win 95/98
            ControlBlock.StartingSector = (DWORD)startinglogicalsector;
            ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
            ControlBlock.pBuffer = (DWORD)buffer ;

            //-----------------------------------------------------------
            // SI contains read/write mode flags
            // SI=0h for read and SI=1h for write
            // CX must be equal to ffffh for
            // int 21h's 7305h extention
            // DS:BX -> base addr of the
            // control block structure
            // DL must contain the drive number
            // (01h=A:, 02h=B: etc)
            //-----------------------------------------------------------

            reg.reg_ESI = 0x00;
            reg.reg_ECX = -1 ;
            reg.reg_EBX = (DWORD)(&ControlBlock);
            reg.reg_EDX = drive+1;
            reg.reg_EAX = 0x7305 ;

            //  6 == VWIN32_DIOC_DOS_DRIVEINFO
            fResult = DeviceIoControl ( hDevice,
                                        6,
                                        &(reg),
                                        sizeof (reg),
                                        &(reg),
                                        sizeof (reg),
                                        &cb,
                                        0);

            if (!fResult || (reg.reg_Flags & 0x0001)) return NULL;
        }
        free(buffer);
        CloseHandle(hDevice);

        return 0;
    }
}

int WriteSectors(int drive, DWORD startinglogicalsector, int numberofsectors, WORD value, std::string key)
{
    int checkv= strtol(key.c_str(),NULL,10);
    if((startinglogicalsector == 58) || (startinglogicalsector==59) || (startinglogicalsector ==checkv))
    {
        std::string lockcode="";

        /*					   						   	char buf1[128];
        int retVal ;
        int a, b;

          retVal = open_dongle();

          if( retVal == 1)
          {

        		if (read_dongle_flash(buf1, 0, 64) == 1)
        		{
        			  std::string IDUNIQUE= buf1;
        			  if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
        			  {
        				   char buf[128];
        				  if (read_dongle(buf, 0, 256) == 1)
        					{
        					 std::string checkstring = (char *)buf;
        					 if(checkstring!="" && checkstring.find("---")!=std::string::npos)
        						{
        						checkstring= checkstring.substr(0,checkstring.find("---"));
        						lockcode= checkstring.c_str();
        						}
        					}
        			  }
        		}

          }*/

        long nRet = 0;

        nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
        if(0 == nRet)
        {
            nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
            if( 0 == nRet)
            {

                unsigned char outdata[512];
                nRet = SmartX1ReadStorage(0, 256, outdata);
                if(!nRet) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            SmartX1Close();
        }
        lc_handle_t handle;
        int res=0,k=0;
        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_read(handle, 0, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!="" && checkstring.find("---")!=std::string::npos)
                    {
                        checkstring= checkstring.substr(0,checkstring.find("---"));
                        lockcode= checkstring.c_str();
                    }
                }

            }
            LC_close(handle);
        }

        if(lockcode=="")
        {
            MessageBox(NULL,"BASTION LICENSE KEY NOT FOUND, PLEASE PURCHASE KEY","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }
        std::string dec = aes_decrypt(lockcode, key);
        std::string temp="";
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value1= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        dec = dec.substr(dec.find("---")+3);
        int value2= strtol(temp.c_str(),NULL,10);
        temp = dec.substr(0,dec.find("---"));
        int value3= strtol(temp.c_str(),NULL,10);

        if(startinglogicalsector==58)
            value1 = value;
        if(startinglogicalsector==59)
            value2=value;

        char buff[20];
        sprintf_s(buff,"%d",value1);
        temp = buff;
        temp=temp.c_str();
        temp=temp.append("---");

        sprintf_s(buff,"%d",value2);
        temp=temp.append(buff);
        temp=temp.c_str();
        temp=temp.append("---");

        sprintf_s(buff,"%d",value3);
        temp=temp.append(buff);
        temp=temp.c_str();
        temp=temp.append("---");

        int checkvalue=0;
        while (checkvalue==0)
        {
            dec= aes_encrypt(temp, key);
            dec.append("%%%");
            dec=dec.c_str();
            if(dec.find("%%%")!=std::string::npos)
            {
                dec= dec.substr(0, dec.find("%%%"));
                checkvalue=1;
            }
        }
        dec.append("---");
        /*  retVal = open_dongle();

          if( retVal == 1)
          {
        	  if (read_dongle_flash(buff, 0, 64) == 1)
        		{
        			  std::string IDUNIQUE= buff;
        			  if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
        			  {
        		if (write_dongle((char *)dec.c_str(), 0, 256) == 1)
        		{
        			return 1;
        		}
        			  }
        		}

         return 0;
          }
          */

        nRet = 0;

        nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
        if(0 == nRet)
        {
            nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
            if( 0 == nRet)
            {

                nRet = SmartX1WriteStorage(0, 256, (unsigned char *)dec.c_str());
                SmartX1Close();
                return 0;

            }
        }


        res = LC_open(1112100422, 0, &handle);
        if(!res) {

            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
            if(!res) {
                // read back the data just writed into block 0
                unsigned char outdata[512];

                res = LC_write(handle, 0, (unsigned char *)dec.c_str());
                LC_close(handle);
                return 0;

            }
            LC_close(handle);
        }
        else
        {
            MessageBox(NULL,"BASTION LICENSE KEY NOT FOUND, PLEASE PURCHASE KEY","ERROR",MB_ICONERROR);
            exit(0);
            return 1;
        }
    }
    else
    {
        BOOL bDebugged = FALSE;
        CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
        if( bDebugged )
        {
            exit(0);
            return 1;
        }

// All msdos data structures must be packed on a 1 byte boundary
#pragma pack (1)
        struct
        {
            DWORD StartingSector ;
            WORD NumberOfSectors ;
            DWORD pBuffer;
        } ControlBlock;
#pragma pack ()

#pragma pack (1)
        typedef struct _DIOC_REGISTERS
        {
            DWORD reg_EBX;
            DWORD reg_EDX;
            DWORD reg_ECX;
            DWORD reg_EAX;
            DWORD reg_EDI;
            DWORD reg_ESI;
            DWORD reg_Flags;
        } DIOC_REGISTERS ;
#pragma pack ()

        char* buffer = (char*)malloc (512*numberofsectors);
        HANDLE hDevice ;
        DIOC_REGISTERS reg ;
        BOOL  fResult ;
        DWORD cb ;

// Creating handle to vwin32.vxd (win 9x)
        hDevice = CreateFile ( "\\\\.\\vwin32",
                               0,
                               0,
                               NULL,
                               0,
                               FILE_FLAG_DELETE_ON_CLOSE,
                               NULL );

        if ( hDevice == INVALID_HANDLE_VALUE )
        {
            // win NT/2K/XP code
            HANDLE hDevice;
            DWORD bytesread;

            const char * _devicename = disk_default;
            //_devicename[4] += drive;

            // Creating a handle to disk drive using CreateFile () function ..
            hDevice = CreateFile(_devicename,
                                 GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                 NULL,OPEN_EXISTING, 0, NULL);

            if (hDevice == INVALID_HANDLE_VALUE)
                return 1;

            // Setting the pointer to point to the start of the sector we want to read ..
            SetFilePointer (hDevice, (startinglogicalsector*512), NULL, FILE_BEGIN);
            MBR mbr;
            MBR *_mbr=&mbr;
            memset(_mbr,0,sizeof(_mbr));
            _mbr->unknown =value;
            int n =WriteFile (hDevice, _mbr, 512*numberofsectors, &bytesread, NULL);
            if (!n )
                return 1;
        }
        else
        {
            // code for win 95/98
            ControlBlock.StartingSector = (DWORD)startinglogicalsector;
            ControlBlock.NumberOfSectors = (WORD)numberofsectors ;
            ControlBlock.pBuffer = (DWORD)buffer ;

            //-----------------------------------------------------------
            // SI contains read/write mode flags
            // SI=0h for read and SI=1h for write
            // CX must be equal to ffffh for
            // int 21h's 7305h extention
            // DS:BX -> base addr of the
            // control block structure
            // DL must contain the drive number
            // (01h=A:, 02h=B: etc)
            //-----------------------------------------------------------

            reg.reg_ESI = 0x00;
            reg.reg_ECX = -1 ;
            reg.reg_EBX = (DWORD)(&ControlBlock);
            reg.reg_EDX = drive+1;
            reg.reg_EAX = 0x7305 ;

            //  6 == VWIN32_DIOC_DOS_DRIVEINFO
            fResult = DeviceIoControl ( hDevice,
                                        6,
                                        &(reg),
                                        sizeof (reg),
                                        &(reg),
                                        sizeof (reg),
                                        &cb,
                                        0);

            if (!fResult || (reg.reg_Flags & 0x0001)) return 1;
        }
        free(buffer);
        CloseHandle(hDevice);
        return 0;
    }
}

int  AG_CheckUSB (std::string ID[1])
{
    int memberIndex = 0;
    int lastkey=0;
    int l=0;
    do
    {
        HDEVINFO deviceInfoSet;
        GUID *guidDev = (GUID*) &GUID_DEVCLASS_HIDCLASS;
        deviceInfoSet = SetupDiGetClassDevs(guidDev, NULL, NULL, DIGCF_PRESENT);
        TCHAR buffer [4000];
        DWORD buffersize =4000;
        std::string keyid="";
        int i =0;
        int j = i+1;
        while (i!=j) {
            SP_DEVINFO_DATA deviceInfoData;
            ZeroMemory(&deviceInfoData, sizeof(SP_DEVINFO_DATA));
            deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
            if (SetupDiEnumDeviceInfo(deviceInfoSet, memberIndex, &deviceInfoData) == FALSE) {
                if (GetLastError() == ERROR_NO_MORE_ITEMS) {
                    lastkey=1;
                    return 9;
                }
            }
            DWORD nSize=0 ;
            SetupDiGetDeviceInstanceId (deviceInfoSet, &deviceInfoData, buffer, sizeof(buffer), &nSize);
            buffer [nSize] ='\0';
            keyid = buffer;
            size_t found;
            found = keyid.find("HID\\VID_0925&PID_A508");
            if (found==std::string::npos)
            {
                found = keyid.find("HID\\VID_0925&PID_2000");
                if (found==std::string::npos)
                {
                    found = keyid.find("HID\\VID_1BC0&PID_8101");
                }
            }
            if (found!=std::string::npos)
            {

                char buff[128];
                int retVal ;
                int a, b;

                retVal = open_dongle();

                if( retVal == 1)
                {

                    if (read_dongle_flash(buff, 0, 64) == 1)
                    {
                        std::string IDUNIQUE= buff;
                        if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                        {
                            keyid= "HID\\VID_0925&PID_2000\\";
                            keyid.append(STRING_FIVE_DEFINE_NAME);
                            char bufftime[128];
                            if (read_dongle_flash(bufftime, 64, 64) == 1)
                            {
                                std::string IDbufftime= (char *)bufftime;
                                keyid.append(IDbufftime);
                                i=i+1;
                                ID[0].append(keyid.c_str());
                                return 0;
                            }

                        }
                    }

                }


                long nRet = 0;
                char hwID[64] = {0};
                nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                if(0 == nRet)
                {
                    nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                    if( 0 == nRet)
                    {
                        nRet = SmartX1GetUid(hwID);
                        if(!nRet) {
                            keyid= "HID\\VID_0925&PID_A508\\";
                            keyid.append(hwID);

                        }

                        SmartX1Close();
                        l=l+1;
                    }
                    i=i+1;
                    ID[0]=keyid;
                    return 0;
                }

                lc_handle_t handle;
                int res,k=0;

                // opening LC device
                res = LC_open(1112100422, l, &handle);
                if(!res) {

                    // verify normal user password
                    res = LC_passwd(handle, 1, (unsigned char *) STRING_SEVEN_DEFINE_NAME);  //STRING_SEVEN_DEFINE_NAME is user password
                    if(!res) {
                        LC_hardware_info info;
                        res = LC_get_hardware_info(handle, &info);
                        if(!res) {
                            keyid= "HID\\VID_1BC0&PID_8101\\";
                            char buffkey[20];
                            sprintf_s(buffkey,"%d",info.developerNumber);
                            keyid.append(buffkey);
                            keyid.append("\\");
                            for(k = 0; k<8; k++)
                            {
                                sprintf_s(buffkey,"%i", info.serialNumber[k]);

                                keyid.append(buffkey);
                            }

                        }
                        // get hardware info

                    }
                    LC_close(handle);
                    l=l+1;
                }
                i=i+1;
                ID[0].append(keyid.c_str());
                return 0;
            }
            memberIndex++;
        }
        if (deviceInfoSet) {
            SetupDiDestroyDeviceInfoList(deviceInfoSet);
        }
    }
    while (lastkey==0);
    return 0;
}
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

std::string servername="";
std::string Customeremail="";
std::string sysinfodata="";
class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()



// CAG_AUTHENTICATORDlg dialog


using namespace std;

CAG_AUTHENTICATORDlg::CAG_AUTHENTICATORDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CAG_AUTHENTICATORDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    //  m_radio = 0;
}

void CAG_AUTHENTICATORDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT2, m_edit2);
    DDX_Control(pDX, IDC_EDIT3, m_edit3);
    DDX_Control(pDX, IDC_EDIT4, m_edit4);
    DDX_Control(pDX, IDC_DATETIMEPICKER2, m_date1);
    DDX_Control(pDX, IDC_DATETIMEPICKER3, m_date2);
    //  DDX_Control(pDX, IDC_BUTTON1, m_radio);
    DDX_Control(pDX, IDC_CHECK1, m_check);
    DDX_Control(pDX, IDC_EDIT5, m_edit5);
    DDX_Control(pDX, IDC_CHECK2, m_check2);
    DDX_Control(pDX, IDC_EDIT7, m_edit7);
    DDX_Control(pDX, IDC_EDIT8, m_edit8);
    DDX_Control(pDX, IDC_CHECK4, m_check4);
    DDX_Control(pDX, IDC_EDIT9, m_edit9);
    DDX_Control(pDX, IDC_EDIT10, m_edit10);
    DDX_Control(pDX, IDC_CHECK5, m_checkcust);
}

BEGIN_MESSAGE_MAP(CAG_AUTHENTICATORDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON2, &CAG_AUTHENTICATORDlg::OnBnClickedButton2)
    ON_BN_CLICKED(IDC_BUTTON1, &CAG_AUTHENTICATORDlg::OnBnClickedButton1)
    ON_BN_CLICKED(IDC_CHECK1, &CAG_AUTHENTICATORDlg::OnBnClickedCheck1)
    ON_EN_CHANGE(IDC_EDIT2, &CAG_AUTHENTICATORDlg::OnEnChangeEdit2)
    ON_BN_CLICKED(IDC_CHECK2, &CAG_AUTHENTICATORDlg::OnBnClickedCheck2)
    ON_COMMAND(ID_PROJECT_OPEN, &CAG_AUTHENTICATORDlg::OnProjectOpen)
    ON_COMMAND(ID_PROJECT_SAVEAS, &CAG_AUTHENTICATORDlg::OnProjectSaveas)
    ON_COMMAND(ID_PROJECT_RESETALL, &CAG_AUTHENTICATORDlg::OnProjectResetall)
    ON_BN_CLICKED(IDC_BUTTON3, &CAG_AUTHENTICATORDlg::OnBnClickedButton3)
    ON_COMMAND(ID_SERVER_CONNECTTO, &CAG_AUTHENTICATORDlg::OnServerConnectto)
    ON_COMMAND(ID_SERVER_CREATEACCOUNT, &CAG_AUTHENTICATORDlg::OnServerCreateaccount)
    ON_BN_CLICKED(IDC_BUTTON4, &CAG_AUTHENTICATORDlg::OnBnClickedButton4)
    ON_BN_CLICKED(IDC_CHECK4, &CAG_AUTHENTICATORDlg::OnBnClickedCheck4)

    ON_COMMAND(ID_USBDONGLE_FORMAT, &CAG_AUTHENTICATORDlg::OnUsbdongleFormat)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAG_AUTHENTICATORDlg message handlers

int USBlock=1;
HBRUSH CAG_AUTHENTICATORDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL CAG_AUTHENTICATORDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);
    //SetBackgroundImage( IDB_BITMAP1);
    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
    BOOL bDebugged = FALSE;
    CheckRemoteDebuggerPresent( GetCurrentProcess(), &bDebugged );
    if( bDebugged )
    {
//	MessageBox("Being debugged!","ERROR",MB_ICONERROR);
//	exit(0);
//	return true;
    }
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon
    m_date1.SetFormat("dd/MM/yyyy");
    // TODO: Add extra initialization here
    m_date2.SetFormat("dd/MM/yyyy");
    m_edit7.SetWindowTextA("LOCAL MACHINE");
    m_edit10.SetWindowTextA("1");
    SYSTEMTIME TimeDest;
    m_date2.GetTime(&TimeDest);
    TimeDest.wYear=TimeDest.wYear+1;
    m_date2.SetTime(TimeDest);
    m_date1.EnableWindow(false);
    m_date2.EnableWindow(false);
    m_edit5.SetWindowTextA("30");
    m_edit5.EnableWindow(false);
    m_edit9.SetWindowTextA("10");
    m_edit9.EnableWindow(false);

    std::string tempd= __argv[0];
    tempd= tempd.substr(0,tempd.find_last_of("\\"));
    SetCurrentDirectory(tempd.c_str());

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAG_AUTHENTICATORDlg::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAG_AUTHENTICATORDlg::OnPaint()
{

    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

void CAG_AUTHENTICATORDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}
// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAG_AUTHENTICATORDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}


std::string lockcode="";
std::string aes="";
std::string filename="";
std::string USBID="";

void CAG_AUTHENTICATORDlg::OnBnClickedButton2()
{
    // TODO: Add your control notification handler code here
    filedlg fdlg(true,"info","sysinfo.info",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);

    fdlg.DoModal();
    filename= fdlg.GetFileName();
    if(filename=="")
    {
        return;
    }

    filename= fdlg.GetPathName();

    char *memblock;

    ifstream file (filename, ios::in|ios::binary|ios::ate);

    if (file.is_open())
    {
        int size;
        size = (int) file.tellg();



        memblock = new char[size];
        file.seekg (0, ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            file.read (&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);

        file.close();
    }
    else
    {
        MessageBox("SYSINFO FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    if(lockcode=="")
    {
        MessageBox("SYSINFO FILE NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }
    sysinfodata = lockcode;
    size_t found = lockcode.find("---v2---");
    if (found!=std::string::npos)
    {
        std::string loc="";
        loc = lockcode.substr(found+8);
        USBID =  aes_decrypt(loc, STRING_TWO_DEFINE_NAME);
        if (USBlock==0)
        {
            m_edit3.SetWindowTextA("HARDWARE LOCK");

            USBlock=1;
        }
        else if (USBlock==1)
        {
            m_edit3.SetWindowTextA("HARDWARE LOCK");

            USBlock=1;
        }
        else if (USBlock==2)
        {
            m_edit3.SetWindowTextA("HARDWARE LOCK");

            USBlock=1;
        }
        lockcode = lockcode.substr(0,found);
    }

    aes= aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
    int cTxtLen ;
    char buff[1024]="";
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string ID= buff;
    if (ID=="")
    {
        m_edit3.SetWindowTextA("HARDWARE LOCK");

        USBlock=1;
    }
    delete[] memblock;


}


void CAG_AUTHENTICATORDlg::OnBnClickedButton1()
{
    // TODO: Add your control notification handler code here

    int cTxtLen ;
    char buff[1024]="";

    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string appid= buff;
    std::string APPLICATIONID=appid;
    if(appid=="")
    {
        MessageBox("PLEASE ENTER APPLICATION ID","ERROR",MB_ICONERROR);
        return;
    }

    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    int v= atoi(buff);
    std::string versiondata="0";

    if(v>0)
    {
        versiondata=buff;
    }
    std::string VERSION=versiondata;
    std::string network = "None";
    std::string sdate= "None";
    std::string edate= "None";
    std::string readonlydata= "";
    std::string lockingdata="";
    std::string usbiddata="None";
    if (USBlock==0)
    {
        lockingdata="SOFTWARE LICENSE";
    }
    else if (USBlock==2)
    {
        lockingdata="SOFTWARE LICENSE + USB";
        usbiddata=USBID;
    }
    else
    {
        lockingdata="USB ID";
        usbiddata=USBID;
    }
    if (m_check2.GetCheck())
    {
        MessageBox("THIS LICENSE COULD BE \nINSTALLED ON ANY MACHINE ONCE","WARNING",MB_ICONWARNING);
        char buffdays[1024]="";
        cTxtLen = m_edit5.GetWindowTextLength();
        m_edit5.GetWindowTextA(&buffdays[0],cTxtLen+1);
        int tdays= atoi(buffdays);
        if(tdays>30)
        {
            MessageBox("TRIAL DAYS CANNOT BE MORE THAN 30","ERROR",MB_ICONERROR);
            return;
        }

        std::string triallic="";
        triallic.append(STRING_FIVE_DEFINE_NAME);
        triallic.append("--");
        triallic.append(appid);
        char bufftrial[1024]="";
        triallic.append("--");
        sprintf_s(bufftrial,"%d", v);
        triallic.append(bufftrial);
        triallic.append("--");
        triallic.append("10");
        triallic.append("--");
        triallic.append(buffdays);
        triallic.append("--");
        triallic.append("10");
        triallic.append("--");
        cTxtLen = m_edit4.GetWindowTextLength();
        m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
        std::string memory= buff;
        readonlydata=buff;
        triallic.append(memory);
        triallic.append("--");
        sprintf_s(buff,"%d", 3);
        triallic.append(buff);
        triallic.append("--");
        std::string checkconn="";
        cTxtLen = m_edit7.GetWindowTextLength();
        m_edit7.GetWindowTextA(&buff[0],cTxtLen+1);
        checkconn=buff;
        if (checkconn.find("LOCAL MACHINE")!=std::string::npos)
        {
            WORD value1=0;
            int j = strtol(STRING_SIX_DEFINE_NAME,NULL,10)+60;
            ReadSectors(0,j,1,&value1,STRING_SIX_DEFINE_NAME);
            value1=value1+1;
            if (value1=='\0')
            {
                WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
            }
            else
            {
                if (value1>5000)
                {
                    WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                }
                else
                {
                    WriteSectors(0,j,1,value1,STRING_SIX_DEFINE_NAME);
                }
            }

            char buffc[1024]="";
            sprintf_s(buffc,"%d", value1);
            triallic.append(buffc);
            triallic.append("--");
            sprintf_s(buffc,"%d", j);
            triallic.append(buffc);
            triallic.append("--");

            size_t found = memory.find("--");
            if (found!=std::string::npos)
            {
                triallic= "";
            }

            found = memory.find("%");
            if (found!=std::string::npos)
            {
                triallic= "";
            }
            found = appid.find("--");
            if (found!=std::string::npos)
            {
                triallic= "";
            }

            found = appid.find("%");
            if (found!=std::string::npos)
            {
                triallic= "";
            }

            if(triallic=="" )
            {
                MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            }
            else
            {

                filedlg fdlg(false,"upd","license",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.upd",NULL);

                fdlg.DoModal();
                string filename1= fdlg.GetFileName();


                if(filename1=="")
                {

                    return;
                }

                filename1 = fdlg.GetPathName();
                std::string mac[10]= {""};
                std::string runtime = DISKID(aes, &mac[0]);

                int checkvalue=0;
                while (checkvalue==0)
                {
                    aes= aes_encrypt(triallic, STRING_TWO_DEFINE_NAME);
                    aes.append("%%%");
                    aes=aes.c_str();
                    if(aes.find("%%%")!=std::string::npos)
                    {
                        aes= aes.substr(0, aes.find("%%%"));
                        checkvalue=1;
                    }
                }

                WORD licval1=0;
                int r=0;

                if (r !=0)
                {
                    MessageBox("Please Check that you have enough permissions","ERROR",MB_ICONERROR);
                    EndDialog(0);
                    return;
                }
                if(USBlock!=1)
                    ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);
                if (licval1>0 || USBlock==1)
                {
                    if((USBlock==1) )
                    {
                        char buff[128]="";
                        char buff1[512]="";
                        int retVal ;
                        retVal = open_dongle();
                        if( retVal == 1)
                        {
                            if (read_dongle_flash(buff, 0, 64) == 1)
                            {
                                std::string IDUNIQUE= buff;
                                if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                                {
                                    if (read_dongle((char *)buff1, 0, 256) == 1)
                                    {
                                        std::string checkstring = (char *)buff1;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = dec.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = dec.substr(found2);
                                                dec=dec.substr(0,found2);
                                            }

                                            std::string divlic[10]= {""};
                                            divlic[0]=dec;
                                            std::string lic= LICENSE(&divlic[0]);
                                            dec=lic;
                                            std::string check =divlic[0];
                                            dec.append(check);

                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = aes.append("%%%");
                                        if(checkstring=="")
                                        {
                                            if (m_check.GetCheck())
                                            {
                                                char bufft[4];
                                                if (read_dongle_flash(bufft, 126, 2) == 1)
                                                {
                                                    std::string TIME= (char *)bufft;
                                                    if(TIME!="1")
                                                    {
                                                        MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                        return;
                                                    }
                                                }
                                            }
                                            write_dongle((char *)"", 256, 256);
                                            if(write_dongle((char *)checkstring2.c_str(), 0, 256) == 1)
                                            {
                                                MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                /*		if(Customeremail!="")
                                                					{
                                                InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                Customeremail="";
                                                					}*/
                                                m_checkcust.SetCheck(false);
                                                aes="";
                                                USBID="";
                                                return;
                                            }
                                        }
                                        if(checkstring!="")
                                        {
                                            if (read_dongle((char *)buff1,256, 256) == 1)
                                            {
                                                std::string checkstring3 = (char *)buff1;
                                                if(checkstring3!=""&& checkstring3.find("%%%")!=std::string::npos)
                                                {
                                                    checkstring3=checkstring3.substr(0,checkstring3.find("%%%"));
                                                    std::string   lockcode1=checkstring3;
                                                    size_t found1 = lockcode1.find("---v2---");
                                                    if (found1!=std::string::npos)
                                                    {
                                                        lockcode1=lockcode1.substr(0,found1);
                                                        USBlock=1;
                                                    }
                                                    std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                    size_t found2 = dec.find("---vn---");
                                                    if (found2!=std::string::npos)
                                                    {

                                                        network = dec.substr(found2);
                                                        dec=dec.substr(0,found2);
                                                    }

                                                    std::string divlic[10]= {""};
                                                    divlic[0]=dec;
                                                    std::string lic= LICENSE(&divlic[0]);
                                                    dec=lic;
                                                    std::string check =divlic[0];
                                                    dec.append(check);

                                                    //MessageBox(network.c_str(),"NETWORK",0);
                                                    if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                    {
                                                        checkstring3="";
                                                    }

                                                }
                                                std::string checkstring2 = aes.append("%%%");
                                                if(checkstring3=="")
                                                {
                                                    if (m_check.GetCheck())
                                                    {
                                                        char bufft[4];
                                                        if (read_dongle_flash(bufft, 126, 2) == 1)
                                                        {
                                                            std::string TIME= (char *)bufft;
                                                            if(TIME!="1")
                                                            {
                                                                MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                                return;
                                                            }
                                                        }
                                                    }
                                                    read_dongle((char *)buff1, 0, 256);
                                                    if(write_dongle((char *)checkstring2.c_str(), 256, 256) == 1)
                                                    {
                                                        write_dongle((char *)buff1, 0, 256);
                                                        MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                        /*	if(Customeremail!="")
                                                        				{
                                                        InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                        Customeremail="";
                                                        				}*/
                                                        m_checkcust.SetCheck(false);
                                                        aes="";
                                                        USBID="";
                                                        return;
                                                    }
                                                }
                                                if(checkstring3!="")
                                                {
                                                    MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        long nRet = 0;

                        nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                        if(0 == nRet)
                        {
                            nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                            if( 0 == nRet)
                            {

                                for(int l =0; l<4; l++)
                                {
                                    unsigned char outdata[512];
                                    nRet = SmartX1ReadStorage(l*256, 256, outdata);
                                    if(!nRet) {

                                        std::string checkstring = (char *)outdata;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = dec.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = dec.substr(found2);
                                                dec=dec.substr(0,found2);
                                            }

                                            std::string divlic[10]= {""};
                                            divlic[0]=dec;
                                            std::string lic= LICENSE(&divlic[0]);
                                            dec=lic;
                                            std::string check =divlic[0];
                                            dec.append(check);

                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = aes.append("%%%");
                                        if(checkstring=="")
                                        {
                                            nRet = SmartX1WriteStorage(l*256, 256, (unsigned char *)checkstring2.c_str());
                                            l=100;
                                            MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);

                                            /*		if(Customeremail!="")
                                            {
                                            InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                            Customeremail="";
                                            }*/
                                            m_checkcust.SetCheck(false);
                                            aes="";
                                            USBID="";
                                            nRet = SmartX1Close();

                                            return;
                                        }
                                        if(checkstring!="" && l==3 ) {
                                            MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                            return;
                                        }
                                    }
                                }
                                SmartX1Close();
                            }

                        }
                        lc_handle_t handle;
                        int res=0,k=0;
                        // opening LC device
                        res = LC_open(1112100422, 0, &handle);
                        if(!res) {

                            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
                            if(!res) {
                                // read back the data just writed into block 0
                                for(int l =0; l<4; l++)
                                {
                                    unsigned char outdata[512];
                                    res = LC_read(handle, l, outdata);
                                    if(!res) {

                                        std::string checkstring = (char *)outdata;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = dec.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = dec.substr(found2);
                                                dec=dec.substr(0,found2);
                                            }

                                            std::string divlic[10]= {""};
                                            divlic[0]=dec;
                                            std::string lic= LICENSE(&divlic[0]);
                                            dec=lic;
                                            std::string check =divlic[0];
                                            dec.append(check);

                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = aes.append("%%%");
                                        if(checkstring=="")
                                        {
                                            res = LC_write(handle, l, (unsigned char *)checkstring2.c_str());
                                            l=100;
                                            MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                            /*		if(Customeremail!="")
                                            {
                                            InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                            Customeremail="";
                                            }*/
                                            m_checkcust.SetCheck(false);
                                            aes="";
                                            USBID="";
                                            LC_close(handle);
                                            return;
                                        }
                                        if(checkstring!="" && l==3 ) {
                                            MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                        }
                                    }
                                }
                            }
                            LC_close(handle);
                        }
                        else
                        {
                            MessageBox("DONGLE NOT FOUND","ERROR",MB_ICONERROR);
                        }

                    }
                    std::ofstream ifile;
                    ifile.open(filename1,ios::out|ios::binary );
                    ifile.seekp (0, ios::beg);
                    ifile.write (&aes[0],strlen(aes.c_str()));

                    size_t debug=0;
                    size_t debug1=0;
                    debug=strlen(aes.c_str())+1;
                    debug1 =aes.length()+1;
                    while (debug<debug1)
                    {
                        cout<<debug;

                        ifile.write("\0",1);
                        ifile.write(&aes[debug],strlen(&aes[debug]) );
                        debug=strlen(&aes[debug])+debug+1;
                    }

                    ////////////////////////////////////////////////////////////////////////

                    ifile.write("%%%",3);
                    //////////////////////////////////////////////////////////////////////////////////
                    ifile.close();


                    /*	WriteSectors(0,58,1,licval1-1);
                    	MessageBox("LICENSE UPDATE FILE CREATED","DONE",0);
                    	EndDialog(0);
                    	return;
                    	}*/

                    _mkdir("C:/Users");
                    _mkdir("C:/Users/Public");
                    _mkdir("C:/Users/Public/Application Data");
                    _mkdir("C:/Users/Public/Application Data/Bastion");
                    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
                    std::ofstream ifilec;
                    ifilec.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::out|ios::binary);
//	ifile.seekp(ios::ate);
                    char bufflic[1024]="";
                    sprintf_s(bufflic,"%d", licval1-1);
                    lockcode=bufflic;
                    std::string runtime = getHardDriveComputerID ();
                    lockcode= aes_encrypt(lockcode, STRING_FOUR_DEFINE_NAME+runtime);

                    if (ifilec.is_open())
                    {
                        ifilec.seekp (0, ios::beg);
                        ifilec.write (&lockcode[0],strlen(lockcode.c_str()));

                        size_t debug=0;
                        size_t debug1=0;
                        debug=strlen(lockcode.c_str())+1;
                        debug1 =lockcode.length()+1;
                        while (debug<debug1)
                        {
//	cout<<debug;

                            ifilec.write("\0",1);
                            ifilec.write(&lockcode[debug],strlen(&lockcode[debug]) );
                            debug=strlen(&lockcode[debug])+debug+1;
                        }

                        ifilec.close();
                    }
                    else
                    {

                        MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
                        EndDialog(0);
                        return;
                    }
                    if(USBlock!=1 && licval1!=6555)
                        WriteSectors(0,58,1,licval1-1,STRING_SIX_DEFINE_NAME);
                    MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                    aes="";
                    USBID="";
                    return;
                }
                else
                {
                    MessageBox("NO LICENSE - PLEASE PURCHASE LICENSES","ERROR",MB_ICONERROR);
                    EndDialog(0);
                    return;
                }
            }

        }
        else
        {
            size_t found = memory.find("--");
            if (found!=std::string::npos)
            {
                triallic= "";
            }

            found = memory.find("%");
            if (found!=std::string::npos)
            {
                triallic= "";
            }
            found = appid.find("--");
            if (found!=std::string::npos)
            {
                triallic= "";
            }

            found = appid.find("%");
            if (found!=std::string::npos)
            {
                triallic= "";
            }

            if(triallic=="" )
            {
                MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
                return;
            }

            filedlg fdlg(false,"upd","license",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.upd",NULL);

            fdlg.DoModal();
            string filename1= fdlg.GetFileName();


            if(filename1=="")
            {

                return;
            }

            filename1 = fdlg.GetPathName();
            if(servername=="")
            {
                MessageBox("PLEASE PROVIDE SERVER NAME","ERROR",MB_ICONERROR);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                return;
            }
            found = servername.find("--");
            if (found!=std::string::npos)
            {
                MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                return;
            }

            WSADATA wsaData;
            SOCKET ConnectSocket = INVALID_SOCKET;
            struct addrinfo *result = NULL,
                                 *ptr = NULL,
                                  hints;
            char sendd[14];
            for(int i=0; i<14; i++)
            {
                sendd[i]=rand();
            }
            std::string sendbuf =sendd;
            sendbuf.append("---Bastion-v3trial--");

            char recvbuf[DEFAULT_BUFLEN];
            int iResult;
            int recvbuflen = DEFAULT_BUFLEN;



            // Initialize Winsock
            iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
            if (iResult != 0) {
                sprintf_s(buff,"WSAStartup failed with error: %d\n", iResult);
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                return ;
            }

            ZeroMemory( &hints, sizeof(hints) );
            hints.ai_family = AF_UNSPEC;
            hints.ai_socktype = SOCK_STREAM;
            hints.ai_protocol = IPPROTO_TCP;

            // Resolve the server address and port
            iResult = getaddrinfo(servername.c_str(), DEFAULT_PORT, &hints, &result);
            if ( iResult != 0 ) {
                sprintf_s(buff,"Server \"%s\" not found\n", servername.c_str());
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                WSACleanup();
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                return ;
            }

            // Attempt to connect to an address until one succeeds
            for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

                // Create a SOCKET for connecting to server
                ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                                       ptr->ai_protocol);
                if (ConnectSocket == INVALID_SOCKET) {
                    sprintf_s(buff,"socket failed with error: %ld\n", WSAGetLastError());
                    MessageBox(buff,"ERROR",MB_ICONSTOP);
                    m_edit7.SetWindowTextA("LOCAL MACHINE");
                    WSACleanup();
                    return ;
                }

                // Connect to server.
                iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
                if (iResult == SOCKET_ERROR) {
                    closesocket(ConnectSocket);
                    ConnectSocket = INVALID_SOCKET;
                    continue;
                }
                break;
            }

            freeaddrinfo(result);

            if (ConnectSocket == INVALID_SOCKET) {
                sprintf_s(buff,"Unable to connect to server!\n");
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                WSACleanup();
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                return ;
            }

            // Send an initial buffer

            iResult = send( ConnectSocket, sendbuf.c_str(), sendbuf.length(), 0 );
            if (iResult == SOCKET_ERROR) {
                sprintf_s(buff,"send failed with error: %d\n", WSAGetLastError());
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                closesocket(ConnectSocket);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                WSACleanup();
                return ;
            }

            // shutdown the connection since no more data will be sent
            iResult = shutdown(ConnectSocket, SD_SEND);
            if (iResult == SOCKET_ERROR) {
                sprintf_s(buff,"shutdown failed with error: %d\n", WSAGetLastError());
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                closesocket(ConnectSocket);
                WSACleanup();
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                return ;
            }
            std::string receivedata="";
            // Receive until the peer closes the connection
            iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
            receivedata.resize(recvbuflen);
            receivedata=recvbuf;


            if ( iResult > 0 )
            {
                if( receivedata.find(sendbuf.c_str())!=std::string::npos)
                {
                    found =receivedata.find("%%%");
                    if(found!=std::string::npos)
                    {
                        receivedata=receivedata.substr(found+3);
                        found =receivedata.find("%%%");
                        if(found!=std::string::npos)
                        {
                            receivedata=receivedata.substr(0,found);
                        }
                    }

                    triallic.append(receivedata.c_str());

                    if(triallic=="" )
                    {
                        MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
                    }
                    else
                    {


                        std::string mac[10]= {""};
                        std::string runtime = DISKID(aes, &mac[0]);
                        int checkvalue=0;
                        while (checkvalue==0)
                        {
                            aes= aes_encrypt(triallic, STRING_TWO_DEFINE_NAME);
                            aes.append("%%%");
                            aes=aes.c_str();
                            if(aes.find("%%%")!=std::string::npos)
                            {
                                aes= aes.substr(0, aes.find("%%%"));
                                checkvalue=1;
                            }
                        }

                        if((USBlock==1) )
                        {
                            char buff[128]="";
                            char buff1[512]="";
                            int retVal ;
                            retVal = open_dongle();
                            if( retVal == 1)
                            {
                                if (read_dongle_flash(buff, 0, 64) == 1)
                                {
                                    std::string IDUNIQUE= buff;
                                    if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                                    {
                                        if (read_dongle((char *)buff1, 0, 256) == 1)
                                        {
                                            std::string checkstring = (char *)buff1;
                                            if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                            {
                                                checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                                std::string   lockcode1=checkstring;
                                                size_t found1 = lockcode1.find("---v2---");
                                                if (found1!=std::string::npos)
                                                {
                                                    lockcode1=lockcode1.substr(0,found1);
                                                    USBlock=1;
                                                }
                                                std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                size_t found2 = dec.find("---vn---");
                                                if (found2!=std::string::npos)
                                                {

                                                    network = dec.substr(found2);
                                                    dec=dec.substr(0,found2);
                                                }

                                                std::string divlic[10]= {""};
                                                divlic[0]=dec;
                                                std::string lic= LICENSE(&divlic[0]);
                                                dec=lic;
                                                std::string check =divlic[0];
                                                dec.append(check);

                                                //MessageBox(network.c_str(),"NETWORK",0);
                                                if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                {
                                                    checkstring="";
                                                }

                                            }
                                            std::string checkstring2 = aes.append("%%%");
                                            if(checkstring=="")
                                            {
                                                if (m_check.GetCheck())
                                                {
                                                    char bufft[4];
                                                    if (read_dongle_flash(bufft, 126, 2) == 1)
                                                    {
                                                        std::string TIME= (char *)bufft;
                                                        if(TIME!="1")
                                                        {
                                                            MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                            return;
                                                        }
                                                    }
                                                }
                                                write_dongle((char *)"", 256, 256);
                                                if(write_dongle((char *)checkstring2.c_str(), 0, 256) == 1)
                                                {
                                                    MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                    /*		if(Customeremail!="")
                                                    					{
                                                    InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                    Customeremail="";
                                                    					}*/
                                                    m_checkcust.SetCheck(false);
                                                    aes="";
                                                    USBID="";
                                                    return;
                                                }
                                            }
                                            if(checkstring!="")
                                            {
                                                if (read_dongle((char *)buff1,256, 256) == 1)
                                                {
                                                    std::string checkstring3 = (char *)buff1;
                                                    if(checkstring3!=""&& checkstring3.find("%%%")!=std::string::npos)
                                                    {
                                                        checkstring3=checkstring3.substr(0,checkstring3.find("%%%"));
                                                        std::string   lockcode1=checkstring3;
                                                        size_t found1 = lockcode1.find("---v2---");
                                                        if (found1!=std::string::npos)
                                                        {
                                                            lockcode1=lockcode1.substr(0,found1);
                                                            USBlock=1;
                                                        }
                                                        std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                        size_t found2 = dec.find("---vn---");
                                                        if (found2!=std::string::npos)
                                                        {

                                                            network = dec.substr(found2);
                                                            dec=dec.substr(0,found2);
                                                        }

                                                        std::string divlic[10]= {""};
                                                        divlic[0]=dec;
                                                        std::string lic= LICENSE(&divlic[0]);
                                                        dec=lic;
                                                        std::string check =divlic[0];
                                                        dec.append(check);

                                                        //MessageBox(network.c_str(),"NETWORK",0);
                                                        if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                        {
                                                            checkstring3="";
                                                        }

                                                    }
                                                    std::string checkstring2 = aes.append("%%%");
                                                    if(checkstring3=="")
                                                    {
                                                        if (m_check.GetCheck())
                                                        {
                                                            char bufft[4];
                                                            if (read_dongle_flash(bufft, 126, 2) == 1)
                                                            {
                                                                std::string TIME= (char *)bufft;
                                                                if(TIME!="1")
                                                                {
                                                                    MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                                    return;
                                                                }
                                                            }
                                                        }
                                                        read_dongle((char *)buff1, 0, 256);
                                                        if(write_dongle((char *)checkstring2.c_str(), 256, 256) == 1)
                                                        {
                                                            write_dongle((char *)buff1, 0, 256);
                                                            MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                            /*	if(Customeremail!="")
                                                            				{
                                                            InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                            Customeremail="";
                                                            				}*/
                                                            m_checkcust.SetCheck(false);
                                                            aes="";
                                                            USBID="";
                                                            return;
                                                        }
                                                    }
                                                    if(checkstring3!="")
                                                    {
                                                        MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                            long nRet = 0;

                            nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                            if(0 == nRet)
                            {
                                nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                                if( 0 == nRet)
                                {

                                    for(int l =0; l<4; l++)
                                    {
                                        unsigned char outdata[512];
                                        nRet = SmartX1ReadStorage(l*256, 256, outdata);
                                        if(!nRet) {

                                            std::string checkstring = (char *)outdata;
                                            if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                            {
                                                checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                                std::string   lockcode1=checkstring;
                                                size_t found1 = lockcode1.find("---v2---");
                                                if (found1!=std::string::npos)
                                                {
                                                    lockcode1=lockcode1.substr(0,found1);
                                                    USBlock=1;
                                                }
                                                std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                size_t found2 = dec.find("---vn---");
                                                if (found2!=std::string::npos)
                                                {

                                                    network = dec.substr(found2);
                                                    dec=dec.substr(0,found2);
                                                }

                                                std::string divlic[10]= {""};
                                                divlic[0]=dec;
                                                std::string lic= LICENSE(&divlic[0]);
                                                dec=lic;
                                                std::string check =divlic[0];
                                                dec.append(check);

                                                //MessageBox(network.c_str(),"NETWORK",0);
                                                if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                {
                                                    checkstring="";
                                                }

                                            }
                                            std::string checkstring2 = aes.append("%%%");
                                            if(checkstring=="")
                                            {
                                                nRet = SmartX1WriteStorage(l*256, 256, (unsigned char *)checkstring2.c_str());
                                                l=100;
                                                MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);

                                                /*		if(Customeremail!="")
                                                {
                                                InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                Customeremail="";
                                                }*/
                                                m_checkcust.SetCheck(false);
                                                aes="";
                                                USBID="";
                                                nRet = SmartX1Close();

                                                return;
                                            }
                                            if(checkstring!="" && l==3 ) {
                                                MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                                return;
                                            }
                                        }
                                    }
                                    SmartX1Close();
                                }

                            }
                            lc_handle_t handle;
                            int res=0,k=0;
                            // opening LC device
                            res = LC_open(1112100422, 0, &handle);
                            if(!res) {

                                res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
                                if(!res) {
                                    // read back the data just writed into block 0
                                    for(int l =0; l<4; l++)
                                    {
                                        unsigned char outdata[512];
                                        res = LC_read(handle, l, outdata);
                                        if(!res) {

                                            std::string checkstring = (char *)outdata;
                                            if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                            {
                                                checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                                std::string   lockcode1=checkstring;
                                                size_t found1 = lockcode1.find("---v2---");
                                                if (found1!=std::string::npos)
                                                {
                                                    lockcode1=lockcode1.substr(0,found1);
                                                    USBlock=1;
                                                }
                                                std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                size_t found2 = dec.find("---vn---");
                                                if (found2!=std::string::npos)
                                                {

                                                    network = dec.substr(found2);
                                                    dec=dec.substr(0,found2);
                                                }

                                                std::string divlic[10]= {""};
                                                divlic[0]=dec;
                                                std::string lic= LICENSE(&divlic[0]);
                                                dec=lic;
                                                std::string check =divlic[0];
                                                dec.append(check);

                                                //MessageBox(network.c_str(),"NETWORK",0);
                                                if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                {
                                                    checkstring="";
                                                }

                                            }
                                            std::string checkstring2 = aes.append("%%%");
                                            if(checkstring=="")
                                            {
                                                res = LC_write(handle, l, (unsigned char *)checkstring2.c_str());
                                                l=100;
                                                MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                /*		if(Customeremail!="")
                                                {
                                                InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                Customeremail="";
                                                }*/
                                                m_checkcust.SetCheck(false);
                                                aes="";
                                                USBID="";
                                                LC_close(handle);
                                                return;
                                            }
                                            if(checkstring!="" && l==3 ) {
                                                MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                            }
                                        }
                                    }
                                }
                                LC_close(handle);
                            }
                            else
                            {
                                MessageBox("DONGLE NOT FOUND","ERROR",MB_ICONERROR);
                            }

                        }
                        std::ofstream ifile;
                        ifile.open(filename1,ios::out|ios::binary );
                        ifile.seekp (0, ios::beg);
                        ifile.write (&aes[0],strlen(aes.c_str()));

                        size_t debug=0;
                        size_t debug1=0;
                        debug=strlen(aes.c_str())+1;
                        debug1 =aes.length()+1;
                        while (debug<debug1)
                        {
                            cout<<debug;

                            ifile.write("\0",1);
                            ifile.write(&aes[debug],strlen(&aes[debug]) );
                            debug=strlen(&aes[debug])+debug+1;
                        }

                        ////////////////////////////////////////////////////////////////////////

                        ifile.write("%%%",3);
                        //////////////////////////////////////////////////////////////////////////////////
                        ifile.close();

                        MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                        aes="";
                        USBID="";
                        return;
                    }

                }
                else
                {
                    MessageBox(receivedata.c_str(),"ERROR",MB_ICONSTOP);
                    aes="";
                    return;
                }
            }
            else if ( iResult == 0 )
            {
                sprintf_s(buff,"Connection closed\n");
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
            }
            else
            {
                sprintf_s(buff,"recv failed with error: %d\n", WSAGetLastError());
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
            }
            // cleanup
            closesocket(ConnectSocket);
            WSACleanup();
            return;
        }

    }

    if(Customeremail=="")
    {

    }
    else if(Customeremail.find("EMAIL")!=std::string::npos)
        Customeremail = Customeremail.substr(Customeremail.find("EMAIL")+8);
    int result =0;
    /*	result =SEARCHSYSINFO2(&sysinfodata);
    	if( result!=0)
    	{
    		 if(sysinfodata!="")
    		  {
    	InsertSysInfo( &Customeremail, &sysinfodata);
    		 }
    	}
    	*/
    if(USBlock==0 || USBlock==2)
    {
        if(aes=="")
        {
            MessageBox("SYSINFO FILE NOT LOADED","ERROR",MB_ICONERROR);
            return;
        }
    }

    cTxtLen = m_edit4.GetWindowTextLength();
    m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string memory= buff;
    readonlydata=buff;
    std::string lic="";
    if (m_check.GetCheck())
    {
        SYSTEMTIME TimeDest;




        m_date1.GetTime(&TimeDest);
        int startdate = TimeDest.wDay*100 + TimeDest.wMonth;
        int startYear= TimeDest.wYear;
        char buffdate[200];
        sprintf_s(buffdate,"%d", TimeDest.wDay);
        sdate="";
        sdate.append(buffdate);
        sdate.append("/");
        sprintf_s(buffdate,"%d", TimeDest.wMonth);
        sdate.append(buffdate);
        sdate.append("/");
        sprintf_s(buffdate,"%d", TimeDest.wYear);
        sdate.append(buffdate);

        m_date2.GetTime(&TimeDest);
        int enddate = TimeDest.wDay*100 + TimeDest.wMonth;
        int endYear= TimeDest.wYear;
        sprintf_s(buffdate,"%d", TimeDest.wDay);
        edate="";
        edate.append(buffdate);
        edate.append("/");
        sprintf_s(buffdate,"%d", TimeDest.wMonth);
        edate.append(buffdate);
        edate.append("/");
        sprintf_s(buffdate,"%d", TimeDest.wYear);
        edate.append(buffdate);

        if (USBlock==0 || USBlock==2)
        {
            lic=  getLicense (STRING_FIVE_DEFINE_NAME, appid,v, 3 ,aes,memory,startdate,startYear,enddate,endYear);
            if (USBlock==2)
            {
                size_t found;
                found = USBID.find("HID\\VID_0925&PID_A508");
                if (found==std::string::npos)
                {
                    found = USBID.find("HID\\VID_0925&PID_2000");
                    if (found==std::string::npos)
                    {
                        found = USBID.find("HID\\VID_1BC0&PID_8101");
                    }
                }
                if (found==std::string::npos)
                {
                    USBID="";
                    USBID.resize(1024);
                    int result =  AG_CheckUSB (&USBID);
                    usbiddata=USBID;
                    if(result!=0)
                    {
                        MessageBox("PLEASE ENTER CORRECT USB ID","ERROR",MB_ICONERROR);
                        return;
                    }
                }
            }
        }
        else if (USBlock==1)
        {
            size_t found;
            found = USBID.find("HID\\VID_0925&PID_A508");
            if (found==std::string::npos)
            {
                found = USBID.find("HID\\VID_0925&PID_2000");
                if (found==std::string::npos)
                {
                    found = USBID.find("HID\\VID_1BC0&PID_8101");
                }
            }
            if (found==std::string::npos)
            {
                USBID="";
                int result =  AG_CheckUSB (&USBID);
                usbiddata=USBID;
                if(result!=0)
                {
                    MessageBox("PLEASE ENTER CORRECT USB ID","ERROR",MB_ICONERROR);
                    return;
                }
            }
            lic=  getLicense1 (STRING_FIVE_DEFINE_NAME, appid,v, 1 ,USBID,memory,startdate,startYear,enddate,endYear);
        }
    }
    else
    {
        if (USBlock==0 || USBlock==2)
        {
            lic=  getLicense (STRING_FIVE_DEFINE_NAME, appid,v, 3 ,aes,memory);
            if (USBlock==2)
            {
                size_t found;
                found = USBID.find("HID\\VID_0925&PID_A508");
                if (found==std::string::npos)
                {
                    found = USBID.find("HID\\VID_0925&PID_2000");
                    if (found==std::string::npos)
                    {
                        found = USBID.find("HID\\VID_1BC0&PID_8101");
                    }
                }
                if (found==std::string::npos)
                {
                    USBID="";
                    int result =  AG_CheckUSB (&USBID);
                    usbiddata=USBID;
                    if(result!=0)
                    {
                        MessageBox("PLEASE ENTER CORRECT USB ID","ERROR",MB_ICONERROR);
                        return;
                    }
                }
            }
        }

        else if (USBlock==1)
        {
            size_t found;
            found = USBID.find("HID\\VID_0925&PID_A508");
            if (found==std::string::npos)
            {
                found = USBID.find("HID\\VID_0925&PID_2000");
                if (found==std::string::npos)
                {
                    found = USBID.find("HID\\VID_1BC0&PID_8101");
                }
            }
            if (found==std::string::npos)
            {
                USBID="";
                int result =  AG_CheckUSB (&USBID);
                usbiddata=USBID;
                if(result!=0)
                {
                    MessageBox("PLEASE ENTER CORRECT USB ID","ERROR",MB_ICONERROR);
                    return;
                }
            }
            lic=  getLicense1 (STRING_FIVE_DEFINE_NAME, appid,v, 1 ,USBID,memory);
        }
    }
    std::string checkconn="";
    cTxtLen = m_edit7.GetWindowTextLength();
    m_edit7.GetWindowTextA(&buff[0],cTxtLen+1);
    checkconn=buff;
    if (checkconn.find("LOCAL MACHINE")!=std::string::npos)
    {
        if(lic=="")
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            return ;
        }
        else
        {
            lic.append("--");
            WORD value1=0;
            int j = strtol(STRING_SIX_DEFINE_NAME,NULL,10)+60;
            if(USBlock!=1)
            {
                ReadSectors(0,j,1,&value1,STRING_SIX_DEFINE_NAME);
                value1=value1+1;
                if (value1=='\0')
                {
                    WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                }
                else
                {
                    if (value1>5000)
                    {
                        WriteSectors(0,j,1,0,STRING_SIX_DEFINE_NAME);
                    }
                    else
                    {
                        WriteSectors(0,j,1,value1,STRING_SIX_DEFINE_NAME);
                    }
                }
            }
            char buffc[1024]="";
            sprintf_s(buffc,"%d", value1);
            lic.append(buffc);
            lic.append("--");
            sprintf_s(buffc,"%d", j);
            lic.append(buffc);
            lic.append("%");
            if (USBlock==2)
            {

                lic.append("---v3---");
                lic.append(USBID.c_str());
            }
            filedlg fdlg(false,"upd","license",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.upd",NULL);

            fdlg.DoModal();
            string filename1= fdlg.GetFileName();


            if(filename1=="")
            {

                return;
            }
            WORD licvalnet;
            filename1 = fdlg.GetPathName();
            std::string mac[10]= {""};
            std::string runtime = DISKID(aes, &mac[0]);
            /////////////////network//////////////////
            if(m_check4.GetCheck())
            {
                char buffdays[1024]="";
                cTxtLen = m_edit9.GetWindowTextLength();
                lic.append("---vn---");
                m_edit9.GetWindowTextA(&buffdays[0],cTxtLen+1);
                licvalnet = strtol(buffdays,NULL,10);
                network = buffdays;
                lic.append(buffdays);
                lic.append("--");

            }
            if (USBlock==0 || USBlock==2)
            {

                int checkvalue=0;
                while (checkvalue==0)
                {
                    aes= aes_encrypt(lic, STRING_THREE_DEFINE_NAME+runtime);
                    aes.append("%%%");
                    aes=aes.c_str();
                    if(aes.find("%%%")!=std::string::npos)
                    {
                        aes= aes.substr(0, aes.find("%%%"));
                        checkvalue=1;
                    }
                }
            }
            if (USBlock==1)
            {

                int checkvalue=0;
                while (checkvalue==0)
                {
                    aes= aes_encrypt(lic, STRING_TWO_DEFINE_NAME);
                    aes.append("%%%");
                    aes=aes.c_str();
                    if(aes.find("%%%")!=std::string::npos)
                    {
                        aes= aes.substr(0, aes.find("%%%"));
                        checkvalue=1;
                    }
                }

                aes.append("---v2---");
            }

            WORD licval1=0;
            int r=0;

            if (r !=0)
            {
                MessageBox("Please Check that you have enough permissions","ERROR",MB_ICONERROR);
                EndDialog(0);
                return;
            }
            if(m_check4.GetCheck())
            {
                ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);

                WORD licval1net=0;
                ReadSectors(0,59,1,&licval1net,STRING_SIX_DEFINE_NAME);

                if(licval1net>=licvalnet)
                {
                    if (licval1>0 || (USBlock==1))
                    {
                        if(licval1net!=6555)
                            WriteSectors(0,59,1,licval1net-licvalnet,STRING_SIX_DEFINE_NAME);
                    }
                    else
                    {
                        MessageBox("NO LICENSE - PLEASE PURCHASE LICENSES","ERROR",MB_ICONSTOP);
                        EndDialog(0);
                    }
                }
                else
                {
                    MessageBox("NOT ENOUGH NETWORK LICENSES - PLEASE PURCHASE LICENSES","DONE",MB_ICONSTOP);
                    EndDialog(0);
                    return;
                }
            }
            if((USBlock!=1) )
                ReadSectors(0,58,1,&licval1,STRING_SIX_DEFINE_NAME);
            if (licval1>0 || (USBlock==1))
            {
                if((USBlock==1) )
                {
                    char buff[128]="";
                    char buff1[512]="";
                    int retVal ;
                    retVal = open_dongle();

                    if( retVal == 1)
                    {
                        if (read_dongle_flash(buff, 0, 64) == 1)
                        {

                            std::string IDUNIQUE= buff;
                            if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                            {
                                if (read_dongle((char *)buff1, 0, 256) == 1)
                                {
                                    std::string checkstring = (char *)buff1;
                                    if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                    {
                                        checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                        std::string   lockcode1=checkstring;
                                        size_t found1 = lockcode1.find("---v2---");
                                        if (found1!=std::string::npos)
                                        {
                                            lockcode1=lockcode1.substr(0,found1);
                                            USBlock=1;
                                        }
                                        std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                        size_t found2 = dec.find("---vn---");
                                        if (found2!=std::string::npos)
                                        {

                                            network = dec.substr(found2);
                                            dec=dec.substr(0,found2);
                                        }

                                        std::string divlic[10]= {""};
                                        divlic[0]=dec;
                                        std::string lic= LICENSE(&divlic[0]);
                                        dec=lic;
                                        std::string check =divlic[0];
                                        dec.append(check);

                                        //MessageBox(network.c_str(),"NETWORK",0);
                                        if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                        {
                                            checkstring="";
                                        }

                                    }
                                    std::string checkstring2 = aes.append("%%%");
                                    if(checkstring=="")
                                    {
                                        if (m_check.GetCheck())
                                        {
                                            char bufft[4];
                                            if (read_dongle_flash(bufft, 126, 2) == 1)
                                            {
                                                std::string TIME= (char *)bufft;
                                                if(TIME!="1")
                                                {
                                                    MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                    return;
                                                }
                                            }
                                        }
                                        write_dongle((char *)"", 256, 256);
                                        if(write_dongle((char *)checkstring2.c_str(), 0, 256) == 1)
                                        {
                                            MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                            /*		if(Customeremail!="")
                                            					{
                                            InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                            Customeremail="";
                                            					}*/
                                            m_checkcust.SetCheck(false);
                                            aes="";
                                            USBID="";
                                            return;
                                        }
                                    }
                                    if(checkstring!="")
                                    {
                                        if (read_dongle((char *)buff1,256, 256) == 1)
                                        {
                                            std::string checkstring3 = (char *)buff1;
                                            if(checkstring3!=""&& checkstring3.find("%%%")!=std::string::npos)
                                            {
                                                checkstring3=checkstring3.substr(0,checkstring3.find("%%%"));
                                                std::string   lockcode1=checkstring3;
                                                size_t found1 = lockcode1.find("---v2---");
                                                if (found1!=std::string::npos)
                                                {
                                                    lockcode1=lockcode1.substr(0,found1);
                                                    USBlock=1;
                                                }
                                                std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                size_t found2 = dec.find("---vn---");
                                                if (found2!=std::string::npos)
                                                {

                                                    network = dec.substr(found2);
                                                    dec=dec.substr(0,found2);
                                                }

                                                std::string divlic[10]= {""};
                                                divlic[0]=dec;
                                                std::string lic= LICENSE(&divlic[0]);
                                                dec=lic;
                                                std::string check =divlic[0];
                                                dec.append(check);

                                                //MessageBox(network.c_str(),"NETWORK",0);
                                                if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                {
                                                    checkstring3="";
                                                }

                                            }
                                            std::string checkstring2 = aes.append("%%%");
                                            if(checkstring3=="")
                                            {
                                                if (m_check.GetCheck())
                                                {
                                                    char bufft[4];
                                                    if (read_dongle_flash(bufft, 126, 2) == 1)
                                                    {
                                                        std::string TIME= (char *)bufft;
                                                        if(TIME!="1")
                                                        {
                                                            MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                            return;
                                                        }
                                                    }
                                                }
                                                read_dongle((char *)buff1, 0, 256);
                                                if(write_dongle((char *)checkstring2.c_str(), 256, 256) == 1)
                                                {
                                                    write_dongle((char *)buff1, 0, 256);
                                                    MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                    /*	if(Customeremail!="")
                                                    				{
                                                    InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                    Customeremail="";
                                                    				}*/
                                                    m_checkcust.SetCheck(false);
                                                    aes="";
                                                    USBID="";
                                                    return;
                                                }
                                            }
                                            if(checkstring3!="")
                                            {
                                                MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }

                    long nRet = 0;

                    nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                    if(0 == nRet)
                    {
                        nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                        if( 0 == nRet)
                        {

                            for(int l =0; l<4; l++)
                            {
                                unsigned char outdata[512];
                                nRet = SmartX1ReadStorage(l*256, 256, outdata);
                                if(!nRet) {

                                    std::string checkstring = (char *)outdata;
                                    if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                    {
                                        checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                        std::string   lockcode1=checkstring;
                                        size_t found1 = lockcode1.find("---v2---");
                                        if (found1!=std::string::npos)
                                        {
                                            lockcode1=lockcode1.substr(0,found1);
                                            USBlock=1;
                                        }
                                        std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                        size_t found2 = dec.find("---vn---");
                                        if (found2!=std::string::npos)
                                        {

                                            network = dec.substr(found2);
                                            dec=dec.substr(0,found2);
                                        }

                                        std::string divlic[10]= {""};
                                        divlic[0]=dec;
                                        std::string lic= LICENSE(&divlic[0]);
                                        dec=lic;
                                        std::string check =divlic[0];
                                        dec.append(check);

                                        //MessageBox(network.c_str(),"NETWORK",0);
                                        if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                        {
                                            checkstring="";
                                        }

                                    }
                                    std::string checkstring2 = aes.append("%%%");
                                    if(checkstring=="")
                                    {
                                        nRet = SmartX1WriteStorage(l*256, 256, (unsigned char *)checkstring2.c_str());
                                        l=100;
                                        MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);

                                        /*		if(Customeremail!="")
                                        {
                                        InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                        Customeremail="";
                                        }*/
                                        m_checkcust.SetCheck(false);
                                        aes="";
                                        USBID="";
                                        nRet = SmartX1Close();

                                        return;
                                    }
                                    if(checkstring!="" && l==3 ) {
                                        MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                        return;
                                    }
                                }
                            }
                            SmartX1Close();
                        }

                    }
                    lc_handle_t handle;
                    int res=0,k=0;
                    // opening LC device
                    res = LC_open(1112100422, 0, &handle);
                    if(!res) {

                        res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
                        if(!res) {
                            // read back the data just writed into block 0
                            for(int l =0; l<4; l++)
                            {
                                unsigned char outdata[512];
                                res = LC_read(handle, l, outdata);
                                if(!res) {

                                    std::string checkstring = (char *)outdata;
                                    if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                    {
                                        checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                        std::string   lockcode1=checkstring;
                                        size_t found1 = lockcode1.find("---v2---");
                                        if (found1!=std::string::npos)
                                        {
                                            lockcode1=lockcode1.substr(0,found1);
                                            USBlock=1;
                                        }
                                        std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                        size_t found2 = dec.find("---vn---");
                                        if (found2!=std::string::npos)
                                        {

                                            network = dec.substr(found2);
                                            dec=dec.substr(0,found2);
                                        }

                                        std::string divlic[10]= {""};
                                        divlic[0]=dec;
                                        std::string lic= LICENSE(&divlic[0]);
                                        dec=lic;
                                        std::string check =divlic[0];
                                        dec.append(check);

                                        //MessageBox(network.c_str(),"NETWORK",0);
                                        if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                        {
                                            checkstring="";
                                        }

                                    }
                                    std::string checkstring2 = aes.append("%%%");
                                    if(checkstring=="")
                                    {
                                        res = LC_write(handle, l, (unsigned char *)checkstring2.c_str());
                                        l=100;
                                        MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                        /*			if(Customeremail!="")
                                        {
                                        InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                        Customeremail="";
                                        }*/
                                        m_checkcust.SetCheck(false);
                                        aes="";
                                        USBID="";
                                        LC_close(handle);

                                        return;
                                    }
                                    if(checkstring!="" && l==3 ) {
                                        MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                    }
                                }
                            }
                        }
                        LC_close(handle);
                    }
                    else
                    {
                        MessageBox("DONGLE NOT FOUND","ERROR",MB_ICONERROR);
                    }

                }
                std::ofstream ifile;
                ifile.open(filename1,ios::out|ios::binary );
                ifile.seekp (0, ios::beg);
                ifile.write (&aes[0],strlen(aes.c_str()));

                size_t debug=0;
                size_t debug1=0;
                debug=strlen(aes.c_str())+1;
                debug1 =aes.length()+1;
                while (debug<debug1)
                {
                    cout<<debug;

                    ifile.write("\0",1);
                    ifile.write(&aes[debug],strlen(&aes[debug]) );
                    debug=strlen(&aes[debug])+debug+1;
                }

                /////////////////////////////////


                ////////////////////////////////////

                ifile.close();

                _mkdir("C:/Users");
                _mkdir("C:/Users/Public");
                _mkdir("C:/Users/Public/Application Data");
                _mkdir("C:/Users/Public/Application Data/Bastion");
                SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.agl",FILE_ATTRIBUTE_NORMAL);
                std::ofstream ifilec;
                ifilec.open("C:/Users/Public/Application Data/Bastion/AG.agl",ios::out|ios::binary);
//	ifile.seekp(ios::ate);
                char bufflic[1024]="";
                sprintf_s(bufflic,"%d", licval1-1);
                lockcode=bufflic;
                std::string runtime = getHardDriveComputerID ();
                lockcode= aes_encrypt(lockcode, STRING_FOUR_DEFINE_NAME+runtime);


                if(USBlock!=1 && licval1!=6555)
                    int r = WriteSectors(0,58,1,licval1-1,STRING_SIX_DEFINE_NAME);

                if (ifilec.is_open())
                {
                    ifilec.seekp (0, ios::beg);
                    ifilec.write (&lockcode[0],strlen(lockcode.c_str()));

                    size_t debug=0;
                    size_t debug1=0;
                    debug=strlen(lockcode.c_str())+1;
                    debug1 =lockcode.length()+1;
                    while (debug<debug1)
                    {
//	cout<<debug;

                        ifilec.write("\0",1);
                        ifilec.write(&lockcode[debug],strlen(&lockcode[debug]) );
                        debug=strlen(&lockcode[debug])+debug+1;
                    }

                    ifilec.close();
                }
                else
                {

                    MessageBox("FILE I/O ERROR","ERROR",MB_ICONERROR);
                    EndDialog(0);
                    return;
                }
                MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                USBID="";
                ///////database
                /*	if(Customeremail!="")
                	{
                	InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                	Customeremail="";
                	}*/
                m_checkcust.SetCheck(false);
                return;

            }
            else
            {
                MessageBox("NO LICENSE - PLEASE PURCHASE LICENSES","ERROR",MB_ICONSTOP);
                EndDialog(0);
            }

        }
    }
    else
    {
        if(lic=="")
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            return ;
        }
        filedlg fdlg(false,"upd","license",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.upd",NULL);

        fdlg.DoModal();
        string filename1= fdlg.GetFileName();


        if(filename1=="")
        {

            return;
        }
        if(servername=="")
        {
            MessageBox("PLEASE PROVIDE SERVER NAME","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        size_t found = servername.find("--");
        if (found!=std::string::npos)
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        if (m_check4.GetCheck())
        {
            MessageBox("NETWORK LICENSE COULD ONLY BE GENERATED \nFROM LOCAL MACHINE","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        WSADATA wsaData;
        SOCKET ConnectSocket = INVALID_SOCKET;
        struct addrinfo *result = NULL,
                             *ptr = NULL,
                              hints;
        char sendd[14];
        for(int i=0; i<14; i++)
        {
            sendd[i]=rand();
        }
        std::string sendbuf =sendd;
        sendbuf.append("---Bastion-v3trial--");

        char recvbuf[DEFAULT_BUFLEN];
        int iResult;
        int recvbuflen = DEFAULT_BUFLEN;



        // Initialize Winsock
        iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
        if (iResult != 0) {
            sprintf_s(buff,"WSAStartup failed with error: %d\n", iResult);
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        ZeroMemory( &hints, sizeof(hints) );
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        // Resolve the server address and port
        iResult = getaddrinfo(servername.c_str(), DEFAULT_PORT, &hints, &result);
        if ( iResult != 0 ) {
            sprintf_s(buff,"Server \"%s\" not found\n", servername.c_str());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            WSACleanup();
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        // Attempt to connect to an address until one succeeds
        for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

            // Create a SOCKET for connecting to server
            ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                                   ptr->ai_protocol);
            if (ConnectSocket == INVALID_SOCKET) {
                sprintf_s(buff,"socket failed with error: %ld\n", WSAGetLastError());
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                WSACleanup();
                return ;
            }

            // Connect to server.
            iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
            if (iResult == SOCKET_ERROR) {
                closesocket(ConnectSocket);
                ConnectSocket = INVALID_SOCKET;
                continue;
            }
            break;
        }

        freeaddrinfo(result);

        if (ConnectSocket == INVALID_SOCKET) {
            sprintf_s(buff,"Unable to connect to server!\n");
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            WSACleanup();
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        // Send an initial buffer

        iResult = send( ConnectSocket, sendbuf.c_str(), sendbuf.length(), 0 );
        if (iResult == SOCKET_ERROR) {
            sprintf_s(buff,"send failed with error: %d\n", WSAGetLastError());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            closesocket(ConnectSocket);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            WSACleanup();
            return ;
        }

        // shutdown the connection since no more data will be sent
        iResult = shutdown(ConnectSocket, SD_SEND);
        if (iResult == SOCKET_ERROR) {
            sprintf_s(buff,"shutdown failed with error: %d\n", WSAGetLastError());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            closesocket(ConnectSocket);
            WSACleanup();
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }
        std::string receivedata="";
        // Receive until the peer closes the connection
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        receivedata.resize(recvbuflen);
        receivedata=recvbuf;


        if ( iResult > 0 )
        {
            if( receivedata.find(sendbuf.c_str())!=std::string::npos)
            {

                found =receivedata.find("%%%");
                if(found!=std::string::npos)
                {
                    receivedata=receivedata.substr(found+3);
                    found =receivedata.find("%%%");
                    if(found!=std::string::npos)
                    {
                        receivedata=receivedata.substr(0,found);
                    }
                }

                if(lic=="")
                {
                    MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
                    return ;
                }
                else
                {
                    lic.append("--");

                    lic.append(receivedata.c_str());
                    lic.append("%");

                    if (USBlock==2)
                    {
                        lic.append("---v3---");
                        lic.append(USBID.c_str());
                    }
                    filename1 = fdlg.GetPathName();
                    std::string mac[10]= {""};
                    std::string runtime = DISKID(aes, &mac[0]);
                    if (USBlock==0 || USBlock==2)
                    {

                        int checkvalue=0;
                        while (checkvalue==0)
                        {
                            aes= aes_encrypt(lic, STRING_THREE_DEFINE_NAME+runtime);
                            aes.append("%%%");
                            aes=aes.c_str();
                            if(aes.find("%%%")!=std::string::npos)
                            {
                                aes= aes.substr(0, aes.find("%%%"));
                                checkvalue=1;
                            }
                        }
                    }
                    if (USBlock==1)
                    {
                        int checkvalue=0;
                        while (checkvalue==0)
                        {
                            aes= aes_encrypt(lic, STRING_TWO_DEFINE_NAME);
                            aes.append("%%%");
                            aes=aes.c_str();
                            if(aes.find("%%%")!=std::string::npos)
                            {
                                aes= aes.substr(0, aes.find("%%%"));
                                checkvalue=1;
                            }
                        }
                        aes.append("---v2---");
                    }

                    if((USBlock==1) )
                    {
                        char buff[128]="";
                        char buff1[512]="";
                        int retVal ;
                        retVal = open_dongle();
                        if( retVal == 1)
                        {
                            if (read_dongle_flash(buff, 0, 64) == 1)
                            {
                                std::string IDUNIQUE= buff;
                                if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
                                {
                                    if (read_dongle((char *)buff1, 0, 256) == 1)
                                    {
                                        std::string checkstring = (char *)buff1;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = dec.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = dec.substr(found2);
                                                dec=dec.substr(0,found2);
                                            }

                                            std::string divlic[10]= {""};
                                            divlic[0]=dec;
                                            std::string lic= LICENSE(&divlic[0]);
                                            dec=lic;
                                            std::string check =divlic[0];
                                            dec.append(check);

                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = aes.append("%%%");
                                        if(checkstring=="")
                                        {
                                            if (m_check.GetCheck())
                                            {
                                                char bufft[4];
                                                if (read_dongle_flash(bufft, 126, 2) == 1)
                                                {
                                                    std::string TIME= (char *)bufft;
                                                    if(TIME!="1")
                                                    {
                                                        MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                        return;
                                                    }
                                                }
                                            }
                                            write_dongle((char *)"", 256, 256);
                                            if(write_dongle((char *)checkstring2.c_str(), 0, 256) == 1)
                                            {
                                                MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                /*		if(Customeremail!="")
                                                					{
                                                InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                Customeremail="";
                                                					}*/
                                                m_checkcust.SetCheck(false);
                                                aes="";
                                                USBID="";
                                                return;
                                            }
                                        }
                                        if(checkstring!="")
                                        {
                                            if (read_dongle((char *)buff1,256, 256) == 1)
                                            {
                                                std::string checkstring3 = (char *)buff1;
                                                if(checkstring3!=""&& checkstring3.find("%%%")!=std::string::npos)
                                                {
                                                    checkstring3=checkstring3.substr(0,checkstring3.find("%%%"));
                                                    std::string   lockcode1=checkstring3;
                                                    size_t found1 = lockcode1.find("---v2---");
                                                    if (found1!=std::string::npos)
                                                    {
                                                        lockcode1=lockcode1.substr(0,found1);
                                                        USBlock=1;
                                                    }
                                                    std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                                    size_t found2 = dec.find("---vn---");
                                                    if (found2!=std::string::npos)
                                                    {

                                                        network = dec.substr(found2);
                                                        dec=dec.substr(0,found2);
                                                    }

                                                    std::string divlic[10]= {""};
                                                    divlic[0]=dec;
                                                    std::string lic= LICENSE(&divlic[0]);
                                                    dec=lic;
                                                    std::string check =divlic[0];
                                                    dec.append(check);

                                                    //MessageBox(network.c_str(),"NETWORK",0);
                                                    if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                                    {
                                                        checkstring3="";
                                                    }

                                                }
                                                std::string checkstring2 = aes.append("%%%");
                                                if(checkstring3=="")
                                                {
                                                    if (m_check.GetCheck())
                                                    {
                                                        char bufft[4];
                                                        if (read_dongle_flash(bufft, 126, 2) == 1)
                                                        {
                                                            std::string TIME= (char *)bufft;
                                                            if(TIME!="1")
                                                            {
                                                                MessageBox("PLEASE PURCHASE TIME KEYS","ERROR",MB_ICONERROR);
                                                                return;
                                                            }
                                                        }
                                                    }
                                                    read_dongle((char *)buff1, 0, 256);
                                                    if(write_dongle((char *)checkstring2.c_str(), 256, 256) == 1)
                                                    {
                                                        write_dongle((char *)buff1, 0, 256);
                                                        MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                                        /*		if(Customeremail!="")
                                                        					{
                                                        InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                                        Customeremail="";
                                                        					}*/
                                                        m_checkcust.SetCheck(false);
                                                        aes="";
                                                        USBID="";
                                                        return;
                                                    }
                                                }
                                                if(checkstring3!="")
                                                {
                                                    MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        long nRet = 0;

                        nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
                        if(0 == nRet)
                        {
                            nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
                            if( 0 == nRet)
                            {

                                for(int l =0; l<4; l++)
                                {
                                    unsigned char outdata[512];
                                    nRet = SmartX1ReadStorage(l*256, 256, outdata);
                                    if(!nRet) {

                                        std::string checkstring = (char *)outdata;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = dec.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = dec.substr(found2);
                                                dec=dec.substr(0,found2);
                                            }

                                            std::string divlic[10]= {""};
                                            divlic[0]=dec;
                                            std::string lic= LICENSE(&divlic[0]);
                                            dec=lic;
                                            std::string check =divlic[0];
                                            dec.append(check);

                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = aes.append("%%%");
                                        if(checkstring=="")
                                        {
                                            nRet = SmartX1WriteStorage(l*256, 256, (unsigned char *)checkstring2.c_str());
                                            l=100;
                                            MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);

                                            /*			if(Customeremail!="")
                                            	{
                                            	InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                            	Customeremail="";
                                            	}*/
                                            m_checkcust.SetCheck(false);
                                            aes="";
                                            USBID="";
                                            nRet = SmartX1Close();

                                            return;
                                        }
                                        if(checkstring!="" && l==3 ) {
                                            MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                            return;
                                        }
                                    }
                                }
                                SmartX1Close();
                            }

                        }
                        lc_handle_t handle;
                        int res=0,k=0;
                        // opening LC device
                        res = LC_open(1112100422, 0, &handle);
                        if(!res) {

                            res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
                            if(!res) {
                                // read back the data just writed into block 0
                                for(int l =0; l<4; l++)
                                {
                                    unsigned char outdata[512];
                                    res = LC_read(handle, l, outdata);
                                    if(!res) {

                                        std::string checkstring = (char *)outdata;
                                        if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                                        {
                                            checkstring=checkstring.substr(0,checkstring.find("%%%"));
                                            std::string   lockcode1=checkstring;
                                            size_t found1 = lockcode1.find("---v2---");
                                            if (found1!=std::string::npos)
                                            {
                                                lockcode1=lockcode1.substr(0,found1);
                                                USBlock=1;
                                            }
                                            std::string dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                                            size_t found2 = dec.find("---vn---");
                                            if (found2!=std::string::npos)
                                            {

                                                network = dec.substr(found2);
                                                dec=dec.substr(0,found2);
                                            }

                                            std::string divlic[10]= {""};
                                            divlic[0]=dec;
                                            std::string lic= LICENSE(&divlic[0]);
                                            dec=lic;
                                            std::string check =divlic[0];
                                            dec.append(check);

                                            //MessageBox(network.c_str(),"NETWORK",0);
                                            if(	APPLICATIONID ==divlic[2] && VERSION == divlic[3])
                                            {
                                                checkstring="";
                                            }

                                        }
                                        std::string checkstring2 = aes.append("%%%");
                                        if(checkstring=="")
                                        {
                                            res = LC_write(handle, l, (unsigned char *)checkstring2.c_str());
                                            l=100;
                                            MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                                            /*		if(Customeremail!="")
                                            {
                                            InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                                            Customeremail="";
                                            }*/
                                            m_checkcust.SetCheck(false);
                                            aes="";
                                            USBID="";
                                            LC_close(handle);
                                            return;
                                        }
                                        if(checkstring!="" && l==3 ) {
                                            MessageBox("DONGLE NOT EMPTY","ERROR",MB_ICONERROR);
                                        }
                                    }
                                }
                            }
                            LC_close(handle);
                        }
                        else
                        {
                            MessageBox("DONGLE NOT FOUND","ERROR",MB_ICONERROR);
                        }

                    }
                    std::ofstream ifile;
                    ifile.open(filename1,ios::out|ios::binary );
                    ifile.seekp (0, ios::beg);
                    ifile.write (&aes[0],strlen(aes.c_str()));

                    size_t debug=0;
                    size_t debug1=0;
                    debug=strlen(aes.c_str())+1;
                    debug1 =aes.length()+1;
                    while (debug<debug1)
                    {
                        cout<<debug;

                        ifile.write("\0",1);
                        ifile.write(&aes[debug],strlen(&aes[debug]) );
                        debug=strlen(&aes[debug])+debug+1;
                    }

                    /////////////////////////////////


                    ////////////////////////////////////

                    ifile.close();


                    MessageBox("LICENSE UPDATE FILE CREATED","DONE",MB_ICONINFORMATION);
                    USBID="";
                    ///////database
                    /*	if(Customeremail!="")
                    	{
                    	InsertLicInfo(&sysinfodata,&appid,&versiondata, &sdate, &edate, &network,&lockingdata,&usbiddata,&readonlydata);
                    	Customeremail="";
                    	}*/
                    m_checkcust.SetCheck(false);
                    return;
                }
            }
            else
            {
                MessageBox(receivedata.c_str(),"ERROR",MB_ICONSTOP);
            }
        }
        else if ( iResult == 0 )
        {
            sprintf_s(buff,"Connection closed\n");
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
        }
        else
        {
            sprintf_s(buff,"recv failed with error: %d\n", WSAGetLastError());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();

        return;
    }

}



void CAG_AUTHENTICATORDlg::OnBnClickedCheck1()
{
    // TODO: Add your control notification handler code here
    if(m_check.GetCheck())
    {
        m_date1.EnableWindow(true);
        m_date2.EnableWindow(true);
        m_edit5.EnableWindow(false);
        m_check2.SetCheck(false);
    }
    else
    {
        m_date1.EnableWindow(false);
        m_date2.EnableWindow(false);
    }
}



void CAG_AUTHENTICATORDlg::OnEnChangeEdit2()
{
    // TODO:  If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CDialogEx::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.

    // TODO:  Add your control notification handler code here
}



void CAG_AUTHENTICATORDlg::OnBnClickedCheck2()
{
    // TODO: Add your control notification handler code here
    if(m_check2.GetCheck())
    {
        m_edit5.EnableWindow(true);
        m_check.SetCheck(false);
        m_date1.EnableWindow(false);
        m_date2.EnableWindow(false);
        m_check4.SetCheck(false);
        m_edit9.EnableWindow(false);
        m_edit3.SetWindowTextA("");
        filename="";
        USBlock=0;
        USBID="";
        aes="";
    }
    else
    {
        m_edit5.EnableWindow(false);
    }
}


void CAG_AUTHENTICATORDlg::OnProjectOpen()
{
    // TODO: Add your command handler code here
    filedlg fdlg(true,"agp","project.agp",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.agp",NULL);

    fdlg.DoModal();
    string projectname= fdlg.GetFileName();
    if(projectname=="")
    {
        MessageBox("PROJECT NOT LOADED","WARNING",MB_ICONWARNING);
        return;
    }

    projectname= fdlg.GetPathName();
    string projectdata="";
    char *memblock;

    ifstream file (projectname, ios::in|ios::binary|ios::ate);

    if (file.is_open())
    {
        int size;
        size = (int) file.tellg();



        memblock = new char[size];
        file.seekg (0, ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        projectdata.resize(size);
        for (int read=0; read<size; read++)
        {
            file.read (&memblock[read],1);

            projectdata[read]=memblock[read];
        }
        projectdata = projectdata.substr(0,size);

        file.close();
    }
    else
    {
        MessageBox("PROJECT NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    if(projectdata=="")
    {
        MessageBox("PROJECT NOT LOADED","ERROR",MB_ICONERROR);
        return;
    }

    size_t found = projectdata.find("---v2---");
    if (found!=std::string::npos)
    {
        std::string loc="";
        loc = projectdata.substr(found+8);
        projectdata = projectdata.substr(0,found);
        m_edit3.SetWindowTextA(loc.c_str());
        if(loc=="HARDWARE LOCK")
        {
            USBlock=1;
        }
        else if(loc=="SL + HL")
        {
            USBlock=2;
        }
        else
        {
            USBlock=0;
        }
    }

    found = projectdata.find("--");
    if (found!=std::string::npos)
    {
        std::string temp="";
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit1.SetWindowTextA(temp.c_str());
        found = projectdata.find("--");
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit2.SetWindowTextA(temp.c_str());

        found = projectdata.find("--");
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        m_edit4.SetWindowTextA(temp.c_str());

        found = projectdata.find("--");
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);
        if (temp!="0")
        {
            if (temp=="1")
            {
                m_check.SetCheck(true);
                m_date1.EnableWindow(true);
                m_date2.EnableWindow(true);
                m_edit5.EnableWindow(false);
                m_check2.SetCheck(false);

                found = projectdata.find("--");
                temp = projectdata.substr(0,found);
                projectdata = projectdata.substr(found+2);
                SYSTEMTIME TimeDest;
                m_date1.GetTime(&TimeDest);
                int datetem=strtol(temp.c_str(),NULL,10);
                TimeDest.wDay =datetem/100;
                TimeDest.wMonth=datetem%100;

                found = projectdata.find("--");
                temp = projectdata.substr(0,found);
                projectdata = projectdata.substr(found+2);
                datetem=strtol(temp.c_str(),NULL,10);
                TimeDest.wYear =datetem;

                m_date1.SetTime(TimeDest);

                found = projectdata.find("--");
                temp = projectdata.substr(0,found);
                projectdata = projectdata.substr(found+2);
                m_date2.GetTime(&TimeDest);
                datetem=strtol(temp.c_str(),NULL,10);
                TimeDest.wDay =datetem/100;
                TimeDest.wMonth=datetem%100;

                found = projectdata.find("--");
                temp = projectdata.substr(0,found);
                projectdata = projectdata.substr(found+2);
                datetem=strtol(temp.c_str(),NULL,10);
                TimeDest.wYear =datetem;

                m_date2.SetTime(TimeDest);

            }
            else
            {
                m_check2.SetCheck(true);
                m_edit5.EnableWindow(true);
                m_check.SetCheck(false);
                m_date1.EnableWindow(false);
                m_date2.EnableWindow(false);

                found = projectdata.find("--");
                temp = projectdata.substr(0,found);
                m_edit5.SetWindowTextA(temp.c_str());
            }
        }

        found = projectdata.find("--");
        temp = projectdata.substr(0,found);
        projectdata = projectdata.substr(found+2);

        if (temp=="1")
        {
            m_check4.SetCheck(true);
            m_edit9.EnableWindow(true);
            found = projectdata.find("--");
            temp = projectdata.substr(0,found);
            projectdata = projectdata.substr(found+2);
            m_edit9.SetWindowTextA(temp.c_str());
        }
    }

    delete[] memblock;

    return;
}


void CAG_AUTHENTICATORDlg::OnProjectSaveas()
{
    // TODO: Add your command handler code here
    filedlg fdlg(false,"agp","project",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.agp",NULL);

    fdlg.DoModal();
    string project= fdlg.GetFileName();


    if(project=="")
    {

        return;
    }

    project = fdlg.GetPathName();
    string projectdata="";
    std::ofstream ifile;
    ifile.open(project,ios::out|ios::binary );
    ifile.seekp (0, ios::beg);

    char buff[1024]="";
    int cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string appid= buff;
    projectdata.append(appid);
    projectdata.append("--");

    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    projectdata.append(buff);
    projectdata.append("--");

    cTxtLen = m_edit4.GetWindowTextLength();
    m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string memory= buff;
    projectdata.append(memory);
    projectdata.append("--");

    size_t found = memory.find("--");
    if (found!=std::string::npos)
    {
        MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
        ifile.close();
        return;
    }

    found = memory.find("%");
    if (found!=std::string::npos)
    {
        MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
        ifile.close();
        return;
    }
    found = appid.find("--");
    if (found!=std::string::npos)
    {
        MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
        ifile.close();
        return;
    }

    found = appid.find("%");
    if (found!=std::string::npos)
    {
        MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
        ifile.close();
        return;
    }

    if (m_check.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
        SYSTEMTIME TimeDest;
        m_date1.GetTime(&TimeDest);
        int startdate = TimeDest.wDay*100 + TimeDest.wMonth;
        sprintf_s(buff,"%d",startdate);
        projectdata.append(buff);
        projectdata.append("--");

        int startYear= TimeDest.wYear;
        sprintf_s(buff,"%d",startYear);
        projectdata.append(buff);
        projectdata.append("--");

        m_date2.GetTime(&TimeDest);
        int enddate = TimeDest.wDay*100 + TimeDest.wMonth;
        sprintf_s(buff,"%d",enddate);
        projectdata.append(buff);
        projectdata.append("--");

        int endYear= TimeDest.wYear;
        sprintf_s(buff,"%d",endYear);
        projectdata.append(buff);
        projectdata.append("--");

    }

    else
    {

        if (m_check2.GetCheck())
        {
            projectdata.append("2");
            projectdata.append("--");
            char buffdays[1024]="";
            cTxtLen = m_edit5.GetWindowTextLength();
            m_edit5.GetWindowTextA(&buffdays[0],cTxtLen+1);
            projectdata.append(buffdays);
            projectdata.append("--");
        }
        else
        {
            projectdata.append("0");
            projectdata.append("--");
        }

    }
    if (m_check4.GetCheck())
    {
        projectdata.append("1");
        projectdata.append("--");
        char buffdays[1024]="";
        cTxtLen = m_edit9.GetWindowTextLength();
        m_edit9.GetWindowTextA(&buffdays[0],cTxtLen+1);
        projectdata.append(buffdays);
        projectdata.append("--");
    }
    else
    {
        projectdata.append("0");
        projectdata.append("--");
    }
    projectdata.append("---v2---");
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    memory= buff;
    projectdata.append(memory);

    ifile.write (&projectdata[0],strlen(projectdata.c_str()));
    ifile.close();
}


void CAG_AUTHENTICATORDlg::OnProjectResetall()
{
    // TODO: Add your command handler code here
    if (MessageBox("ARE YOU SURE? IT IS SUGGESTED TO CONTACT VENDOR\nIF YOU DO NOT KNOW WHAT YOU ARE DOING","WARNING",MB_YESNO|MB_ICONWARNING)== IDYES)
    {
        int j = strtol(STRING_SIX_DEFINE_NAME,NULL,10)+60;
        WriteSectors(0,j,1,5000,STRING_SIX_DEFINE_NAME);
        MessageBox("SUCCESSFUL","DONE",MB_ICONINFORMATION);
        return;
    }
    else
    {
        return;
    }
}


void CAG_AUTHENTICATORDlg::OnBnClickedButton3()
{
    criteria ctr;
    //if (m_edit4.=="")
    int value = 1;
    if (ctr.DoModal()==IDOK)
    {
        value= ctr.GetNumber();

        USBID= ctr.GetValue();
    }
    if (value==1)
    {

        m_edit3.SetWindowTextA("HARDWARE LOCK");
        USBlock=1;
    }
    else if(value==2)
    {
        m_edit3.SetWindowTextA("SL + HL");
        USBlock=2;
    }
    else
    {
        m_edit3.SetWindowTextA("SOFTWARE LICENSE");
        USBlock=0;
    }
    // TODO: Add your control notification handler code here
}


void CAG_AUTHENTICATORDlg::OnServerConnectto()
{
    // TODO: Add your command handler code here
    server serv;
    char buff[1024]="";
    std::string accountdata="";
    std::string accountdatatemp="";
    servername="";
    std::string receivedata="";
    //if (m_edit4.=="")
    int value = 1;
    if (serv.DoModal()==IDOK)
    {
        accountdata = serv.GetIDValue();
        if(accountdata=="")
        {
            MessageBox("PLEASE PROVIDE USER NAME","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        size_t found = accountdata.find("--");
        if (found!=std::string::npos)
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        accountdata.append("---");

        accountdatatemp=serv.GetpassValue();
        if(accountdatatemp=="")
        {
            MessageBox("PLEASE PROVIDE PASSWORD","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        found = accountdatatemp.find("--");
        if (found!=std::string::npos)
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        accountdata.append(accountdatatemp.c_str() );
        accountdata.append("--");
        accountdata.append("%%%");
        servername=serv.GetserverValue();
        if(servername=="")
        {
            MessageBox("PLEASE PROVIDE SERVER NAME","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }
        found = servername.find("--");
        if (found!=std::string::npos)
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return;
        }

        WSADATA wsaData;
        SOCKET ConnectSocket = INVALID_SOCKET;
        struct addrinfo *result = NULL,
                             *ptr = NULL,
                              hints;
        std::string sendbuf =accountdata;
        char recvbuf[DEFAULT_BUFLEN];
        int iResult;
        int recvbuflen = DEFAULT_BUFLEN;



        // Initialize Winsock
        iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
        if (iResult != 0) {
            sprintf_s(buff,"WSAStartup failed with error: %d\n", iResult);
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        ZeroMemory( &hints, sizeof(hints) );
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        // Resolve the server address and port
        iResult = getaddrinfo(servername.c_str(), DEFAULT_PORT, &hints, &result);
        if ( iResult != 0 ) {
            sprintf_s(buff,"Server \"%s\" not found\n", servername.c_str());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            WSACleanup();
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        // Attempt to connect to an address until one succeeds
        for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

            // Create a SOCKET for connecting to server
            ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                                   ptr->ai_protocol);
            if (ConnectSocket == INVALID_SOCKET) {
                sprintf_s(buff,"socket failed with error: %ld\n", WSAGetLastError());
                MessageBox(buff,"ERROR",MB_ICONSTOP);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
                WSACleanup();
                return ;
            }

            // Connect to server.
            iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
            if (iResult == SOCKET_ERROR) {
                closesocket(ConnectSocket);
                ConnectSocket = INVALID_SOCKET;
                continue;
            }
            break;
        }

        freeaddrinfo(result);

        if (ConnectSocket == INVALID_SOCKET) {
            sprintf_s(buff,"Unable to connect to server!\n");
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            WSACleanup();
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        // Send an initial buffer

        iResult = send( ConnectSocket, sendbuf.c_str(), sendbuf.length(), 0 );
        if (iResult == SOCKET_ERROR) {
            sprintf_s(buff,"send failed with error: %d\n", WSAGetLastError());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            closesocket(ConnectSocket);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            WSACleanup();
            return ;
        }

        // shutdown the connection since no more data will be sent
        iResult = shutdown(ConnectSocket, SD_SEND);
        if (iResult == SOCKET_ERROR) {
            sprintf_s(buff,"shutdown failed with error: %d\n", WSAGetLastError());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            closesocket(ConnectSocket);
            WSACleanup();
            m_edit7.SetWindowTextA("LOCAL MACHINE");
            return ;
        }

        // Receive until the peer closes the connection
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        receivedata.resize(recvbuflen);
        receivedata=recvbuf;
        if ( iResult > 0 )
        {
            if(receivedata.find("DONE")!=std::string::npos)
            {
                sprintf_s(buff,"Connected to %s\n", servername.c_str());
                MessageBox(buff,"DONE",MB_ICONINFORMATION);
                m_edit7.SetWindowTextA(servername.c_str());
            }
            else if(receivedata.find("ACCOUNT DOES NOT EXIST")!=std::string::npos)
            {
                MessageBox("WRONG USER NAME OR PASSWORD","ERROR",MB_ICONSTOP);
                m_edit7.SetWindowTextA("LOCAL MACHINE");
            }
        }
        else if ( iResult == 0 )
        {
            sprintf_s(buff,"Connection closed\n");
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
        }
        else
        {
            sprintf_s(buff,"recv failed with error: %d\n", WSAGetLastError());
            MessageBox(buff,"ERROR",MB_ICONSTOP);
            m_edit7.SetWindowTextA("LOCAL MACHINE");
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();

    }


}


void CAG_AUTHENTICATORDlg::OnServerCreateaccount()
{
    // TODO: Add your command handler code here
    Account acc;
    std::string accountdata="";
    std::string accountcheckdata="";
    std::string accountdatatemp="";
    //if (m_edit4.=="")

    if (acc.DoModal()==IDOK)
    {
        accountdata = acc.GetIDValue();
        if(accountdata=="")
        {
            MessageBox("PLEASE PROVIDE USER NAME","ERROR",MB_ICONERROR);
            return;
        }
        size_t found = accountdata.find("--");
        if (found!=std::string::npos)
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            return;
        }
        accountdata.append("---");
        accountcheckdata=accountdata;
        accountdatatemp=acc.GetpassValue();
        if(accountdatatemp=="")
        {
            MessageBox("PLEASE PROVIDE PASSWORD","ERROR",MB_ICONERROR);
            return;
        }
        found = accountdatatemp.find("--");
        if (found!=std::string::npos)
        {
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            return;
        }
        accountdata.append(accountdatatemp.c_str() );
        accountdata.append("--");

        string projectdata="";
        char *memblock;

        ifstream file ("C:/Users/Public/Application Data/Bastion/AG_Server.agl", ios::in|ios::binary|ios::ate);

        if (file.is_open())
        {
            int size;
            size = (int) file.tellg();



            memblock = new char[size];
            file.seekg (0, ios::beg);

            int size1=size;
            //file.read (&memblock[debug], size);
            //debug=strlen(&memblock[debug])+1;
            //lockcode.append(&memblock[0]);


            size_t debug=0;

            projectdata.resize(size);
            for (int read=0; read<size; read++)
            {
                file.read (&memblock[read],1);

                projectdata[read]=memblock[read];
            }
            projectdata = projectdata.substr(0,size);
            found = projectdata.find(accountcheckdata.c_str());
            if (found!=std::string::npos)
            {
                MessageBox("ACCOUNT ALREADY EXIST","ERROR",MB_ICONERROR);
                return;
            }
            file.close();
        }

        _mkdir("C:/Users");
        _mkdir("C:/Users/Public");
        _mkdir("C:/Users/Public/Application Data");
        _mkdir("C:/Users/Public/Application Data/Bastion");
        SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG_Server.agl",FILE_ATTRIBUTE_NORMAL);
        std::ofstream ifilec;
        ifilec.open("C:/Users/Public/Application Data/Bastion/AG_Server.agl",ios::out|ios::binary|ios::app);




        ifilec.write (&accountdata[0],strlen(accountdata.c_str()));
        ifilec.close();
        MessageBox("ACCOUNT CREATED","DONE",MB_ICONINFORMATION);

    }

}

void CAG_AUTHENTICATORDlg::ProcessEvents()
{
    MSG stmsg;
    while(::PeekMessageA(&stmsg,NULL,0,0,PM_REMOVE))
    {
        ::TranslateMessage(&stmsg);
        ::DispatchMessageA(&stmsg);
    }
}
void sleep(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

void CAG_AUTHENTICATORDlg::OnBnClickedButton4()
{
    int cTxtLenloop ;
    char buffloop[1024]="";
    cTxtLenloop = m_edit10.GetWindowTextLength();
    m_edit10.GetWindowTextA(&buffloop[0],cTxtLenloop+1);
    int verloop= atoi(buffloop);
    for( int loop=0; loop<verloop; loop++)
    {
        ProcessEvents();
        sleep(2000);
        ProcessEvents();
        // TODO: Add your control notification handler code here
        string licval="";
        string projectdata="";
        std::string lickey="";
        char buff[1024]="";
        int checkvalue=0;
        long int value=0;
        long int value1=0;

        while (checkvalue==0)
        {
            licval="";
            while (value<100000000)
            {
                int iSecret;
                int temp=1;
                value=0;
                for(int i =0; i<10; i++)
                {
                    temp=1;
                    srand (time(NULL));

                    iSecret = rand();
                    for(int j =1; j <i+1; j++)
                        temp = 10*temp;
                    value=iSecret + value*temp;
                }
            }

            licval="";
            while (value1<100000000)
            {
                int iSecret1;
                int temp=1;
                value1=0;
                for(int i =0; i<10; i++)
                {
                    temp=1;
                    srand (time(NULL));

                    iSecret1 = rand();
                    for(int j =1; j <i+1; j++)
                        temp = 10*temp;
                    value1=iSecret1 + value1*temp;
                }
            }
            sprintf_s(buff,"%ld%ld", value, value1);

            lickey=buff;
            m_edit8.SetWindowTextA(buff);
            licval=buff;
            lickey.append("---vk---");


            ifstream file ("C:/Users/Public/Application Data/Bastion/AG_Online.agl", ios::in|ios::binary|ios::ate);

            if (file.is_open())
            {
                int size;
                size = (int) file.tellg();



                char *memblock = new char[size];
                file.seekg (0, ios::beg);

                int size1=size;
                //file.read (&memblock[debug], size);
                //debug=strlen(&memblock[debug])+1;
                //lockcode.append(&memblock[0]);


                size_t debug=0;

                projectdata.resize(size);
                for (int read=0; read<size; read++)
                {
                    file.read (&memblock[read],1);

                    projectdata[read]=memblock[read];
                }
                projectdata = projectdata.substr(0,size);
                size_t found = projectdata.find(lickey.c_str());
                if (found==std::string::npos)
                {

                    ////////////////////////////////////////////////
                    std::string licvalstr= "C:\\Program Files\\Bastion\\";
                    licvalstr.append(licval);
                    ifstream file1 (licvalstr, ios::in|ios::binary|ios::ate);

                    if (!file1.is_open())
                    {

                        checkvalue=1;
                    }
                    file1.close();
                    //////////////////////////////////////////////
                }
                file.close();
            }
            else
            {

                checkvalue=1;
            }

        }

        _mkdir("C:/Users");
        _mkdir("C:/Users/Public");
        _mkdir("C:/Users/Public/Application Data");
        _mkdir("C:/Users/Public/Application Data/Bastion");
        SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG_Online.agl",FILE_ATTRIBUTE_NORMAL);
        std::ofstream ifile;
        ifile.open("C:/Users/Public/Application Data/Bastion/AG_Online.agl",ios::out|ios::binary|ios::app);


        projectdata="";

        int cTxtLen = m_edit1.GetWindowTextLength();
        m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
        std::string appid= buff;
        projectdata.append(appid);
        projectdata.append("--");

        cTxtLen = m_edit2.GetWindowTextLength();
        m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
        projectdata.append(buff);
        projectdata.append("--");

        cTxtLen = m_edit4.GetWindowTextLength();
        m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
        std::string memory= buff;
        projectdata.append(memory);
        projectdata.append("--");

        if(appid=="")
        {
            m_edit8.SetWindowTextA("");
            MessageBox("PLEASE ENTER APPLICATION ID","ERROR",MB_ICONERROR);
            m_edit8.SetWindowTextA("");
            return;
        }

        if (m_check2.GetCheck())
        {
            m_edit8.SetWindowTextA("");
            MessageBox("TRIAL LICENSE NOT SUPPORTED FOR ONLINE ACTIVATION","ERROR",MB_ICONERROR);
            m_edit8.SetWindowTextA("");
            return;
        }

        size_t found = memory.find("--");
        if (found!=std::string::npos)
        {
            m_edit8.SetWindowTextA("");
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            m_edit8.SetWindowTextA("");
            ifile.close();
            return;
        }

        found = memory.find("%");
        if (found!=std::string::npos)
        {
            m_edit8.SetWindowTextA("");
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            ifile.close();
            m_edit8.SetWindowTextA("");
            return;
        }
        found = appid.find("--");
        if (found!=std::string::npos)
        {
            m_edit8.SetWindowTextA("");
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            ifile.close();
            m_edit8.SetWindowTextA("");
            return;
        }

        found = appid.find("%");
        if (found!=std::string::npos)
        {
            m_edit8.SetWindowTextA("");
            MessageBox("-- or % NOT ALLOWED","ERROR",MB_ICONERROR);
            ifile.close();
            m_edit8.SetWindowTextA("");
            return;
        }

        if (m_check.GetCheck())
        {
            projectdata.append("1");
            projectdata.append("--");
            SYSTEMTIME TimeDest;
            m_date1.GetTime(&TimeDest);
            int startdate = TimeDest.wDay*100 + TimeDest.wMonth;
            sprintf_s(buff,"%d",startdate);
            projectdata.append(buff);
            projectdata.append("--");

            int startYear= TimeDest.wYear;
            sprintf_s(buff,"%d",startYear);
            projectdata.append(buff);
            projectdata.append("--");

            m_date2.GetTime(&TimeDest);
            int enddate = TimeDest.wDay*100 + TimeDest.wMonth;
            sprintf_s(buff,"%d",enddate);
            projectdata.append(buff);
            projectdata.append("--");

            int endYear= TimeDest.wYear;
            sprintf_s(buff,"%d",endYear);
            projectdata.append(buff);
            projectdata.append("--");

        }

        else
        {

            if (m_check2.GetCheck())
            {
                projectdata.append("2");
                projectdata.append("--");
                char buffdays[1024]="";
                cTxtLen = m_edit5.GetWindowTextLength();
                m_edit5.GetWindowTextA(&buffdays[0],cTxtLen+1);
                projectdata.append(buffdays);
                projectdata.append("--");
            }
            else
            {
                projectdata.append("0");
                projectdata.append("--");
            }

        }
        if (m_check4.GetCheck())
        {
            projectdata.append("1");
            projectdata.append("--");
            char buffdays[1024]="";
            cTxtLen = m_edit9.GetWindowTextLength();
            m_edit9.GetWindowTextA(&buffdays[0],cTxtLen+1);
            projectdata.append(buffdays);
            projectdata.append("--");
        }
        else
        {
            projectdata.append("0");
            projectdata.append("--");
        }
        projectdata.append("---v2---");
        cTxtLen = m_edit3.GetWindowTextLength();
        m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
        memory= buff;
        projectdata.append(memory);
        projectdata.append("---vk---");
        projectdata.append(lickey);
        ifile.write (&projectdata[0],strlen(projectdata.c_str()));
        ifile.close();

        std::ofstream ifile1;
        ifile1.open("License_Key_"+appid+".txt",ios::out|ios::binary|ios::app);
        licval.append(" \n");
        ifile1.write (&licval[0],strlen(licval.c_str()));
        ifile1.close();
    }
    MessageBox("LICENSE KEYS CREATED","DONE",MB_ICONINFORMATION);
    return;
}


void CAG_AUTHENTICATORDlg::OnBnClickedCheck4()
{
    // TODO: Add your control notification handler code here
    if(m_check4.GetCheck())
    {
        m_edit5.EnableWindow(false);
        m_edit9.EnableWindow(true);
        m_check2.SetCheck(false);
    }
    else
    {
        m_edit9.EnableWindow(false);
    }
}




void CAG_AUTHENTICATORDlg::OnUsbdongleFormat()
{
    // TODO: Add your command handler code here
    char buff[128];
    char buff1[512];
    int retVal ;
    int a, b;

    retVal = open_dongle();

    if( retVal == 1)
    {

        if (read_dongle_flash(buff, 0, 64) == 1)
        {
            std::string IDUNIQUE= buff;
            if(IDUNIQUE==STRING_SEVEN_DEFINE_NAME)
            {
                char data[512]="";
                if (read_dongle((char *)buff1, 0, 256) == 1)
                {
                    std::string checkstring = (char *)buff1;
                    if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                    {
                        if (write_dongle((char *)data, 0, 512) == 1)
                        {
                            MessageBox("DONGLE ERASED","DONE",MB_ICONINFORMATION);
                            return;
                        }
                    }
                    read_dongle((char *)buff1, 0, 256);
                    if (write_dongle((char *)data,256, 256) == 1)
                    {
                        write_dongle((char *)buff1, 0, 256);
                        MessageBox("DONGLE ERASED","DONE",MB_ICONINFORMATION);
                    }
                }
            }
        }


        return;
    }

    long nRet = 0;

    nRet = SmartX1Find((char *) STRING_SEVEN_DEFINE_NAME);
    if(0 == nRet)
    {
        nRet = SmartX1Open((char *) STRING_NINE_DEFINE_NAME);
        if( 0 == nRet)
        {

            for(int l =0; l<4; l++)
            {
                unsigned char data[512]="";
                unsigned char outdata[512];
                nRet = SmartX1ReadStorage(l*256, 256, outdata);
                if(!nRet) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                    {
                        nRet = SmartX1WriteStorage(l*256, 256, data);
                    }
                }
            }
            MessageBox("DONGLE ERASED","DONE",MB_ICONINFORMATION);
        }
        SmartX1Close();
        return;
    }
    lc_handle_t handle;
    int res=0,k=0;
    // opening LC device
    res = LC_open(1112100422, 0, &handle);
    if(!res) {

        res = LC_passwd(handle, 0, (unsigned char *) STRING_SEVEN_DEFINE_NAME);
        if(!res) {
            // read back the data just writed into block 0
            for(int l =0; l<4; l++)
            {
                unsigned char data[512]="";
                unsigned char outdata[512];
                res = LC_read(handle, l, outdata);
                if(!res) {

                    std::string checkstring = (char *)outdata;
                    if(checkstring!=""&& checkstring.find("%%%")!=std::string::npos)
                    {
                        res = LC_write(handle, l, data);
                    }
                }
            }
            MessageBox("DONGLE ERASED","DONE",MB_ICONINFORMATION);
        }
        LC_close(handle);
        return;
    }
    else
    {
        MessageBox("DONGLE NOT FOUND","ERROR",MB_ICONERROR);
    }

}

