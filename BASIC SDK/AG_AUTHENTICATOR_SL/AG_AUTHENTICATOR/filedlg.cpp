// filedlg.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "filedlg.h"


// filedlg

IMPLEMENT_DYNAMIC(filedlg, CFileDialog)

filedlg::filedlg(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
                 DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
    CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd)
{

}

filedlg::~filedlg()
{
}


BEGIN_MESSAGE_MAP(filedlg, CFileDialog)
END_MESSAGE_MAP()



// filedlg message handlers


