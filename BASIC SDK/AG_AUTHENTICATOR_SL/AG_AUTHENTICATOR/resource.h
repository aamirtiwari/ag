//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AG_AUTHENTICATOR.rc
//
#define IDOK2                           2
#define IDOK3                           3
#define IDOK4                           4
#define IDOK5                           6
#define IDOK6                           7
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_AG_AUTHENTICATOR_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     131
#define IDI_ICON1                       131
#define IDD_DIALOG3                     132
#define IDI_ICON2                       132
#define IDD_DIALOG4                     133
#define IDI_ICON3                       133
#define IDI_ICON4                       134
#define IDI_ICON5                       135
#define IDI_ICON6                       136
#define IDB_BITMAP1                     140
#define IDB_BITMAP2                     141
#define IDB_BITMAP3                     143
#define IDB_BITMAP4                     145
#define IDB_BITMAP5                     146
#define IDB_BITMAP6                     147
#define IDB_BITMAP7                     148
#define IDB_BITMAP8                     149
#define IDB_BITMAP9                     150
#define IDB_BITMAP10                    151
#define IDB_BITMAP11                    152
#define IDB_BITMAP12                    155
#define IDC_BUTTON1                     1000
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_BUTTON2                     1003
#define IDC_EDIT6                       1003
#define IDC_EDIT3                       1004
#define IDC_EDIT11                      1004
#define IDC_BUTTON3                     1005
#define IDC_EDIT4                       1006
#define IDC_RADIO1                      1007
#define IDC_EDIT5                       1007
#define IDC_BUTTON4                     1008
#define IDC_EDIT9                       1009
#define IDC_DATETIMEPICKER2             1013
#define IDC_DATETIMEPICKER3             1014
#define IDC_CHECK1                      1015
#define IDC_CHECK2                      1016
#define IDC_EDIT7                       1017
#define IDC_CHECK3                      1017
#define IDC_EDIT8                       1018
#define IDC_CHECK4                      1019
#define IDC_TREE1                       1019
#define IDC_EDIT10                      1020
#define IDC_TREE2                       1020
#define IDC_CHECK5                      1021
#define ID_PROJECT_OPEN                 32771
#define ID_PROJECT_SAVEAS               32772
#define ID_PROJECT_RESETALL             32773
#define ID_SERVER_CONNECTTO             32774
#define ID_SERVER_CREATEACCOUNT         32775
#define ID_CUSTOMER_CREATENEW           32776
#define ID_CUSTOMER_LINKTOEXISTING      32777
#define ID_USBDONGLE_FORMAT             32778
#define ID_HELP32779                    32779
#define ID_CUSTOMER_HELP                32780
#define ID_CUSTOMER_FORMATDONGLE        32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        156
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
