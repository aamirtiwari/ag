/**
 * sample.h file
 * Dongle prototypes 
 */

#ifndef _COPYLOCK_H_
#define _COPYLOCK_H_

int open_dongle (void) ;
int write_dongle (char *buf, int from, int count);
int read_dongle (char *buf, int from, int count) ;
int write_dongle_flash (char *buf, int from, int count);
int read_dongle_flash (char *buf, int from, int count) ;
void close_dongle (void);

#endif /* _SAMPLE_H_ */
