// Account.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "Account.h"
#include "afxdialogex.h"


// Account dialog

IMPLEMENT_DYNAMIC(Account, CDialogEx)

Account::Account(CWnd* pParent /*=NULL*/)
    : CDialogEx(Account::IDD, pParent)
{

}

Account::~Account()
{
}

void Account::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}
void Account::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT2, m_edit2);
}
HBRUSH Account::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
BOOL Account::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));

    return TRUE;
}
BEGIN_MESSAGE_MAP(Account, CDialogEx)
    ON_BN_CLICKED(IDOK, &Account::OnBnClickedOk)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// Account message handlers


void Account::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    char buff[1024]="";
    int cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    m_value1=buff;
    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    m_value2=buff;
    CDialogEx::OnOK();
}
