// AuthGuru_net.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "AuthGuru_net.h"
#include <windows.h>
#include <WinBase.h>
#include <malloc.h>
#include <string>
#include "AG_Client.h"
#include <fstream>
#include "Stringobf.h"
#include "Encrypt_decrypt.h"
#using <Bastion_net.dll>

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Bulwark_net;


// This is an example of an exported function.
AUTHGURU_NET_API int addnetsection(DWORD input, DWORD input1, const char * lpFileName)
{



    DWORD read=0;
    // Read the original file

    HANDLE hOriginalFilenew = CreateFile(lpFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFilenew == INVALID_HANDLE_VALUE)
        return 1;


    DWORD dwFileSizenew = GetFileSize(hOriginalFilenew, NULL);
    if (dwFileSizenew == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFilenew);
        return 1;
    }
    CloseHandle(hOriginalFilenew);

    HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
        return 1;

    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer1 = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer1, dwFileSize, &dwNumberOfBytesRead, NULL);



    CloseHandle(hOriginalFile);

    int size =((dwFileSize/AES_BLOCK_SIZE)-1)*AES_BLOCK_SIZE;

    dwFileSize= sizeof(unsigned char)*size;

    unsigned char* lpBuffer = (LPBYTE)malloc(dwFileSize);
    lpBuffer=aes_decrypt_code(lpBuffer1, STRING_TWO_DEFINE_NAME,dwFileSize);

    Bulwark_net::BLWNET bw;
    array< Byte >^ byteArray = gcnew array< Byte >(dwFileSizenew + 2);

    PIMAGE_DOS_HEADER pImageDosHeader1 = (PIMAGE_DOS_HEADER)lpBuffer;
    if (pImageDosHeader1->e_magic != IMAGE_DOS_SIGNATURE)
        return 1;


    PIMAGE_NT_HEADERS pImageNtHeaders1 = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader1->e_lfanew);
    if (pImageNtHeaders1->Signature != IMAGE_NT_SIGNATURE)
        return 1;


    pImageNtHeaders1->OptionalHeader.AddressOfEntryPoint= input;
    pImageNtHeaders1->OptionalHeader.DataDirectory[14].VirtualAddress = input1;

//	  DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
//    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;

//	LPBYTE sectionsData = (lpBuffer + pImageNtHeaders->OptionalHeader.SizeOfHeaders);

    // convert native pointer to System::IntPtr with C-Style cast
    Marshal::Copy((IntPtr)lpBuffer,byteArray, 0, dwFileSizenew);

//  for ( int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++ ) {
//     char dc =  *(Byte^)   byteArray->GetValue(i);
//     Console::Write((Char)dc);
//  }

    free(lpBuffer);
    free(lpBuffer1);

    bw.loadm(byteArray );

    bw.loadm12( );
    return 0;
}

// This is the constructor of a class that has been exported.
// see AuthGuru_net.h for the class definition
/*CAuthGuru_net::CAuthGuru_net()
{
	return;
}*/
namespace AuthGuru_net
{

public ref class AuthGuru
{
// This is an example of an exported function.
public:
    Type^ AG_loadlibrary( char * lpFileName, String^ Classname, String^ APPID, int version)
    {
        DWORD read=0;
        // Read the original file

        HANDLE hOriginalFilenew = CreateFile(lpFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        Type^ retobj;

        if (hOriginalFilenew == INVALID_HANDLE_VALUE)
            return retobj;


        DWORD dwFileSizenew = GetFileSize(hOriginalFilenew, NULL);
        if (dwFileSizenew == INVALID_FILE_SIZE)
        {
            CloseHandle(hOriginalFilenew);
            return retobj;
        }
        CloseHandle(hOriginalFilenew);

        HANDLE hOriginalFile = CreateFile(lpFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


        if (hOriginalFile == INVALID_HANDLE_VALUE)
            return retobj;

        DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
        if (dwFileSize == INVALID_FILE_SIZE)
        {
            CloseHandle(hOriginalFile);
            return retobj;
        }


        LPBYTE lpBuffer1 = (LPBYTE)malloc(dwFileSize);

        DWORD dwNumberOfBytesRead = 0;
        ReadFile(hOriginalFile, lpBuffer1, dwFileSize, &dwNumberOfBytesRead, NULL);



        CloseHandle(hOriginalFile);

        int size =((dwFileSize/AES_BLOCK_SIZE)-1)*AES_BLOCK_SIZE;

        dwFileSize= sizeof(unsigned char)*size;

        unsigned char* lpBuffer = (LPBYTE)malloc(dwFileSize);
        lpBuffer=aes_decrypt_code1(lpBuffer1, STRING_ONE_DEFINE_NAME,dwFileSize,lpBuffer);

        Bulwark_net::BLWNET bw;
        array< Byte >^ byteArray = gcnew array< Byte >(dwFileSizenew + 2);

        PIMAGE_DOS_HEADER pImageDosHeader1 = (PIMAGE_DOS_HEADER)lpBuffer;
        if (pImageDosHeader1->e_magic != IMAGE_DOS_SIGNATURE)
            return retobj;


        PIMAGE_NT_HEADERS pImageNtHeaders1 = (PIMAGE_NT_HEADERS)(lpBuffer + pImageDosHeader1->e_lfanew);
        if (pImageNtHeaders1->Signature != IMAGE_NT_SIGNATURE)
            return retobj;

//	  DWORD originalSizeOfImage = pImageNtHeaders->OptionalHeader.SizeOfImage;
//    DWORD originalSizeOfHeaders = pImageNtHeaders->OptionalHeader.SizeOfHeaders;

//	LPBYTE sectionsData = (lpBuffer + pImageNtHeaders->OptionalHeader.SizeOfHeaders);

        // convert native pointer to System::IntPtr with C-Style cast
        Marshal::Copy((IntPtr)lpBuffer,byteArray, 0, dwFileSizenew);

//  for ( int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++ ) {
//     char dc =  *(Byte^)   byteArray->GetValue(i);
//     Console::Write((Char)dc);
//  }

        free(lpBuffer);
        free(lpBuffer1);
        retobj = bw.loadmnew(byteArray, Classname, APPID, version );
        return retobj;
    }

}
;
}
