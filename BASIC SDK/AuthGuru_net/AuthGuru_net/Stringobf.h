#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "5TU/f4RJVylp1YHdc0oqSKQ2D7kG3bMCNBeZ+AFaznv9WuxhLgsIEPtrwi6O8jmX"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("1qfg1qfg1qfg1qfg1qfI1N5=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("1ZyBDqVrH/3L1/fsDqK+HR4BDZfsDq5g1Z3wYt4B1ZV5")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("1ZyBDqVrH/35")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("1Z3wYt4B1ZV5")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("SEjHKS3NK+KH0fjoV/Vg55==")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("1Zf5")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("SE3sY/+I1qNI55==")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("Y/Dw1INr1ZD5")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("7ZTBHqK+7/5g7ZDt1QSt1I1t1ZYBDQ0+DZYeHqDwYtf5")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif