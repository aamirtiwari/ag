/**
 * sample.h file
 * Dongle prototypes 
 */

#ifndef _COPYLOCK_H_
#define _COPYLOCK_H_

//---- old functions (still supported)
int open_dongle (void) ;
int write_dongle (char *buf, int from, int count);
int read_dongle (char *buf, int from, int count) ;
int write_dongle_flash (char *buf, int from, int count);
int read_dongle_flash (char *buf, int from, int count) ;
void close_dongle (void);

//---- new functions
bool InitDrivers(void) ;
int GetDongleCount(void) ;
int OpenDongle (int dNo) ;
int WriteDongleMem (int dNo, char *buf, int from, int count);
int ReadDongleMem (int dNo, char *buf, int from, int count) ;
int WriteDongleFlash (int dNo, char *buf, int from, int count);
int ReadDongleFlash (int dNo, char *buf, int from, int count) ;
void CloseDongle (int dNo);
void FreeDrivers(void) ;

#endif /* _COPYLOCK_H_ */
