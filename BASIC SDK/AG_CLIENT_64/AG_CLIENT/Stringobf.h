#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "+vTSwgEzRkVibNcq8M6QAmjf3e2nWxBd9KDJYHU4FsoLury1lO/CGP0pXZhI57ta"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("bQwObQwObQwObQwObQbpbl+=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("bJkK3QRpcSWlbSw/3QmYcEgK3Jw/3Q+ObJWXN0gKbJR+")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("bJkK3QRpcSW+")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("bJWXN0gKbJR+")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("MHKQQGeARSR0b9+=")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("bJ3/++==")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("MHKQmCRPbSACb9+=")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("NS3XbC9pbJ3+")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("eJvKcQmYeS+OeJ30bjA0bCb0bJNK3jMY3JNDcQ3XN0w+")
	#pragma comment(lib, "strenc64") 
 	void StrencDecode(char* buffer, char* Base64CharacterMap); 
 	const char* GetDecryptedString(const char* encryptedString) 
 	{ 
 		static char string[45]; 
 		strcpy(string, encryptedString); 
 		StrencDecode(string, STRINGOBF_KEY); 
 		return string; 
 	} 
 #endif