using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;
using AG_STATUS_CODE = System.UInt32;
namespace AG_API.Constants
{
public class Constants
{
    //STATUS CODES

    public const int AG_SUCCESS				=	0;
    public const int AG_FILE_IO_ERROR		=	1;
    public const int AG_NO_LICENSE			=	2;
    public const int AG_DATE_TAMPERED		=	3;
    public const int AG_EXPIRED				=	4;
    public const int AG_INVALID_LICENSE		=	5;
    public const int AG_EMPTY_MEMORY		=	6;
    public const int AG_INVALID_MEMORY		=	7;
    public const int AG_TERMINAL_SESSION	=	8;
    public const int AG_NO_KEY				=	9;

}
}
namespace AG_API.Interface
{
public class AGAPI
{
    private  const string LibraryName ="AG_CLIENT.dll";

    [DllImport(LibraryName, EntryPoint = "AG_IsAuthentic", CharSet = CharSet.Ansi)]
    public static extern int AG_IsAuthentic(ref string Handle,
                                            ref string AppId,
                                            int Version,int netflag , ref string servername1);

    [DllImport(LibraryName, EntryPoint = "AG_Start_Date", CharSet = CharSet.Ansi)]
    public static extern int AG_Start_Date(ref int date, ref int month, ref int year,
                                           ref string AppId,
                                           int Version,int netflag , ref string servername);

    [DllImport(LibraryName, EntryPoint = "AG_USBID", CharSet = CharSet.Ansi)]
    public static extern int AG_USBID( ref string keyID,ref int memberIndex,ref string APPID, int version);

}


}
