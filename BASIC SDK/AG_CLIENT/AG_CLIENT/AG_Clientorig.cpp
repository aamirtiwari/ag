#include "stdafx.h"
#define WIN32_LEAN_AND_MEAN
#include <iostream>
#include <Windows.h>
#include <direct.h>
#include <fstream>
#include <stdio.h>
#include <string>
#include "Encrypt_decrypt.h"
#include "DIVIDEINFO.h"
#include "HARDDISKID.h"
#include "READ.h"
#include "MAC.h"
#include "Stringobf.h"
#include <Setupapi.h>
#include <devguid.h>
#include <Psapi.h>
#include <tchar.h>
#include <strsafe.h>
#include <time.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT "27016"
#define EXPORT __declspec(dllexport)

using std::string;
int usballow=0;
int networkflag=0;
BOOL remote =0;
int background=60000;
string AppIdnew="";
size_t founddata;
std::string ID="";
std::string lockcode = "";
std::string file="";
std::string dec="";
std::string lockcode1="";
std::string divlic[10]= {""};
std::string mac[5]= {""};
std::string checkmac ="";
string HardDriveSerialNumbers1="";
std::string dec1[1]= {""};
std::string out[1]= {""};
std::string harddisk="";
std::string buff1[1]= {"Bastion Infotech, Quick protect data"};
std::string Data="";
std::string returnval="";
char ses1[1024]= {"None"};
char *ses=&ses1[0];
char buff[1024]="";
int status=2;
int datacheck=0;
size_t sizer=1024;
std::string rsession="";
std::string runtime="";
std::string lic="";
std::string check="";
std::string disk="";
std::string Datanew="";
std::string Handlenew="";
std::string datafiles8="";
std::string datafiles1="";
std::string datafiles3="";
std::string datafiles4="";
std::string datafiles5="";
std::string datafiles6="";
std::string datafiles7="";
std::string datafiles9="";
std::string IDname="";
std::string servernamenew="";
std::string ver="0";
std::string temp="";
std::string encfile="";
std::string enckey1="";
std::string enckey2="";
std::string encfile1="";
std::string encfile2="";
std::string encfile3="";
std::string encfile4="";
std::string encfile5="";
std::string encfile6="";
std::string datanew1="";
HANDLE  hLib;
int cdata=0;
void* pMem;
void* pMem2;
LPVOID pmem1;
HANDLE crtfileah;
HANDLE crtfilewh ;
HANDLE threadh ;
HANDLE writfileh;
HANDLE hookh;
HANDLE unhookh;
HANDLE fnameh;
HANDLE ench;
HANDLE datafilesh;
HANDLE createdatafilesh;
HANDLE g_maint;
BOOL bSuccess = FALSE;
TCHAR pszFilename[MAX_PATH+1];
HANDLE hFileMap11=0x0;
HANDLE hFileMap10=0x0;
HANDLE hFileMap9=0x0;
HANDLE hFileMap1=0x0;
HANDLE hFileMap2=0x0;
HANDLE hFileMap3=0x0;
HANDLE hFileMap4=0x0;
HANDLE hFileMap5=0x0;
HANDLE hFileMap13=0x0;
HANDLE hFileMap6=0x0;
HANDLE hFileMap7=0x0;
HANDLE hFileMap8=0x0;
HANDLE hFileMap12=0x0;
HANDLE hUpdatedFile;
HANDLE hndl1;
HANDLE hUpdatedFile4;
int dwFileSizenew;

DWORD dwFileSizeHi = 0;
DWORD dwFileSizeLo ;
DWORD dwAddr;
DWORD nofbytes;
DWORD dwFileSize1;
#pragma pack(push,1)
struct SHookThunk {
    BYTE jmp;
    DWORD addr;
    BYTE  ret;
};
#pragma pack(pop)
SHookThunk ht;

int AG_start_date_net(std::string Handle[1], std::string servername[1])
{

    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                         *ptr = NULL,
                          hints;

    std::string sendbuf =Handle[0];

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {

        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {

        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {

            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {

        return 1;
    }

    // Send an initial buffer
    std::string serverdata ="" ;
    std::string in ="";
    in=Handle[0];
    in.append("---startdateread---");
    serverdata.resize(1024);
    if(in!="")
    {
        int checkvalue=0;
        while (checkvalue==0)
        {
            serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
            serverdata.append("%%%");
            serverdata=serverdata.c_str();
            if(serverdata.find("%%%")!=std::string::npos)
                checkvalue=1;
        }
    }
    else
        serverdata="";
    iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
    if (iResult == SOCKET_ERROR) {

        WSACleanup();
        return 1;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {

        closesocket(ConnectSocket);
        WSACleanup();

        return 1;
    }
    std::string receivedata="";
    for(int i =0; i<1024; i++)
    {
        recvbuf[i]='\0';
    }
    // Receive until the peer closes the connection
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    in.resize(recvbuflen);
    in=recvbuf;
    receivedata.resize(1024);

    if ( iResult > 0 )
    {

        for(int i = 1024; i >= 1; i--)
        {
            if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
            {
                recvbuf[i-1] = '\0';
                break;
            }
        }
        size_t foundc;
        size_t found;
        foundc=in.find("%%%");
        if( foundc!=std::string::npos)
        {
            in=in.substr(0,foundc);
            receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
        }
        if(receivedata.find("---startda")!=std::string::npos)
        {
            Handle[0]= receivedata.substr(0,receivedata.find("---startda"));
            return 0;
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

}

int AG_end_date_net(std::string Handle[1], std::string servername[1])
{

    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                         *ptr = NULL,
                          hints;

    std::string sendbuf =Handle[0];

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {

        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {

        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {

            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {

        return 1;
    }

    // Send an initial buffer
    std::string serverdata ="" ;
    std::string in ="";
    in=Handle[0];
    in.append("---enddateread---");
    serverdata.resize(1024);
    if(in!="")
    {
        int checkvalue=0;
        while (checkvalue==0)
        {
            serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
            serverdata.append("%%%");
            serverdata=serverdata.c_str();
            if(serverdata.find("%%%")!=std::string::npos)
                checkvalue=1;
        }
    }
    else
        serverdata="";
    iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
    if (iResult == SOCKET_ERROR) {

        WSACleanup();
        return 1;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {

        closesocket(ConnectSocket);
        WSACleanup();

        return 1;
    }
    std::string receivedata="";
    for(int i =0; i<1024; i++)
    {
        recvbuf[i]='\0';
    }
    // Receive until the peer closes the connection
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    in.resize(recvbuflen);
    in=recvbuf;
    receivedata.resize(1024);

    if ( iResult > 0 )
    {

        for(int i = 1024; i >= 1; i--)
        {
            if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
            {
                recvbuf[i-1] = '\0';
                break;
            }
        }
        size_t foundc;
        size_t found;
        foundc=in.find("%%%");
        if( foundc!=std::string::npos)
        {
            in=in.substr(0,foundc);
            receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
        }
        if(receivedata.find("---endda")!=std::string::npos)
        {
            Handle[0]= receivedata.substr(0,receivedata.find("---endda"));
            return 0;
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

}
int AG_USBID_net(std::string Handle[1], std::string servername[1])
{

    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                         *ptr = NULL,
                          hints;

    std::string sendbuf =Handle[0];

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {

        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {

        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {

            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {

        return 1;
    }

    // Send an initial buffer
    std::string serverdata ="" ;
    std::string in ="";
    in=Handle[0];
    in.append("---USBIDreadvalue---");
    serverdata.resize(1024);
    if(in!="")
    {
        int checkvalue=0;
        while (checkvalue==0)
        {
            serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
            serverdata.append("%%%");
            serverdata=serverdata.c_str();
            if(serverdata.find("%%%")!=std::string::npos)
                checkvalue=1;
        }
    }
    else
        serverdata="";
    iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
    if (iResult == SOCKET_ERROR) {

        WSACleanup();
        return 1;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {

        closesocket(ConnectSocket);
        WSACleanup();

        return 1;
    }
    std::string receivedata="";
    for(int i =0; i<1024; i++)
    {
        recvbuf[i]='\0';
    }
    // Receive until the peer closes the connection
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    in.resize(recvbuflen);
    in=recvbuf;
    receivedata.resize(1024);

    if ( iResult > 0 )
    {

        for(int i = 1024; i >= 1; i--)
        {
            if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
            {
                recvbuf[i-1] = '\0';
                break;
            }
        }
        size_t foundc;
        size_t found;
        foundc=in.find("%%%");
        if( foundc!=std::string::npos)
        {
            in=in.substr(0,foundc);
            receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
        }
        if(receivedata.find("---USBIDre")!=std::string::npos)
        {
            Handle[0]= receivedata.substr(0,receivedata.find("---USBIDre"));
            return 0;
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

}


int AG_IsAuthentic_Net(std::string appID, int version, std::string Handle[1], std::string servername[1]);
extern "C" EXPORT int __cdecl  AG_Release(std::string Handle[1], std::string servername[1]);

extern "C" EXPORT int __cdecl  AG_Start_Date  (int &date,int &month,int &year , std::string AppId[1], int version, int netflag=0 , std::string servername[1]=NULL)
{

    lockcode = "";
    file="";
    status=2;
    _mkdir("C:/Users");
    _mkdir("C:/Users/Public");
    _mkdir("C:/Users/Public/Application Data");
    _mkdir("C:/Users/Public/Application Data/Bastion");
    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
    //file.append();
    size_t found ;
    std::string USBID = "";
    char *memblock;
    std::ifstream ifile;
    ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",std::ios::in|std::ios::binary|std::ios::ate);

    if(ifile==NULL)
    {
        goto LOOPBREAKNEW;
    }
    if (ifile.is_open())
    {
        int size=0;
        size = (int) ifile.tellg();


        memblock = new char[size];
        ifile.seekg (0, std::ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            ifile.read(&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);


        ifile.close();
    }
    else
    {
        goto LOOPBREAKNEW;

    }

    if (lockcode == "")
    {
        delete[] memblock;
        goto LOOPBREAKNEW;
    }

    dec="";
    lockcode1="";
    size_t found1;
    int USBlock=0;

    //  	hItem1[0]=m_tree.InsertItem("STANDALONE",0,0);
    while (lockcode!="")
    {
        found = lockcode.find("%%%");
        if (found!=std::string::npos)
        {

            lockcode1=lockcode.substr(0,found);

            found1 = lockcode1.find("---v2---");
            if (found1!=std::string::npos)
            {
                lockcode1=lockcode1.substr(0,found1);
                USBlock=1;
            }
            else
            {
                USBlock=0;
            }
            if (USBlock==0)
            {
                runtime = getHardDriveComputerID ();
                dec = aes_decrypt(lockcode1, STRING_THREE_DEFINE_NAME + runtime);
                found1 = dec.find("---v3---");
                if (found1!=std::string::npos)
                {

                    USBID = dec.substr(found1+8);
                    dec=dec.substr(0,found1);
                    size_t found2 = USBID.find("---vn---");
                    if (found2!=std::string::npos)
                    {
                        networkflag=1;
                        USBID=USBID.substr(0,found2);
                        USBlock=2;
                    }
                    else
                    {
                        networkflag=0;
                    }
                    USBlock=2;
                }
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {

                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }
            else
            {
                dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {
                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }


            WORD upw=0;
            WORD upw1=0;

            divlic[0]=dec;
            lic= LICENSE(&divlic[0]);
            dec=lic;
            check =divlic[0];
            dec.append(check);
            int ver=strtol(divlic[3].c_str(),NULL,10);
            AppIdnew=AppId[0].c_str();

            if (divlic[2]==AppIdnew && version==ver && divlic[1] ==STRING_FIVE_DEFINE_NAME && networkflag==0)
            {


                date= strtol(divlic[4].c_str(),NULL,10);
                month= date%100;
                date=date/100;
                year= strtol(divlic[5].c_str(),NULL,10);
                status=0;
            }
            lockcode = lockcode.substr(found+3);
        }
        else
        {
            // delete[] memblock;
            lockcode=="";
        }

    }
    delete[] memblock;
LOOPBREAKNEW:
    if(netflag!=0)
    {
        std::string data=AppIdnew.c_str();
        char buff[10];
        sprintf_s(buff,"%d",version);
        data.append("%%%");
        data.append(buff);
        std::string servernametemp=servernamenew;
        if(servernametemp=="")
        {
            if(AG_IsAuthentic_Net(AppIdnew.c_str(),version,&Handlenew,&servernametemp))
                return status;
            AG_Release(&Handlenew,&servernametemp);
            if(servernametemp!="")
            {
                status = AG_start_date_net(&data,&servernametemp);
                if(status==0)
                {
                    date = strtol(data.substr(0,data.find("--")).c_str(),NULL,10);
                    data = data.substr(data.find("--")+2);
                    month = strtol(data.substr(0,data.find("--")).c_str(),NULL,10);
                    data = data.substr(data.find("--")+2);
                    year = strtol(data.substr(0,data.find("--")).c_str(),NULL,10);
                    data = data.substr(data.find("--")+2);
                    return 0;
                }
            }
        }
    }
    return status;
}

extern "C" EXPORT int __cdecl  AG_End_Date  (int &date,int &month,int &year , std::string AppId[1], int version, int netflag=0 , std::string servername[1]=NULL)
{

    lockcode = "";
    file="";
    status=2;
    _mkdir("C:/Users");
    _mkdir("C:/Users/Public");
    _mkdir("C:/Users/Public/Application Data");
    _mkdir("C:/Users/Public/Application Data/Bastion");
    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
    //file.append();
    size_t found ;
    std::string USBID = "";
    char *memblock;
    std::ifstream ifile;
    ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",std::ios::in|std::ios::binary|std::ios::ate);

    if(ifile==NULL)
    {
        goto LOOPBREAKNEW;
    }
    if (ifile.is_open())
    {
        int size=0;
        size = (int) ifile.tellg();


        memblock = new char[size];
        ifile.seekg (0, std::ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            ifile.read(&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);


        ifile.close();
    }
    else
    {
        goto LOOPBREAKNEW;

    }

    if (lockcode == "")
    {
        delete[] memblock;
        goto LOOPBREAKNEW;
    }

    dec="";
    lockcode1="";
    size_t found1;
    int USBlock=0;

    //  	hItem1[0]=m_tree.InsertItem("STANDALONE",0,0);
    while (lockcode!="")
    {
        found = lockcode.find("%%%");
        if (found!=std::string::npos)
        {

            lockcode1=lockcode.substr(0,found);

            found1 = lockcode1.find("---v2---");
            if (found1!=std::string::npos)
            {
                lockcode1=lockcode1.substr(0,found1);
                USBlock=1;
            }
            else
            {
                USBlock=0;
            }
            if (USBlock==0)
            {
                runtime = getHardDriveComputerID ();
                dec = aes_decrypt(lockcode1, STRING_THREE_DEFINE_NAME + runtime);
                found1 = dec.find("---v3---");
                if (found1!=std::string::npos)
                {

                    USBID = dec.substr(found1+8);
                    dec=dec.substr(0,found1);
                    size_t found2 = USBID.find("---vn---");
                    if (found2!=std::string::npos)
                    {
                        networkflag=1;
                        USBID=USBID.substr(0,found2);
                        USBlock=2;
                    }
                    else
                    {
                        networkflag=0;
                    }
                    USBlock=2;
                }
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {

                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }
            else
            {
                dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {
                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }


            WORD upw=0;
            WORD upw1=0;

            divlic[0]=dec;
            lic= LICENSE(&divlic[0]);
            dec=lic;
            check =divlic[0];
            dec.append(check);
            int ver=strtol(divlic[3].c_str(),NULL,10);
            AppIdnew=AppId[0].c_str();

            if (divlic[2]==AppIdnew && version==ver && divlic[1] ==STRING_FIVE_DEFINE_NAME && networkflag==0)
            {


                date= strtol(divlic[6].c_str(),NULL,10);
                month= date%100;
                date=date/100;
                year= strtol(divlic[7].c_str(),NULL,10);
                status=0;
            }
            lockcode = lockcode.substr(found+3);
        }
        else
        {
            // delete[] memblock;
            lockcode=="";
        }

    }
    delete[] memblock;
LOOPBREAKNEW:
    if(netflag!=0)
    {
        std::string data=AppIdnew.c_str();
        char buff[10];
        sprintf_s(buff,"%d",version);
        data.append("%%%");
        data.append(buff);
        std::string servernametemp=servernamenew;
        if(servernametemp=="")
        {
            if(AG_IsAuthentic_Net(AppIdnew.c_str(),version,&Handlenew,&servernametemp))
                return status;
            AG_Release(&Handlenew,&servernametemp);
            if(servernametemp!="")
            {
                status = AG_end_date_net(&data,&servernametemp);
                if(status==0)
                {
                    date = strtol(data.substr(0,data.find("--")).c_str(),NULL,10);
                    data = data.substr(data.find("--")+2);
                    month = strtol(data.substr(0,data.find("--")).c_str(),NULL,10);
                    data = data.substr(data.find("--")+2);
                    year = strtol(data.substr(0,data.find("--")).c_str(),NULL,10);
                    data = data.substr(data.find("--")+2);
                    return 0;
                }
            }
        }
    }
    return status;
}

extern "C" EXPORT int __cdecl AG_USBID (std::string IDp[1], int &memberIndex, std::string APPID[1], int version)
{
    int lastkey=0;
    do
    {
        HDEVINFO deviceInfoSet;
        GUID *guidDev = (GUID*) &GUID_DEVCLASS_USB;
        deviceInfoSet = SetupDiGetClassDevs(guidDev, NULL, NULL, DIGCF_PRESENT | DIGCF_PROFILE);
        TCHAR buffer [4000];
        DWORD buffersize =4000;
        std::string keyid="";
        int i =0;
        int j = i+1;
        while (i!=j) {
            SP_DEVINFO_DATA deviceInfoData;
            ZeroMemory(&deviceInfoData, sizeof(SP_DEVINFO_DATA));
            deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
            if (SetupDiEnumDeviceInfo(deviceInfoSet, memberIndex, &deviceInfoData) == FALSE) {
                if (GetLastError() == ERROR_NO_MORE_ITEMS) {
                    lastkey=1;
                    IDp[0]="";
                    goto LOOPBREAK;
                }
            }
            DWORD nSize=0 ;
            SetupDiGetDeviceInstanceId (deviceInfoSet, &deviceInfoData, buffer, sizeof(buffer), &nSize);
            buffer [nSize] ='\0';
            keyid = buffer;
            size_t found;


            found = keyid.find("USB\\VID_054C");
            if (found!=std::string::npos)
            {
                i=i+1;
                IDp[0]=keyid;
                return 0;
            }
            memberIndex++;
        }
        if (deviceInfoSet) {
            SetupDiDestroyDeviceInfoList(deviceInfoSet);
        }
    }
    while (lastkey==0);
    IDp[0]="";
LOOPBREAK:
    std::string data=APPID[0].c_str();
    char buff[10];
    sprintf_s(buff,"%d",version);
    data.append("%%%");
    data.append(buff);
    std::string servernametemp=servernamenew;
    if(servernametemp=="")
    {
        if(AG_IsAuthentic_Net(AppIdnew.c_str(),version,&Handlenew,&servernametemp))
            return 9;
        AG_Release(&Handlenew,&servernametemp);

        std::string keyid="";
        if(servernametemp!="")
        {
            status =  AG_USBID_net(&keyid, &servernametemp);
            if(status==0)
            {
                IDp[0] = keyid;
                return 0;
            }
        }
    }
    return 9;
}

int AG_IsAuthentic_Net(std::string appID, int version, std::string Handle[1], std::string servername[1])
{
    std::string Handletemp="";
    Handletemp.resize(1024);
    char returnval[1024]="";
    sprintf_s(returnval,10,"%d",version);
    char* pPath;
    pPath = getenv ("AG_SERVER");
    if(pPath==NULL)
    {
        int portno = ::_wtoi(L"27016");

        std::string p = "---vnbroadcast---";


        WORD w = MAKEWORD(1,1);
        WSADATA wsadata;
        ::WSAStartup(w, &wsadata);



        SOCKET s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if(s == -1)
        {
            return 1;
        }
        char opt = 1;
        setsockopt(s, SOL_SOCKET, SO_BROADCAST, (char*)&opt, sizeof(char));
        SOCKADDR_IN brdcastaddr;
        memset(&brdcastaddr,0, sizeof(brdcastaddr));
        brdcastaddr.sin_family = AF_INET;
        brdcastaddr.sin_port = htons(portno);
        brdcastaddr.sin_addr.s_addr = INADDR_BROADCAST;
        int len = sizeof(brdcastaddr);
        char sbuf[1024];

        std::string in=STRING_FIVE_DEFINE_NAME;
        std::string serverdata="";
        in.append("--");
        in.append(appID);
        in.append("--");
        in.append(returnval);

        in.append(p);
        srand (time(NULL));
        int val = rand();
        sprintf_s(returnval,10,"%d",val);
        in.append(returnval);
        serverdata.resize(1024);
        if(in!="")
        {
            int checkvalue=0;
            while (checkvalue==0)
            {
                serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
                serverdata.append("%%%");
                serverdata=serverdata.c_str();
                if(serverdata.find("%%%")!=std::string::npos)
                    checkvalue=1;
            }
        }
        else
            serverdata="";
        int ret = sendto(s, serverdata.c_str(), serverdata.size(), 0, (sockaddr*)&brdcastaddr, len);
        if(ret < 0)
        {
            return 1;
        }


        char rbuf[1024];

        std::string receivedata="";
        for(int i =0; i<1024; i++)
        {
            rbuf[i]='\0';
        }

        fd_set fds ;
        int n ;
        struct timeval tv ;

// Set up the file descriptor set.
        FD_ZERO(&fds) ;
        FD_SET(s, &fds) ;

// Set up the struct timeval for the timeout.
        tv.tv_sec = 5 ;
        tv.tv_usec = 0 ;

// Wait until timeout or data received.
        n = select ( s, &fds, NULL, NULL, &tv ) ;
        if ( n != 0)
        {
            ret = recvfrom(s,rbuf, 1024, 0, (sockaddr*)&brdcastaddr, &len);
            //ret = recv(s, rbuf, 1024, 0);
            in.resize(1024);
            in=rbuf;
            receivedata.resize(1024);

            if ( ret > 0 )
            {

                for(int i = 1024; i >= 1; i--)
                {
                    if(rbuf[i] == '\n' && rbuf[i - 1] == '\r')
                    {
                        rbuf[i-1] = '\0';
                        break;
                    }
                }
                size_t foundc;
                foundc=in.find("%%%");
                if( foundc!=std::string::npos)
                {
                    in=in.substr(0,foundc);
                    receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
                }
                char *p = inet_ntoa(brdcastaddr.sin_addr);
                Handletemp = receivedata.c_str();
                servername[0] = p;
                if(receivedata.find("---vi---")!=std::string::npos)
                {
                    Handletemp = receivedata.substr(0, receivedata.find("---vi---")+8);
                    receivedata=  receivedata.substr(receivedata.find("---vi---")+8);
                    receivedata=  receivedata.substr(0,receivedata.find("---"));
                    Handletemp.append(receivedata);
                    Handle[0]= Handletemp;
                    return 0;
                }
                Handle[0]=Handletemp;
            }
        }
        else
        {
            Handle[0] = "NO SERVER RESPONSE";
            return 1;
        }
        ::closesocket(s);
    }
    else
    {
        servername[0] = pPath;
        WSADATA wsaData;
        SOCKET ConnectSocket = INVALID_SOCKET;
        struct addrinfo *result = NULL,
                             *ptr = NULL,
                              hints;

        std::string sendbuf =Handle[0];

        char recvbuf[DEFAULT_BUFLEN];
        int iResult;
        int recvbuflen = DEFAULT_BUFLEN;



        // Initialize Winsock
        iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
        if (iResult != 0) {

            return 1;
        }

        ZeroMemory( &hints, sizeof(hints) );
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
        iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
        if ( iResult != 0 ) {

            return 1;
        }

        // Attempt to connect to an address until one succeeds
        for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

            // Create a SOCKET for connecting to server
            ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                                   ptr->ai_protocol);
            if (ConnectSocket == INVALID_SOCKET) {

                WSACleanup();
                return 1;
            }

            // Connect to server.
            iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
            if (iResult == SOCKET_ERROR) {
                closesocket(ConnectSocket);
                ConnectSocket = INVALID_SOCKET;
                continue;
            }
            break;
        }

        freeaddrinfo(result);

        if (ConnectSocket == INVALID_SOCKET) {

            return 1;
        }

        // Send an initial buffer
        std::string p = "---vnbroadcast---";
        std::string in=STRING_FIVE_DEFINE_NAME;
        std::string serverdata="";
        in.append("--");
        in.append(appID);
        in.append("--");
        in.append(returnval);

        in.append(p);
        srand (time(NULL));
        int val = rand();
        sprintf_s(returnval,10,"%d",val);
        in.append(returnval);
        serverdata.resize(1024);

        if(in!="")
        {
            int checkvalue=0;
            while (checkvalue==0)
            {
                serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
                serverdata.append("%%%");
                serverdata=serverdata.c_str();
                if(serverdata.find("%%%")!=std::string::npos)
                    checkvalue=1;
            }
        }
        else
            serverdata="";
        iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
        if (iResult == SOCKET_ERROR) {

            WSACleanup();
            return 1;
        }

        // shutdown the connection since no more data will be sent
        iResult = shutdown(ConnectSocket, SD_SEND);
        if (iResult == SOCKET_ERROR) {

            closesocket(ConnectSocket);
            WSACleanup();

            return 1;
        }
        std::string receivedata="";
        for(int i =0; i<1024; i++)
        {
            recvbuf[i]='\0';
        }
        // Receive until the peer closes the connection
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        in.resize(recvbuflen);
        in=recvbuf;
        receivedata.resize(1024);

        if ( iResult > 0 )
        {

            for(int i = 1024; i >= 1; i--)
            {
                if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
                {
                    recvbuf[i-1] = '\0';
                    break;
                }
            }
            size_t foundc;
            size_t found;
            foundc=in.find("%%%");
            if( foundc!=std::string::npos)
            {
                in=in.substr(0,foundc);
                receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
            }
            char *p = pPath;
            Handle[0] = receivedata.c_str();
            servername[0] = p;
            if(receivedata.find("---vi---")!=std::string::npos)
            {
                Handle[0] = receivedata.substr(0, receivedata.find("---vi---")+8);
                receivedata=  receivedata.substr(receivedata.find("---vi---")+8);
                receivedata=  receivedata.substr(0,receivedata.find("---"));
                Handle[0].append(receivedata);
                return 0;
            }
        }
        closesocket(ConnectSocket);
        WSACleanup();
    }
}

int AG_Update(std::string Handle[1], std::string servername[1])
{

    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                         *ptr = NULL,
                          hints;

    std::string sendbuf =Handle[0];

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {

        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {

        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {

            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {

        return 1;
    }

    // Send an initial buffer
    std::string serverdata ="" ;
    std::string in ="";
    in=Handle[0];
    in.append("---nobroadcast---");
    serverdata.resize(1024);
    if(in!="")
    {
        int checkvalue=0;
        while (checkvalue==0)
        {
            serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
            serverdata.append("%%%");
            serverdata=serverdata.c_str();
            if(serverdata.find("%%%")!=std::string::npos)
                checkvalue=1;
        }
    }
    else
        serverdata="";
    iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
    if (iResult == SOCKET_ERROR) {

        WSACleanup();
        return 1;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {

        closesocket(ConnectSocket);
        WSACleanup();

        return 1;
    }
    std::string receivedata="";
    for(int i =0; i<1024; i++)
    {
        recvbuf[i]='\0';
    }
    // Receive until the peer closes the connection
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    in.resize(recvbuflen);
    in=recvbuf;
    receivedata.resize(1024);

    if ( iResult > 0 )
    {

        for(int i = 1024; i >= 1; i--)
        {
            if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
            {
                recvbuf[i-1] = '\0';
                break;
            }
        }
        size_t foundc;
        size_t found;
        foundc=in.find("%%%");
        if( foundc!=std::string::npos)
        {
            in=in.substr(0,foundc);
            receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
        }
        if(receivedata.find("---nobroad")!=std::string::npos)
        {
            return 0;
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();
    }

}

extern "C" EXPORT int __cdecl  AG_Release(std::string Handle[1], std::string servername[1])
{

    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                         *ptr = NULL,
                          hints;

    std::string sendbuf =Handle[0];

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {

        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
//   iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    iResult = getaddrinfo(servername[0].c_str(), DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {

        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ; ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {

            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {

        return 1;
    }

    // Send an initial buffer
    std::string serverdata ="" ;
    std::string in ="";
    in=Handle[0];
    in.append("---deeletefile---");
    serverdata.resize(1024);
    if(in!="")
    {
        int checkvalue=0;
        while (checkvalue==0)
        {
            serverdata= aes_encrypt(in, STRING_TWO_DEFINE_NAME);
            serverdata.append("%%%");
            serverdata=serverdata.c_str();
            if(serverdata.find("%%%")!=std::string::npos)
                checkvalue=1;
        }
    }
    else
        serverdata="";
    iResult = send( ConnectSocket, serverdata.c_str(), serverdata.length(), 0 );
    if (iResult == SOCKET_ERROR) {

        WSACleanup();
        return 1;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {

        closesocket(ConnectSocket);
        WSACleanup();

        return 1;
    }
    std::string receivedata="";
    for(int i =0; i<1024; i++)
    {
        recvbuf[i]='\0';
    }
    // Receive until the peer closes the connection
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    in.resize(recvbuflen);
    in=recvbuf;
    receivedata.resize(1024);

    if ( iResult > 0 )
    {

        for(int i = 1024; i >= 1; i--)
        {
            if(recvbuf[i] == '\n' && recvbuf[i - 1] == '\r')
            {
                recvbuf[i-1] = '\0';
                break;
            }
        }
        size_t foundc;
        size_t found;
        foundc=in.find("%%%");
        if( foundc!=std::string::npos)
        {
            in=in.substr(0,foundc);
            receivedata= aes_decrypt(in, STRING_TWO_DEFINE_NAME);
        }
        if(receivedata.find("---deelete")!=std::string::npos)
        {

            return 0;
        }
        // cleanup
        closesocket(ConnectSocket);
        WSACleanup();
    }

}


extern "C" EXPORT int __cdecl AG_AllowUSB (std::string Handle[1],BOOL flag)
{
    sprintf_s(buff,"%d", flag);
    Handle[0].append("---");
    Handle[0].append(buff);
    return 0;
}

extern "C" EXPORT int __cdecl AG_CheckUSB (std::string IDp[1])
{
    int memberIndex = 0;
    int lastkey=0;
    do
    {
        HDEVINFO deviceInfoSet;
        GUID *guidDev = (GUID*) &GUID_DEVCLASS_USB;
        deviceInfoSet = SetupDiGetClassDevs(guidDev, NULL, NULL, DIGCF_PRESENT | DIGCF_PROFILE);
        TCHAR buffer [4000];
        DWORD buffersize =4000;
        std::string keyid="";
        int i =0;
        int j = i+1;
        while (i!=j) {
            SP_DEVINFO_DATA deviceInfoData;
            ZeroMemory(&deviceInfoData, sizeof(SP_DEVINFO_DATA));
            deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
            if (SetupDiEnumDeviceInfo(deviceInfoSet, memberIndex, &deviceInfoData) == FALSE) {
                if (GetLastError() == ERROR_NO_MORE_ITEMS) {
                    lastkey=1;
                    IDp[0]="";
                    return 9;
                }
            }
            DWORD nSize=0 ;
            SetupDiGetDeviceInstanceId (deviceInfoSet, &deviceInfoData, buffer, sizeof(buffer), &nSize);
            buffer [nSize] ='\0';
            keyid = buffer;
            size_t found;


            found = keyid.find(IDp[0].c_str());
            if (found!=std::string::npos)
            {
                i=i+1;
                IDp[0]=keyid;
                return 0;
            }
            memberIndex++;
        }
        if (deviceInfoSet) {
            SetupDiDestroyDeviceInfoList(deviceInfoSet);
        }
    }
    while (lastkey==0);
    IDp[0]="";
    return 9;
}

extern "C" EXPORT std::string __cdecl AG_Error(int status1)
{
    switch(status1)
    {
    case 0:
        return "AG_SUCCESS";
        break;
    case 1:
        return  "AG_FILE_IO_ERROR";
        break;
    case 2:
        return  "AG_NO_LICENSE";
        break;
    case 3:
        return "AG_DATE_TAMPERED";
        break;
    case 4:
        return "AG_EXPIRED";
        break;
    case 5:
        return  "AG_INVALID_LICENSE";
        break;
    case 6:
        return  "AG_EMPTY_MEMORY";
        break;
    case 7:
        return  "AG_INVALID_MEMORY";
        break;
    case 8:
        return "AG_TERMINAL_SESSION";
        break;
    case 9:
        return "AG_NO_KEY";
        break;
    }
    return "";
}

int  AG_IsAuthentic1 (std::string Handle[1], std::string AppId[1], int version, int netflag=0 , std::string servername[1]=NULL)
{
    if(netflag ==1)
    {
        status = AG_IsAuthentic_Net(AppId[0].c_str(), version, &Handlenew, &servername[0]);
        if(status == 0)
        {
            Handle[0] = Handlenew;
            return status;
        }
    }
    lockcode = "";
    file="";
    status=2;
    _mkdir("C:/Users");
    _mkdir("C:/Users/Public");
    _mkdir("C:/Users/Public/Application Data");
    _mkdir("C:/Users/Public/Application Data/Bastion");
    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
    //file.append();
    size_t found ;
    if(Handle[0]!="" )
    {
        found =Handle[0].find("---");
        if (found!=std::string::npos)
        {

            usballow = strtol((Handle[0].substr(found +3)).c_str(),NULL,20);
        }
        if(Handle[0].substr(0,found)!="" )
        {
            remote = strtol((Handle[0].substr(0,found)).c_str(),NULL,20);
        }
    }


    std::ifstream ifile;
    ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",std::ios::in|std::ios::binary|std::ios::ate);

    if(ifile==NULL)
    {
        return 1;
    }
    if(remote==0)
    {
        _dupenv_s(&ses,&sizer,"sessionname");

        rsession=ses1;


        int foundrdp= rsession.find("RDP");
        if (foundrdp!=std::string::npos)
        {
            rsession="RDP";
        }
        BOOL statusrem = GetSystemMetrics(SM_REMOTECONTROL);


        if (statusrem!=0 || rsession=="RDP")
        {
            ifile.close();
            return 8;
        }
        delete ses;
    }

    char *memblock;

    if (ifile.is_open())
    {
        int size=0;
        size = (int) ifile.tellg();


        memblock = new char[size];
        ifile.seekg (0, std::ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            ifile.read(&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);


        ifile.close();
    }
    else
    {
        return 1;

    }

    if (lockcode == "")
    {
        delete[] memblock;
        return 1;
    }

    dec="";
    lockcode1="";
    size_t found1;
    int USBlock=0;
    std::string USBID = "";
    //  	hItem1[0]=m_tree.InsertItem("STANDALONE",0,0);
    while (lockcode!="")
    {
        found = lockcode.find("%%%");
        if (found!=std::string::npos)
        {

            lockcode1=lockcode.substr(0,found);

            found1 = lockcode1.find("---v2---");
            if (found1!=std::string::npos)
            {
                lockcode1=lockcode1.substr(0,found1);
                USBlock=1;
            }
            else
            {
                USBlock=0;
            }
            if (USBlock==0)
            {
                runtime = getHardDriveComputerID ();
                dec = aes_decrypt(lockcode1, STRING_THREE_DEFINE_NAME + runtime);
                found1 = dec.find("---v3---");
                if (found1!=std::string::npos)
                {

                    USBID = dec.substr(found1+8);
                    dec=dec.substr(0,found1);
                    size_t found2 = USBID.find("---vn---");
                    if (found2!=std::string::npos)
                    {
                        networkflag=1;
                        USBID=USBID.substr(0,found2);
                        USBlock=2;
                    }
                    else
                    {
                        networkflag=0;
                    }
                    USBlock=2;
                }
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {

                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }
            else
            {
                dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {
                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }

            Handle[0]=lockcode1;
            if (USBlock==1)
            {
                Handle[0].append("---v2---");
            }
            Handle[0].append("%%%");
            buff;
            sprintf_s(buff,"%d", remote);
            Handle[0].append(buff);
            WORD upw=0;
            WORD upw1=0;

            divlic[0]=dec;
            lic= LICENSE(&divlic[0]);
            dec=lic;
            check =divlic[0];
            dec.append(check);
            int ver=strtol(divlic[3].c_str(),NULL,10);
            AppIdnew=AppId[0].c_str();

            if (divlic[2]==AppIdnew && version==ver && divlic[1] ==STRING_FIVE_DEFINE_NAME && networkflag==0)
            {


                long date= strtol(divlic[6].c_str(),NULL,10);
                long  month= date%100;
                WORD enddate=(WORD)date;
                WORD endmonth=(WORD)month;
                date=date/100;
                long endyear= strtol(divlic[7].c_str(),NULL,10);
                if (date==0 || month==0)
                {
                    long date1= strtol(divlic[9].c_str(),NULL,10);


                    ///////////////
                    if (date1 ==3)
                    {

                        HardDriveSerialNumbers1="";
                        HardDriveSerialNumbers1 = getHardDriveComputerID ();

                        harddisk =  DISKID(check,&mac[0]);

                        checkmac=GetMACaddress();
                        int cmac=0;

                        int found1 = checkmac.find("--");
                        if (found1!=std::string::npos)
                        {

                            checkmac.append("--");

                            int i=0;
                            while (checkmac!="")
                            {

                                if (found1!=std::string::npos)
                                {
                                    if(i<5)
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");

                                        if(mac[i]!="")
                                        {
                                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                                            {
                                                cmac=1;
                                            }
                                        }
                                        i++;
                                    }
                                    else
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");
                                    }
                                }
                                else
                                {
                                    disk = checkmac.substr(0,found1);
                                    checkmac = checkmac.substr(found1+2);
                                    found1 = checkmac.find("--");
                                }
                            }

                        }
                        if(USBlock==2)
                        {
                            ID = USBID;
                            int ret =  AG_CheckUSB (&ID);
                            if (HardDriveSerialNumbers1==harddisk && cmac==1 && ID==USBID)
                            {
                                delete[] memblock;
                                return 0;
                            }
                        }
                        else
                        {
                            if (HardDriveSerialNumbers1==harddisk && cmac==1)
                            {
                                delete[] memblock;
                                return 0;
                            }
                        }


                    }
/////////////////////////////////////////////
                    else if (date1 ==1 && usballow==1)
                    {

                        harddisk =  DISKID(check,&mac[0]);
                        ID=harddisk;
                        int ret =  AG_CheckUSB (&ID);
                        if (harddisk==ID)
                        {
                            delete[] memblock;
                            return 0;
                        }
                    }

                }

                else
                {
                    long date1= strtol(divlic[9].c_str(),NULL,10);

                    ////////////////////////////////////////////////////////////
                    if (date1 ==3)
                    {

                        HardDriveSerialNumbers1="";
                        HardDriveSerialNumbers1 = getHardDriveComputerID ();
//	std::string mac[5]={""};
                        harddisk =  DISKID(check,&mac[0]);
                        checkmac ="";
                        checkmac = GetMACaddress();
                        int cmac=0;
                        checkmac.append("--");
                        int found1 = checkmac.find("--");

                        if (found1!=std::string::npos)
                        {

                            int i=0;
                            while (checkmac!="")
                            {

                                if (found1!=std::string::npos)
                                {
                                    if(i<5)
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");

                                        if(mac[i]!="")
                                        {
                                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                                            {
                                                cmac=1;
                                            }
                                        }
                                        i++;
                                    }
                                    else
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");
                                    }
                                }
                                else
                                {
                                    disk = checkmac.substr(0,found1);
                                    checkmac = checkmac.substr(found1+2);
                                    found1 = checkmac.find("--");
                                }
                            }

                        }
                        if(USBlock==2)
                        {
                            ID = USBID;
                            int ret =  AG_CheckUSB (&ID);
                            if ( ID==USBID)
                            {
                                delete[] memblock;
                                USBlock==0;
                            }
                        }
                        if (HardDriveSerialNumbers1==harddisk && cmac==1 && USBlock==0 )
                        {
                            WORD  value=0;

                            WORD date=0;
                            WORD year=0;
                            getdate_year( &date,  &year);

                            int r = ReadSectors(0,56,1,&value);


                            if(value==year)
                            {

                                WriteSectors(0,56,1,year);

                                r = ReadSectors(0,55,1,&value);
                                if((value%100)<=(date%100))
                                {
                                    if ((value%100)!=(date%100))
                                    {
                                        WriteSectors(0,55,1,date);
                                        ///////
                                        if(endyear==year)
                                        {
                                            if(endmonth>=(date%100))
                                            {
                                                if (endmonth!=(date%100))
                                                {
                                                    delete[] memblock;
                                                    return 0;
                                                }
                                                else if(endmonth==(date%100) )
                                                {
                                                    if(enddate>=(date/100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }
                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }

                                        }
                                        else if(endyear>year)
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }

                                        else
                                        {
                                            Handle[0]="";
                                            status=4;
                                        }

                                    }
                                    else if((value%100)==(date%100) )
                                    {
                                        if((value/100)<=(date/100))
                                        {
                                            WriteSectors(0,55,1,date);
                                            ///////

                                            if(endyear==year)
                                            {
                                                if(endmonth>=(date%100))
                                                {
                                                    if (endmonth!=(date%100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else if(endmonth==(date%100) )
                                                    {
                                                        if(enddate>=(date/100))
                                                        {
                                                            delete[] memblock;
                                                            return 0;
                                                        }
                                                        else
                                                        {
                                                            Handle[0]="";
                                                            status= 4;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }

                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else if(endyear>year)
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }

                                            else
                                            {
                                                Handle[0]="";
                                                status=4;
                                            }

                                        }
                                        else
                                        {
                                            delete[] memblock;
                                            Handle[0]="";
                                            return 3;
                                        }
                                    }

                                }
                                else
                                {

                                    delete[] memblock;
                                    Handle[0]="";
                                    return 3;
                                }

                            }

                            else if(value<year)
                            {
                                WriteSectors(0,56,1,year);
                                WriteSectors(0,55,1,date);
                                ////////

                                if(endyear==year)
                                {
                                    if(endmonth<=(date%100))
                                    {
                                        if (endmonth!=(date%100))
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }
                                        else if(endmonth==(date%100) )
                                        {
                                            if(enddate<=(date/100))
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }
                                        }
                                        else
                                        {
                                            Handle[0]="";
                                            status= 4;
                                        }

                                    }
                                    else
                                    {
                                        Handle[0]="";
                                        status= 4;
                                    }

                                }
                                else if(endyear<year)
                                {
                                    delete[] memblock;
                                    return 0;
                                }

                                else
                                {
                                    Handle[0]="";
                                    status=4;
                                }

                            }

                            else
                            {
                                delete[] memblock;
                                Handle[0]="";
                                return 3;
                            }


                        }


                    }

                    ////////////////////////////////////////////////////////

                    else if (date1 ==1 && usballow==1)
                    {
///////////////////////////////////////
                        harddisk =  DISKID(check,&mac[0]);
                        ID=harddisk;
                        int ret =  AG_CheckUSB (&ID);


                        if (harddisk==ID)
                        {
                            WORD  value=0;

                            WORD date=0;
                            WORD year=0;
                            getdate_year( &date,  &year);

                            int r = ReadSectors(0,56,1,&value);


                            if(value==year)
                            {

                                WriteSectors(0,56,1,year);

                                r = ReadSectors(0,55,1,&value);
                                if((value%100)<=(date%100))
                                {
                                    if ((value%100)!=(date%100))
                                    {
                                        WriteSectors(0,55,1,date);
                                        ///////
                                        if(endyear==year)
                                        {
                                            if(endmonth>=(date%100))
                                            {
                                                if (endmonth!=(date%100))
                                                {
                                                    delete[] memblock;
                                                    return 0;
                                                }
                                                else if(endmonth==(date%100) )
                                                {
                                                    if(enddate>=(date/100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }
                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }

                                        }
                                        else if(endyear>year)
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }

                                        else
                                        {
                                            Handle[0]="";
                                            status=4;
                                        }

                                    }
                                    else if((value%100)==(date%100) )
                                    {
                                        if((value/100)<=(date/100))
                                        {
                                            WriteSectors(0,55,1,date);
                                            ///////

                                            if(endyear==year)
                                            {
                                                if(endmonth>=(date%100))
                                                {
                                                    if (endmonth!=(date%100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else if(endmonth==(date%100) )
                                                    {
                                                        if(enddate>=(date/100))
                                                        {
                                                            delete[] memblock;
                                                            return 0;
                                                        }
                                                        else
                                                        {
                                                            Handle[0]="";
                                                            status= 4;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }

                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else if(endyear>year)
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }

                                            else
                                            {
                                                Handle[0]="";
                                                status=4;
                                            }

                                        }
                                        else
                                        {
                                            delete[] memblock;
                                            Handle[0]="";
                                            return 3;
                                        }
                                    }

                                }
                                else
                                {

                                    delete[] memblock;
                                    Handle[0]="";
                                    return 3;
                                }

                            }

                            else if(value<year)
                            {
                                WriteSectors(0,56,1,year);
                                WriteSectors(0,55,1,date);
                                ////////

                                if(endyear==year)
                                {
                                    if(endmonth<=(date%100))
                                    {
                                        if (endmonth!=(date%100))
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }
                                        else if(endmonth==(date%100) )
                                        {
                                            if(enddate<=(date/100))
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }
                                        }
                                        else
                                        {
                                            Handle[0]="";
                                            status= 4;
                                        }

                                    }
                                    else
                                    {
                                        Handle[0]="";
                                        status= 4;
                                    }

                                }
                                else if(endyear<year)
                                {
                                    delete[] memblock;
                                    return 0;
                                }

                                else
                                {
                                    Handle[0]="";
                                    status=4;
                                }

                            }

                            else
                            {
                                delete[] memblock;
                                Handle[0]="";
                                return 3;
                            }


                        }


                        /////////////////////////////////////////


                    }

                }

            }
            lockcode = lockcode.substr(found+3);
        }
        else
        {
            // delete[] memblock;
            lockcode=="";
        }

    }
    delete[] memblock;
    Handle[0]="";
    return status;
}

extern "C" EXPORT int __cdecl  AG_IsAuthentic  (std::string Handle[1], std::string AppId[1], int version, int netflag=0 , std::string servername[1]=NULL)
{
    if(netflag ==1)
    {
        status = AG_IsAuthentic_Net(AppId[0].c_str(), version, &Handlenew, &servername[0]);
        if(status == 0)
        {
            Handle[0] = Handlenew;
            return status;
        }
    }
    lockcode = "";
    file="";
    status=2;
    _mkdir("C:/Users");
    _mkdir("C:/Users/Public");
    _mkdir("C:/Users/Public/Application Data");
    _mkdir("C:/Users/Public/Application Data/Bastion");
    SetFileAttributes("C:/Users/Public/Application Data/Bastion/AG.af",FILE_ATTRIBUTE_NORMAL);
    //file.append();
    size_t found ;
    if(Handle[0]!="" )
    {
        found =Handle[0].find("---");
        if (found!=std::string::npos)
        {

            usballow = strtol((Handle[0].substr(found +3)).c_str(),NULL,20);
        }
        if(Handle[0].substr(0,found)!="" )
        {
            remote = strtol((Handle[0].substr(0,found)).c_str(),NULL,20);
        }
    }


    std::ifstream ifile;
    ifile.open("C:/Users/Public/Application Data/Bastion/AG.af",std::ios::in|std::ios::binary|std::ios::ate);

    if(ifile==NULL)
    {
        return 1;
    }
    if(remote==0)
    {
        _dupenv_s(&ses,&sizer,"sessionname");

        rsession=ses1;


        int foundrdp= rsession.find("RDP");
        if (foundrdp!=std::string::npos)
        {
            rsession="RDP";
        }
        BOOL statusrem = GetSystemMetrics(SM_REMOTECONTROL);


        if (statusrem!=0 || rsession=="RDP")
        {
            ifile.close();
            return 8;
        }
        delete ses;
    }

    char *memblock;

    if (ifile.is_open())
    {
        int size=0;
        size = (int) ifile.tellg();


        memblock = new char[size];
        ifile.seekg (0, std::ios::beg);

        int size1=size;
        //file.read (&memblock[debug], size);
        //debug=strlen(&memblock[debug])+1;
        //lockcode.append(&memblock[0]);


        size_t debug=0;

        lockcode.resize(size);
        for (int read=0; read<size; read++)
        {
            ifile.read(&memblock[read],1);

            lockcode[read]=memblock[read];
        }
        lockcode = lockcode.substr(0,size);


        ifile.close();
    }
    else
    {
        return 1;

    }

    if (lockcode == "")
    {
        delete[] memblock;
        return 1;
    }

    dec="";
    lockcode1="";
    size_t found1;
    int USBlock=0;
    std::string USBID = "";
    //  	hItem1[0]=m_tree.InsertItem("STANDALONE",0,0);
    while (lockcode!="")
    {
        found = lockcode.find("%%%");
        if (found!=std::string::npos)
        {

            lockcode1=lockcode.substr(0,found);

            found1 = lockcode1.find("---v2---");
            if (found1!=std::string::npos)
            {
                lockcode1=lockcode1.substr(0,found1);
                USBlock=1;
            }
            else
            {
                USBlock=0;
            }
            if (USBlock==0)
            {
                runtime = getHardDriveComputerID ();
                dec = aes_decrypt(lockcode1, STRING_THREE_DEFINE_NAME + runtime);
                found1 = dec.find("---v3---");
                if (found1!=std::string::npos)
                {

                    USBID = dec.substr(found1+8);
                    dec=dec.substr(0,found1);
                    size_t found2 = USBID.find("---vn---");
                    if (found2!=std::string::npos)
                    {
                        networkflag=1;
                        USBID=USBID.substr(0,found2);
                        USBlock=2;
                    }
                    else
                    {
                        networkflag=0;
                    }
                    USBlock=2;
                }
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {

                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }
            else
            {
                dec = aes_decrypt(lockcode1, STRING_TWO_DEFINE_NAME);
                size_t found2 = dec.find("---vn---");
                if (found2!=std::string::npos)
                {
                    networkflag=1;
                    dec=dec.substr(0,found2);
                }
                else
                {
                    networkflag=0;
                }
            }

            Handle[0]=lockcode1;
            if (USBlock==1)
            {
                Handle[0].append("---v2---");
            }
            Handle[0].append("%%%");
            buff;
            sprintf_s(buff,"%d", remote);
            Handle[0].append(buff);
            WORD upw=0;
            WORD upw1=0;

            divlic[0]=dec;
            lic= LICENSE(&divlic[0]);
            dec=lic;
            check =divlic[0];
            dec.append(check);
            int ver=strtol(divlic[3].c_str(),NULL,10);
            AppIdnew=AppId[0].c_str();

            if (divlic[2]==AppIdnew && version==ver && divlic[1] ==STRING_FIVE_DEFINE_NAME && networkflag==0)
            {


                long date= strtol(divlic[6].c_str(),NULL,10);
                long  month= date%100;
                WORD enddate=(WORD)date;
                WORD endmonth=(WORD)month;
                date=date/100;
                long endyear= strtol(divlic[7].c_str(),NULL,10);
                if (date==0 || month==0)
                {
                    long date1= strtol(divlic[9].c_str(),NULL,10);


                    ///////////////
                    if (date1 ==3)
                    {

                        HardDriveSerialNumbers1="";
                        HardDriveSerialNumbers1 = getHardDriveComputerID ();

                        harddisk =  DISKID(check,&mac[0]);

                        checkmac=GetMACaddress();
                        int cmac=0;

                        int found1 = checkmac.find("--");
                        if (found1!=std::string::npos)
                        {

                            checkmac.append("--");

                            int i=0;
                            while (checkmac!="")
                            {

                                if (found1!=std::string::npos)
                                {
                                    if(i<5)
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");

                                        if(mac[i]!="")
                                        {
                                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                                            {
                                                cmac=1;
                                            }
                                        }
                                        i++;
                                    }
                                    else
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");
                                    }
                                }
                                else
                                {
                                    disk = checkmac.substr(0,found1);
                                    checkmac = checkmac.substr(found1+2);
                                    found1 = checkmac.find("--");
                                }
                            }

                        }
                        if(USBlock==2)
                        {
                            ID = USBID;
                            int ret =  AG_CheckUSB (&ID);
                            if (HardDriveSerialNumbers1==harddisk && cmac==1 && ID==USBID)
                            {
                                delete[] memblock;
                                return 0;
                            }
                        }
                        else
                        {
                            if (HardDriveSerialNumbers1==harddisk && cmac==1)
                            {
                                delete[] memblock;
                                return 0;
                            }
                        }


                    }
/////////////////////////////////////////////
                    else if (date1 ==1 && usballow==1)
                    {

                        harddisk =  DISKID(check,&mac[0]);
                        ID=harddisk;
                        int ret =  AG_CheckUSB (&ID);
                        if (harddisk==ID)
                        {
                            delete[] memblock;
                            return 0;
                        }
                    }

                }

                else
                {
                    long date1= strtol(divlic[9].c_str(),NULL,10);

                    ////////////////////////////////////////////////////////////
                    if (date1 ==3)
                    {

                        HardDriveSerialNumbers1="";
                        HardDriveSerialNumbers1 = getHardDriveComputerID ();
//	std::string mac[5]={""};
                        harddisk =  DISKID(check,&mac[0]);
                        checkmac ="";
                        checkmac = GetMACaddress();
                        int cmac=0;
                        checkmac.append("--");
                        int found1 = checkmac.find("--");

                        if (found1!=std::string::npos)
                        {

                            int i=0;
                            while (checkmac!="")
                            {

                                if (found1!=std::string::npos)
                                {
                                    if(i<5)
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");

                                        if(mac[i]!="")
                                        {
                                            if(mac[0]==disk || mac[1]==disk || mac[2]==disk || mac[3]==disk || mac[4]==disk)
                                            {
                                                cmac=1;
                                            }
                                        }
                                        i++;
                                    }
                                    else
                                    {
                                        disk = checkmac.substr(0,found1);
                                        checkmac = checkmac.substr(found1+2);
                                        found1 = checkmac.find("--");
                                    }
                                }
                                else
                                {
                                    disk = checkmac.substr(0,found1);
                                    checkmac = checkmac.substr(found1+2);
                                    found1 = checkmac.find("--");
                                }
                            }

                        }
                        if(USBlock==2)
                        {
                            ID = USBID;
                            int ret =  AG_CheckUSB (&ID);
                            if ( ID==USBID)
                            {
                                delete[] memblock;
                                USBlock==0;
                            }
                        }
                        if (HardDriveSerialNumbers1==harddisk && cmac==1 && USBlock==0 )
                        {
                            WORD  value=0;

                            WORD date=0;
                            WORD year=0;
                            getdate_year( &date,  &year);

                            int r = ReadSectors(0,56,1,&value);


                            if(value==year)
                            {

                                WriteSectors(0,56,1,year);

                                r = ReadSectors(0,55,1,&value);
                                if((value%100)<=(date%100))
                                {
                                    if ((value%100)!=(date%100))
                                    {
                                        WriteSectors(0,55,1,date);
                                        ///////
                                        if(endyear==year)
                                        {
                                            if(endmonth>=(date%100))
                                            {
                                                if (endmonth!=(date%100))
                                                {
                                                    delete[] memblock;
                                                    return 0;
                                                }
                                                else if(endmonth==(date%100) )
                                                {
                                                    if(enddate>=(date/100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }
                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }

                                        }
                                        else if(endyear>year)
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }

                                        else
                                        {
                                            Handle[0]="";
                                            status=4;
                                        }

                                    }
                                    else if((value%100)==(date%100) )
                                    {
                                        if((value/100)<=(date/100))
                                        {
                                            WriteSectors(0,55,1,date);
                                            ///////

                                            if(endyear==year)
                                            {
                                                if(endmonth>=(date%100))
                                                {
                                                    if (endmonth!=(date%100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else if(endmonth==(date%100) )
                                                    {
                                                        if(enddate>=(date/100))
                                                        {
                                                            delete[] memblock;
                                                            return 0;
                                                        }
                                                        else
                                                        {
                                                            Handle[0]="";
                                                            status= 4;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }

                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else if(endyear>year)
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }

                                            else
                                            {
                                                Handle[0]="";
                                                status=4;
                                            }

                                        }
                                        else
                                        {
                                            delete[] memblock;
                                            Handle[0]="";
                                            return 3;
                                        }
                                    }

                                }
                                else
                                {

                                    delete[] memblock;
                                    Handle[0]="";
                                    return 3;
                                }

                            }

                            else if(value<year)
                            {
                                WriteSectors(0,56,1,year);
                                WriteSectors(0,55,1,date);
                                ////////

                                if(endyear==year)
                                {
                                    if(endmonth<=(date%100))
                                    {
                                        if (endmonth!=(date%100))
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }
                                        else if(endmonth==(date%100) )
                                        {
                                            if(enddate<=(date/100))
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }
                                        }
                                        else
                                        {
                                            Handle[0]="";
                                            status= 4;
                                        }

                                    }
                                    else
                                    {
                                        Handle[0]="";
                                        status= 4;
                                    }

                                }
                                else if(endyear<year)
                                {
                                    delete[] memblock;
                                    return 0;
                                }

                                else
                                {
                                    Handle[0]="";
                                    status=4;
                                }

                            }

                            else
                            {
                                delete[] memblock;
                                Handle[0]="";
                                return 3;
                            }


                        }


                    }

                    ////////////////////////////////////////////////////////

                    else if (date1 ==1 && usballow==1)
                    {
///////////////////////////////////////
                        harddisk =  DISKID(check,&mac[0]);
                        ID=harddisk;
                        int ret =  AG_CheckUSB (&ID);


                        if (harddisk==ID)
                        {
                            WORD  value=0;

                            WORD date=0;
                            WORD year=0;
                            getdate_year( &date,  &year);

                            int r = ReadSectors(0,56,1,&value);


                            if(value==year)
                            {

                                WriteSectors(0,56,1,year);

                                r = ReadSectors(0,55,1,&value);
                                if((value%100)<=(date%100))
                                {
                                    if ((value%100)!=(date%100))
                                    {
                                        WriteSectors(0,55,1,date);
                                        ///////
                                        if(endyear==year)
                                        {
                                            if(endmonth>=(date%100))
                                            {
                                                if (endmonth!=(date%100))
                                                {
                                                    delete[] memblock;
                                                    return 0;
                                                }
                                                else if(endmonth==(date%100) )
                                                {
                                                    if(enddate>=(date/100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }
                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }

                                        }
                                        else if(endyear>year)
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }

                                        else
                                        {
                                            Handle[0]="";
                                            status=4;
                                        }

                                    }
                                    else if((value%100)==(date%100) )
                                    {
                                        if((value/100)<=(date/100))
                                        {
                                            WriteSectors(0,55,1,date);
                                            ///////

                                            if(endyear==year)
                                            {
                                                if(endmonth>=(date%100))
                                                {
                                                    if (endmonth!=(date%100))
                                                    {
                                                        delete[] memblock;
                                                        return 0;
                                                    }
                                                    else if(endmonth==(date%100) )
                                                    {
                                                        if(enddate>=(date/100))
                                                        {
                                                            delete[] memblock;
                                                            return 0;
                                                        }
                                                        else
                                                        {
                                                            Handle[0]="";
                                                            status= 4;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Handle[0]="";
                                                        status= 4;
                                                    }

                                                }
                                                else
                                                {
                                                    Handle[0]="";
                                                    status= 4;
                                                }

                                            }
                                            else if(endyear>year)
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }

                                            else
                                            {
                                                Handle[0]="";
                                                status=4;
                                            }

                                        }
                                        else
                                        {
                                            delete[] memblock;
                                            Handle[0]="";
                                            return 3;
                                        }
                                    }

                                }
                                else
                                {

                                    delete[] memblock;
                                    Handle[0]="";
                                    return 3;
                                }

                            }

                            else if(value<year)
                            {
                                WriteSectors(0,56,1,year);
                                WriteSectors(0,55,1,date);
                                ////////

                                if(endyear==year)
                                {
                                    if(endmonth<=(date%100))
                                    {
                                        if (endmonth!=(date%100))
                                        {
                                            delete[] memblock;
                                            return 0;
                                        }
                                        else if(endmonth==(date%100) )
                                        {
                                            if(enddate<=(date/100))
                                            {
                                                delete[] memblock;
                                                return 0;
                                            }
                                            else
                                            {
                                                Handle[0]="";
                                                status= 4;
                                            }
                                        }
                                        else
                                        {
                                            Handle[0]="";
                                            status= 4;
                                        }

                                    }
                                    else
                                    {
                                        Handle[0]="";
                                        status= 4;
                                    }

                                }
                                else if(endyear<year)
                                {
                                    delete[] memblock;
                                    return 0;
                                }

                                else
                                {
                                    Handle[0]="";
                                    status=4;
                                }

                            }

                            else
                            {
                                delete[] memblock;
                                Handle[0]="";
                                return 3;
                            }


                        }


                        /////////////////////////////////////////


                    }

                }

            }
            lockcode = lockcode.substr(found+3);
        }
        else
        {
            // delete[] memblock;
            lockcode=="";
        }

    }
    delete[] memblock;
    Handle[0]="";
    return status;
}


extern "C" EXPORT int __cdecl  AG_Encrypt(std::string in[1], std::string out[1])
{

    WaitForSingleObject(ench,500);

    temp= aes_encrypt(in[0], STRING_ONE_DEFINE_NAME);
    out[0]=temp;
    ReleaseMutex(ench);
    return 0;
}

extern "C" EXPORT int __cdecl  AG_Decrypt(std::string in[1],std::string out[1])
{
    WaitForSingleObject(ench,500);
    temp= aes_decrypt(in[0], STRING_ONE_DEFINE_NAME);
    out[0]=temp;
    ReleaseMutex(ench);
    return 0;
}

VOID HookThunkInit(SHookThunk & ht,DWORD dwAddr)
{
    ht.jmp = 0xe9;
    ht.addr = dwAddr;
    ht.ret = 0xc3;
}


BOOL HookAPIFunction(LPCSTR lpModuleName,LPCSTR lpFunctionName,LPCVOID lpNewFunction,SHookThunk & htOldFunctionCode)
{
    WaitForSingleObject(hookh,500);
    //Get the library handle if loaded into this process.
    hLib= GetModuleHandle(lpModuleName);
    if(!hLib)
    {
        ReleaseMutex(hookh);
        return FALSE;
    }
    //Get the function address if exported from module.
    dwAddr =  (DWORD)GetProcAddress((HMODULE)hLib,lpFunctionName);
    if(!dwAddr)
    {
        ReleaseMutex(hookh);
        return FALSE;
    }

    HookThunkInit(ht,((DWORD)lpNewFunction - dwAddr - 5));

    //Backup old code
    if(!ReadProcessMemory(GetCurrentProcess(), (LPVOID)dwAddr, &htOldFunctionCode, sizeof(SHookThunk), NULL))
    {
        ReleaseMutex(hookh);
        return FALSE;
    }
    //Write  new code
    if(WriteProcessMemory(GetCurrentProcess(), (LPVOID)dwAddr, &ht, sizeof(SHookThunk), NULL))
    {
        ReleaseMutex(hookh);
        return TRUE;
    }
    ReleaseMutex(hookh);
    return FALSE;;
}
BOOL UnhookAPIFunction(LPCSTR lpModuleName,LPCSTR lpFunctionName, SHookThunk & htOldFunctionCode)
{
    WaitForSingleObject(unhookh,500);
    //Get the library handle if loaded into this process.
    hLib= GetModuleHandle(lpModuleName);
    if(!hLib)
    {
        ReleaseMutex(unhookh);
        return FALSE;
    }
    //Get the function address if exported from module.
    dwAddr =  (DWORD)GetProcAddress((HMODULE)hLib,lpFunctionName);
    if(!dwAddr)
    {
        ReleaseMutex(unhookh);
        return FALSE;
    }
    // Restore original code of function.
    if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)dwAddr, &htOldFunctionCode,sizeof(SHookThunk), NULL))
    {
        ReleaseMutex(unhookh);
        return TRUE;
    }
    ReleaseMutex(unhookh);
    return FALSE;
}

char * GetFileNameFromHandle(HANDLE hFile, int flgs) ;

SHookThunk g_createfilea;

HANDLE WINAPI crtfilea(
    LPCSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
);


SHookThunk g_writfile;

int WINAPI writfile(
    HANDLE hFile,
    unsigned char * lpBuffer,
    DWORD nNumberOfBytesToWrite,
    LPDWORD lpNumberOfBytesWritten,
    LPOVERLAPPED lpOverlapped
)
{



    WaitForSingleObject(writfileh,500);
    WaitForSingleObject(datafilesh,500);
    int writefilechek=0;
    while(writefilechek==0)
    {
        writefilechek = UnhookAPIFunction("KERNEL32.dll","WriteFile",g_writfile);
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);



    int statusnew=2;
    encfile3="";
    encfile3.resize(MAX_PATH+1);
    encfile3= GetFileNameFromHandle( hFile,0 );
    std::string datafilestemp="";

    WaitForSingleObject(datafilesh,500);

    datafilestemp.resize(datafiles8.size());

    datafiles4.resize(datafiles8.size());

    datafiles4=datafiles8;
    ReleaseMutex(datafilesh);
    datafiles4.append(";");
    cdata=0;

    while(datafiles4!="")
    {
        founddata = datafiles4.find(";");
        if(founddata!=std::string::npos )
        {
            datafilestemp= datafiles4.substr(0,founddata);
            if ( encfile3.find(".lock")==std::string::npos && encfile3.find(datafilestemp.c_str())!=std::string::npos && datafilestemp !="" && datacheck==0)
            {
                cdata=1;
                datafiles4 ="";
            }
            else
            {
                datafiles4 =datafiles4.substr(founddata+1);
            }
        }
        else
        {
            datafiles4="";
        }

    }

    if (cdata==1)
    {

        encfile3 = encfile3.substr(encfile3.find_last_of("\\"));
        encfile3 = encfile3.substr(0, encfile3.find(".bastion.temp"));


        if(encfile5.find(encfile3)!=std::string::npos)
        {
            hFileMap5=hFileMap12;
            encfile6 = encfile5.substr(0,encfile5.find_last_of("\\"));
            encfile6.append(encfile3);
        }
        else
        {
            hFileMap5=hFileMap13;
            encfile6 = encfile6.substr(0,encfile6.find_last_of("\\"));
            encfile6.append(encfile3);
        }

        unsigned char* lpBuffertemp = (LPBYTE)malloc(64000000);
        statusnew = WriteFile(
                        hFile,
                        lpBuffer,
                        nNumberOfBytesToWrite,
                        lpNumberOfBytesWritten,
                        lpOverlapped
                    );
        DWORD currentpos = SetFilePointer(hFile,0,NULL,FILE_CURRENT);
        DWORD sizeoffile = GetFileSize(hFile, NULL);







        writefilechek=0;
        DWORD  dwNumberOfBytesRead;
        size_t textLength;
        if(sizeoffile%16==0)
        {
            textLength = sizeoffile;
        }
        else
        {
            textLength = ((sizeoffile / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

        }
        if(textLength<=64000000)
        {
            SetFilePointer(hFile,0,NULL,FILE_BEGIN);
            ReadFile(hFile, lpBuffertemp, textLength, &dwNumberOfBytesRead, NULL);


            lpBuffertemp=aes_encrypt_code1((unsigned char *)lpBuffertemp, STRING_ONE_DEFINE_NAME+enckey1,textLength,lpBuffertemp);

            SetFilePointer(hFileMap5,0,NULL,FILE_BEGIN);
            // Write the original headers
            DWORD dwNumberOfBytesWritten;
            WriteFile(
                hFileMap5,
                lpBuffertemp,
                textLength,
                lpNumberOfBytesWritten,
                NULL
            );

        }
        else
        {

            SetFilePointer(hFile,0,NULL,FILE_BEGIN);
            ReadFile(hFile, lpBuffertemp, 64000000, &dwNumberOfBytesRead, NULL);


            lpBuffertemp=aes_encrypt_code1((unsigned char *)lpBuffertemp, STRING_ONE_DEFINE_NAME+enckey1,64000000,lpBuffertemp);

            SetFilePointer(hFileMap5,0,NULL,FILE_BEGIN);
            // Write the original headers
            DWORD dwNumberOfBytesWritten;
            WriteFile(
                hFileMap5,
                lpBuffertemp,
                64000000,
                lpNumberOfBytesWritten,
                NULL
            );

            for(int i=64000000; i<textLength; i=i+64000000)
            {
                SetFilePointer(hFile,i,NULL,FILE_BEGIN);
                if(textLength-i<=64000000)
                {
                    ReadFile(hFile, lpBuffertemp, textLength-i, &dwNumberOfBytesRead, NULL);


                    lpBuffertemp=aes_encrypt_code1((unsigned char *)lpBuffertemp, STRING_ONE_DEFINE_NAME+enckey1,textLength-i,lpBuffertemp);

                    SetFilePointer(hFileMap5,i,NULL,FILE_BEGIN);
                    // Write the original headers
                    DWORD dwNumberOfBytesWritten;
                    WriteFile(
                        hFileMap5,
                        lpBuffertemp,
                        textLength-i,
                        lpNumberOfBytesWritten,
                        NULL
                    );
                }
                else
                {
                    ReadFile(hFile, lpBuffertemp, 64000000, &dwNumberOfBytesRead, NULL);

                    lpBuffertemp=aes_encrypt_code1((unsigned char *)lpBuffertemp, STRING_ONE_DEFINE_NAME+enckey1,64000000,lpBuffertemp);

                    SetFilePointer(hFileMap5,i,NULL,FILE_BEGIN);
                    // Write the original headers
                    DWORD dwNumberOfBytesWritten;
                    WriteFile(
                        hFileMap5,
                        lpBuffertemp,
                        64000000,
                        lpNumberOfBytesWritten,
                        NULL
                    );
                }
            }

        }

        if(enckey1!="")
        {
            WaitForSingleObject(crtfileah,500);
            WaitForSingleObject(datafilesh,500);
            int writefilechek=0;
            while(writefilechek==0)
            {
                writefilechek = UnhookAPIFunction("KERNEL32.dll","CreateFileA",g_createfilea);
            }
            writefilechek=0;
            HANDLE hUpdatedFileenckey = CreateFile((encfile5+".cfg").c_str(), GENERIC_WRITE,  FILE_SHARE_READ| FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL);

            while(writefilechek==0)
            {
                writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileA",crtfilea,g_createfilea);//Hooking
            }
            writefilechek=0;
            ReleaseMutex(datafilesh);
            ReleaseMutex(crtfileah);
            size_t textLength1;
            if(enckey1.length()%16==0)
                textLength1 = enckey1.length();
            else
                textLength1 = ((enckey1.length() / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;
            char buffkey[1024];
            WriteFile(hUpdatedFileenckey, aes_encrypt_code1((unsigned char *)enckey1.c_str(), STRING_ONE_DEFINE_NAME, textLength1,(unsigned char *)buffkey), textLength1, &dwNumberOfBytesRead, NULL);
            CloseHandle(hUpdatedFileenckey);
        }

        *lpNumberOfBytesWritten= nNumberOfBytesToWrite;
        SetFilePointer(hFile,currentpos,NULL,FILE_BEGIN);

        free(lpBuffertemp);
    }
    else
    {

        if(datacheck==0 && encfile2.find(":\\")!=std::string::npos )// && encfile3.find("\\")!=std::string::npos)
        {

            DWORD currentpos = SetFilePointer(hFile,0,NULL,FILE_CURRENT);
            statusnew = WriteFile(
                            hFile,
                            lpBuffer,
                            nNumberOfBytesToWrite,
                            lpNumberOfBytesWritten,
                            lpOverlapped
                        );

            writefilechek=0;

            DWORD dwNumberOfBytesRead;
            unsigned char *lpBuffer2= new unsigned char[3200];

            DWORD sizeoffile = GetFileSize(hFile, NULL);
            if (sizeoffile>=3200 && SetFilePointer(hFile,0,NULL,FILE_BEGIN)!= INVALID_SET_FILE_POINTER)
            {
                ReadFile(hFile, lpBuffer2, 3200, &dwNumberOfBytesRead, NULL);
                lpBuffer2[3119]='\0';
            }
            if (SetFilePointer(hFile,currentpos,NULL,FILE_BEGIN)!= INVALID_SET_FILE_POINTER)
                statusnew = WriteFile(
                                hFile,
                                lpBuffer,
                                nNumberOfBytesToWrite,
                                lpNumberOfBytesWritten,
                                lpOverlapped
                            );

            encfile="";
            encfile = (char *)lpBuffer2;

            if(encfile.find(encfile4)!=std::string::npos && encfile!="" && encfile4!="" && encfile3.find(datafilestemp.c_str())==std::string::npos)
            {

                size_t textLength;
                if(sizeoffile%16==0)
                {
                    textLength = sizeoffile;
                }
                else
                {
                    textLength = ((sizeoffile / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;

                }

                lpBuffer2=aes_encrypt_code1((unsigned char *)lpBuffer, STRING_ONE_DEFINE_NAME+enckey1,16,lpBuffer2);

                // Write the original headers
                DWORD dwNumberOfBytesWritten;
                statusnew = WriteFile(
                                hFile,
                                lpBuffer2,
                                16,
                                lpNumberOfBytesWritten,
                                NULL
                            );

                SetFilePointer(hFile,currentpos,NULL,FILE_BEGIN);
                *lpNumberOfBytesWritten= nNumberOfBytesToWrite;
                free(lpBuffer2);

            }

        }
        else
        {
            statusnew = WriteFile(
                            hFile,
                            lpBuffer,
                            nNumberOfBytesToWrite,
                            lpNumberOfBytesWritten,
                            lpOverlapped
                        );
        }
    }
    WaitForSingleObject(datafilesh,500);

    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","WriteFile",writfile,g_writfile);//Hooking
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    ReleaseMutex(writfileh);

    return statusnew;


}


DWORD WINAPI threadfunctioncreate(LPVOID param)
{
    WaitForSingleObject(threadh,INFINITE);
    unsigned char* lpBuffer = new unsigned char[64000000];
    DWORD dwNumberOfBytesRead;
    datacheck=1;
    WaitForSingleObject(createdatafilesh,INFINITE);

    ReleaseMutex(createdatafilesh);
    WaitForSingleObject(writfileh,500);
    WaitForSingleObject(datafilesh,500);
    int writefilechek=0;
    while(writefilechek==0)
    {
        writefilechek =  UnhookAPIFunction("KERNEL32.dll","WriteFile",g_writfile);
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    for(long int i=64000000; i<dwFileSizenew; i=i+64000000)
    {
        if(dwFileSizenew==0 ||hFileMap3== INVALID_HANDLE_VALUE)
        {
            if(hFileMap9!=INVALID_HANDLE_VALUE)
                CloseHandle(hFileMap9);
            if(hFileMap3!=INVALID_HANDLE_VALUE)
                CloseHandle(hFileMap3);
            datacheck=0;
            delete lpBuffer;
            WaitForSingleObject(datafilesh,500);
            while(writefilechek==0)
            {
                writefilechek = HookAPIFunction("KERNEL32.dll","WriteFile",writfile,g_writfile);//Hooking
            }
            writefilechek=0;
            ReleaseMutex(datafilesh);
            ReleaseMutex(writfileh);
            ReleaseMutex(threadh);

            return 0;
        }
        if(dwFileSizenew-i<=64000000 && dwFileSizenew-i>0)
        {
            SetFilePointer(hFileMap9,i,NULL,FILE_BEGIN);
            ReadFile(hFileMap9, lpBuffer, dwFileSizenew-i, &dwNumberOfBytesRead, NULL);
            DWORD dwNumberOfBytesWritten;
            lpBuffer=aes_decrypt_code1(lpBuffer, STRING_ONE_DEFINE_NAME+enckey2,dwFileSizenew-i,lpBuffer);

            SetFilePointer(hFileMap3,i,NULL,FILE_BEGIN);

            WriteFile(hFileMap3, lpBuffer, dwFileSizenew-i, &dwNumberOfBytesWritten, NULL);
        }
        else if(dwFileSizenew-i>0)
        {
            SetFilePointer(hFileMap9,i,NULL,FILE_BEGIN);
            ReadFile(hFileMap9, lpBuffer, 64000000, &dwNumberOfBytesRead, NULL);
            DWORD dwNumberOfBytesWritten;
            lpBuffer=aes_decrypt_code1(lpBuffer, STRING_ONE_DEFINE_NAME+enckey2,64000000,lpBuffer);

            SetFilePointer(hFileMap3,i,NULL,FILE_BEGIN);

            WriteFile(hFileMap3, lpBuffer, 64000000, &dwNumberOfBytesWritten, NULL);
        }

    }
    if(hFileMap9!=INVALID_HANDLE_VALUE)
        CloseHandle(hFileMap9);
    if(hFileMap3!=INVALID_HANDLE_VALUE)
        CloseHandle(hFileMap3);
    datacheck=0;
    delete lpBuffer;
    WaitForSingleObject(datafilesh,500);
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","WriteFile",writfile,g_writfile);//Hooking
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    ReleaseMutex(writfileh);
    ReleaseMutex(threadh);
    return 0;
}

HANDLE WINAPI crtfilea(
    LPCSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
)
{

    WaitForSingleObject(crtfileah,500);
    WaitForSingleObject(datafilesh,500);
    int writefilechek=0;
    while(writefilechek==0)
    {
        writefilechek = UnhookAPIFunction("KERNEL32.dll","CreateFileA",g_createfilea);
    }
    writefilechek=0;

    ReleaseMutex(datafilesh);

    encfile2="";
    encfile2.resize(MAX_PATH+1);
    encfile2=lpFileName;

    Handlenew="";
    Handlenew.resize(1024);
    datafiles3="";

    std::string datafilestemp="";
    if(datafiles8=="" || IDname=="")
    {


        datafiles6="";
        hUpdatedFile4=INVALID_HANDLE_VALUE;
        hUpdatedFile4 = CreateFileA("Bastion.cfg", GENERIC_READ,FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
        if(hUpdatedFile4==INVALID_HANDLE_VALUE)
        {
            hFileMap8=INVALID_HANDLE_VALUE;

            hFileMap8=  CreateFileA(
                            lpFileName,
                            dwDesiredAccess ,
                            dwShareMode, lpSecurityAttributes,
                            dwCreationDisposition,
                            dwFlagsAndAttributes, hTemplateFile
                        );
            WaitForSingleObject(datafilesh,500);
            while(writefilechek==0)
            {
                writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileA",crtfilea,g_createfilea);//Hooking
            }
            writefilechek=0;
            ReleaseMutex(datafilesh);
            ReleaseMutex(crtfileah);


            return hFileMap8;
        }
        dwFileSize1 = GetFileSize(hUpdatedFile4, NULL);
        if(dwFileSize1==0 || dwFileSize1== INVALID_FILE_SIZE || encfile2.find(":\\")==std::string::npos)
        {
            if(hUpdatedFile4!=INVALID_HANDLE_VALUE)
                CloseHandle(hUpdatedFile4);
            hFileMap8=INVALID_HANDLE_VALUE;
            hFileMap8=  CreateFileA(
                            lpFileName,
                            dwDesiredAccess ,
                            dwShareMode, lpSecurityAttributes,
                            dwCreationDisposition,
                            dwFlagsAndAttributes, hTemplateFile
                        );
            WaitForSingleObject(datafilesh,500);
            while(writefilechek==0)
            {
                writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileA",crtfilea,g_createfilea);//Hooking
            }
            writefilechek=0;
            ReleaseMutex(datafilesh);
            ReleaseMutex(crtfileah);

            return hFileMap8;
        }
        char* lpuffer = (char *)malloc(dwFileSize1);
        std::string datafiles="";
        std::string datafilest="";

        datafiles6="";

        ReadFile(hUpdatedFile4, lpuffer, dwFileSize1, &nofbytes, NULL);


        datafiles.resize(dwFileSize1);
        datafilestemp.resize(dwFileSize1);
        datafiles=lpuffer;
        datafiles6.resize(dwFileSize1);
        AG_Decrypt(&datafiles, &datafiles6);
        size_t found= datafiles6.find("%%%");
        if(found!=std::string::npos)
        {
            datafilest = datafiles6.substr(0,found);
            datafiles6= datafiles6.substr(found+3);
            found= datafiles6.find("%%%");
            if(found!=std::string::npos)
            {
                IDname=  datafiles6.substr(0,found);
                datafiles6= datafiles6.substr(found+3);
                found= datafiles6.find("%%%");
                if(found!=std::string::npos)
                {
                    ver=  datafiles6.substr(0,found);
                    enckey1= datafiles6.substr(found+3);
                }

            }
        }

        WaitForSingleObject(datafilesh,500);





        datafiles8.resize(datafilest.size());
        datafiles8 = datafilest;
        datafiles6 = datafiles8;
        datafiles7 = IDname;
        datafiles9 =   ver;
        ReleaseMutex(datafilesh);

        free(lpuffer);
        if(hUpdatedFile4!=INVALID_HANDLE_VALUE)
            CloseHandle(hUpdatedFile4);
    }
    else
    {
        WaitForSingleObject(datafilesh,500);
        datafiles6 = datafiles8;

        IDname = datafiles7 ;
        ver = datafiles9 ;
        ReleaseMutex(datafilesh);

    }

    datafiles6.append(";");
    cdata=0;
    while(datafiles6!="")
    {
        founddata = datafiles6.find(";");
        if(founddata!=std::string::npos )
        {
            datafilestemp= datafiles6.substr(0,founddata);
            if (encfile2.find(".lock")==std::string::npos && encfile2.find(datafilestemp.c_str())!=std::string::npos && datafilestemp !="" && writefilechek==0)
            {
                cdata=1;
                datafiles6  ="";
            }
            else
            {
                datafiles6 =datafiles6.substr(founddata+1);
            }
        }
        else
        {
            datafiles6="";
        }

    }
    if (cdata==1 )
    {
        if(enckey1!="")
        {
            HANDLE hUpdatedFileenckey = CreateFile((encfile2+".cfg").c_str(), GENERIC_READ,  FILE_SHARE_READ| FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);
            if(hUpdatedFileenckey!=INVALID_HANDLE_VALUE)
            {
                SetFilePointer(hUpdatedFileenckey,0,NULL,FILE_BEGIN);
                unsigned char buffkey[1024];
                unsigned char* buffkey1 = new unsigned char[1024];
                DWORD textLength = GetFileSize(hUpdatedFileenckey, NULL);
                DWORD dwNumberOfBytesRead=0;
                ReadFile(hUpdatedFileenckey, buffkey, textLength, &dwNumberOfBytesRead, NULL);

                buffkey1 = aes_decrypt_code1(buffkey, STRING_ONE_DEFINE_NAME, textLength,buffkey1);
                if(strtol(enckey1.c_str(),NULL,10)>=strtol((char *)buffkey1,NULL,10))
                    enckey2=(char *)buffkey1;
                delete buffkey1;
                CloseHandle(hUpdatedFileenckey);
            }
        }
        if(hFileMap13!=INVALID_HANDLE_VALUE)
        {
            CloseHandle(hFileMap2);
            CloseHandle(hFileMap13);
        }

        dwFileSizenew=0;


        WaitForSingleObject(threadh,INFINITE);
        ReleaseMutex(threadh);
        if(hFileMap3!=INVALID_HANDLE_VALUE)
            CloseHandle(hFileMap3);
        hFileMap3=INVALID_HANDLE_VALUE;
        CloseHandle(hndl1);
        WaitForSingleObject(createdatafilesh,INFINITE);
        if(hFileMap10!=INVALID_HANDLE_VALUE)
        {
            CloseHandle(hFileMap10);

        }
        WaitForSingleObject(writfileh,500);
        WaitForSingleObject(datafilesh,500);
        while(writefilechek==0)
        {
            writefilechek = UnhookAPIFunction("KERNEL32.dll","WriteFile",g_writfile);
        }
        writefilechek=0;
        ReleaseMutex(datafilesh);
        if(servernamenew=="")
        {
            if(encfile2.find(":\\")!=std::string::npos)
            {
                int version =0;
                if(ver!="")
                {
                    version = atoi(ver.c_str());
                }
                int statusnew=0;
                Handlenew="";
                AG_AllowUSB (&Handlenew,1);
                statusnew= AG_IsAuthentic1 (&Handlenew,&IDname, version,0);

                if(statusnew!=0)
                {
                    HWND hndl = GetActiveWindow();
                    returnval=  AG_Error(statusnew);
                    MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
                    exit(0);
                }

            }
        }
//	///datacheck=0;

        hFileMap2= CreateFileA(
                       lpFileName,
                       GENERIC_READ | GENERIC_WRITE ,          // open for writing
                       FILE_SHARE_READ | FILE_SHARE_WRITE,                      // do not share
                       NULL,                   // default security
                       dwCreationDisposition,             // create new file only
                       FILE_ATTRIBUTE_NORMAL,  // normal file
                       NULL);
        if (hFileMap2==INVALID_HANDLE_VALUE)
        {
            hFileMap8=INVALID_HANDLE_VALUE;
        }
        else
        {
            DWORD dwFileSize = GetFileSize(hFileMap2, NULL);



            unsigned char* lpBuffer = new unsigned char[64000000];

            DWORD dwNumberOfBytesRead = 0;

            unsigned char *lpBuffer2= new unsigned char [64000000];


            std::string encfilename = lpFileName;
            std::string encfilenamenew= "C:/Users/Public/Application Data/Bastion/";
            size_t found=  encfilename.find_last_of("\\");
            if(encfile2.find("\\Project\\Backup")==std::string::npos)
            {
                encfile5= lpFileName;
                hFileMap12= CreateFileA(
                                lpFileName,
                                GENERIC_READ | GENERIC_WRITE ,          // open for writing
                                FILE_SHARE_READ | FILE_SHARE_WRITE,                      // do not share
                                NULL,                   // default security
                                dwCreationDisposition,             // create new file only
                                FILE_ATTRIBUTE_NORMAL,  // normal file
                                NULL);
            }
            encfilename=encfilename.substr(found);
            encfilename.append(".bastion.temp");
            encfilenamenew.append(encfilename);
            hFileMap6=INVALID_HANDLE_VALUE;
            hFileMap6= CreateFileA(
                           encfilenamenew.c_str(),
                           GENERIC_READ | GENERIC_WRITE ,          // open for writing
                           FILE_SHARE_READ | FILE_SHARE_WRITE |FILE_SHARE_DELETE,                      // do not share
                           NULL,                   // default security
                           CREATE_NEW,             // create new file only
                           FILE_FLAG_DELETE_ON_CLOSE |FILE_ATTRIBUTE_HIDDEN |FILE_ATTRIBUTE_SYSTEM,  // normal file
                           NULL);
            if(dwFileSize>16)
            {
                if(dwFileSize>=3200)
                {
                    encfile4="";
                    SetFilePointer(hFileMap2,0,NULL,FILE_BEGIN);
                    ReadFile(hFileMap2, lpBuffer, 3200, &dwNumberOfBytesRead, NULL);
                    lpBuffer2=aes_decrypt_code1(lpBuffer, STRING_ONE_DEFINE_NAME+enckey2,3200,lpBuffer2);
                    lpBuffer2[3119]='\0';
                    encfile4=( char *)lpBuffer2;
                }
            }
            if(dwFileSize<=15)
            {

                DWORD dwNumberOfBytesWritten;
                hFileMap8=INVALID_HANDLE_VALUE;
                WriteFile(hFileMap6, "NONENONENONENONE", 16, &dwNumberOfBytesWritten, NULL);
                hFileMap8=CreateFileA(
                              encfilenamenew.c_str(),
                              GENERIC_READ | GENERIC_WRITE ,          // open for writing
                              FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,                      // do not share
                              NULL,                   // default security
                              OPEN_EXISTING,             // create new file only
                              FILE_FLAG_DELETE_ON_CLOSE |FILE_ATTRIBUTE_HIDDEN |FILE_ATTRIBUTE_SYSTEM,  // normal file
                              NULL);



                SetFilePointer(hFileMap8,0,NULL,FILE_BEGIN);
                if(hFileMap6!=INVALID_HANDLE_VALUE)
                    CloseHandle(hFileMap6);
            }

            else if(dwFileSize<=64000000)
            {


                SetFilePointer(hFileMap2,0,NULL,FILE_BEGIN);
                ReadFile(hFileMap2, lpBuffer, dwFileSize, &dwNumberOfBytesRead, NULL);
                DWORD dwNumberOfBytesWritten;
                lpBuffer2=aes_decrypt_code1(lpBuffer, STRING_ONE_DEFINE_NAME+enckey2,dwFileSize,lpBuffer2);

                SetFilePointer(hFileMap6,0,NULL,FILE_BEGIN);
                WriteFile(hFileMap6, lpBuffer2, dwFileSize, &dwNumberOfBytesWritten, NULL);




                hFileMap8=INVALID_HANDLE_VALUE;
                hFileMap8=CreateFileA(
                              encfilenamenew.c_str(),
                              GENERIC_READ | GENERIC_WRITE ,          // open for writing
                              FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,                      // do not share
                              NULL,                   // default security
                              OPEN_EXISTING,             // create new file only
                              FILE_FLAG_DELETE_ON_CLOSE |FILE_ATTRIBUTE_HIDDEN |FILE_ATTRIBUTE_SYSTEM,  // normal file
                              NULL);

                if(hFileMap6!=INVALID_HANDLE_VALUE)
                    CloseHandle(hFileMap6);

            }
            else
            {
                dwFileSizenew= dwFileSize;
                encfile1=lpFileName;
                SetFilePointer(hFileMap2,0,NULL,FILE_BEGIN);
                ReadFile(hFileMap2, lpBuffer, 64000000, &dwNumberOfBytesRead, NULL);
                DWORD dwNumberOfBytesWritten;
                lpBuffer2=aes_decrypt_code1(lpBuffer, STRING_ONE_DEFINE_NAME+enckey2,64000000,lpBuffer2);
                SetFilePointer(hFileMap6,0,NULL,FILE_BEGIN);
                WriteFile(hFileMap6, lpBuffer2, 64000000, &dwNumberOfBytesWritten, NULL);



                hFileMap8=INVALID_HANDLE_VALUE;
                hFileMap8=CreateFileA(
                              encfilenamenew.c_str(),
                              GENERIC_READ | GENERIC_WRITE ,          // open for writing
                              FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,                      // do not share
                              NULL,                   // default security
                              OPEN_EXISTING,             // create new file only
                              FILE_FLAG_DELETE_ON_CLOSE |FILE_ATTRIBUTE_HIDDEN |FILE_ATTRIBUTE_SYSTEM,  // normal file
                              NULL);


                hFileMap3= CreateFileA(
                               encfilenamenew.c_str(),
                               GENERIC_READ | GENERIC_WRITE ,          // open for writing
                               FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,                      // do not share
                               NULL,                   // default security
                               OPEN_EXISTING,             // create new file only
                               FILE_FLAG_DELETE_ON_CLOSE |FILE_ATTRIBUTE_HIDDEN |FILE_ATTRIBUTE_SYSTEM,  // normal file
                               NULL);




                LPTHREAD_START_ROUTINE threadFunc= &threadfunctioncreate;

                LPVOID param=NULL;

                LPDWORD threadID= NULL;

                hndl1 = CreateThread(NULL,
                                     0,
                                     threadFunc,
                                     param,
                                     0,
                                     threadID);
                hFileMap9= CreateFileA(
                               encfile1.c_str(),
                               GENERIC_READ | GENERIC_WRITE ,          // open for writing
                               FILE_SHARE_READ | FILE_SHARE_WRITE,                      // do not share
                               NULL,                   // default security
                               OPEN_EXISTING,             // create new file only
                               FILE_ATTRIBUTE_NORMAL,  // normal file
                               NULL);


                if(hFileMap6!=INVALID_HANDLE_VALUE)
                    CloseHandle(hFileMap6);
                hFileMap10=hFileMap8;

            }

            if(hFileMap2!=INVALID_HANDLE_VALUE)
                CloseHandle(hFileMap2);


            delete lpBuffer;
            delete lpBuffer2;


        }

        hFileMap13=  CreateFileA(
                         lpFileName,
                         dwDesiredAccess ,
                         dwShareMode, lpSecurityAttributes,
                         dwCreationDisposition,
                         dwFlagsAndAttributes, hTemplateFile
                     );



        WaitForSingleObject(datafilesh,500);
        while(writefilechek==0)
        {
            writefilechek = HookAPIFunction("KERNEL32.dll","WriteFile",writfile,g_writfile);//Hooking
        }
        writefilechek=0;
        ReleaseMutex(datafilesh);

        ReleaseMutex(writfileh);
        ReleaseMutex(createdatafilesh);
    }
    else
    {
        hFileMap8=  CreateFileA(
                        lpFileName,
                        dwDesiredAccess ,
                        dwShareMode, lpSecurityAttributes,
                        dwCreationDisposition,
                        dwFlagsAndAttributes, hTemplateFile
                    );


    }
    WaitForSingleObject(datafilesh,500);
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileA",crtfilea,g_createfilea);//Hooking

    }
    writefilechek=0;

    ReleaseMutex(datafilesh);

    ReleaseMutex(crtfileah);

    return hFileMap8;
}

char * GetFileNameFromHandle(HANDLE hFile, int flgs)
{
    WaitForSingleObject(fnameh,500);
    bSuccess = FALSE;
    dwFileSizeHi = 0;
    dwFileSizeLo = GetFileSize(hFile, &dwFileSizeHi);
    if( dwFileSizeLo == 0 && dwFileSizeHi == 0 )
    {
        ReleaseMutex(fnameh);
        return "";
    }

    if( hFile== INVALID_HANDLE_VALUE )
    {
        ReleaseMutex(fnameh);
        return "";
    }
    hFileMap1= INVALID_HANDLE_VALUE;
    // Create a file mapping object.
    /*	if(flgs==0)
    	{
    		WaitForSingleObject(crtfileorigah,500);

    	UnhookAPIFunction("KERNEL32.dll","CreateFileMappingA",g_crtfileoriga);

       hFileMap1 = CreateFileMappingA(hFile,
                        NULL,
                        PAGE_READWRITE,
                        0,
                        1,
                        NULL);
     HookAPIFunction("KERNEL32.dll","CreateFileMappingA",crtfileoriga,g_crtfileoriga);//Hooking
    	ReleaseMutex(crtfileorigah);

    	}
    	if (flgs==1)
    	{ */

    hFileMap1 = CreateFileMappingA(hFile,
                                   NULL,
                                   PAGE_READONLY,
                                   0,
                                   1,
                                   NULL);
//	}
    if (hFileMap1!= INVALID_HANDLE_VALUE)
    {

        // Create a file mapping to get the file name.

        pMem2 = MapViewOfFile(hFileMap1, FILE_MAP_READ, 0, 0, 1);
        if (pMem2)
        {
            if (GetMappedFileName (GetCurrentProcess(),
                                   pMem2,
                                   pszFilename,
                                   MAX_PATH))
                bSuccess = TRUE;

            UnmapViewOfFile(pMem2);
        }
        else
        {
            ReleaseMutex(fnameh);
            return "";
        }

        if(hFileMap1!=INVALID_HANDLE_VALUE)
            CloseHandle(hFileMap1);
    }
    else
    {
        ReleaseMutex(fnameh);
        return "";
    }
    ReleaseMutex(fnameh);
    return(pszFilename);
}



SHookThunk g_writfileex;

int WINAPI writfileex(
    HANDLE hFile,
    unsigned char * lpBuffer,
    DWORD nNumberOfBytesToWrite,
    LPOVERLAPPED lpOverlapped,
    LPOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
)
{

    WaitForSingleObject(writfileh,500);
    WaitForSingleObject(datafilesh,500);
    int writefilechek=0;
    while(writefilechek==0)
    {
        writefilechek = UnhookAPIFunction("KERNEL32.dll","WriteFileEx",g_writfileex);
    }
    writefilechek=0;

    ReleaseMutex(datafilesh);

    WaitForSingleObject(crtfileah,500);
    WaitForSingleObject(datafilesh,500);
    while(writefilechek==0)
    {
        writefilechek = UnhookAPIFunction("KERNEL32.dll","CreateFileA",g_createfilea);
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);


    int statusnew=2;
    char *lpBuffer1= new char[MAX_PATH+1];

    lpBuffer1= GetFileNameFromHandle( hFile ,0);

    encfile3="";
    encfile3.resize(MAX_PATH+1);
    encfile3= (char *)lpBuffer1;
    std::string datafilestemp="";

    datafiles4 ="";
    WaitForSingleObject(datafilesh,500);
    datafilestemp.resize(datafiles8.size());

    datafiles4.resize(datafiles8.size());

    datafiles4=datafiles8;
    ReleaseMutex(datafilesh);
    datafiles4.append(";");
    cdata=0;
    while(datafiles4!="")
    {
        founddata = datafiles4.find(";");
        if(founddata!=std::string::npos )
        {
            datafilestemp= datafiles4.substr(0,founddata);
            if (encfile3.find(datafilestemp.c_str())!=std::string::npos && datafilestemp !="" && datacheck==0)
            {
                cdata=1;
                datafiles4 ="";
            }
            else
            {
                datafiles4 =datafiles4.substr(founddata+1);
            }
        }
        else
        {
            datafiles4="";
        }

    }
    if (cdata==1 )
    {
        unsigned char* lpBuffer2 = (LPBYTE)malloc(16);
        //MessageBox(NULL,(char *)lpBuffer,"DONE",0);
        size_t found = encfile3.find(".bastion.temp");

        if(found!=std::string::npos )
        {
            encfile3=encfile3.substr(0,found);

            found=  encfile2.find_last_of("\\");
            encfile2=encfile2.substr(0,found);

            found=  encfile3.find_last_of("\\");
            encfile3=encfile3.substr(found);

            encfile2.append(encfile3);
            if(hFile!=INVALID_HANDLE_VALUE)
                CloseHandle(hFile);
            hFileMap5=INVALID_HANDLE_VALUE;
            hFileMap5= CreateFileA(
                           encfile2.c_str(),
                           GENERIC_READ | GENERIC_WRITE ,          // open for writing
                           FILE_SHARE_READ | FILE_SHARE_WRITE,                      // do not share
                           NULL,                   // default security
                           OPEN_EXISTING,             // create new file only
                           FILE_ATTRIBUTE_NORMAL,  // normal file
                           NULL);
        }
        size_t textLength;
        if(nNumberOfBytesToWrite%16==0)
        {
            textLength = nNumberOfBytesToWrite;
        }
        else
        {
            textLength = ((nNumberOfBytesToWrite / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;
            for(int i=nNumberOfBytesToWrite; i<textLength; i++) {
                lpBuffer[i]=' ';
            }
        }
        for(int i=0; i<textLength; i=i+16)
        {

            SetFilePointer(hFileMap5,i,NULL,FILE_BEGIN);

            lpBuffer2=aes_encrypt_code1((unsigned char *)lpBuffer+i, STRING_ONE_DEFINE_NAME+enckey1,16,lpBuffer2);


            // Write the original headers
            DWORD dwNumberOfBytesWritten;
            SetFilePointer(hFileMap5,i,NULL,FILE_BEGIN);
            statusnew = WriteFileEx(
                            hFileMap5,
                            lpBuffer2,
                            16,
                            lpOverlapped,
                            lpCompletionRoutine
                        );

        }
        free(lpBuffer2);
    }

    else
    {

        if(datacheck==0)
        {
            if(hFile!=INVALID_HANDLE_VALUE)
                CloseHandle(hFile);
            hFileMap5=INVALID_HANDLE_VALUE;

            hFileMap5= CreateFileA(
                           encfile3.c_str(),
                           GENERIC_READ | GENERIC_WRITE ,          // open for writing
                           FILE_SHARE_READ | FILE_SHARE_WRITE,                      // do not share
                           NULL,                   // default security
                           OPEN_EXISTING,             // create new file only
                           FILE_ATTRIBUTE_NORMAL,  // normal file
                           NULL);

            writefilechek=0;

            DWORD dwNumberOfBytesRead;
            SetFilePointer(hFileMap5,0,NULL,FILE_BEGIN);
            ReadFile(hFileMap5, lpBuffer, 16, &dwNumberOfBytesRead, NULL);
            if(encfile4==( char *)lpBuffer)
            {
                unsigned char *lpBuffer2= new unsigned char[16];
                size_t textLength;
                if(nNumberOfBytesToWrite%16==0)
                {
                    textLength = nNumberOfBytesToWrite;
                }
                else
                {
                    textLength = ((nNumberOfBytesToWrite / AES_BLOCK_SIZE) + 1) * AES_BLOCK_SIZE;
                    for(int i=nNumberOfBytesToWrite; i<textLength; i++) {
                        lpBuffer[i]=' ';
                    }
                }
                for(int i=0; i<textLength; i=i+16)
                {

                    for(int i=0; i<16; i++) {
                        lpBuffer2[i]=' ';
                    }
                    SetFilePointer(hFileMap5,i,NULL,FILE_BEGIN);

                    lpBuffer2=aes_encrypt_code1((unsigned char *)lpBuffer+i, STRING_ONE_DEFINE_NAME+enckey1,16,lpBuffer2);


                    // Write the original headers
                    DWORD dwNumberOfBytesWritten;
                    SetFilePointer(hFileMap5,i,NULL,FILE_BEGIN);
                    statusnew = WriteFileEx(
                                    hFileMap5,
                                    lpBuffer2,
                                    16,
                                    lpOverlapped,
                                    lpCompletionRoutine
                                );

                }
                free(lpBuffer2);

            }
        }
        else
        {
            statusnew = WriteFileEx(
                            hFile,
                            lpBuffer,
                            nNumberOfBytesToWrite,
                            lpOverlapped,
                            lpCompletionRoutine
                        );
        }
    }

    WaitForSingleObject(datafilesh,500);
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileA",crtfilea,g_createfilea);//Hooking

    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    ReleaseMutex(crtfileah);
    WaitForSingleObject(datafilesh,500);
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","WriteFileEx",writfileex,g_writfileex);//Hooking

    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    ReleaseMutex(writfileh);

    return statusnew;
}



BOOL UnicodeToMByte(LPCWSTR unicodeStr, LPSTR multiByteStr, DWORD size)
{
    // Get the required size of the buffer that receives the multiByte string.
    DWORD minSize;
    minSize = WideCharToMultiByte(CP_OEMCP,NULL,unicodeStr,-1,NULL,0,NULL,FALSE);
    if(size < minSize)
    {
        return FALSE;
    }
    // Convert string from Unicode to multi-byte.
    WideCharToMultiByte(CP_OEMCP,NULL,unicodeStr,-1,multiByteStr,size,NULL,FALSE);
    return TRUE;
}


SHookThunk g_createfilew;


HANDLE WINAPI crtfilew(
    LPCWSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
)
{
    WaitForSingleObject(crtfilewh,500);
    WaitForSingleObject(datafilesh,500);
    int writefilechek=0;

    while(writefilechek==0)
    {
        writefilechek = UnhookAPIFunction("KERNEL32.dll","CreateFileW",g_createfilew);
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);

    char *lpbuffer1= new char [MAX_PATH+1];
    UnicodeToMByte(lpFileName,lpbuffer1, MAX_PATH);
    hFileMap4= CreateFileA(
                   lpbuffer1,
                   dwDesiredAccess,
                   dwShareMode, lpSecurityAttributes,
                   dwCreationDisposition,
                   dwFlagsAndAttributes, hTemplateFile
               );
    WaitForSingleObject(datafilesh,500);
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileW",crtfilew,g_createfilew);//Hooking

    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    ReleaseMutex(crtfilewh);
    return hFileMap4;

}





extern "C" EXPORT int __cdecl AG_APPDATA(std::string datafiles2[1],std::string inID[1],std::string ver[1], std::string enckey="")
{
    std::string datafiles="";
    datafiles1.resize(datafiles2[0].size()+inID[0].size() +sizeof(int)+6);
    datafiles.resize(datafiles2[0].size()+inID[0].size() +sizeof(int)+6);

    datafiles=datafiles2[0];
    datafiles.append("%%%");
    datafiles.append(inID[0].c_str());
    datafiles.append("%%%");
    datafiles.append(ver[0].c_str());
    datafiles.append("%%%");
    if(!AG_IsAuthentic1(&Handlenew,&enckey,0,0))
        datafiles.append(enckey.c_str());
    else
        datafiles.append("");
    crtfileah = CreateMutex(NULL,0,NULL);
    crtfilewh = CreateMutex(NULL,0,NULL);
    threadh = CreateMutex(NULL,0,NULL);
    fnameh = CreateMutex(NULL,0,NULL);

    writfileh = CreateMutex(NULL,0,NULL);
    hookh = CreateMutex(NULL,0,NULL);
    unhookh = CreateMutex(NULL,0,NULL);
    hUpdatedFile=INVALID_HANDLE_VALUE;
    ench= CreateMutex(NULL,0,NULL);
    createdatafilesh= CreateMutex(NULL,0,NULL);
    datafilesh= CreateMutex(NULL,0,NULL);
    hUpdatedFile = CreateFileA("Bastion.cfg", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, NULL, NULL);

    AG_Encrypt(&datafiles, &datafiles1);
    WriteFile(hUpdatedFile, datafiles1.c_str(), datafiles1.size(), &nofbytes, NULL);
    if(hUpdatedFile!=INVALID_HANDLE_VALUE)
        CloseHandle(hUpdatedFile);
    WaitForSingleObject(datafilesh,500);
    int writefilechek=0;
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","WriteFile",writfile,g_writfile);
    }
    writefilechek=0;

//HookAPIFunction("KERNEL32.dll","WriteFileEx",writfileex,g_writfileex);
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileA",crtfilea,g_createfilea);
    }
    writefilechek=0;
    while(writefilechek==0)
    {
        writefilechek = HookAPIFunction("KERNEL32.dll","CreateFileW",crtfilew,g_createfilew);//Hooking
    }
    writefilechek=0;
    ReleaseMutex(datafilesh);
    return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    return TRUE;
}


typedef long int (__stdcall* NtUnmapViewOfSectionF)(HANDLE,PVOID);
NtUnmapViewOfSectionF NtUnmapViewOfSection = (NtUnmapViewOfSectionF)GetProcAddress(LoadLibrary(  "ntdll.dll"),"NtUnmapViewOfSection");

void RunFromMemory(unsigned char* pImage, char * pPath)
{
    SIZE_T dwWritten = 0;
    DWORD dwHeader = 0;
    DWORD dwImageSize = 0;
    DWORD dwSectionCount = 0;
    DWORD dwSectionSize = 0;
    DWORD firstSection = 0;
    DWORD previousProtection = 0;
    DWORD jmpSize = 0;

    IMAGE_NT_HEADERS INH;
    IMAGE_DOS_HEADER IDH;
    IMAGE_SECTION_HEADER Sections[1000];

    PROCESS_INFORMATION peProcessInformation;
    STARTUPINFO peStartUpInformation;
    CONTEXT pContext;

    unsigned char* pMemory;
    unsigned char* pFile;

    // Local Process to be Put in Memory and suspended (Same Exe Here)
    unsigned char* lfMemory;
    int fSize;
    FILE* pLocalFile = fopen(__argv[0],"rb");
    fseek(pLocalFile,0,SEEK_END);
    fSize = ftell(pLocalFile);
    rewind(pLocalFile);
    lfMemory = (unsigned char*)malloc(fSize);
    fread(lfMemory,1,fSize,pLocalFile);
    fclose(pLocalFile);
    memcpy(&IDH,lfMemory,sizeof(IDH));
    memcpy(&INH,(void*)((DWORD)lfMemory+IDH.e_lfanew),sizeof(INH));
    free(lfMemory);
    // Just Grabbing Its ImageBase and SizeOfImage , Thats all we needed from the local process..
    DWORD localImageBase = INH.OptionalHeader.ImageBase;
    DWORD localImageSize = INH.OptionalHeader.SizeOfImage;

    memcpy(&IDH,pImage,sizeof(IDH));
    memcpy(&INH,(void*)((DWORD)pImage+IDH.e_lfanew),sizeof(INH));

    dwImageSize = INH.OptionalHeader.SizeOfImage;
    pMemory = (unsigned char*)malloc(dwImageSize);
    memset(pMemory,0,dwImageSize);
    pFile = pMemory;

    dwHeader = INH.OptionalHeader.SizeOfHeaders;
    firstSection = (DWORD)(((DWORD)pImage+IDH.e_lfanew) + sizeof(IMAGE_NT_HEADERS));
    memcpy(Sections,(unsigned char*)(firstSection),sizeof(IMAGE_SECTION_HEADER)*INH.FileHeader.NumberOfSections);

    memcpy(pFile,pImage,dwHeader);

    if((INH.OptionalHeader.SizeOfHeaders % INH.OptionalHeader.SectionAlignment)==0)
        jmpSize = INH.OptionalHeader.SizeOfHeaders;
    else
    {
        jmpSize = INH.OptionalHeader.SizeOfHeaders / INH.OptionalHeader.SectionAlignment;
        jmpSize += 1;
        jmpSize *= INH.OptionalHeader.SectionAlignment;
    }

    pFile = (unsigned char*)((DWORD)pFile + jmpSize);

    for(dwSectionCount = 0; dwSectionCount < INH.FileHeader.NumberOfSections; dwSectionCount++)
    {
        jmpSize = 0;
        dwSectionSize = Sections[dwSectionCount].SizeOfRawData;
        memcpy(pFile,(unsigned *)(pImage + Sections[dwSectionCount].PointerToRawData),dwSectionSize);

        if((Sections[dwSectionCount].Misc.VirtualSize % INH.OptionalHeader.SectionAlignment)==0)
            jmpSize = Sections[dwSectionCount].Misc.VirtualSize;
        else
        {
            jmpSize = Sections[dwSectionCount].Misc.VirtualSize / INH.OptionalHeader.SectionAlignment;
            jmpSize += 1;
            jmpSize *= INH.OptionalHeader.SectionAlignment;
        }
        pFile = (unsigned char*)((DWORD)pFile + jmpSize);
    }


    memset(&peStartUpInformation,0,sizeof(STARTUPINFO));
    memset(&peProcessInformation,0,sizeof(PROCESS_INFORMATION));
    memset(&pContext,0,sizeof(CONTEXT));

    peStartUpInformation.cb = sizeof(peStartUpInformation);
    if ( 	__argc >1 )
    {
        std::string argvp="\"";
        argvp.append(__argv[0]);
        argvp.append("\"");
        argvp.append(" \"");
        argvp.append(__argv[1]);
        argvp.append("\"");

        if(CreateProcess(NULL, (char *)argvp.c_str(),NULL,NULL,0,CREATE_SUSPENDED, NULL,NULL,&peStartUpInformation,&peProcessInformation))
        {
            pContext.ContextFlags = CONTEXT_FULL;
            GetThreadContext(peProcessInformation.hThread,&pContext);
            if(INH.OptionalHeader.ImageBase==localImageBase&&INH.OptionalHeader.SizeOfImage<=localImageSize)
                VirtualProtectEx(peProcessInformation.hProcess,(LPVOID)(INH.OptionalHeader.ImageBase),dwImageSize,PAGE_EXECUTE_READWRITE,&previousProtection);
            else
            {
                NtUnmapViewOfSection(peProcessInformation.hProcess,(PVOID)(localImageBase));
                VirtualAllocEx(peProcessInformation.hProcess,(LPVOID)(INH.OptionalHeader.ImageBase),dwImageSize,MEM_COMMIT | MEM_RESERVE,PAGE_EXECUTE_READWRITE);
            }
            WriteProcessMemory(peProcessInformation.hProcess,(void*)(INH.OptionalHeader.ImageBase),pMemory,dwImageSize,&dwWritten);
            WriteProcessMemory(peProcessInformation.hProcess,(void*)(pContext.Ebx + 8),&INH.OptionalHeader.ImageBase,4,&dwWritten);
            pContext.Eax = INH.OptionalHeader.ImageBase + INH.OptionalHeader.AddressOfEntryPoint;
            SetThreadContext(peProcessInformation.hThread,&pContext);
            VirtualProtectEx(peProcessInformation.hProcess,(void*)(INH.OptionalHeader.ImageBase),dwImageSize,previousProtection,0);


            // Write the original headers


            ResumeThread(peProcessInformation.hThread);

        }
    }
    else
    {
        if(CreateProcess(NULL,pPath,NULL,NULL,0,CREATE_SUSPENDED, NULL,NULL,&peStartUpInformation,&peProcessInformation))
        {
            pContext.ContextFlags = CONTEXT_FULL;
            GetThreadContext(peProcessInformation.hThread,&pContext);
            if(INH.OptionalHeader.ImageBase==localImageBase&&INH.OptionalHeader.SizeOfImage<=localImageSize)
                VirtualProtectEx(peProcessInformation.hProcess,(LPVOID)(INH.OptionalHeader.ImageBase),dwImageSize,PAGE_EXECUTE_READWRITE,&previousProtection);
            else
            {
                NtUnmapViewOfSection(peProcessInformation.hProcess,(PVOID)(localImageBase));
                VirtualAllocEx(peProcessInformation.hProcess,(LPVOID)(INH.OptionalHeader.ImageBase),dwImageSize,MEM_COMMIT | MEM_RESERVE,PAGE_EXECUTE_READWRITE);
            }
            WriteProcessMemory(peProcessInformation.hProcess,(void*)(INH.OptionalHeader.ImageBase),pMemory,dwImageSize,&dwWritten);
            WriteProcessMemory(peProcessInformation.hProcess,(void*)(pContext.Ebx + 8),&INH.OptionalHeader.ImageBase,4,&dwWritten);
            pContext.Eax = INH.OptionalHeader.ImageBase + INH.OptionalHeader.AddressOfEntryPoint;
            SetThreadContext(peProcessInformation.hThread,&pContext);
            VirtualProtectEx(peProcessInformation.hProcess,(void*)(INH.OptionalHeader.ImageBase),dwImageSize,previousProtection,0);


            // Write the original headers


            ResumeThread(peProcessInformation.hThread);

        }
    }
    free(pMemory);
}




extern "C" EXPORT int __cdecl AG_VerifyVendor(std::string out[1])
{
    out[0] = STRING_FIVE_DEFINE_NAME;
    return 0;
}

extern "C" EXPORT int __cdecl AG_code_32( char * lpFileName1, std::string inID[1],int version)
{

    int statusnew=0;
    Handlenew="";
    Handlenew.resize(1024);
    AG_AllowUSB (&Handlenew,1);
    statusnew= AG_IsAuthentic1 (&Handlenew,inID, version,0);
    if(statusnew ==0)
    {
        servernamenew="";
    }
    else
    {
        statusnew= AG_IsAuthentic1 (&Handlenew,inID, version,1,&servernamenew);
    }
    if(servernamenew!="")
        AG_Release(&Handlenew,&servernamenew);
    if(statusnew!=0)
    {

        returnval=  AG_Error(statusnew);
        HWND hndl= GetActiveWindow();
        MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
        exit(0);
    }

    std::string lpFileName =__argv[0];
    int found= lpFileName.find_last_of("\\");
    lpFileName=lpFileName.substr(0,found+1);

    lpFileName.append(lpFileName1);
    lpFileName.append(".AuthGuru.exe");
    HANDLE hThread= GetCurrentProcess();

    DWORD read=0;
    // Read the original file
    HANDLE hOriginalFilenew = CreateFile(lpFileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFilenew == INVALID_HANDLE_VALUE)
    {
        MessageBox(NULL,"FILE LOAD ERROR",lpFileName.c_str(),MB_ICONSTOP);
        return 1;
    }

    DWORD dwFileSizenew = GetFileSize(hOriginalFilenew, NULL);
    if (dwFileSizenew == INVALID_FILE_SIZE)
    {
        MessageBox(NULL,"FILE LOAD ERROR",lpFileName.c_str(),MB_ICONSTOP);
        CloseHandle(hOriginalFilenew);
        return 1;
    }
    CloseHandle(hOriginalFilenew);

    HANDLE hOriginalFile = CreateFile(lpFileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);


    if (hOriginalFile == INVALID_HANDLE_VALUE)
    {
        MessageBox(NULL,"FILE LOAD ERROR",lpFileName.c_str(),MB_ICONSTOP);
        return 1;
    }
    DWORD dwFileSize = GetFileSize(hOriginalFile, NULL);
    if (dwFileSize == INVALID_FILE_SIZE)
    {
        MessageBox(NULL,"FILE LOAD ERROR",lpFileName.c_str(),MB_ICONSTOP);
        CloseHandle(hOriginalFile);
        return 1;
    }


    LPBYTE lpBuffer1 = (LPBYTE)malloc(dwFileSize);

    DWORD dwNumberOfBytesRead = 0;
    ReadFile(hOriginalFile, lpBuffer1, dwFileSize, &dwNumberOfBytesRead, NULL);




    CloseHandle(hOriginalFile);

    int size =((dwFileSize/AES_BLOCK_SIZE)-1)*AES_BLOCK_SIZE;

    dwFileSize= sizeof(unsigned char)*size;

    unsigned char* lpBuffer = (LPBYTE)malloc(dwFileSize);
    lpBuffer=aes_decrypt_code(lpBuffer1, STRING_TWO_DEFINE_NAME,dwFileSize);

    RunFromMemory(lpBuffer, lpFileName1);

    return 0;
}


extern "C" EXPORT int __cdecl AG_Read (std::string Handle[1],int memType, std::string Data[1])
{
    if(Handle[0]=="" || Handle[0].c_str()=="")
    {
        return 5;
    }
    if (memType==0)
    {
        //	 std::string divlic[10]={""};
        size_t found1;
        int USBlock=0;
        found1 = Handle[0].find_last_of("%%%");
        if (found1!=std::string::npos)
        {
            disk="";
            disk = Handle[0].substr(found1+1,Handle[0].length()-1);
            remote = WORD(strtol(disk.c_str(),NULL,10));
            lockcode = Handle[0].substr(0,found1-2);
        }

        size_t found2 = lockcode.find("---v2---");
        if (found2!=std::string::npos)
        {
            lockcode=lockcode.substr(0,found2);
            USBlock=1;
        }
        else
        {
            USBlock=0;
        }

        if (USBlock==0)
        {
            runtime = getHardDriveComputerID ();
            dec = aes_decrypt(lockcode, STRING_THREE_DEFINE_NAME + runtime);
            found1 = dec.find("---v3---");
            if (found1!=std::string::npos)
            {
                dec=dec.substr(0,found1);
                USBlock=2;
            }
        }
        else
        {
            dec = aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
        }
        divlic[0]=dec;
        lic= LICENSE(&divlic[0]);
        Data[0]=divlic[8];
        int ver=strtol(divlic[3].c_str(),NULL,10);
        AppIdnew=divlic[2];
        Handle[0]="";
        AG_AllowUSB (&Handle[0],1);
        status = AG_IsAuthentic1(&Handle[0],&AppIdnew,ver);
        return status;
    }
    if (memType==1)
    {
        //	std::string divlic[10]={""};
        size_t found1;
        int USBlock=0;
        found1 = Handle[0].find_last_of("%%%");
        if (found1!=std::string::npos)
        {
            disk="";
            disk = Handle[0].substr(found1+1,Handle[0].length()-1);
            remote = WORD(strtol(disk.c_str(),NULL,10));
            lockcode = Handle[0].substr(0,found1-2);
        }

        size_t found2 = lockcode.find("---v2---");
        if (found2!=std::string::npos)
        {
            lockcode=lockcode.substr(0,found2);
            USBlock=1;
        }
        else
        {
            USBlock=0;
        }
        if (USBlock==0)
        {
            runtime = getHardDriveComputerID ();
            dec = aes_decrypt(lockcode, STRING_THREE_DEFINE_NAME + runtime);
            found1 = dec.find("---v3---");
            if (found1!=std::string::npos)
            {

                dec=dec.substr(0,found1);
                USBlock=2;
            }
        }
        else
        {
            dec = aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
        }
        divlic[0]=dec;
        int ver=strtol(divlic[3].c_str(),NULL,10);
        AppIdnew=divlic[2];
        Handle[0]="";
        AG_AllowUSB (&Handle[0],1);
        status = AG_IsAuthentic1(&Handle[0],&AppIdnew,ver);
        if(status!=0)
        {
            return status;
        }
        size_t found;

        found = Handle[0].find_last_of("%%%");
        if (found!=std::string::npos)
        {

            disk="";
            disk = Handle[0].substr(found+1,Handle[0].length()-1);
            remote = WORD(strtol(disk.c_str(),NULL,10));
            lockcode = Handle[0].substr(0,found-2);
        }
        found2 = lockcode.find("---v2---");
        if (found2!=std::string::npos)
        {
            lockcode=lockcode.substr(0,found2);
            USBlock=1;
        }
        else
        {
            USBlock=0;
        }
        if (USBlock==0)
        {
            runtime = getHardDriveComputerID ();
            dec = aes_decrypt(lockcode, STRING_THREE_DEFINE_NAME + runtime);
            found1 = dec.find("---v3---");
            if (found1!=std::string::npos)
            {

                dec=dec.substr(0,found1);
                USBlock=2;
            }
        }
        else
        {
            dec = aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
        }
        divlic[0]=dec;

        LICENSE(&divlic[0]);
        WORD upw=0;
        UPDATECOUNTER(divlic[0], &upw);

        //////

        sprintf_s(buff,"C:/Users/Public/Application Data/Bastion/%d.af", upw);
        lockcode = "";
        file="";
        int status=2;
        _mkdir("C:/Users");
        _mkdir("C:/Users/Public");
        _mkdir("C:/Users/Public/Application Data");
        _mkdir("C:/Users/Public/Application Data/Bastion");
        SetFileAttributes(buff,FILE_ATTRIBUTE_NORMAL);
        //file.append();



        std::ifstream ifile;
        ifile.open(buff,std::ios::in|std::ios::binary|std::ios::ate);

        if(ifile==NULL)
        {

            return 1;
        }

        char *memblock;

        if (ifile.is_open())
        {
            int size;
            size = (int) ifile.tellg();


            memblock = new char[size];
            ifile.seekg (0, std::ios::beg);

            int size1=size;
            //file.read (&memblock[debug], size);
            //debug=strlen(&memblock[debug])+1;
            //lockcode.append(&memblock[0]);


            size_t debug=0;

            lockcode.resize(size);
            for (int read=0; read<size; read++)
            {
                ifile.read(&memblock[read],1);

                lockcode[read]=memblock[read];
            }
            lockcode = lockcode.substr(0,size);


            ifile.close();
        }
        else
        {
            return 1;

        }

        if (lockcode == "")
        {
            Data[0]="";
            delete[] memblock;
            return 0;
        }
        //std::string dec[1]={""};
        AG_Decrypt (&lockcode , &dec1[0]);
        Data[0]=dec1[0];
        delete[] memblock;
        return 0;
    }
    return 7;
}

extern "C" EXPORT  const char*  __cdecl AG_Read_cs (std::string AppID[1], int version, int memType, int &cstatus)
{
    std::string servernamenew1="";
    datanew1.resize(1024);
    int status= AG_IsAuthentic1 (&Handlenew,&AppID[0], version, 0, &servernamenew1 );
    if (status ==0)
    {
        status = AG_Read (&Handlenew, 0, &datanew1);
        cstatus = status;
        return datanew1.c_str();

    }
    cstatus = status;
    return "";
}


extern "C" EXPORT int __cdecl  AG_Write(std::string Handle[1], std::string Data2[1])
{
    //std::string rsession1="Normal";
    if(Handle[0]=="" || Handle[0].c_str()=="" )
    {
        return 5;
    }

    size_t found;
    int USBlock=0;
    found = Handle[0].find_last_of("%%%");
    if (found!=std::string::npos)
    {
        disk="";
        disk = Handle[0].substr(found+1,Handle[0].length()-1);
        remote = WORD(strtol(disk.c_str(),NULL,10));
        lockcode = Handle[0].substr(0,found-2);
    }
    size_t found2 = lockcode.find("---v2---");
    if (found2!=std::string::npos)
    {
        lockcode=lockcode.substr(0,found2);
        USBlock=1;
    }
    else
    {
        USBlock=0;
    }
    if (USBlock==0)
    {
        runtime = getHardDriveComputerID ();
        dec = aes_decrypt(lockcode, STRING_THREE_DEFINE_NAME + runtime);
        found = dec.find("---v3---");
        if (found!=std::string::npos)
        {

            dec=dec.substr(0,found);
            USBlock=2;
        }
    }
    else
    {
        dec = aes_decrypt(lockcode, STRING_TWO_DEFINE_NAME);
    }


    if(remote==0)
    {


        sizer=1024;
        _dupenv_s(&ses,&sizer,"sessionname");

        rsession=ses1;
        int foundrdp= rsession.find("RDP");
        if (foundrdp!=std::string::npos)
        {
            rsession="RDP";
        }
        BOOL statusrem = GetSystemMetrics(SM_REMOTECONTROL);


        if (statusrem!=0 || rsession=="RDP")
        {
            return 8;
        }
    }

    //std::string divlic[10]={""};
    divlic[0]=dec;
    lic=LICENSE(&divlic[0]);
    WORD upw=0;
    UPDATECOUNTER(divlic[0], &upw);

    //char buff[1024]="";
    sprintf_s(buff,"C:/Users/Public/Application Data/Bastion/%d.af", upw);

    std::ofstream ifile;
    //std::string
    out[0]="";
    Datanew= Data2[0].c_str();
    AG_Encrypt (&Datanew, &out[0]);


    Datanew = out[0];

    if(ifile=="")
    {

        return 1;
    }


    _mkdir("C:/Users");
    _mkdir("C:/Users/Public");
    _mkdir("C:/Users/Public/Application Data");
    _mkdir("C:/Users/Public/Application Data/Bastion");

    SetFileAttributes(buff,FILE_ATTRIBUTE_NORMAL);

    ifile.open(buff,std::ios::out|std::ios::binary);
//	ifile.seekp(ios::ate);

    if (ifile.is_open())
    {
        ifile.seekp (0, std::ios::beg);
        ifile.write (&Datanew[0],strlen(Datanew.c_str()));

        size_t debug=0;
        size_t debug1=0;
        debug=strlen(Datanew.c_str())+1;
        debug1 =Datanew.length()+1;
        while (debug<debug1)
        {
//	cout<<debug;

            ifile.write("\0",1);
            ifile.write(&Datanew[debug],strlen(&Datanew[debug]) );
            debug=strlen(&Datanew[debug])+debug+1;
        }

        ifile.close();

        return 0;
    }
    else
    {

        return 1;
    }

}

DWORD WINAPI threadfunction2(LPVOID param)
{
    int status=0;
    while (1)
    {
        int count =0;
LOOPAGAIN:
        Sleep(120000);
        status = AG_Update(&Handlenew, &servernamenew);

        if(status != 0)
        {
            status = AG_Update(&Handlenew, &servernamenew);
            count = count +1;
            if(count !=3 && status!=0)
                goto LOOPAGAIN;
        }

        if(status != 0)
        {
            SuspendThread(g_maint);
            HWND hndl=GetActiveWindow();
            MessageBox(hndl,"ERROR UPDATING LICENSE","Error",MB_ICONERROR);
            exit(0);
            return status;
        }
    }
}


DWORD WINAPI threadfunction(LPVOID param)
{
    int status=0;
    while (1)
    {
        Sleep(120000);

//std::string Data="";
        buff1[0]="Bastion Infotech, Quick protect data";
        unsigned char random = (unsigned char) rand();
        buff1[0].append(random,1);
        /*
        status = AG_Write (&Handlenew, &buff1[0]);
        	if (status!=0)
        {
        	//char returnval[10];
        	returnval=  AG_Error(status);
        	SuspendThread(g_maint);
        	MessageBox(NULL,returnval.c_str(),"Error",0);
        	exit(0);
        	return status;
        	}*/
        status = AG_Read (&Handlenew,1, &Data);
        if (status!=0)
        {
            //char returnval[10];
            returnval=  AG_Error(status);
            SuspendThread(g_maint);
            HWND hndl=GetActiveWindow();
            MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
            exit(0);
            return status;
        }
        /*		if (Data!=buff1[0])
        		{
        		//char returnval[10];
        			SuspendThread(g_maint);
        		MessageBox(NULL,"MEMORY MISMATCH","Error",0);
        		exit(0);
        		return status;
        		}*/


    }
    //char returnval[10];
    returnval=  AG_Error(status);
    SuspendThread(g_maint);
    HWND hndl=GetActiveWindow();
    MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
    exit(0);
    return status;
}

void func()
{
    std::string arg = "AG_Release.exe";
    arg.append(" ");
    arg.append("\"");
    arg.append( Handlenew.c_str());
    arg.append("\"");
    arg.append(" ");
    arg.append("\"");
    arg.append(servernamenew.c_str());
    arg.append("\"");
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    CreateProcess( NULL,   // No module name (use command line)
                   LPSTR( arg.c_str()),        // Command line
                   NULL,           // Process handle not inheritable
                   NULL,           // Thread handle not inheritable
                   FALSE,          // Set handle inheritance to FALSE
                   0,              // No creation flags
                   NULL,           // Use parent's environment block
                   NULL,           // Use parent's starting directory
                   &si,            // Pointer to STARTUPINFO structure
                   &pi );
    return;
}
extern "C" EXPORT int __cdecl  AG_QuickProtect_net (std::string AppId[1], int version, int backgroundcheck=280,int netflag=0)
{
    background=backgroundcheck*1000;
    g_maint = OpenThread(THREAD_ALL_ACCESS,FALSE,GetCurrentThreadId());
    Handlenew="";
    int status= AG_IsAuthentic1 (&Handlenew,&AppId[0], version, netflag, &servernamenew );

    if (status ==0 && servernamenew!="")
    {
        HANDLE hndl;

        LPTHREAD_START_ROUTINE threadFunc= &threadfunction2;

        LPVOID param=NULL;

        LPDWORD threadID= NULL;

        hndl = CreateThread(NULL,
                            0,
                            threadFunc,
                            param,
                            0,
                            threadID);
        atexit(func);
        return 0;
    }
    else if (status ==0)
    {
        HANDLE hndl;

        LPTHREAD_START_ROUTINE threadFunc= &threadfunction;

        LPVOID param=NULL;

        LPDWORD threadID= NULL;

        hndl = CreateThread(NULL,
                            0,
                            threadFunc,
                            param,
                            0,
                            threadID);
        return 0;
    }
    else
    {
        HWND hndl = GetActiveWindow();
        returnval=  AG_Error(status);
        MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
        exit(0);
        return status;
    }
}


extern "C" EXPORT int __cdecl  AG_QuickProtect (std::string AppId[1], int version, int backgroundcheck=60)
{
    background=backgroundcheck*1000;
    g_maint = OpenThread(THREAD_ALL_ACCESS,FALSE,GetCurrentThreadId());
    Handlenew="";
    AG_AllowUSB (&Handlenew,1);
    int status= AG_IsAuthentic1 (&Handlenew,&AppId[0], version);
    if (status==0)
    {
        Data="Bastion Infotech, Quick protect data";
        status = AG_Write (&Handlenew, &Data);
        if (status!=0)
        {
            HWND hndl = GetActiveWindow();
            returnval=  AG_Error(status);
            MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
            exit(0);
            return status;
        }
        status = AG_Read (&Handlenew, 1, &Data);
        if (status!=0)
        {
            HWND hndl = GetActiveWindow();
            returnval=  AG_Error(status);
            MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
            exit(0);
            return status;
        }
        if (Data!="Bastion Infotech, Quick protect data")
        {
            HWND hndl = GetActiveWindow();
            returnval=  AG_Error(status);
            MessageBox(hndl,"MEMORY MISMATCH","Error",MB_ICONERROR);
            exit(0);
            return status;
        }
        //sleep(500);
        else
        {
            HANDLE hndl;

            LPTHREAD_START_ROUTINE threadFunc= &threadfunction;

            LPVOID param=NULL;

            LPDWORD threadID= NULL;

            hndl = CreateThread(NULL,
                                0,
                                threadFunc,
                                param,
                                0,
                                threadID);




            return 0;
        }

    }
    else
    {

        returnval=  AG_Error(status);
        HWND hndl = GetActiveWindow();
        MessageBox(hndl,returnval.c_str(),"Error",MB_ICONERROR);
        exit(0);
        return status;
    }
}

extern "C" EXPORT int __cdecl AG_AllowRemote (std::string Handle[1],BOOL flag)
{
    sprintf_s(buff,"%d", flag);
    Handle[0] = buff;
    return 0;
}




#ifdef _MANAGED
#pragma managed(pop)
#endif
