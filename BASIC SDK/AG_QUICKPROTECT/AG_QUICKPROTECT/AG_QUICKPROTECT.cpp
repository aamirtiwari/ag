
// AG_QUICKPROTECT.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "AG_QUICKPROTECT.h"
#include "AG_QUICKPROTECTDlg.h"
#include <string>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAG_QUICKPROTECTApp

BEGIN_MESSAGE_MAP(CAG_QUICKPROTECTApp, CWinApp)
    ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CAG_QUICKPROTECTApp construction

CAG_QUICKPROTECTApp::CAG_QUICKPROTECTApp()
{
    // support Restart Manager
    m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

    // TODO: add construction code here,
    // Place all significant initialization in InitInstance
}


// The one and only CAG_QUICKPROTECTApp object

CAG_QUICKPROTECTApp theApp;


// CAG_QUICKPROTECTApp initialization

BOOL CAG_QUICKPROTECTApp::InitInstance()
{
    // InitCommonControlsEx() is required on Windows XP if an application
    // manifest specifies use of ComCtl32.dll version 6 or later to enable
    // visual styles.  Otherwise, any window creation will fail.
    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof(InitCtrls);
    // Set this to include all the common control classes you want to use
    // in your application.
    InitCtrls.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&InitCtrls);

    CWinApp::InitInstance();


    AfxEnableControlContainer();

    // Create the shell manager, in case the dialog contains
    // any shell tree view or shell list view controls.
    CShellManager *pShellManager = new CShellManager;

    // Standard initialization
    // If you are not using these features and wish to reduce the size
    // of your final executable, you should remove from the following
    // the specific initialization routines you do not need
    // Change the registry key under which our settings are stored
    // TODO: You should modify this string to be something appropriate
    // such as the name of your company or organization
    HKEY hKey;
    DWORD dwDisp = 0;
    LPDWORD lpdwDisp = &dwDisp;
    CString l_strExampleKey = "SOFTWARE\\Classes\\qpproj_auto_file\\shell\\open\\command";


    CString l_strStringSample = "",l_strStringSampleVal ;//= "\"D:\\com\\AKLM_sysinfo\\Debug\\AKLM_sysinfo.exe\" \"%1\"";
    std::string l_strStringSampleVal1="";
    l_strStringSampleVal1.append("\"");
    l_strStringSampleVal1.append(__argv[0]);

    l_strStringSampleVal1.append("\" \"%1\"");

    l_strStringSampleVal=l_strStringSampleVal1.c_str();
    LONG iSuccess = RegCreateKeyEx( HKEY_CLASSES_ROOT, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }
    iSuccess = RegCreateKeyEx( HKEY_LOCAL_MACHINE, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }
    l_strExampleKey = ".qpproj";
    l_strStringSample = "", l_strStringSampleVal = "qpproj_auto_file";;

    iSuccess = RegCreateKeyEx( HKEY_CLASSES_ROOT, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }

    iSuccess = RegCreateKeyEx( HKEY_LOCAL_MACHINE, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }

    l_strExampleKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.qpproj\\OpenWithList";
    l_strStringSample = "a", l_strStringSampleVal = "AG_QUICKPROTECT.exe";;

    iSuccess = RegCreateKeyEx( HKEY_CURRENT_USER, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);
    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }

    l_strStringSample = "MRUList", l_strStringSampleVal = "a";
    dwDisp=1;
    iSuccess = RegCreateKeyEx( HKEY_CURRENT_USER, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);
    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }
    RegCloseKey(hKey);

    CAG_QUICKPROTECTDlg dlg;
    m_pMainWnd = &dlg;
    INT_PTR nResponse = dlg.DoModal();
    if (nResponse == IDOK)
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with OK
    }
    else if (nResponse == IDCANCEL)
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with Cancel
    }

    // Delete the shell manager created above.
    if (pShellManager != NULL)
    {
        delete pShellManager;
    }

    // Since the dialog has been closed, return FALSE so that we exit the
    //  application, rather than start the application's message pump.
    return FALSE;
}

