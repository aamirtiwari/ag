//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AG_QUICKPROTECT.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_AG_QUICKPROTECT_DIALOG      102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDI_ICON1                       130
#define IDI_ICON2                       131
#define IDI_ICON3                       132
#define IDI_ICON4                       133
#define IDI_ICON5                       134
#define IDI_ICON6                       135
#define IDB_BITMAP1                     136
#define IDB_BITMAP2                     137
#define IDB_BITMAP3                     138
#define IDB_BITMAP4                     139
#define IDB_BITMAP5                     140
#define IDB_BITMAP6                     141
#define IDC_BUTTON1                     1000
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_CHECK1                      1003
#define IDC_CHECK2                      1004
#define IDC_EDIT3                       1005
#define IDC_BUTTON2                     1006
#define IDC_BUTTON3                     1007
#define IDC_EDIT4                       1008
#define IDC_EDIT5                       1009
#define IDC_CHECK3                      1010
#define IDC_CHECK4                      1011
#define IDC_EDIT6                       1012
#define IDC_PROGRESS1                   1013
#define IDC_CHECK5                      1014
#define IDC_CHECK6                      1015
#define IDC_EDIT7                       1016
#define ID_PROJECT_SAVEAS               32771
#define ID_DATAFILES_OPENAG             32772
#define ID_PROJECT_SAVEAS32773          32773
#define ID_TOOLS_ENABLESIGNTOOL         32774
#define ID_TOOLS_MERGEDLLSWITHEXE       32775
#define ID_TOOLS_HELP                   32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
