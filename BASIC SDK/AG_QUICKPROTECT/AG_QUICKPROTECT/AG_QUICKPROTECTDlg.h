
// AG_QUICKPROTECTDlg.h : header file
//

#pragma once


// CAG_QUICKPROTECTDlg dialog
class CAG_QUICKPROTECTDlg : public CDialogEx
{
// Construction
public:
	CAG_QUICKPROTECTDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_AG_QUICKPROTECT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void ProcessEvents();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton2();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	CEdit m_edit1;
	CEdit m_edit2;
	CEdit m_edit3;
	CButton m_check1;
	CButton m_check2;
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck1();
	CEdit m_edit4;
	CEdit m_edit5;
	CButton m_check3;
	afx_msg void OnBnClickedCheck3();
	CButton m_check4;
	CEdit m_edit6;
	afx_msg void OnBnClickedCheck4();
	CProgressCtrl m_progress;
	afx_msg void OnBnClickedCheck5();
	CButton m_check5;
	afx_msg void OnDatafilesOpenag();
	CEdit m_edit7;
	CButton m_check6;
	afx_msg void OnBnClickedCheck6();
	afx_msg void OnProjectSaveas32773();
	afx_msg void OnProjectSaveas();
	CButton m_button1;
	afx_msg void OnToolsEnablesigntool();
	afx_msg void OnToolsMergedllswithexe();
	afx_msg void OnToolsHelp();
};
