﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Threading;
using AG_API.Interface;
using AG_API.Constants;
namespace Bulwark_net
{
public unsafe class BLWNET
{
    public int loadm(byte[] bin)
    {
        //  FileStream _FileStream = new FileStream(file_name, FileMode.Open);
        // BinaryReader _BinaryReader = new BinaryReader(_FileStream);
        // byte[] bin = _BinaryReader.ReadBytes(Convert.ToInt32(_FileStream.Length));

        Assembly a = Assembly.Load(bin);
        //   Form f = (Form)a.CreateInstance(a.EntryPoint.ReflectedType.FullName); f.Show();

        // search for the Entry Point

        MethodInfo method = a.EntryPoint;
        if (method != null)
        {
            // create an istance of the Startup form Main method
            Thread.CurrentThread.SetApartmentState(ApartmentState.STA);
            object o = a.CreateInstance(method.Name);
            if (a.EntryPoint.ToString() == "Void Main()")
            {

                method.Invoke(o, null);
                return 0;
            }
            else
            {
                // invoke the application starting point
                String[] Data = { "" };
                method.Invoke(o, new object[] { Data });
                return 0;
            }
        }
        return 1;
    }

    public Type loadmnew(byte[] bin, String Classname, String APPID, int version)
    {
        char[] handlechar = new char[1024];
        string Handle = new string(handlechar);
        char[] serverchar = new char[1024];
        string servernameout = new string(serverchar);

        int status = AGAPI.AG_IsAuthentic(ref Handle, ref APPID, version, 0, ref servernameout);

        if (status != Constants.AG_SUCCESS)
        {
            return null;
        }

        //  FileStream _FileStream = new FileStream(file_name, FileMode.Open);
        // BinaryReader _BinaryReader = new BinaryReader(_FileStream);
        // byte[] bin = _BinaryReader.ReadBytes(Convert.ToInt32(_FileStream.Length));
        Assembly a = Assembly.Load(bin);
        //   Form f = (Form)a.CreateInstance(a.EntryPoint.ReflectedType.FullName); f.Show();

        // search for the Entry Point

        foreach (Type oType in a.GetTypes())
        {
            if (oType.Name == Classname)
            {

                return oType;
            }
        }

        return null;
    }
}
}
