// AuthToken.cpp : Implementation of CAuthToken
#include "stdafx.h"
#include "AuthTokenserver.h"
#include "AuthToken.h"
#include <comutil.h>
#include <string>
/////////////////////////////////////////////////////////////////////////////
// CAuthToken

STDMETHODIMP CAuthToken::InterfaceSupportsErrorInfo(REFIID riid)
{
    static const IID* arr[] =
    {
        &IID_IAuthToken,
    };
    for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
    {
        if (InlineIsEqualGUID(*arr[i],riid))
            return S_OK;
    }
    return S_FALSE;
}

STDMETHODIMP CAuthToken::OnStartPage (IUnknown* pUnk)
{
    if(!pUnk)
        return E_POINTER;

    CComPtr<IScriptingContext> spContext;
    HRESULT hr;

    // Get the IScriptingContext Interface
    hr = pUnk->QueryInterface(IID_IScriptingContext, (void **)&spContext);
    if(FAILED(hr))
        return hr;

    // Get Request Object Pointer
    hr = spContext->get_Request(&m_piRequest);
    if(FAILED(hr))
    {
        spContext.Release();
        return hr;
    }

    // Get Response Object Pointer
    hr = spContext->get_Response(&m_piResponse);
    if(FAILED(hr))
    {
        m_piRequest.Release();
        return hr;
    }

    // Get Server Object Pointer
    hr = spContext->get_Server(&m_piServer);
    if(FAILED(hr))
    {
        m_piRequest.Release();
        m_piResponse.Release();
        return hr;
    }

    // Get Session Object Pointer
    hr = spContext->get_Session(&m_piSession);
    if(FAILED(hr))
    {
        m_piRequest.Release();
        m_piResponse.Release();
        m_piServer.Release();
        return hr;
    }

    // Get Application Object Pointer
    hr = spContext->get_Application(&m_piApplication);
    if(FAILED(hr))
    {
        m_piRequest.Release();
        m_piResponse.Release();
        m_piServer.Release();
        m_piSession.Release();
        return hr;
    }
    m_bOnStartPageCalled = TRUE;
    return S_OK;
}

STDMETHODIMP CAuthToken::OnEndPage ()
{
    m_bOnStartPageCalled = FALSE;
    // Release all interfaces
    m_piRequest.Release();
    m_piResponse.Release();
    m_piServer.Release();
    m_piSession.Release();
    m_piApplication.Release();

    return S_OK;
}


STDMETHODIMP CAuthToken::IsAuthenticServer(BSTR* bCookie1,BSTR *bCookie)
{

    int res, i;
    char* temp=_com_util::ConvertBSTRToString(*bCookie1);


    unsigned char key[20] = {0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, \
                             0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b
                            }; // this should be the hmac key in your LC device
	unsigned char digest_local[20]={'4'};
	unsigned char digest_device[20]={'4'};

    res = 0;
    if(res) {
        CComVariant vtOut(_T(""));
        *bCookie=::SysAllocString(V_BSTR(&vtOut));
        return 1;
    }
    digest_local[20]='\0';
    digest_device[20]='\0';
    char buff[100];
    sprintf_s(buff,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",digest_device[0],digest_device[1],digest_device[2],digest_device[3],digest_device[4],digest_device[5],digest_device[6],digest_device[7],digest_device[8],digest_device[9],digest_device[10],digest_device[11],digest_device[12],digest_device[13],digest_device[14],digest_device[15],digest_device[16],digest_device[17],digest_device[18],digest_device[19]);

    CComVariant vtOut(_T((char *)buff));
    *bCookie=::SysAllocString(V_BSTR(&vtOut));
    // Copy text from the input parameter to the output parameter
    delete[] temp;
    SysFreeString(*bCookie);
    // Fire an event to notify web page
    return 0;
}

