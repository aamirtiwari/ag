

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Mon Aug 23 15:31:59 2021
 */
/* Compiler settings for AuthTokenserver.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __SimpleActiveX_h__
#define __SimpleActiveX_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IAuthToken_FWD_DEFINED__
#define __IAuthToken_FWD_DEFINED__
typedef interface IAuthToken IAuthToken;
#endif 	/* __IAuthToken_FWD_DEFINED__ */


#ifndef __AuthToken_FWD_DEFINED__
#define __AuthToken_FWD_DEFINED__

#ifdef __cplusplus
typedef class AuthToken AuthToken;
#else
typedef struct AuthToken AuthToken;
#endif /* __cplusplus */

#endif 	/* __AuthToken_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IAuthToken_INTERFACE_DEFINED__
#define __IAuthToken_INTERFACE_DEFINED__

/* interface IAuthToken */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IAuthToken;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AF4810E0-46A2-4356-8D0E-24922A24760B")
    IAuthToken : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE OnStartPage( 
            /* [in] */ IUnknown *piUnk) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnEndPage( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsAuthenticServer( 
            /* [in] */ BSTR *bCookie1,
            /* [retval][out] */ BSTR *bCookie) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAuthTokenVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAuthToken * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAuthToken * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAuthToken * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAuthToken * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAuthToken * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAuthToken * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAuthToken * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *OnStartPage )( 
            IAuthToken * This,
            /* [in] */ IUnknown *piUnk);
        
        HRESULT ( STDMETHODCALLTYPE *OnEndPage )( 
            IAuthToken * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsAuthenticServer )( 
            IAuthToken * This,
            /* [in] */ BSTR *bCookie1,
            /* [retval][out] */ BSTR *bCookie);
        
        END_INTERFACE
    } IAuthTokenVtbl;

    interface IAuthToken
    {
        CONST_VTBL struct IAuthTokenVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAuthToken_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAuthToken_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAuthToken_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAuthToken_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAuthToken_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAuthToken_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAuthToken_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAuthToken_OnStartPage(This,piUnk)	\
    ( (This)->lpVtbl -> OnStartPage(This,piUnk) ) 

#define IAuthToken_OnEndPage(This)	\
    ( (This)->lpVtbl -> OnEndPage(This) ) 

#define IAuthToken_IsAuthenticServer(This,bCookie1,bCookie)	\
    ( (This)->lpVtbl -> IsAuthenticServer(This,bCookie1,bCookie) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAuthToken_INTERFACE_DEFINED__ */



#ifndef __SIMPLEACTIVEXLib_LIBRARY_DEFINED__
#define __SIMPLEACTIVEXLib_LIBRARY_DEFINED__

/* library SIMPLEACTIVEXLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_SIMPLEACTIVEXLib;

EXTERN_C const CLSID CLSID_AuthToken;

#ifdef __cplusplus

class DECLSPEC_UUID("7E5C1BFD-6EA0-4107-AE55-ADA3FC4E0F79")
AuthToken;
#endif
#endif /* __SIMPLEACTIVEXLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


