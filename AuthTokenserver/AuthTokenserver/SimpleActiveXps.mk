
SimpleActiveXps.dll: dlldata.obj SimpleActiveX_p.obj SimpleActiveX_i.obj
	link /dll /out:SimpleActiveXps.dll /def:SimpleActiveXps.def /entry:DllMain dlldata.obj SimpleActiveX_p.obj SimpleActiveX_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del SimpleActiveXps.dll
	@del SimpleActiveXps.lib
	@del SimpleActiveXps.exp
	@del dlldata.obj
	@del SimpleActiveX_p.obj
	@del SimpleActiveX_i.obj
