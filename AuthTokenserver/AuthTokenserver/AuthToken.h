// AuthToken.h : Declaration of the CAuthToken

#ifndef __AuthToken_H_
#define __AuthToken_H_

#include "resource.h"       // main symbols
#include <asptlb.h>         // Active Server Pages Definitions

/////////////////////////////////////////////////////////////////////////////
// CAuthToken
class ATL_NO_VTABLE CAuthToken : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CAuthToken, &CLSID_AuthToken>,
	public ISupportErrorInfo,
	public IDispatchImpl<IAuthToken, &IID_IAuthToken, &LIBID_SIMPLEACTIVEXLib>
{
public:
	CAuthToken()
	{ 
		m_bOnStartPageCalled = FALSE;
	}

public:

DECLARE_REGISTRY_RESOURCEID(IDR_AuthToken)

DECLARE_PROTECT_FINAL_CONSTRUCT()
DECLARE_NOT_AGGREGATABLE(CAuthToken)

BEGIN_COM_MAP(CAuthToken)
	COM_INTERFACE_ENTRY(IAuthToken)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IAuthToken
public:
	STDMETHOD(IsAuthenticServer)(BSTR* bCookie1,/*[out,retval]*/ BSTR *bCookie);
	//Active Server Pages Methods
	STDMETHOD(OnStartPage)(IUnknown* IUnk);
	STDMETHOD(OnEndPage)();

private:
	CComPtr<IRequest> m_piRequest;					//Request Object
	CComPtr<IResponse> m_piResponse;				//Response Object
	CComPtr<ISessionObject> m_piSession;			//Session Object
	CComPtr<IServer> m_piServer;					//Server Object
	CComPtr<IApplicationObject> m_piApplication;	//Application Object
	BOOL m_bOnStartPageCalled;						//OnStartPage successful?

};

#endif //__AuthToken_H_
