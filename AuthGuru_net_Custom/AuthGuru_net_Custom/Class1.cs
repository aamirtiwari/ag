﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthGuru_net;
using System.Reflection;
namespace AuthGuru_net_Custom
{
public class AuthGuru_net_Class
{
    static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public Type AG_Gettype(String str, String Clasname, String APPID, int version)
    {
        byte[] bytes = Encoding.ASCII.GetBytes(str);
        Type classobj;

        unsafe
        {
            fixed (byte* p = bytes)
            {
                AuthGuru ag = new AuthGuru();
                sbyte* sp = (sbyte*)p;
                classobj = ag.AG_loadlibrary(sp, Clasname, APPID, version);
            }
        }
        return classobj;

    }
}
}
