// StdDLL.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include <string.h>
#include "AG_Client.h"
BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    return TRUE;
}

// Pass Arrays to C/C++ Functions Expecting a Pointer
BSTR  __declspec (dllexport) __stdcall AG_Read_vb(char * buff, int *status) {

    std::string AppID=buff;
    int cstatus=0;
    std::string outval= AG_Read_cs (&AppID, 0, 0, cstatus);
    *status=cstatus;
    BSTR Message;

    Message = SysAllocStringByteLen (outval.c_str() , outval.length());
    return Message;
}

BSTR  __declspec (dllexport) __stdcall AG_IsAuthentic_vb(char * buff, int *status) {

    std::string AppID=buff;
    int cstatus=0;
    std::string outval= "";
    outval.resize(1024);
    cstatus=AG_IsAuthentic (&outval,&AppID, 0);
    *status=cstatus;
    BSTR Message;

    Message = SysAllocStringByteLen (outval.c_str() , outval.length());
    return Message;
}

BSTR  __declspec (dllexport) __stdcall AG_Encrypt_vb(char * buff, int *status) {

    std::string AppID=buff;
    int cstatus=0;
    std::string outval= "";
    outval.resize(AppID.length());
    cstatus=AG_Encrypt (&AppID,&outval);
    *status=cstatus;
    BSTR Message;

    Message = SysAllocStringByteLen (outval.c_str() , outval.length());
    return Message;
}

BSTR  __declspec (dllexport) __stdcall AG_Decrypt_vb(char * buff, int *status) {

    std::string AppID=buff;
    int cstatus=0;
    std::string outval= "";
    outval.resize(AppID.length());
    cstatus=AG_Decrypt (&AppID,&outval);
    *status=cstatus;
    BSTR Message;

    Message = SysAllocStringByteLen (outval.c_str() , outval.length());
    return Message;
}
VOID  __declspec (dllexport) __stdcall AG_QuickProtect_net_vb(char * buff, int *status) {

    std::string AppID=buff;
    int cstatus=0;
    std::string outval= "";
    outval.resize(AppID.length());
    cstatus=AG_QuickProtect_net (&AppID,0,280,1);
    *status=cstatus;
    return ;
}

