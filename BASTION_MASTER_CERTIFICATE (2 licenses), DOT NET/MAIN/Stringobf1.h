#ifndef STRINGOBF1_KEY
	#define STRINGOBF1_KEY "yCl/nUa2zMFIScfbRPsYTBKWv4O0tj1JLE6k85VhHdXmuqprND39eigQowA7+ZGx"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("SYnDSYnDSYnDSYnDSYneSRy=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("SkMEvYzQf/tNS/n3vYB8faUEvkn3vYyDSktocgUESkzy")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("SkMEvYzQf/ty")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("SktocgUESkzy")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("Pn52sBPCYUjbT8DnzU4UY8PbT6y9Syy=")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("S9yy")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF1_KEY);
		return string;
	}
#endif