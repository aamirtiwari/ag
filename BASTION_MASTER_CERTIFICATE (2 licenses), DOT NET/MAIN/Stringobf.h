#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "1+OBihXQg3RVnvwNF4mE8j59sYdKTSP07DpIcGHreM2aZ/CktuzUJAqoylxbLW6f"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("nEiunEiunEiunEiunEgAw11=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("nI3DsEgowBTtnBizsEjcwXhDsIizsE1unITyvqhDnIg1")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("nI3DsEgowBT1")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("nITyvqhDnIg1")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("8JhmF8t7nEFo11==")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("nEFo11==")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("8A+mEUgJwE8yvt1=")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("vBsynU7onIs1")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("YI+DwEjcYB1uYIsqn58qnUnqnIvDs54csIvpwEsyvqi1")
	#pragma comment(lib, "strenc64") 
 	void StrencDecode(char* buffer, char* Base64CharacterMap); 
 	const char* GetDecryptedString(const char* encryptedString) 
 	{ 
 		static char string[45]; 
 		strcpy(string, encryptedString); 
 		StrencDecode(string, STRINGOBF_KEY); 
 		return string; 
 	} 
 #endif