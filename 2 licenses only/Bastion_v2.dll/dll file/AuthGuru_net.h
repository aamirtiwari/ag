// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the AUTHGURU_NET_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// AUTHGURU_NET_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef AUTHGURU_NET_EXPORTS
#define AUTHGURU_NET_API __declspec(dllexport)
#else
#define AUTHGURU_NET_API __declspec(dllimport)
#endif

// This class is exported from the AuthGuru_net.dll
/*class AUTHGURU_NET_API CAuthGuru_net {
public:
	CAuthGuru_net(void);
	// TODO: add your methods here.
};*/


AUTHGURU_NET_API int addnetsection(DWORD input, DWORD input1,  const char * filepass);
