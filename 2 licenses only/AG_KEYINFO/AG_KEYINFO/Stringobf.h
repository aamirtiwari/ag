#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "YSzIMt21A6oJKsW+na0jUec73g4bHm/yGlTF9Dd8BZuNqOfVLvirXxRhkCP5QpwE"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("KjMvKjMvKjMvKjMvKjnLWnY=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("KF6l3jAhWIHLKIMi3je9W2tl3FMi3jYvKFHksRtlKFAY")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("KF6l3jAhWIHY")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("KFHksRtlKFAY")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("neSUet66czYiWjGY")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("KF9kYY==")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("sI3ksIAiKrKY")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("sI3kKrGhKF3Y")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("gFSlWje9gIYvgF3RKcURKrKRKFsl3ca93FsTWj3ksRMY")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif