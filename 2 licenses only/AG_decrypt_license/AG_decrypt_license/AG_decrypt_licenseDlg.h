
// AG_decrypt_licenseDlg.h : header file
//
#pragma once


// CAG_decrypt_licenseDlg dialog
class CAG_decrypt_licenseDlg : public CDialogEx
{
// Construction
public:
	CAG_decrypt_licenseDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_AG_DECRYPT_LICENSE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

public:	
//	tree treeo;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	CColumnTreeCtrl m_columnTree;
	void InitTree(void);
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	CTreeCtrl m_tree;
	CImageList m_image;
	afx_msg void OnActionsRefresh();
	afx_msg void OnActionsDelete();
};
