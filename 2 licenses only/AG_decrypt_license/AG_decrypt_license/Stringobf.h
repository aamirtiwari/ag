#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "KOdfsC8oA/bXvJHheTpzl1RUEWD9+4jmwVryicM7GZktgYn063PquQISxFBLN25a"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("vzs3vzs3vzs3vzs3vzAQHKK=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("vy/VEzASHf+6vfsPEz1iH8CVEysPEzK3vy+xJICVvyAK")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("vy/VEzASHf+K")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("vy+xJICVvyAK")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("luCpel6wvzeSKK==")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("vzeSKK==")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("lQOpzqAuHzlxJ6K=")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("JfExvqwSvyEK")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("WyOVHz1iWfK3WyEIvRlIvqvIvyJVERTiEyJrHzExJIsK")
	#pragma comment(lib, "strenc")
	void StrencDecode(char* buffer, char* Base64CharacterMap);
	const char* GetDecryptedString(const char* encryptedString)
	{
		static char string[45];
		strcpy(string, encryptedString);
		StrencDecode(string, STRINGOBF_KEY);
		return string;
	}
#endif