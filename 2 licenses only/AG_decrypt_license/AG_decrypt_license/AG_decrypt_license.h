
// AG_decrypt_license.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CAG_decrypt_licenseApp:
// See AG_decrypt_license.cpp for the implementation of this class
//

class CAG_decrypt_licenseApp : public CWinApp
{
public:
	CAG_decrypt_licenseApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAG_decrypt_licenseApp theApp;