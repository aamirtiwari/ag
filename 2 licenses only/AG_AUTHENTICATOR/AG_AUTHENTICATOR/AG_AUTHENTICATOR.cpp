
// AG_AUTHENTICATOR.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "AG_AUTHENTICATORDlg.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAG_AUTHENTICATORApp

BEGIN_MESSAGE_MAP(CAG_AUTHENTICATORApp, CWinApp)
    ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CAG_AUTHENTICATORApp construction

CAG_AUTHENTICATORApp::CAG_AUTHENTICATORApp()
{
    // support Restart Manager
    m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

    // TODO: add construction code here,
    // Place all significant initialization in InitInstance
}


// The one and only CAG_AUTHENTICATORApp object

CAG_AUTHENTICATORApp theApp;


// CAG_AUTHENTICATORApp initialization

BOOL CAG_AUTHENTICATORApp::InitInstance()
{
    CWinApp::InitInstance();


    // Create the shell manager, in case the dialog contains
    // any shell tree view or shell list view controls.
    CShellManager *pShellManager = new CShellManager;

    // Standard initialization
    // If you are not using these features and wish to reduce the size
    // of your final executable, you should remove from the following
    // the specific initialization routines you do not need
    // Change the registry key under which our settings are stored
    // TODO: You should modify this string to be something appropriate
    // such as the name of your company or organization
    HKEY hKey;
    DWORD dwDisp = 0;
    LPDWORD lpdwDisp = &dwDisp;
    CString l_strExampleKey = "SOFTWARE\\Classes\\agp_auto_file\\shell\\open\\command";


    CString l_strStringSample = "",l_strStringSampleVal ;//= "\"D:\\com\\AKLM_sysinfo\\Debug\\AKLM_sysinfo.exe\" \"%1\"";
    std::string l_strStringSampleVal1="";
    l_strStringSampleVal1.append("\"");
    l_strStringSampleVal1.append(__argv[0]);

    l_strStringSampleVal1.append("\" \"%1\"");

    l_strStringSampleVal=l_strStringSampleVal1.c_str();
    LONG iSuccess = RegCreateKeyEx( HKEY_CLASSES_ROOT, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }
    iSuccess = RegCreateKeyEx( HKEY_LOCAL_MACHINE, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }
    l_strExampleKey = ".agp";
    l_strStringSample = "", l_strStringSampleVal = "agp_auto_file";;

    iSuccess = RegCreateKeyEx( HKEY_CLASSES_ROOT, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }

    iSuccess = RegCreateKeyEx( HKEY_LOCAL_MACHINE, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);

    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }

    l_strExampleKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.agp\\OpenWithList";
    l_strStringSample = "a", l_strStringSampleVal = "AG_AUTHENTICATOR.exe";;

    iSuccess = RegCreateKeyEx( HKEY_CURRENT_USER, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);
    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }

    l_strStringSample = "MRUList", l_strStringSampleVal = "a";
    dwDisp=1;
    iSuccess = RegCreateKeyEx( HKEY_CURRENT_USER, l_strExampleKey, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);
    if(iSuccess == ERROR_SUCCESS)
    {
        RegSetValueEx(hKey, l_strStringSample, 0, REG_SZ, (LPBYTE) l_strStringSampleVal.GetBuffer(l_strStringSampleVal.GetLength()), l_strStringSampleVal.GetLength()+ 1);
    }
    RegCloseKey(hKey);

    CAG_AUTHENTICATORDlg dlg;
    m_pMainWnd = &dlg;
    INT_PTR nResponse = dlg.DoModal();
    if (nResponse == IDOK)
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with OK
    }
    else if (nResponse == IDCANCEL)
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with Cancel
    }

    // Delete the shell manager created above.
    if (pShellManager != NULL)
    {
        delete pShellManager;
    }

    // Since the dialog has been closed, return FALSE so that we exit the
    //  application, rather than start the application's message pump.
    return FALSE;
}

