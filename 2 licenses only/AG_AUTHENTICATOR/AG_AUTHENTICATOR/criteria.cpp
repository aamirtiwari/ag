// criteria.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "criteria.h"
#include "afxdialogex.h"


// criteria dialog

IMPLEMENT_DYNAMIC(criteria, CDialogEx)

criteria::criteria(CWnd* pParent /*=NULL*/)
    : CDialogEx(criteria::IDD, pParent)
{

}

criteria::~criteria()
{
}

void criteria::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}
BOOL criteria::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));
    //SetBackgroundImage( IDB_BITMAP1);
    m_check21.SetCheck(true);
    m_edit21.EnableWindow(false);
    m_edit22.EnableWindow(false);
    m_checking =0;
    m_value="";
    return TRUE;
}
void criteria::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    //  DDX_Control(pDX, IDC_CHECK1, m_check1);
    //  DDX_Control(pDX, IDC_CHECK2, m_check2);
    //  DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_CHECK2, m_check22);
    DDX_Control(pDX, IDC_CHECK1, m_check21);
    DDX_Control(pDX, IDC_EDIT1, m_edit21);
    //  DDX_Control(pDX, IDC_CHECK3, m_check33);
    DDX_Control(pDX, IDC_EDIT2, m_edit22);
    //  DDX_Control(pDX, IDC_CHECK3, m_chech23);
    DDX_Control(pDX, IDC_CHECK3, m_check23);
}


BEGIN_MESSAGE_MAP(criteria, CDialogEx)
    ON_BN_CLICKED(IDOK, &criteria::OnBnClickedOk)
    ON_BN_CLICKED(IDC_CHECK1, &criteria::OnBnClickedCheck1)
    ON_BN_CLICKED(IDC_CHECK2, &criteria::OnBnClickedCheck2)
    ON_BN_CLICKED(IDC_CHECK3, &criteria::OnBnClickedCheck3)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

HBRUSH criteria::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
// criteria message handlers

void criteria::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    char buff[1024]="";

    if(m_checking==1)
    {
        int cTxtLen = m_edit21.GetWindowTextLength();
        m_edit21.GetWindowTextA(&buff[0],cTxtLen+1);
    }
    if(m_checking==2)
    {
        int cTxtLen = m_edit22.GetWindowTextLength();
        m_edit22.GetWindowTextA(&buff[0],cTxtLen+1);
    }
    m_value=buff;
    CDialogEx::OnOK();
}


void criteria::OnBnClickedCheck1()
{
    m_check21.SetCheck(true);
    m_check22.SetCheck(false);
    m_edit21.EnableWindow(false);
    m_edit22.EnableWindow(false);
    m_check23.SetCheck(false);
    m_checking =0;
    // TODO: Add your control notification handler code here
}


void criteria::OnBnClickedCheck2()
{
    // TODO: Add your control notification handler code here
    m_check22.SetCheck(true);
    m_check21.SetCheck(false);
    m_check23.SetCheck(false);
    m_edit21.EnableWindow(true);
    m_edit22.EnableWindow(false);
    m_checking =1;

}


void criteria::OnBnClickedCheck3()
{
    // TODO: Add your control notification handler code here
    m_check22.SetCheck(false);
    m_check23.SetCheck(true);
    m_check21.SetCheck(false);
    m_edit22.EnableWindow(true);
    m_edit21.EnableWindow(false);
    m_checking =2;
}
