#pragma once


// criteria dialog

class criteria : public CDialogEx
{
	DECLARE_DYNAMIC(criteria)

public:
	criteria(CWnd* pParent = NULL);   // standard constructor
	virtual ~criteria();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	 CString GetValue() const {return m_value;}
	long GetNumber() { if (m_checking==1)
		{return 1; }
	else if (m_checking==2)
	{
		return 2;
	}
	else
	{
		return 0;
	}
	}
//	CButton m_check1;
//	CButton m_check2;
//	CEdit m_edit1;
	CButton m_check22;
	CButton m_check21;
	CEdit m_edit21;
	int m_checking;
	CString m_value;
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCheck3();
//	CButton m_check33;
	CEdit m_edit22;
//	CButton m_chech23;
	CButton m_check23;
};
