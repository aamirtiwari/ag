// server.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "server.h"
#include "afxdialogex.h"


// server dialog

IMPLEMENT_DYNAMIC(server, CDialogEx)

server::server(CWnd* pParent /*=NULL*/)
    : CDialogEx(server::IDD, pParent)
{

}

server::~server()
{
}

void server::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}

BOOL server::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));
    return TRUE;
}
void server::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    //  DDX_Control(pDX, IDC_EDIT1, m_value1);
    //  DDX_Control(pDX, IDC_EDIT2, m_value2);
    //  DDX_Control(pDX, IDC_EDIT6, m_value3);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT2, m_edit2);
    DDX_Control(pDX, IDC_EDIT6, m_edit3);
}


BEGIN_MESSAGE_MAP(server, CDialogEx)
    ON_BN_CLICKED(IDOK, &server::OnBnClickedOk)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

HBRUSH server::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
// server message handlers


void server::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    char buff[1024]="";
    int cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    m_value1=buff;
    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    m_value2=buff;
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    m_value3=buff;
    CDialogEx::OnOK();
}
