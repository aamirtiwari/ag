#pragma once
#include <string>
using namespace std;
// Account dialog

class Customer : public CDialogEx
{
	DECLARE_DYNAMIC(Customer)

public:
	Customer(CWnd* pParent = NULL);   // standard constructor
	virtual ~Customer();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	CTreeCtrl m_tree;
	CImageList m_image;
	CString m_value;
	afx_msg void OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk4();
	CString GetValue() const {return m_value;}
	CEdit m_edit1;
	CEdit m_edit2;
	CEdit m_edit3;
	CEdit m_edit4;
	afx_msg void OnBnClickedOk3();
	CTreeCtrl m_tree2;
	afx_msg void OnTvnSelchangedTree2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk5();
	afx_msg void OnBnClickedOk6();
};
