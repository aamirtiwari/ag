#pragma once


// Account dialog

class Customer1 : public CDialogEx
{
	DECLARE_DYNAMIC(Customer1)

public:
	Customer1(CWnd* pParent = NULL);   // standard constructor
	virtual ~Customer1();

// Dialog Data
	enum { IDD = IDD_DIALOG5 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void SetBackGroundColor(COLORREF color,BOOL bRepaint=TRUE);
	CString m_value;
	CString GetValue() const {return m_value;}
	CEdit m_edit1;
	CEdit m_edit2;
	CEdit m_edit3;
	CEdit m_edit4;
};
