// Account.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "Customer.h"
#include "afxdialogex.h"
#include <string>
#include "AG_DATA.h"
#include "filedlg.h"
#include <fstream>
using namespace std;
// Account dialog

IMPLEMENT_DYNAMIC(Customer, CDialogEx)

Customer::Customer(CWnd* pParent /*=NULL*/)
    : CDialogEx(Customer::IDD, pParent)
{

}

Customer::~Customer()
{
}

void Customer::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}
void Customer::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_TREE1, m_tree);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT11, m_edit2);
    DDX_Control(pDX, IDC_EDIT2, m_edit3);
    DDX_Control(pDX, IDC_EDIT6, m_edit4);
    DDX_Control(pDX, IDC_TREE2, m_tree2);
}
BOOL Customer::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(0,114,148));

    return TRUE;
}
BEGIN_MESSAGE_MAP(Customer, CDialogEx)
    ON_BN_CLICKED(IDOK, &Customer::OnBnClickedOk)
    ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, &Customer::OnTvnSelchangedTree1)
    ON_BN_CLICKED(IDOK4, &Customer::OnBnClickedOk4)
    ON_BN_CLICKED(IDOK3, &Customer::OnBnClickedOk3)
    ON_NOTIFY(TVN_SELCHANGED, IDC_TREE2, &Customer::OnTvnSelchangedTree2)
    ON_BN_CLICKED(IDOK5, &Customer::OnBnClickedOk5)
    ON_BN_CLICKED(IDOK6, &Customer::OnBnClickedOk6)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

HBRUSH Customer::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(255, 255, 255));
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    default:
        return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
// Account message handlers


void Customer::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    HTREEITEM	hItem12 = m_tree.GetSelectedItem();
    std::string item=m_tree.GetItemText(hItem12);
    if(item.find("EMAIL ")==std::string::npos)
        hItem12 = m_tree.GetParentItem(hItem12);
    m_value = m_tree.GetItemText(hItem12);
    std::string Customeremail=m_value;
    if(Customeremail!= "")
        MessageBox("LINKED","DONE",MB_ICONINFORMATION);
    else
    {
        MessageBox("NO SELECTION MADE","WARNING",MB_ICONWARNING);
        return;
    }
    CDialogEx::OnOK();
}


void Customer::OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult)
{
    HTREEITEM	hItem12 = m_tree.GetSelectedItem();
    std::string item=m_tree.GetItemText(hItem12);
    if(item.find("EMAIL ")==std::string::npos)
        hItem12 = m_tree.GetParentItem(hItem12);
    std::string email = m_tree.GetItemText(hItem12);
    if(email.find("EMAIL")!=std::string::npos)
        email = email.substr(email.find("EMAIL")+8);

    LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
    // TODO: Add your control notification handler code here
    *pResult = 0;
    m_tree2.DeleteAllItems();
    m_image.Create(16, 16, ILC_MASK, 0, 1);
    m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
    m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON3));
    m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
    m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
    m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON1));
    m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
    m_tree2.SetImageList(&m_image,TVSIL_NORMAL);
    HTREEITEM	hItem1[4];

    char * returnval= new char(1024);
    if(email!="")
    {
        returnval = SEARCHID(&email, returnval);
        std::string returnval1= returnval;
        if( returnval1=="")
            return;
        else
        {
            std::string temp= returnval;
            m_image.Create(16, 16, ILC_MASK, 0, 1);
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON3));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON1));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
            m_tree.SetImageList(&m_image,TVSIL_NORMAL);
            HTREEITEM	hItem1[7];
            while(temp!="")
            {
                if(temp.find("%%%")!=std::string::npos)
                {
                    hItem1[0]=m_tree2.InsertItem(("MACHINE ID : "+ temp.substr(0,temp.find("%%%"))).c_str(),5,5);
                    returnval1 = temp.substr(0,temp.find("%%%"));
                    temp = temp.substr(temp.find("%%%")+3);
                    returnval  = SEARCHSYSINFO(&returnval1, returnval);
                    returnval1= returnval;
                    if(returnval1!="")
                    {
                        returnval1 = returnval1.substr(0,returnval1.find("%%%"));
                        returnval  = SEARCHLICENSE(&returnval1, returnval);

                    }
                    returnval1= returnval;
                    if(returnval1!="")
                    {
                        while(returnval1!="")
                        {
                            if(returnval1.find("%%%")!=std::string::npos)
                            {
                                hItem1[1]=m_tree2.InsertItem(("APPID : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),1,1,hItem1[0]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[2]=m_tree2.InsertItem(("VERSION : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),0,0,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[3]=m_tree2.InsertItem(("START DATE : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),4,4,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[3]=m_tree2.InsertItem(("EXPIRATION DATE : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),4,4,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[3]=m_tree2.InsertItem(("NETWORK SEATS : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),3,3,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[3]=m_tree2.InsertItem(("LOCKED ON : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),2,2,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[3]=m_tree2.InsertItem(("USB ID : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),0,0,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                hItem1[3]=m_tree2.InsertItem(("MEMORY DATA : "+ returnval1.substr(0,returnval1.find("%%%"))).c_str(),0,0,hItem1[1]);
                                returnval1 = returnval1.substr(returnval1.find("%%%")+3);
                                m_tree2.Expand(hItem1[0],TVE_EXPAND);
                            }
                            else
                                temp="";
                        }
                    }
                    m_tree2.Expand(hItem1[0],TVE_EXPAND);
                }
                else
                    temp="";
            }
        }
    }

    /*		std::string imput= "Andy";
    		ShowAge(&imput);*/
}


void Customer::OnBnClickedOk4()
{
    // TODO: Add your control notification handler code here
    m_tree.DeleteAllItems();
    int cTxtLen ;
    char buff[1024]="";
    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string email= buff;
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string nam= buff;
    cTxtLen = m_edit4.GetWindowTextLength();
    m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string address= buff;
    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string phone= buff;
    char * returnval= new char(1024);
    if(email!="")
    {
        returnval = SEARCHMAIL(&email, returnval);
        std::string returnval1= returnval;
        if( returnval1=="")
            MessageBox("EMAIL DOES NOT EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
        else
        {
            std::string temp= returnval;
            m_image.Create(16, 16, ILC_MASK, 0, 1);
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON3));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON1));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
            m_tree.SetImageList(&m_image,TVSIL_NORMAL);
            HTREEITEM	hItem1[4];
            while(temp!="")
            {
                if(temp.find("%%%")!=std::string::npos)
                {
                    hItem1[0]=m_tree.InsertItem(("EMAIL : "+ temp.substr(0,temp.find("%%%"))).c_str(),3,3);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[1]=m_tree.InsertItem(("NAME : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[2]=m_tree.InsertItem(("ADRESS : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[3]=m_tree.InsertItem(("PHONE : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    m_tree.Expand(hItem1[0],TVE_EXPAND);
                }
                else
                    temp="";
            }
        }
    }
    else if(nam!="")
    {
        returnval = SEARCHNAME(&nam, returnval);
        std::string returnval1= returnval;
        if( returnval1=="")
            MessageBox( "NAME DOES NOT EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
        else
        {
            std::string temp= returnval;
            m_image.Create(16, 16, ILC_MASK, 0, 1);
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON3));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON1));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
            m_tree.SetImageList(&m_image,TVSIL_NORMAL);
            HTREEITEM	hItem1[4];
            while(temp!="")
            {
                if(temp.find("%%%")!=std::string::npos)
                {
                    hItem1[0]=m_tree.InsertItem(("EMAIL : "+ temp.substr(0,temp.find("%%%"))).c_str(),3,3);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[1]=m_tree.InsertItem(("NAME : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[2]=m_tree.InsertItem(("ADRESS : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[3]=m_tree.InsertItem(("PHONE : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    m_tree.Expand(hItem1[0],TVE_EXPAND);
                }
                else
                    temp="";
            }
        }
    }
    else if(address!="")
    {
        returnval = SEARCHADDRESS(&address, returnval);
        std::string returnval1= returnval;
        if( returnval1=="")
            MessageBox( "ADDRESS DOES NOT EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
        else
        {
            std::string temp= returnval;
            m_image.Create(16, 16, ILC_MASK, 0, 1);
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON3));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON1));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
            m_tree.SetImageList(&m_image,TVSIL_NORMAL);
            HTREEITEM	hItem1[4];
            while(temp!="")
            {
                if(temp.find("%%%")!=std::string::npos)
                {
                    hItem1[0]=m_tree.InsertItem(("EMAIL : "+ temp.substr(0,temp.find("%%%"))).c_str(),3,3);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[1]=m_tree.InsertItem(("NAME : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[2]=m_tree.InsertItem(("ADRESS : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[3]=m_tree.InsertItem(("PHONE : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    m_tree.Expand(hItem1[0],TVE_EXPAND);
                }
                else
                    temp="";
            }
        }
    }
    else if(phone!="")
    {
        returnval = SEARCHPHONE(&phone, returnval);
        std::string returnval1= returnval;
        if( returnval1=="")
            MessageBox( "PHONE DOES NOT EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
        else
        {
            std::string temp= returnval;
            m_image.Create(16, 16, ILC_MASK, 0, 1);
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON4));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON3));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON5));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON6));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON1));
            m_image.Add(AfxGetApp()->LoadIcon(IDI_ICON2));
            m_tree.SetImageList(&m_image,TVSIL_NORMAL);
            HTREEITEM	hItem1[4];
            while(temp!="")
            {
                if(temp.find("%%%")!=std::string::npos)
                {
                    hItem1[0]=m_tree.InsertItem(("EMAIL : "+ temp.substr(0,temp.find("%%%"))).c_str(),3,3);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[1]=m_tree.InsertItem(("NAME : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[2]=m_tree.InsertItem(("ADRESS : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    hItem1[3]=m_tree.InsertItem(("PHONE : "+ temp.substr(0,temp.find("%%%"))).c_str(),0,0,hItem1[0]);
                    temp = temp.substr(temp.find("%%%")+3);
                    m_tree.Expand(hItem1[0],TVE_EXPAND);
                }
                else
                    temp="";
            }
        }
    }
    else
        MessageBox("PLEASE PROVIDE SOME VALUE TO SEARCH","ERROR",MB_ICONERROR);
    return;
}


void Customer::OnBnClickedOk3()
{
    // TODO: Add your control notification handler code here
    HTREEITEM	hItem12 = m_tree2.GetSelectedItem();
    std::string item=m_tree2.GetItemText(hItem12);
    if(item.find("MACHINE ID ")==std::string::npos)
        hItem12 = m_tree2.GetParentItem(hItem12);
    item=m_tree2.GetItemText(hItem12);
    if(item.find("MACHINE ID ")==std::string::npos)
        hItem12 = m_tree2.GetParentItem(hItem12);
    std::string email = m_tree2.GetItemText(hItem12);

    if(email=="")
    {
        MessageBox("PLEASE SELECT A MACHINE","ERROR",MB_ICONERROR);
        return;
    }
    else
    {
        if(email.find("MACHINE ID ")!=std::string::npos)
            email= email.substr(email.find("MACHINE ID ")+13);
        char * returnval= new char(1024);
        std::string returnval1="";
        if(email!="")
        {
            returnval = SEARCHSYSINFO(&email, returnval);
            returnval1= returnval;
            if( returnval1=="")
            {
                MessageBox("SYSINFO DOES NOT EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
                return;
            }
            else
            {
                if(returnval1.find("%%%")!=std::string::npos)
                {
                    returnval1 = returnval1.substr(0,returnval1.find("%%%"));
                }
                else
                    returnval1="";
            }
        }
        if(returnval1=="")
        {
            MessageBox("SYSINFO DOES NOT EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
            return;
        }
        filedlg fdlg(false,"info","sysinfo",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"*.info",NULL);

        fdlg.DoModal();
        string file = "";
        file = fdlg.GetFileName();

        std::ofstream ifile;

        if(file=="")
        {

            return;
        }

        file = fdlg.GetPathName();

        ifile.open(file,ios::out|ios::binary );

        ifile.seekp (0, ios::beg);

        ifile.write (&returnval1[0],strlen(returnval1.c_str()));

        size_t debug=0;
        size_t debug1=0;
        debug=strlen(returnval1.c_str())+1;
        debug1 =returnval1.length()+1;
        while (debug<debug1)
        {
//	cout<<debug;

            ifile.write("\0",1);
            ifile.write(&returnval1[debug],strlen(&returnval1[debug]) );
            debug=strlen(&returnval1[debug])+debug+1;
        }

        ifile.close();
        MessageBox("SYSINFO FILE WRITTEN","DONE",MB_ICONINFORMATION);
        return;
    }
}


void Customer::OnTvnSelchangedTree2(NMHDR *pNMHDR, LRESULT *pResult)
{
    HTREEITEM	hItem12 = m_tree2.GetSelectedItem();
    std::string item=m_tree2.GetItemText(hItem12);
    if(item.find("MACHINE ID ")==std::string::npos)
        hItem12 = m_tree2.GetParentItem(hItem12);
    item=m_tree2.GetItemText(hItem12);
    if(item.find("MACHINE ID ")==std::string::npos)
        hItem12 = m_tree2.GetParentItem(hItem12);
    std::string email = m_tree2.GetItemText(hItem12);

    if(email=="")
    {
        return;
    }
    else
    {
        if(email.find("MACHINE ID ")!=std::string::npos)
            email= email.substr(email.find("MACHINE ID ")+13);
        char * returnval= new char(1024);
        std::string returnval1="";
        if(email!="")
        {
            returnval = SEARCHSYSINFO(&email, returnval);
            returnval1= returnval;
            if( returnval1=="")
            {
                return;
            }
            else
            {
                if(returnval1.find("%%%")!=std::string::npos)
                {
                    returnval1 = returnval1.substr(0,returnval1.find("%%%"));
                }
                else
                    returnval1="";
            }
        }
        if(returnval1=="")
        {
            return;
        }

        string file = "sysinfo_temp.info";

        std::ofstream ifile;

        if(file=="")
        {

            return;
        }


        ifile.open(file,ios::out|ios::binary );

        ifile.seekp (0, ios::beg);

        ifile.write (&returnval1[0],strlen(returnval1.c_str()));

        size_t debug=0;
        size_t debug1=0;
        debug=strlen(returnval1.c_str())+1;
        debug1 =returnval1.length()+1;
        while (debug<debug1)
        {
//	cout<<debug;

            ifile.write("\0",1);
            ifile.write(&returnval1[debug],strlen(&returnval1[debug]) );
            debug=strlen(&returnval1[debug])+debug+1;
        }

        ifile.close();
        return;
    }
}


void Customer::OnBnClickedOk5()
{
    // TODO: Add your control notification handler code here
    int cTxtLen ;
    char buff[1024]="";
    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string email= buff;
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    if(email.find("@")==std::string::npos || email.find(".")==std::string::npos)
    {
        MessageBox("PLEASE PROVIDE A VALID EMAIL ADDRESS","ERROR",MB_ICONERROR);
        return;
    }
    std::string nam= buff;
    cTxtLen = m_edit4.GetWindowTextLength();
    m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string address= buff;
    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string phone= buff;
    int result =0;
    result =SEARCHMAIL2(&email);
    if( result!=0)
    {
        InsertCustomer( &email, &nam,  &address, &phone);
        m_value= email.c_str();
    }
    else
    {
        MessageBox("EMAIL EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
        return;
    }
    MessageBox("CREATED","DONE",MB_ICONINFORMATION);
    CDialogEx::OnOK();
}


void Customer::OnBnClickedOk6()
{
    // TODO: Add your control notification handler code here
    HTREEITEM	hItem12 = m_tree.GetSelectedItem();
    std::string item=m_tree.GetItemText(hItem12);
    if(item.find("EMAIL ")==std::string::npos)
        hItem12 = m_tree.GetParentItem(hItem12);
    m_value = m_tree.GetItemText(hItem12);
    std::string Customeremail=m_value;
    if(Customeremail!= "")
    {
        if(Customeremail.find("EMAIL")!=std::string::npos)
            Customeremail = Customeremail.substr(Customeremail.find("EMAIL")+8);
        DELETECUSTOMER(&Customeremail);
        m_tree.DeleteAllItems();
        MessageBox("CUSTOMER DELETED","DONE",MB_ICONINFORMATION);
    }
    else
    {
        MessageBox("NO SELECTION MADE","WARNING",MB_ICONWARNING);
        return;
    }
    return;
}
