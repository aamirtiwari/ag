// Account.cpp : implementation file
//

#include "stdafx.h"
#include "AG_AUTHENTICATOR.h"
#include "Customer1.h"
#include "afxdialogex.h"
#include "AG_DATA.h"

// Account dialog

IMPLEMENT_DYNAMIC( Customer1, CDialogEx)

Customer1:: Customer1(CWnd* pParent /*=NULL*/)
    : CDialogEx( Customer1::IDD, pParent)
{

}

Customer1::~ Customer1()
{
}

void  Customer1::SetBackGroundColor(COLORREF color,BOOL bRepaint)
{
    CDialogEx::SetBackgroundColor(color);
}
void  Customer1::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit1);
    DDX_Control(pDX, IDC_EDIT11, m_edit2);
    DDX_Control(pDX, IDC_EDIT2, m_edit3);
    DDX_Control(pDX, IDC_EDIT6, m_edit4);
}

BOOL  Customer1::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackGroundColor(RGB(192,192,192));
    return TRUE;
}
BEGIN_MESSAGE_MAP( Customer1, CDialogEx)
    ON_BN_CLICKED(IDOK, & Customer1::OnBnClickedOk)
END_MESSAGE_MAP()


// Account message handlers


void  Customer1::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    int cTxtLen ;
    char buff[1024]="";
    cTxtLen = m_edit1.GetWindowTextLength();
    m_edit1.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string email= buff;
    cTxtLen = m_edit3.GetWindowTextLength();
    m_edit3.GetWindowTextA(&buff[0],cTxtLen+1);
    if(email.find("@")==std::string::npos || email.find(".")==std::string::npos)
    {
        MessageBox("PLEASE PROVIDE A VALID EMAIL ADDRESS","ERROR",MB_ICONERROR);
        return;
    }
    std::string nam= buff;
    cTxtLen = m_edit4.GetWindowTextLength();
    m_edit4.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string address= buff;
    cTxtLen = m_edit2.GetWindowTextLength();
    m_edit2.GetWindowTextA(&buff[0],cTxtLen+1);
    std::string phone= buff;
    int result =0;
    result =SEARCHMAIL2(&email);
    if( result!=0)
    {
        InsertCustomer( &email, &nam,  &address, &phone);
        m_value= email.c_str();
    }
    else
    {
        MessageBox("EMAIL EXISTS IN THE DATABASE","ERROR",MB_ICONERROR);
        return;
    }
    CDialogEx::OnOK();
}
