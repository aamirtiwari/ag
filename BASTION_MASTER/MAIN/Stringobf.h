#ifndef STRINGOBF_KEY
	#define STRINGOBF_KEY "auXW7lZmsqHCwhL+npiGcoOEDyPIV3K1tjf2xdkFNgAM8eTRUYJb4Q5v9BSr/60z"
	#define STRING_ONE_DEFINE_NAME GetDecryptedString("wG7YwG7YwG7YwG7YwGsQLaa=")
	#define STRING_TWO_DEFINE_NAME GetDecryptedString("w2qjDGsvLWVUwW7JDGoxLZljD27JDGaYw2V9h5ljw2sa")
	#define STRING_THREE_DEFINE_NAME GetDecryptedString("w2qjDGsvLWVa")
	#define STRING_FOUR_DEFINE_NAME GetDecryptedString("w2V9h5ljw2sa")
	#define STRING_FIVE_DEFINE_NAME GetDecryptedString("c4lincUtwGnvaa==")
	#define STRING_SIX_DEFINE_NAME GetDecryptedString("wGnvaa==")
	#define STRING_SEVEN_DEFINE_NAME GetDecryptedString("cQuiGbs4LGc9hUa=")
	#define STRING_EIGHT_DEFINE_NAME GetDecryptedString("hWD9wbtvw2Da")
	#define STRING_NINE_DEFINE_NAME GetDecryptedString("y2ujLGoxyWaYy2D5wOc5wbw5w2hjDOpxD2hfLGD9h57a")
	#pragma comment(lib, "strenc64") 
 	void StrencDecode(char* buffer, char* Base64CharacterMap); 
 	const char* GetDecryptedString(const char* encryptedString) 
 	{ 
 		static char string[45]; 
 		strcpy(string, encryptedString); 
 		StrencDecode(string, STRINGOBF_KEY); 
 		return string; 
 	} 
 #endif